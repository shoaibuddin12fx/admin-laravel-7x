<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\notification;
use App\Http\Resources\Notificationcollection;
class Notificationcontroller extends Controller
{
    //
    function index(){
        $users=user::all();
        return view('admin.notification.index')
        ->with('users',$users);
    }
    function sendtouser($id){

         $users=user::where('id','=',$id)->get();
         return view('admin.notification.index')
         ->with('users',$users);


    }
    function save(Request $request){
        // $table->Integer('user_id');
        // $table->string('heading');
        // $table->string('discription');
       // return $request->all();
       $tbl=new notification();
       $tbl->user_id=$request->userid;
       $tbl->heading=$request->Heading;
       $tbl->discription=$request->Discription;
       $tbl->save();
       return back()->with('msg','Message sent');
    }
    function sentnotifications(){

        $noticiations=notification::all();
        $data=new Notificationcollection($noticiations);
        $data=$data->toArray(\request());
        $columns = [];
        if(count($data) > 0){
            $columns=array_keys($data[0]);
        }

        $actions = [
            [
                'label' => 'edit',
                'action' => route('admin.users.show', ':id')
            ],
          ];
          $formaction="admin.review.edit";
        $breadcrumb = "Sent Notifications";
        return view('admin.Payments.index', compact('data', 'formaction','columns', 'breadcrumb'));


    }
    function updatereview(Request $request){
        $noticiation=notification::find($request->id);
        $noticiation->heading=$request->heading;
        $noticiation->discription=$request->discription;
        $noticiation->save();
        return back()->with('msg','Review updated');

    }
}
