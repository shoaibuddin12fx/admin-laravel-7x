<?php

namespace App\Http\Resources;

use App\Models\Invoice;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PaymentViewCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Invoice $invoice){
            return new PaymentViewResource($invoice);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (Invoice $invoice){
            return PaymentViewResource::toObject($invoice);
        });

        return $obj;
    }
}
