<?php

namespace App\Http\Controllers\API;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Product;
use App\Models\Room;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatsController extends Controller
{
    //
    public function fetchMessages(Request $request, $id)
    {
        $current_uid = \auth()->user()->id;
        $messages = Message::where('room_id', $id)->orderby('created_at', 'asc')->get();
        return self::success('messages', ['list' => $messages, 'current_uid' => $current_uid]);
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $sender = \auth()->user();

        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
            'product_id' => 'required|integer'
        ]);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }

        $data = $request->all();

        $product = Product::where('id', $data['product_id'])->first();

        if(!$product){
            return self::failure("Product not found");
        }

        $initiator_id = $sender->id;
        $recepient_id = $product->added_by;

        $query = Room::query();
        $query->whereRaw(
            " ( `initiator_id`, `recipient_id`  ) in ( ( $initiator_id  , $recepient_id  ), ( $recepient_id  , $initiator_id ) )"
        );

        $room = $query->first();
        $chat = $request->input('message');

        if(!$room){
            $room = new Room([
                'initiator_id' => $initiator_id,
                'recipient_id' => $recepient_id,
                'messeges_count' => 0
            ]);
            $room->save();
        }

        $messege = new Message([
            'message' => $chat,
            'user_id' => $initiator_id,
            'room_id' => $room->id
        ]);
        $messege->save();
        $room->messeges_count = $room->messeges_count + 1;
        $room->save();

        return self::success("Message Sent!", ['chat' => $request->input('message') ]);
    }


    public function sendRoomMessage(Request $request)
    {
        $sender = \auth()->user();

        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
            'roomId' => 'required|integer'
        ]);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }

        $data = $request->all();

        $initiator_id = $sender->id;

        $chat = $request->input('message');

        $messege = new Message([
            'message' => $chat,
            'user_id' => $initiator_id,
            'room_id' => $data['roomId']
        ]);

        $messege->save();

        Helper::sendMessagePush($messege);


        return self::success("Message Sent!", ['chat' => $messege ]);
    }

    public function chatList(Request $request){

        $sender = \auth()->user();
        $user_id = $sender->id;

        $query = Room::query();
        $query->where('initiator_id', $user_id);
        $query->orWhere('recipient_id', $user_id);
        $rooms = $query->get();

        $array = [];
        for($i = 0; $i < count($rooms); $i++ ){
            $room = $rooms[$i];
            $messages = Message::where('room_id', $room->id)->orderBy('updated_at', 'desc')->limit(1)->first();
            $whichid = $room->initiator_id == $user_id ? $room->recipient_id : $room->initiator_id;
            $user = User::where('id', $whichid)->first();

            $array[] = [
                'room_id' => $room->id,
                'sender' => $user->fullName,
                'last_message' => $messages->message,
                'last_date' => $messages->created_at->format('m-d-Y H:i s')
            ];

        }

        return self::success("Message Sent!", ['chatList' => $array ]);

    }
}
