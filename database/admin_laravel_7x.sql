-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 24, 2022 at 02:19 AM
-- Server version: 5.6.51-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

SET FOREIGN_KEY_CHECKS=0;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_laravel_7x`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_settings`
--

CREATE TABLE `admin_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `admin_commission` int(11) DEFAULT '0',
  `dollar_per_mile` int(11) DEFAULT '0',
  `min_fare` int(11) DEFAULT '0',
  `max_fare` int(11) DEFAULT '0',
  `booking_cancel_time` int(11) DEFAULT '0',
  `admin_delivery_fee` int(11) DEFAULT '0',
  `min_fare_distance` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `user_id`, `admin_commission`, `dollar_per_mile`, `min_fare`, `max_fare`, `booking_cancel_time`, `admin_delivery_fee`, `min_fare_distance`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 10, 10, 20000, 5, 2, 1, '2020-11-20 05:37:15', '2020-12-03 18:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `cashes`
--

CREATE TABLE `cashes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cashout` int(11) NOT NULL,
  `source` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cashes`
--

INSERT INTO `cashes` (`id`, `user_id`, `cashout`, `source`, `created_at`, `updated_at`) VALUES
(1, 12, 300, 'paypal', '2020-10-24 09:52:32', '2020-10-24 09:52:32'),
(2, 11, 33, 'paypal', '2020-12-03 20:12:58', '2020-12-03 20:12:58'),
(3, 11, 12036, 'paypal', '2020-12-03 20:42:02', '2020-12-03 20:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_type_id` int(11) DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `category_type_id`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 'Toys & Games', NULL, 1, '1CSHyFcMv9DhamII9Kb6', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(2, 'Books, Movies & Music', NULL, 2, '1HKh0pBHJfMREUst9jSy', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(3, 'Women\'s Clothing & Shoes', NULL, 3, '3tbWp5B2zzEbvLhBMxce', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(4, 'Baby Products', NULL, 1, '5MzBEFDNysZFLeee39Uw', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(5, 'Antiques & Collectibles', NULL, 2, '6MDjUXyGnSXesufnwpgJ', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(6, 'Home Decor', NULL, 4, '7RTjF0TY6vgQGQwsEo9l', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(7, 'Major Appliances', NULL, 4, 'G8IwuYLqEaygc8xJEgnD', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(8, 'Men\'s Clothing & Shoes', NULL, 3, 'HBNxhsuO0GkFym2RC8vY', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(9, 'Garden & Outdoor', NULL, 4, 'HVoZ4X3NvJKfXsv1x5Nx', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(10, 'Bath', NULL, 4, 'JPkUZRY4SOWSm29ymg34', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(11, 'Arts & Crafts', NULL, 2, 'Kt6ouh29FGvP7rduthcK', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(12, 'Miscellaneous', NULL, 5, 'NwgK6kbeDTvBXkvjRs8u', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(13, 'Health & Beauty', NULL, 5, 'Oe6zyOJ65SWwJ1QN4pLN', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(14, 'Kid\'s Clothing', NULL, 3, 'Ot7sGj11r2vDBJ0II7w3', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(15, 'Handbags', NULL, 3, 'Pc6tpLjgs8uHYdx1HdwY', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(16, 'Sporting Goods', NULL, 2, 'UBUZBgsY1K1eyhB2uyjC', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(17, 'Office Supplies', NULL, 5, 'VtnWNVvzOo1CwPCRHyCi', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(18, 'Tools & Home Improvement', NULL, 4, 'Wx2kbGyMlWwuujt3dCOX', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(19, 'Furniture', NULL, 4, 'ZrN6irn4Dz4KZre7Dh5P', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(20, 'Home Storage & Organisations', NULL, 4, 'ZyIRPenc8vbWihJDTZRR', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(21, 'Household Supplies & Cleaning', NULL, 4, 'bAzBOWBK0af8XYVNdGyK', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(22, 'Lamps & Lighting', NULL, 4, 'bupFPa0s7IzM5IBpQErR', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(23, 'Kitchen & Dining', NULL, 4, 'goh6Q52m4mvxnfFuEDTI', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(24, 'Kid\'s Clothing', NULL, 1, 'jmaKq2pOr39KnZwngQPr', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(25, 'Video Games & Consoles', NULL, 6, 'lS4EhCiuIdUtVkifvwFJ', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(26, 'Toys & Games', NULL, 2, 'orhQcUpnvrgPVSICK2AS', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(27, 'Musical Instrument', NULL, 2, 'p0DCMOBYAZqwU6ulVIok', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(28, 'Jewellery & Watches', NULL, 3, 'sDZAES7MRz0wvYoKb75e', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(29, 'Auto Parts & Accessories', NULL, 5, 'sSBhItV4tc1vDCalCpoh', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(31, 'Luggage', NULL, 3, 'v4PRx6irEesrrSRomWgu', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(32, 'Computers & Other Electronics', NULL, 6, 'vGlhBdDzGWyjBwVMxR9t', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(33, '', NULL, 3, 'w7FeKadZPkDBquzXxrjh', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(34, 'Bedding', NULL, 4, 'wxwoxKBpgxFzgVv1KtUD', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(35, 'Pet Supplies', NULL, 5, 'yNv3jVebCgLkKaYB1pwB', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(36, 'Cell Phones & Accessories', NULL, 6, 'yPNplEvOQvCnJgWeNpVP', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(37, 'Test Alex', '1599933259.png', 1, NULL, '2020-09-12 12:54:19', '2020-09-12 12:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `category_types`
--

CREATE TABLE `category_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_types`
--

INSERT INTO `category_types` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(2, 'Home', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(3, 'Furniture', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(4, 'Documents', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(5, 'Miscellaneous', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(6, 'Others', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'New Good', '2020-09-24 22:59:57', '2020-09-24 22:59:57'),
(2, 'Old Good', '2020-09-24 22:59:57', '2020-09-24 22:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 0, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'cashout', 'text', 'Cashout', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'source', 'text', 'Source', 1, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(30, 5, 'image', 'text', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(31, 5, 'category_type_id', 'text', 'Category Type Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(32, 5, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(33, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(34, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(37, 6, 'image', 'text', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(38, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(39, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(40, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(41, 7, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(42, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(43, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(44, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(45, 8, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(46, 8, 'product_id', 'text', 'Product Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(47, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(48, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(49, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(50, 9, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(51, 9, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, '{}', 3),
(52, 9, 'capture_id', 'text', 'Capture Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(53, 9, 'job_id', 'text', 'Job Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(54, 9, 'order_id', 'text', 'Order Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(55, 9, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(56, 9, 'timestamp', 'text', 'Timestamp', 0, 1, 1, 1, 1, 1, '{}', 8),
(57, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(58, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(59, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(60, 10, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(61, 10, 'customer_amount', 'text', 'Customer Amount', 1, 1, 1, 1, 1, 1, '{}', 3),
(62, 10, 'driver_amount', 'text', 'Driver Amount', 1, 1, 1, 1, 1, 1, '{}', 4),
(63, 10, 'admin_amount', 'text', 'Admin Amount', 1, 1, 1, 1, 1, 1, '{}', 5),
(64, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(65, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(66, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(67, 12, 'delivery_address', 'text', 'Delivery Address', 0, 1, 1, 1, 1, 1, '{}', 2),
(68, 12, 'delivery_latitude', 'text', 'Delivery Latitude', 0, 1, 1, 1, 1, 1, '{}', 3),
(69, 12, 'delivery_longitude', 'text', 'Delivery Longitude', 0, 1, 1, 1, 1, 1, '{}', 4),
(70, 12, 'distance', 'text', 'Distance', 0, 1, 1, 1, 1, 1, '{}', 5),
(71, 12, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(72, 12, 'expected_delivery_time', 'timestamp', 'Expected Delivery Time', 0, 1, 1, 1, 1, 1, '{}', 7),
(73, 12, 'geohash', 'text', 'Geohash', 0, 1, 1, 1, 1, 1, '{}', 8),
(74, 12, 'item_category', 'text', 'Item Category', 0, 1, 1, 1, 1, 1, '{}', 9),
(75, 12, 'job_address', 'text', 'Job Address', 0, 1, 1, 1, 1, 1, '{}', 10),
(76, 12, 'job_latitude', 'text', 'Job Latitude', 0, 1, 1, 1, 1, 1, '{}', 11),
(77, 12, 'job_longitude', 'text', 'Job Longitude', 0, 1, 1, 1, 1, 1, '{}', 12),
(78, 12, 'job_price', 'text', 'Job Price', 1, 1, 1, 1, 1, 1, '{}', 13),
(79, 12, 'package_size', 'text', 'Package Size', 0, 1, 1, 1, 1, 1, '{}', 14),
(80, 12, 'posted_at', 'text', 'Posted At', 0, 1, 1, 1, 1, 1, '{}', 15),
(81, 12, 'receiver_id', 'text', 'Receiver Id', 0, 1, 1, 1, 1, 1, '{}', 16),
(82, 12, 'priority', 'text', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 17),
(83, 12, 'receiver_name', 'text', 'Receiver Name', 0, 1, 1, 1, 1, 1, '{}', 18),
(84, 12, 'receiver_contact', 'text', 'Receiver Contact', 0, 1, 1, 1, 1, 1, '{}', 19),
(85, 12, 'receiver_instructions', 'text', 'Receiver Instructions', 0, 1, 1, 1, 1, 1, '{}', 20),
(86, 12, 'security_code', 'text', 'Security Code', 0, 1, 1, 1, 1, 1, '{}', 21),
(87, 12, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 22),
(88, 12, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 23),
(89, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 24),
(90, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 25),
(91, 12, 'sender_id', 'text', 'Sender Id', 0, 1, 1, 1, 1, 1, '{}', 26),
(92, 12, 'source_address_appartment', 'text', 'Source Address Appartment', 0, 1, 1, 1, 1, 1, '{}', 27),
(93, 12, 'delivery_address_appartment', 'text', 'Delivery Address Appartment', 0, 1, 1, 1, 1, 1, '{}', 28),
(94, 12, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 29),
(95, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(96, 15, 'device_token', 'text', 'Device Token', 0, 1, 1, 1, 1, 1, '{}', 2),
(97, 15, 'geohash', 'text', 'Geohash', 0, 1, 1, 1, 1, 1, '{}', 3),
(98, 15, 'latitude', 'text', 'Latitude', 0, 1, 1, 1, 1, 1, '{}', 4),
(99, 15, 'longitude', 'text', 'Longitude', 0, 1, 1, 1, 1, 1, '{}', 5),
(100, 15, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 6),
(101, 15, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(102, 15, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 8),
(103, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(104, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(105, 15, 'distance', 'text', 'Distance', 0, 1, 1, 1, 1, 1, '{}', 11),
(106, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(107, 16, 'phone_number', 'text', 'Phone Number', 0, 1, 1, 1, 1, 1, '{}', 2),
(108, 16, 'otp_code', 'text', 'Otp Code', 0, 1, 1, 1, 1, 1, '{}', 3),
(109, 16, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(110, 16, 'is_verified', 'text', 'Is Verified', 0, 1, 1, 1, 1, 1, '{}', 5),
(111, 16, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(112, 16, 'expires', 'timestamp', 'Expires', 0, 1, 1, 1, 1, 1, '{}', 7),
(113, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(114, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(115, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(116, 17, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(117, 17, 'message', 'text', 'Message', 1, 1, 1, 1, 1, 1, '{}', 3),
(118, 17, 'room_id', 'text', 'Room Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(119, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(120, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(121, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(122, 19, 'added_by', 'text', 'Added By', 1, 1, 1, 1, 1, 1, '{}', 2),
(123, 19, 'category_id', 'text', 'Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(124, 19, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 4),
(125, 19, 'extra_labels', 'text', 'Extra Labels', 0, 1, 1, 1, 1, 1, '{}', 5),
(126, 19, 'geohash', 'text', 'Geohash', 0, 1, 1, 1, 1, 1, '{}', 6),
(127, 19, 'insured', 'text', 'Insured', 0, 1, 1, 1, 1, 1, '{}', 7),
(128, 19, 'latitude', 'text', 'Latitude', 1, 1, 1, 1, 1, 1, '{}', 8),
(129, 19, 'longitude', 'text', 'Longitude', 1, 1, 1, 1, 1, 1, '{}', 9),
(130, 19, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 10),
(131, 19, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 11),
(132, 19, 'quantities_available', 'text', 'Quantities Available', 1, 1, 1, 1, 1, 1, '{}', 12),
(133, 19, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 13),
(134, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(135, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(136, 19, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 16),
(137, 19, 'condition_id', 'text', 'Condition Id', 0, 1, 1, 1, 1, 1, '{}', 17),
(138, 19, 'brand', 'text', 'Brand', 0, 1, 1, 1, 1, 1, '{}', 18),
(139, 19, 'vehicle_id', 'text', 'Vehicle Id', 0, 1, 1, 1, 1, 1, '{}', 19),
(140, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(141, 20, 'messeges_count', 'text', 'Messeges Count', 1, 1, 1, 1, 1, 1, '{}', 2),
(142, 20, 'initiator_id', 'text', 'Initiator Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(143, 20, 'recipient_id', 'text', 'Recipient Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(144, 20, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(145, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(146, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(147, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(148, 22, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(149, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(150, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(151, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(152, 23, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(153, 23, 'registration_number', 'text', 'Registration Number', 0, 1, 1, 1, 1, 1, '{}', 3),
(154, 23, 'vehicle_type', 'text', 'Vehicle Type', 0, 1, 1, 1, 1, 1, '{}', 4),
(155, 23, 'driver_licence', 'text', 'Driver Licence', 0, 1, 1, 1, 1, 1, '{}', 5),
(156, 23, 'is_verified', 'text', 'Is Verified', 0, 1, 1, 1, 1, 1, '{}', 6),
(157, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(158, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(159, 23, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 9),
(160, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(161, 24, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(162, 24, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(163, 24, 'amount', 'text', 'Amount', 1, 1, 1, 1, 1, 1, '{}', 4),
(164, 24, 'is_paid', 'text', 'Is Paid', 0, 1, 1, 1, 1, 1, '{}', 5),
(165, 24, 'paid_date', 'timestamp', 'Paid Date', 0, 1, 1, 1, 1, 1, '{}', 6),
(166, 24, 'order_id', 'text', 'Order Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(167, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(168, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(169, 24, 'customer_amount', 'text', 'Customer Amount', 0, 1, 1, 1, 1, 1, '{}', 10),
(170, 24, 'driver_amount', 'text', 'Driver Amount', 0, 1, 1, 1, 1, 1, '{}', 11),
(171, 24, 'admin_amount', 'text', 'Admin Amount', 0, 1, 1, 1, 1, 1, '{}', 12),
(182, 33, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(183, 33, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(184, 33, 'role_id', 'text', 'Role Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(185, 33, 'role_name', 'text', 'Role Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(186, 33, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(187, 33, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(198, 38, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(199, 38, 'owner_id', 'text', 'Owner Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(200, 38, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(201, 38, 'firebase_url', 'text', 'Firebase Url', 1, 1, 1, 1, 1, 1, '{}', 4),
(202, 38, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(203, 38, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(204, 39, 'id', 'text', 'Id', 1, 1, 1, 1, 1, 1, '{}', 1),
(205, 39, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(206, 39, 'sender_id', 'text', 'Sender Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(207, 39, 'reviewer_id', 'text', 'Reviewer Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(208, 39, 'rating', 'text', 'Rating', 0, 1, 1, 1, 1, 1, '{}', 5),
(209, 39, 'review', 'text', 'Review', 0, 1, 1, 1, 1, 1, '{}', 6),
(210, 39, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(211, 39, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(212, 39, 'client_reviewed', 'text', 'Client Reviewed', 0, 1, 1, 1, 1, 1, '{}', 9),
(213, 39, 'driver_reviewed', 'text', 'Driver Reviewed', 0, 1, 1, 1, 1, 1, '{}', 10),
(214, 40, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(215, 40, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(216, 40, 'admin_commission', 'text', 'Admin Commission', 0, 1, 1, 1, 1, 1, '{}', 3),
(217, 40, 'dollar_per_mile', 'text', 'Dollar Per Mile', 0, 1, 1, 1, 1, 1, '{}', 4),
(218, 40, 'min_fare', 'text', 'Min Fare', 0, 1, 1, 1, 1, 1, '{}', 5),
(219, 40, 'max_fare', 'text', 'Max Fare', 0, 1, 1, 1, 1, 1, '{}', 6),
(220, 40, 'booking_cancel_time', 'text', 'Booking Cancel Time', 0, 1, 1, 1, 1, 1, '{}', 7),
(221, 40, 'admin_delivery_fee', 'text', 'Admin Delivery Fee', 0, 1, 1, 1, 1, 1, '{}', 8),
(222, 40, 'min_fare_distance', 'text', 'Min Fare Distance', 0, 1, 1, 1, 1, 1, '{}', 9),
(223, 40, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(224, 40, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(225, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 0, '{}', 5),
(226, 1, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 10),
(227, 1, 'contact', 'text', 'Contact', 0, 1, 1, 1, 1, 1, '{}', 11),
(228, 1, 'city', 'text', 'City', 0, 1, 1, 1, 1, 1, '{}', 12),
(229, 1, 'state', 'text', 'State', 0, 1, 1, 1, 1, 1, '{}', 13),
(230, 1, 'street', 'text', 'Street', 0, 1, 1, 1, 1, 1, '{}', 14),
(231, 1, 'unit', 'text', 'Unit', 0, 1, 1, 1, 1, 1, '{}', 15),
(232, 1, 'zip', 'text', 'Zip', 0, 1, 1, 1, 1, 1, '{}', 16),
(233, 1, 'username', 'text', 'Username', 0, 1, 1, 1, 1, 1, '{}', 17),
(234, 1, 'api_token', 'text', 'Api Token', 0, 0, 0, 0, 0, 0, '{}', 18),
(235, 1, 'fcm_token', 'text', 'Fcm Token', 0, 0, 0, 0, 0, 0, '{}', 19),
(236, 1, 'web_token', 'text', 'Web Token', 0, 0, 0, 0, 0, 0, '{}', 20),
(237, 1, 'device_token', 'text', 'Device Token', 0, 0, 0, 0, 0, 0, '{}', 21),
(238, 1, 'otp_code', 'text', 'Otp Code', 0, 0, 0, 0, 0, 0, '{}', 22),
(239, 1, 'contact_verified', 'text', 'Contact Verified', 1, 0, 0, 0, 0, 0, '{}', 23),
(240, 1, 'email_verified', 'text', 'Email Verified', 1, 0, 0, 0, 0, 0, '{}', 24),
(241, 1, 'email_verified_at_copy1', 'timestamp', 'Email Verified At Copy1', 0, 0, 0, 0, 0, 0, '{}', 25),
(242, 1, 'document_id', 'text', 'Document Id', 0, 0, 0, 0, 0, 0, '{}', 26),
(243, 1, 'remember_token_copy1', 'text', 'Remember Token Copy1', 0, 0, 0, 0, 0, 0, '{}', 27),
(244, 1, 'profile_pic', 'text', 'Profile Pic', 0, 0, 0, 0, 0, 0, '{}', 28),
(245, 1, 'first_name', 'text', 'First Name', 0, 1, 1, 1, 1, 1, '{}', 29),
(246, 1, 'last_name', 'text', 'Last Name', 0, 1, 1, 1, 1, 1, '{}', 30),
(247, 1, 'user_belongsto_role_relationship', 'relationship', 'roles', 0, 1, 1, 1, 1, 1, '{\"scope\":\"driver\",\"model\":\"\\\\App\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"admin_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 31),
(248, 1, 'settings', 'text', 'Settings', 0, 0, 1, 1, 1, 1, '{}', 31),
(249, 1, 'user_hasone_vehicle_relationship', 'relationship', 'vehicles', 0, 1, 1, 1, 1, 1, '{\"model\":\"\\\\App\\\\Models\\\\Vehicle\",\"table\":\"vehicles\",\"type\":\"hasOne\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"registration_number\",\"pivot_table\":\"admin_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 32);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-12-18 05:55:42', '2020-12-26 02:21:42'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(4, 'cashes', 'cashes', 'Cash', 'Cashes', NULL, 'App\\Models\\Cash', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-18 09:08:59', '2020-12-18 09:09:19'),
(5, 'categories', 'categories', 'Category', 'Categories', NULL, 'App\\Models\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(6, 'category_types', 'category-types', 'Category Type', 'Category Types', NULL, 'App\\Models\\CategoryTypes', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(7, 'conditions', 'conditions', 'Condition', 'Conditions', NULL, 'App\\Models\\Conditions', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(8, 'favorites', 'favorites', 'Favorite', 'Favorites', NULL, 'App\\Models\\Favorites', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(9, 'invoices', 'invoices', 'Invoice', 'Invoices', NULL, 'App\\Models\\Invoices', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(10, 'job_prices', 'job-prices', 'Job Price', 'Job Prices', NULL, 'App\\Models\\JobPrices', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(11, 'jobreviews', 'jobreviews', 'Jobreview', 'Jobreviews', NULL, 'App\\Models\\JobReviews', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(12, 'jobs', 'jobs', 'Job', 'Jobs', NULL, 'App\\Models\\Jobs', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(15, 'locations', 'locations', 'Location', 'Locations', NULL, 'App\\Models\\Location', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(16, 'otp_numbers', 'otp-numbers', 'Otp Number', 'Otp Numbers', NULL, 'App\\Models\\OtpNumber', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(17, 'messages', 'messages', 'Message', 'Messages', NULL, 'App\\Models\\Message', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(19, 'products', 'products', 'Product', 'Products', NULL, 'App\\Models\\Products', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(20, 'rooms', 'rooms', 'Room', 'Rooms', NULL, 'App\\Models\\Room', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(22, 'vehicle_sizes', 'vehicle-sizes', 'Vehicle Size', 'Vehicle Sizes', NULL, 'App\\Models\\VehicleSize', NULL, 'VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-18 09:20:14', '2020-12-25 14:46:14'),
(23, 'vehicles', 'vehicles', 'Vehicle', 'Vehicles', NULL, 'App\\Models\\Vehicles', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(24, 'wallets', 'wallets', 'Wallet', 'Wallets', NULL, 'App\\Models\\Wallet', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(33, 'runtime_roles', 'runtime-roles', 'Runtime Role', 'Runtime Roles', NULL, 'App\\Models\\RuntimeRoles', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(38, 'job_urls', 'job-urls', 'Job Url', 'Job Urls', NULL, 'App\\Models\\JobUrl', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(39, 'job_reviews', 'job-reviews', 'Job Review', 'Job Reviews', NULL, 'App\\Models\\JobReview', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(40, 'admin_settings', 'admin-settings', 'Admin Setting', 'Admin Settings', NULL, 'App\\Models\\AdminSetting', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 09:36:30', '2020-12-23 09:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(11) NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 27, 21, '2020-09-05 00:16:45', '2020-09-05 00:16:45'),
(2, 9, 1, '2020-11-25 18:33:47', '2020-11-25 18:33:47'),
(3, 9, 1, '2020-11-25 18:33:47', '2020-11-25 18:33:47');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `capture_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `user_id`, `amount`, `capture_id`, `job_id`, `order_id`, `document_id`, `timestamp`, `created_at`, `updated_at`) VALUES
(3, NULL, 11, '2AC22342N8007443X', '78', NULL, '05KJcxNscaC1J1TNKPBQ', '1592925468331', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(5, NULL, 11, '0E335874U8970894S', '30', NULL, '0DtqQPm2DfFwLMujmBUH', '1595832626296', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(11, NULL, 11, '2A7524374D4396218', '134', NULL, '1IemgjtHJo3oiBeXVd20', '1592415720860', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(12, NULL, 11, '2ED936645C2606801', '63', NULL, '1Iv3tXTaRwNAGD8XweS7', '1596303814587', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(16, NULL, 11, '0E335874U8970894S', '30', NULL, '1ijscTMPBpKRmKbMN5nc', '1595907105387', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(17, NULL, 11, '2ED936645C2606801', '63', NULL, '1kAYRL5eI026nfZRrCHL', '1596296571217', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(19, NULL, 11, '2ED936645C2606801', '63', NULL, '1uSwMAUcGiwiHE60cT0e', '1596267543888', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(21, NULL, 11, '0E335874U8970894S', '30', NULL, '25M3XjMvmKsmAIQyKOaM', '1595883541480', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(25, NULL, 11, '8U4557355R056771V', '66', NULL, '2LGZd3q4M7LtkdAIS4qd', '1594733647978', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(30, NULL, 11, '0E335874U8970894S', '30', NULL, '2nddM8KC0VDgQyhul77i', '1595959047594', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(31, NULL, 11, '2ED936645C2606801', '63', NULL, '2tr8cSg6L5HSW4QAo2j9', '1596282040681', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(34, NULL, 10, '9JK76687C02870028', '136', NULL, '2ut2KkGbRYXGAxe0YPXv', '1593275545407', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(35, NULL, 11, '8U4557355R056771V', '66', NULL, '30l24SfGLVfJglEFP7HQ', '1594596805027', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(47, NULL, 11, '2A7524374D4396218', '134', NULL, '4vrTFIjTS5uR88MkTr5D', '1592283575146', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(51, NULL, 10, '9JK76687C02870028', '136', NULL, '58mS12HnQhWVNzdmQdiH', '1592731985216', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(53, NULL, 11, '2A7524374D4396218', '134', NULL, '5Ya1NYCImaoGAIbIdRTo', '1592675089810', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(54, NULL, 11, '8U4557355R056771V', '66', NULL, '5gfci0fOZW4uv12XFMHa', '1594560304283', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(61, NULL, 11, '0E335874U8970894S', '30', NULL, '6blFVAMZVm8p1rs8RA2w', '1595868990883', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(66, NULL, 11, '95E71356UF668590D', '38', NULL, '7akBT64LAWAXXzUEkv0X', '1592371558472', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(68, NULL, 11, '2AC22342N8007443X', '78', NULL, '7htjABZ7YwemY7xOJdZu', '1592325189259', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(71, NULL, 11, '2ED936645C2606801', '63', NULL, '80ANlR9lAeF2sAk5vZ5T', '1596376317207', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(81, NULL, 11, '2ED936645C2606801', '63', NULL, '90A2oNaSG0V9edXMVfPO', '1596347331675', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(84, NULL, 11, '2AC22342N8007443X', '78', NULL, '9bWheKcNINYeFoXUQepv', '1592369806760', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(86, NULL, 11, '0E335874U8970894S', '30', NULL, '9r0q9Ob6i2tMDymZWDqd', '1595966302289', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(87, NULL, 11, '2AC22342N8007443X', '78', NULL, '9urd9xXdhtZsUDb1biee', '1592559352661', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(89, NULL, 10, '9JK76687C02870028', '136', NULL, 'AFXW8ADUS9yNy0A7UKh1', '1593524333104', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(93, NULL, 11, '95E71356UF668590D', '38', NULL, 'Ajb0kBU5bZ0m98ALWtkN', '1592510161550', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(94, NULL, 11, '8U4557355R056771V', '66', NULL, 'AlBxXNyxmPM9K7AszRjx', '1594643003768', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(95, NULL, 11, '8U4557355R056771V', '66', NULL, 'AoKGLCgSv3QhxHhO5eLS', '1594580677121', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(96, NULL, 11, '2AC22342N8007443X', '78', NULL, 'AqYKwfNsUBgfdCcmEmHC', '1592460373903', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(102, NULL, 11, '8U4557355R056771V', '66', NULL, 'BYgiRzj1jH21Coae3n4J', '1594696258594', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(110, NULL, 10, '9JK76687C02870028', '136', NULL, 'CO9Zci5G2EcytG3iLQXs', '1592995092736', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(111, NULL, 11, '2A7524374D4396218', '134', NULL, 'CQqZz6i1Vp2ACCqEGat5', '1592098550202', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(117, NULL, 11, '8U4557355R056771V', '66', NULL, 'DGMO4SRdqUY32QxoNLyo', '1594686794985', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(118, NULL, 11, '2ED936645C2606801', '63', NULL, 'DY107tO5rdl1Tb5Zscsw', '1596398082942', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(120, NULL, 10, '9JK76687C02870028', '136', NULL, 'DjiGRvmqk0xVPxHy6ZfQ', '1593556488088', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(127, NULL, 10, '9JK76687C02870028', '136', NULL, 'EWejQywwVX8E7xMDxg1X', '1593348672250', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(128, NULL, 10, '9JK76687C02870028', '136', NULL, 'Ec4utciIQEYUcx7H30hf', '1593238196177', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(129, NULL, 11, '8U4557355R056771V', '66', NULL, 'EepbIIiBbcFBXb7BjcS0', '1594665343834', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(130, NULL, 11, '0E335874U8970894S', '30', NULL, 'Ele7MqFVIF009GiWctoo', '1595898125514', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(136, NULL, 11, '8U4557355R056771V', '66', NULL, 'FbkFQDK7bAuFEQuGNg5m', '1594546517183', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(141, NULL, 11, '8U4557355R056771V', '66', NULL, 'GSMcxl9rqGL1Hopl1Yni', '1594574291888', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(145, NULL, 11, '2AC22342N8007443X', '78', NULL, 'Gul5WsMJDFdZDHG0f8aX', '1592828888528', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(146, NULL, 10, '9JK76687C02870028', '136', NULL, 'GvUuJHS3XuCNj5CeROo7', '1592580244566', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(149, NULL, 11, '2ED936645C2606801', '63', NULL, 'HcW44j1ZEgbxUyvya0QO', '1596311054241', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(152, NULL, 11, '8U4557355R056771V', '66', NULL, 'HmbcYoluuUowYXs0le36', '1594608843103', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(156, NULL, 11, '8U4557355R056771V', '66', NULL, 'IAGMBv1vxP2pQ28ZMwhi', '1594587786212', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(158, NULL, 11, '95E71356UF668590D', '38', NULL, 'IHdQb0hZpwVgJOJu4qKG', '1591987957245', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(161, NULL, 11, '2ED936645C2606801', '63', NULL, 'IoEmA1GeKgahQYjMUDTX', '1596261667447', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(164, NULL, 11, '0E335874U8970894S', '30', NULL, 'JZFVrpEOSHhEla7zsjy3', '1595825604972', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(165, NULL, 11, '2A7524374D4396218', '134', NULL, 'JaztTZD1MMypTftFNMQ2', '1591972474338', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(170, NULL, 11, '2AC22342N8007443X', '78', NULL, 'K50m7mFtZDvb5HtTGV1T', '1591986110155', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(173, NULL, 10, '9JK76687C02870028', '136', NULL, 'KRiukLlJ2RU3ml2iSx5M', '1592773935561', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(176, NULL, 11, '95E71356UF668590D', '38', NULL, 'Kss5OCeg4QNNmxpWGjwu', '1592160373271', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(178, NULL, 11, '0E335874U8970894S', '30', NULL, 'LDQ0dWA9FJjlq0FSm4gk', '1595861743448', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(179, NULL, 11, '2ED936645C2606801', '63', NULL, 'LGCMveXFz9gEoUjBbxht', '1596259574278', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(181, NULL, 11, '8U4557355R056771V', '66', NULL, 'LYAxOUuHz1Gi58jryh5B', '1594553677922', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(184, NULL, 11, '2ED936645C2606801', '63', NULL, 'LetVIhisFc4Y1pupmFx2', '1596259637109', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(185, NULL, 11, '0E335874U8970894S', '30', NULL, 'Lg9UKKsqtM9TvVecqO73', '1595824731502', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(186, NULL, 11, '8U4557355R056771V', '66', NULL, 'LhRWZ1ludHNXmM60S85B', '1594726369504', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(187, NULL, 10, '9JK76687C02870028', '136', NULL, 'Ljq5zjUP7tw4xDsl20Ed', '1593163150286', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(188, NULL, 11, '95E71356UF668590D', '38', NULL, 'M1AxzYSvnlEt1arYJO3S', '1593012472665', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(189, NULL, 11, '8U4557355R056771V', '66', NULL, 'M5SOWKfkd2R67TbnjQTN', '1594719061779', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(191, NULL, 11, '2ED936645C2606801', '63', NULL, 'MQH5D5JLzpL3GDdHiDMp', '1596260662244', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(192, NULL, 11, '8U4557355R056771V', '66', NULL, 'MQh2P7gSlE526NQwM5nI', '1594567742028', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(194, NULL, 11, '8U4557355R056771V', '66', NULL, 'MWe18ejTrD7r8gPj8uvb', '1594748145503', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(196, NULL, 11, '2A7524374D4396218', '134', NULL, 'MbOFO9Hn5JWcFg6w4mQy', '1592461532105', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(200, NULL, 11, '95E71356UF668590D', '38', NULL, 'MnRnBqHIKzH81RAAtlmb', '1592924933360', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(204, NULL, 11, '2ED936645C2606801', '63', NULL, 'NBTZjBuOadBP9AlLIInM', '1596318310170', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(205, NULL, 11, '0E335874U8970894S', '30', NULL, 'NGaXy2AduW2z9y6p24Ys', '1595847216772', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(206, NULL, 10, '9JK76687C02870028', '136', NULL, 'NIpsaF5idTobkoryZcvy', '1592628489442', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(207, NULL, 11, '95E71356UF668590D', '38', NULL, 'NM9kGHg2lnzNi8d58RWr', '1591924904705', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(209, NULL, 11, '2AC22342N8007443X', '78', NULL, 'NRkrmuWZJFV49MBHvyoc', '1593012926373', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(210, NULL, 10, '9JK76687C02870028', '136', NULL, 'NbjUVHzlZswSkiVcjG2F', '1593597721494', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(215, NULL, 10, '9JK76687C02870028', '136', NULL, 'OY6MxLTgk8s6SlojVYtF', '1593383784921', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(216, NULL, 11, '2ED936645C2606801', '63', NULL, 'Obqt53AoxBL7R5NXn4W1', '1596369088369', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(217, NULL, 11, '2AC22342N8007443X', '78', NULL, 'Oh2CnJYqG0zPTD5NaCC6', '1592612996734', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(220, NULL, 11, '95E71356UF668590D', '38', NULL, 'Ol8UDD5qXlsJ9xps2yGP', '1591781806806', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(221, NULL, 11, '0E335874U8970894S', '30', NULL, 'OlXSJ2mDZapoBnFyIYWd', '1595915503195', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(232, NULL, 11, '95E71356UF668590D', '38', NULL, 'QhahlxTxj1UITLUTSNpp', '1592732607542', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(234, NULL, 11, '8U4557355R056771V', '66', NULL, 'QxNzmQo7H9gQCuH7JxQv', '1594654217457', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(240, NULL, 11, '2AC22342N8007443X', '78', NULL, 'RUIbcR3sD6ETMlxA9iLw', '1592100318017', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(246, NULL, 10, '9JK76687C02870028', '136', NULL, 'S6lceZRKR0iMeZ50Ahlx', '1592951477991', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(248, NULL, 10, '9JK76687C02870028', '136', NULL, 'SBQdBX3bTiXXUqIQ6ONZ', '1593311904244', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(251, NULL, 11, '2ED936645C2606801', '63', NULL, 'SNH0IHv3uklNg24MeKI4', '1596354583196', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(254, NULL, 11, '0E335874U8970894S', '30', NULL, 'StrSVFTSu6H8jNVbZEhV', '1595825022668', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(256, NULL, 11, '2A7524374D4396218', '134', NULL, 'TBYnPEHoFSdPclQGfSDm', '1592159981573', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(262, NULL, 11, '2AC22342N8007443X', '78', NULL, 'Tq3GSNBq47s4ixjG1B5r', '1592969462275', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(263, NULL, 11, '0E335874U8970894S', '30', NULL, 'TqvjJYnPBV9RwNZcjbeo', '1595930026919', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(266, NULL, 11, '2AC22342N8007443X', '78', NULL, 'UBxHGuRDVAJDfpMk83yw', '1592508438416', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(268, NULL, 11, '2ED936645C2606801', '63', NULL, 'UUwm0XU3j2lA5hnxaE04', '1596390830276', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(272, NULL, 11, '8U4557355R056771V', '66', NULL, 'Usm7YhCffqTI3bQTzLwz', '1594676133116', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(276, NULL, 11, '2AC22342N8007443X', '78', NULL, 'VFI34KTDcqEl58nGPLoU', '1591923206362', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(277, NULL, 10, '9JK76687C02870028', '136', NULL, 'VMynHzrge7yV9pDKCHN3', '1593082418465', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(278, NULL, 11, '2A7524374D4396218', '134', NULL, 'VSNbGeWr7NCDVBW6Frhd', '1592509326562', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(280, NULL, 11, '95E71356UF668590D', '38', NULL, 'VVArXMgeckmzbL8I1I9c', '1592327108770', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(283, NULL, 11, '95E71356UF668590D', '38', NULL, 'VmZgWCBrxjJSyampG7DN', '1593055060894', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(290, NULL, 10, '9JK76687C02870028', '136', NULL, 'WhO2ZBRgcK5sE8qJ7tzJ', '1593122964837', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(296, NULL, 10, '9JK76687C02870028', '136', NULL, 'XT1mHceCkP7HU9JxFbUm', '1593453611389', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(297, NULL, 11, '2A7524374D4396218', '134', NULL, 'XU3GhHTnb6IjJAHFhcnx', '1591718890183', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(298, NULL, 11, '0E335874U8970894S', '30', NULL, 'XiPgn2xaV4qAQNsE0fzn', '1595944555299', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(305, NULL, 11, '2A7524374D4396218', '134', NULL, 'YbwZapWCjLpDxqtapw9o', '1592779899043', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(314, NULL, 11, '2AC22342N8007443X', '78', NULL, 'ZeCLhssomaclm5DqLMhe', '1592233908396', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(315, NULL, 11, '0E335874U8970894S', '30', NULL, 'ZhYz2XmTtwGfvbQ53pdj', '1595890838322', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(316, NULL, 11, '2ED936645C2606801', '63', NULL, 'ZoAAoQToud8gDImIwSJg', '1596259788772', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(322, NULL, 11, '95E71356UF668590D', '38', NULL, 'aUljL4mCYubroOiUeCJ7', '1591826565152', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(323, NULL, 11, '0E335874U8970894S', '30', NULL, 'abtimxTi35FeNJ1Srf80', '1595876254293', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(324, NULL, 11, '2A7524374D4396218', '134', NULL, 'aiYZMVawECW154at9F5g', '1592371137361', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(325, NULL, 11, '2AC22342N8007443X', '78', NULL, 'ajZCB6PG5qWN81FgKnF9', '1592731533741', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(326, NULL, 11, '2A7524374D4396218', '134', NULL, 'amHXmMmRF387y2rUqqgQ', '1591852262812', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(331, NULL, 10, '9JK76687C02870028', '136', NULL, 'bJQdUbiBjzJPEe664DDU', '1593039477893', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(332, NULL, 11, '8U4557355R056771V', '66', NULL, 'bMmayR5h8t9SfctkcPqe', '1594704569916', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(333, NULL, 11, '0E335874U8970894S', '30', NULL, 'bkzv2V2LRx2DhFqFYsG4', '1595824336084', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(343, NULL, 11, '95E71356UF668590D', '38', NULL, 'cj4QMKc2qi60lNc8Fjza', '1592462326932', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(348, NULL, 11, '8U4557355R056771V', '66', NULL, 'dQz6gyvTLoxpQQG9UZVn', '1594620557489', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(352, NULL, 11, '2A7524374D4396218', '134', NULL, 'dlc3i9bpv8kxBfTKpiVZ', '1591644325824', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(354, NULL, 11, '0E335874U8970894S', '30', NULL, 'eLzsiTyn5dF0sZL6GDMd', '1595839975377', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(359, NULL, 11, '2A7524374D4396218', '134', NULL, 'emVAGKvmx2F1omhoygq6', '1591804020127', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(361, NULL, 11, '2ED936645C2606801', '63', NULL, 'ewekfxMFzOSlafEwIzwG', '1596263679262', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(362, NULL, 11, '2ED936645C2606801', '63', NULL, 'eyDqcYlQCzAeSQB6d8tU', '1596289294376', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(365, NULL, 11, '8U4557355R056771V', '66', NULL, 'fcBd2YvYhIi8Giv2ZzbI', '1594768608305', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(369, NULL, 11, '95E71356UF668590D', '38', NULL, 'g3EHGYLB3S9mOt8VLF6O', '1592829218067', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(374, NULL, 10, '9JK76687C02870028', '136', NULL, 'gsvCNYqspFPD4ukKbzQP', '1593419020667', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(381, NULL, 11, '8U4557355R056771V', '66', NULL, 'hRbgp7R9BJiZCEO30HjD', '1594631874165', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(393, NULL, 10, '9JK76687C02870028', '136', NULL, 'iHfROZSowwQaN7lEWUB9', '1593200330851', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(396, NULL, 11, '95E71356UF668590D', '38', NULL, 'iZk2xzEGk5tvQod4b4d0', '1592969093778', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(397, NULL, 11, '0E335874U8970894S', '30', NULL, 'icQVloZdfSdScxTy4jfb', '1595951807512', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(400, NULL, 11, '2AC22342N8007443X', '78', NULL, 'iiWwphjMrVE3M6W5G6O3', '1592674744139', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(408, NULL, 11, '8U4557355R056771V', '66', NULL, 'kHlftyFWDvqR3h5Dp0TK', '1594755677206', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(410, NULL, 11, '95E71356UF668590D', '38', NULL, 'kLR8hL1KMN382T3Malaf', '1592877730663', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(414, NULL, 11, '95E71356UF668590D', '38', NULL, 'kl9kxAI31bWXY1WbJsxt', '1592676186509', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(425, NULL, 11, '95E71356UF668590D', '38', NULL, 'ls93eWj0tYb2ITI3c5Wj', '1591874764154', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(429, NULL, 11, '2A7524374D4396218', '134', NULL, 'mzPcSravJzVYXqQTgJrC', '1592560481389', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(438, NULL, 11, '2ED936645C2606801', '63', NULL, 'o3m8IgDYOXUgaE4GrJL2', '1596361850071', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(442, NULL, 10, '9JK76687C02870028', '136', NULL, 'oKnduITM3E0XBSkErqAT', '1592682824378', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(443, NULL, 11, '2ED936645C2606801', '63', NULL, 'oXdIxyHfGDKbLoMU8ZTm', '1596325570294', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(444, NULL, 11, '2A7524374D4396218', '134', NULL, 'oeNILvIq87DbvjFmggdz', '1592878296692', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(447, NULL, 11, '2AC22342N8007443X', '78', NULL, 'okIlV1hnn4ZgBXn7SSiX', '1592878496369', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(448, NULL, 11, '2A7524374D4396218', '134', NULL, 'onqO45LsE9jqHyRwpH3T', '1591905315027', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(449, NULL, 11, '95E71356UF668590D', '38', NULL, 'oozkEeh2zCLtw4UIQ4Py', '1592236009577', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(450, NULL, 11, '95E71356UF668590D', '38', NULL, 'orSyKgLuwStzA1zii6dr', '1592615422551', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(461, NULL, 11, '95E71356UF668590D', '38', NULL, 'pnsWFnIGc72zpC9S7Ft8', '1592047996663', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(462, NULL, 11, '2ED936645C2606801', '63', NULL, 'pp0HGV29TJa6oDUnqMwU', '1596274791757', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(463, NULL, 11, '2A7524374D4396218', '134', NULL, 'pp2fJhNAuTq4YCfDShOh', '1592614282394', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(464, NULL, 11, '2AC22342N8007443X', '78', NULL, 'q9k50mN2LaJKYctDhheH', '1593055321860', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(466, NULL, 11, '95E71356UF668590D', '38', NULL, 'qG1idcHDplLm7yH7zdq3', '1592561224218', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(470, NULL, 11, '2AC22342N8007443X', '78', NULL, 'qh7DdTcQXsP9vhkG1MAX', '1591873587048', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(471, NULL, 11, '2AC22342N8007443X', '78', NULL, 'qjaTMe8yV5Kt6dJFPuPV', '1592282294849', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(473, NULL, 11, '2A7524374D4396218', '134', NULL, 'qm6MjHx45KYx79kKb1WP', '1592038661164', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(474, NULL, 11, '0E335874U8970894S', '30', NULL, 'qzwRqYvVSnOmBWwz6Px8', '1595824497103', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(477, NULL, 11, '2AC22342N8007443X', '78', NULL, 'rXynuy2ayOOkvbtJXki8', '1592158281877', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(479, NULL, 11, '2ED936645C2606801', '63', NULL, 'rmkQTVWJNdRNk7yo6z7l', '1596340082954', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(480, NULL, 11, '0E335874U8970894S', '30', NULL, 'roBBkaEsrqGvdEB6m4Wx', '1595937307498', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(481, NULL, 11, '2AC22342N8007443X', '78', NULL, 's8mmKBa1s3Kld2Rh1Soy', '1591825463437', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(484, NULL, 10, '9JK76687C02870028', '136', NULL, 'sOHydlSYfbHuATCzxXez', '1593488733409', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(490, NULL, 11, '0E335874U8970894S', '30', NULL, 't1OrESQh85pd7wdiq0SQ', '1595854467193', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(492, NULL, 11, '0E335874U8970894S', '30', NULL, 'tNlatK98Q0MJF6Nh0eEJ', '1595828676965', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(509, NULL, 11, '2AC22342N8007443X', '78', NULL, 'ugno8Vr0kQh1GMJjC1KV', '1592414315541', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(511, NULL, 11, '95E71356UF668590D', '38', NULL, 'ukx1EHjjnGAvSfXaarDd', '1592102150030', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(515, NULL, 11, '8U4557355R056771V', '66', NULL, 'vKy3gPTl8OPfRHUK5ABh', '1594740892671', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(519, NULL, 11, '2A7524374D4396218', '134', NULL, 'w3Jw3FQzOo79EIvapgZc', '1592235878048', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(520, NULL, 11, '2ED936645C2606801', '63', NULL, 'wABQVJTU4fz86mhs7N3S', '1596332821309', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(521, NULL, 11, '2ED936645C2606801', '63', NULL, 'wFOk395dduFQUnzLOreV', '1596260120830', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(526, NULL, 10, '9JK76687C02870028', '136', NULL, 'wKg9xFLfYrUu6BsDzIBR', '1592816384507', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(531, NULL, 11, '2A7524374D4396218', '134', NULL, 'wrUel3fcQxQFvLJiimgU', '1592731578757', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(532, NULL, 11, '2A7524374D4396218', '134', NULL, 'xC62jvJ7gQIrR1JB8Mpf', '1591680275890', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(533, NULL, 11, '2AC22342N8007443X', '78', NULL, 'xGP6TL3HtwXOO5hh1cJt', '1591780633922', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(535, NULL, 10, '9JK76687C02870028', '136', NULL, 'xI6CiQ6O4XGphmvbn8Lb', '1592860738796', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(538, NULL, 11, '95E71356UF668590D', '38', NULL, 'xVWMSH7CeQ0psIYxvHFh', '1592416138922', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(542, NULL, 11, '0E335874U8970894S', '30', NULL, 'y4lzJtRda26mLGEXpTbS', '1595826685690', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(544, NULL, 11, '8U4557355R056771V', '66', NULL, 'yIpEakyi5ey7iY65JdvJ', '1594711811474', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(546, NULL, 11, '2AC22342N8007443X', '78', NULL, 'yhbwwHGHPxVIZGTdFG7a', '1592045870676', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(548, NULL, 11, '2A7524374D4396218', '134', NULL, 'yktbT3617oxIF3JynMLV', '1592327206460', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(556, NULL, NULL, NULL, NULL, NULL, '3zkkGid4rZtaTt0aMQTV', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(557, NULL, NULL, NULL, NULL, NULL, 'DI8Q5xwiKpYnX2J58M1O', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(558, NULL, NULL, NULL, NULL, NULL, 'FXerJyvpp5HmuXKjUMwo', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(559, NULL, NULL, NULL, NULL, NULL, 'bbBemZorT3o3C0RM0ILo', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(560, NULL, NULL, NULL, NULL, NULL, 'cEo042HGUu3GqduHsSKa', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(561, NULL, NULL, NULL, NULL, NULL, 'g1jXf1lkaHLVoLBBTR0x', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(562, NULL, NULL, NULL, NULL, NULL, 'jg42ZEDBTMWaV3tRMVAZ', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(563, NULL, NULL, NULL, NULL, NULL, 'lxqfT8ABjswc1kWTuypt', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(564, 9, 11, '1RL7143626754501K', '32', '1RL7143626754501K', NULL, NULL, '2020-11-08 11:29:46', '2020-11-08 11:29:46'),
(565, 9, 11, '3UL1799233335963Y', '32', '3UL1799233335963Y', NULL, NULL, '2020-11-08 11:32:14', '2020-11-08 11:32:14'),
(566, 9, 11, '-1', '32', '8WM82737RN8252011', NULL, NULL, '2020-11-08 11:34:35', '2020-11-08 11:34:35'),
(567, 9, 11, '3WH73450V8647543X', '32', '18F40743NG587482U', NULL, NULL, '2020-11-08 12:06:46', '2020-11-08 12:06:46'),
(568, 9, 11, '67R74195L7514794D', '32', '6487209537123714W', NULL, NULL, '2020-11-08 12:16:05', '2020-11-08 12:16:05'),
(569, 9, 11, '1G135603TR0449643', '32', '21U095421C429050T', NULL, NULL, '2020-11-08 12:21:40', '2020-11-08 12:21:40'),
(570, 9, 11, '3VE93794S5054010K', '33', '1DH12631MH6941741', NULL, NULL, '2020-11-13 16:04:11', '2020-11-13 16:04:11'),
(571, 9, 11, '8R541627N99980909', '34', '97563285BF1210907', NULL, NULL, '2020-11-13 16:52:20', '2020-11-13 16:52:20'),
(572, 9, 7080, '0D481122AR241015B', '1', '7W165611YA207060P', NULL, NULL, '2020-12-03 18:32:31', '2020-12-03 18:32:31'),
(573, 9, 6372, '42788812W0746711N', '1', '3ME45987MM392590E', NULL, NULL, '2020-12-03 19:43:29', '2020-12-03 19:43:29'),
(574, 9, 12036, '7RS52191DM777914D', '4', '9UF40488VB728745W', NULL, NULL, '2020-12-03 20:21:47', '2020-12-03 20:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `delivery_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_latitude` double DEFAULT NULL,
  `delivery_longitude` double DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expected_delivery_time` timestamp NULL DEFAULT NULL,
  `geohash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_latitude` double DEFAULT NULL,
  `job_longitude` double DEFAULT NULL,
  `job_price` double NOT NULL DEFAULT '0',
  `package_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `posted_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver_contact` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver_instructions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `source_address_appartment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_address_appartment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `delivery_address`, `delivery_latitude`, `delivery_longitude`, `distance`, `description`, `expected_delivery_time`, `geohash`, `item_category`, `job_address`, `job_latitude`, `job_longitude`, `job_price`, `package_size`, `posted_at`, `receiver_id`, `priority`, `receiver_name`, `receiver_contact`, `receiver_instructions`, `security_code`, `status`, `document_id`, `created_at`, `updated_at`, `sender_id`, `source_address_appartment`, `delivery_address_appartment`, `deleted_at`) VALUES
(1, 'Mavdi Chowkdi, Rajkot, Gujarat 360004, India', 24.9180271, 67.0970916, 3, NULL, '2020-12-17 00:00:00', NULL, 'Electronic', 'nr. Shree Mahalaxmi, Suryoday Society, Nalanda Society, Rajkot, Gujarat 360001, India', 24.9180271, 67.0970916, 6372, 'Sedan', NULL, 12, 'Flexible', 'Test Receiver', '7041439950', NULL, '883651', 'Pending', NULL, '2020-12-03 19:42:46', '2020-12-03 19:59:41', 9, 'Honda Show room', 'Hotel RK', NULL),
(2, 'Madhapar, Rajkot, Gujarat 360006, India', 22.3315435, 70.7660939, 3.4, NULL, NULL, NULL, 'Furniture', 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 12036, 'Sedan', NULL, 22, 'Immediate', 'Test Receiver', '7041439950', NULL, '703610', 'Accepted', NULL, '2020-12-03 20:19:38', '2022-08-23 17:41:24', 10, 'Hotel Indira', 'Flat Krishna', NULL),
(3, 'Kolkata, West Bengal, India', 22.3315435, 70.7660939, 3, NULL, NULL, NULL, 'Furniture', 'Maharashtra,Mumbai,India', 22.2883798, 70.7709278, 7897, 'Sedan', NULL, 22, 'Immediate', 'Test Receiver', '7041439950', NULL, '904439', 'Accepted', NULL, '2020-12-03 20:20:26', '2022-08-23 17:54:41', 13, 'Hotel Indira', 'Flat Krishna', NULL),
(4, 'Maharashtra,Mumbai,India', 22.3315435, 70.7660939, 1000000, NULL, NULL, NULL, 'Furniture', 'Kolkata, West Bengal, India', 22.2883798, 70.7709278, 3662, 'Sedan', NULL, 22, 'Immediate', 'Test Receiver', '7041439950', NULL, '368339', 'Accepted', NULL, '2020-12-03 20:21:14', '2022-08-23 17:55:56', 9, 'Hotel Indira', 'Flat Krishna', NULL),
(5, 'Shah Faisal Colony, Karachi, Pakistan', 22.3315435, 70.7660939, 1, NULL, NULL, NULL, 'Miscellaneous', 'Garden west, Karachi, Pakistan', 22.2883798, 70.7709278, 3540, 'Sedan', NULL, 22, 'Immediate', NULL, NULL, '123546', '292338', 'Accepted', NULL, '2020-12-17 12:34:01', '2022-08-23 18:02:00', 12, '12', '123', NULL),
(6, 'Via Aldo Manuzio, 66b, 00153 Roma RM, Italy', 22.3315435, 70.7660939, 945, NULL, NULL, NULL, 'Electronic', 'Testelt, 3272 Scherpenheuvel-Zichem, Belgium', 22.2883798, 70.7709278, 222, 'Truck', NULL, 22, 'Immediate', 'test', 'test', NULL, '826456', 'Accepted', NULL, '2022-05-11 00:57:01', '2022-08-23 17:59:52', 10, 'test', 'test', NULL),
(7, 'Via Aldo Manuzio, 66b, 00153 Roma RM, Italy', 22.3315435, 70.7660939, 945, NULL, NULL, NULL, 'Electronic', 'Testelt, 3272 Scherpenheuvel-Zichem, Belgium', 22.2883798, 70.7709278, 11, 'Truck', NULL, 22, 'Immediate', 'asd', '45645646', NULL, '510908', 'Accepted', NULL, '2022-05-11 00:58:23', '2022-08-23 18:03:47', 4, 'test', 'test', NULL),
(8, 'Asansol Station, Bus Stand, Drysdale Rd, Munshi Bazar, Asansol, West Bengal 713301, India', 22.3315435, 70.7660939, 0, NULL, '2022-05-11 07:00:00', NULL, 'Electronic', 'Asia', 22.2883798, 70.7709278, 21, 'Sedan', NULL, 22, 'Flexible', 'fdsfds', 'dfsdsf', NULL, '879494', 'Accepted', NULL, '2022-05-12 20:14:41', '2022-08-23 18:06:17', 13, 'as', 'as', NULL),
(9, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-05-24 21:44:06', NULL, '1', '', 24.936448, 67.0531584, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, 'Pending', NULL, '2022-05-24 21:44:06', '2022-05-24 21:44:06', NULL, NULL, NULL, NULL),
(10, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-05-26 19:17:15', NULL, '1', '', 24.9102336, 67.0564352, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, 'Pending', NULL, '2022-05-26 19:17:15', '2022-05-26 19:17:15', NULL, NULL, NULL, NULL),
(11, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-05-26 20:41:35', NULL, '31', '', 24.887913, 67.0583857, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, 'Pending', NULL, '2022-05-26 20:41:35', '2022-05-26 20:41:35', NULL, NULL, NULL, NULL),
(12, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-05-26 20:42:01', NULL, '1', '', 24.9102336, 67.0564352, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, 'Pending', NULL, '2022-05-26 20:42:01', '2022-05-26 20:42:01', NULL, NULL, NULL, NULL),
(13, 'Amsterdam, Netherlands', 22.3315435, 70.7660939, NULL, NULL, NULL, NULL, 'Home', 'Austin, TX, USA', 22.2883798, 70.7709278, 4545, 'truck', NULL, 22, 'Immediate', 'madhav', '013213245445', NULL, '571516', 'Accepted', NULL, '2022-05-27 00:34:11', '2022-08-23 18:10:01', 10, 'sadd', 'qwq', NULL),
(14, 'Bebelstraße 36, 65462 Ginsheim-Gustavsburg, Germany', 49.9993026, 8.3226174, 455, NULL, NULL, NULL, 'Home', 'ZI du Forum, 42110 Feurs, France', 249091488, 67104036, 0, 'truck', NULL, NULL, 'Immediate', 'vb', 'bv', NULL, '544207', 'Pending', NULL, '2022-05-27 15:24:33', '2022-05-27 15:24:34', 12, 'fdg', 'dg', NULL),
(15, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', 'Powsińska 31, 02-903 Warszawa, Poland', 249091488, 67104036, 32, 'truck', NULL, NULL, 'Immediate', 'asda', 'asd', NULL, '787806', 'Pending', NULL, '2022-05-27 17:49:05', '2022-05-27 17:49:05', 12, 'dsa', 'asd', NULL),
(16, '1598, Sabra, West Bengal 721445, India', 21.9881959, 87.4047164, 995, NULL, NULL, NULL, 'Home', 'C-21, Block C 5, Hauz Khas Enclave, Hauz Khas, New Delhi, Delhi 110016, India', 249091488, 67104036, 32, 'truck', NULL, NULL, 'Immediate', 'sdsa', 'dsad', NULL, '446144', 'Pending', NULL, '2022-05-27 17:50:35', '2022-05-27 17:50:35', 12, 'dsa', 'sad', NULL),
(17, 'Sadar Bazaar, New Delhi, Delhi, India', 28.6585105, 77.2127305, 268, NULL, NULL, NULL, 'Miscellaneous', 'Dasuya, Punjab 144205, India', 249091488, 67104036, 0, 'truck', NULL, NULL, 'Immediate', 'sd', 'sad', NULL, '419278', 'Pending', NULL, '2022-05-27 17:53:30', '2022-05-27 17:53:30', 12, 'dsad', 'sad', NULL),
(18, 'Dadar, Mumbai, Maharashtra, India', 19.0177989, 72.847812, NULL, NULL, NULL, NULL, 'Home', 'Dimokritou 6 & Germanikis Scholis Athinon, Marousi 151 23, Greece', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', 'sas', 'sas', NULL, '475864', 'Pending', NULL, '2022-05-27 17:58:58', '2022-05-27 17:58:58', 12, 'dsad', 'dsa', NULL),
(19, 'San Francisco, CA, USA', 37.7749295, -122.4194155, NULL, NULL, NULL, NULL, 'Home', 'Aschaffenburg, Germany', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '639633', 'Pending', NULL, '2022-05-27 18:01:38', '2022-05-27 18:01:38', NULL, 'as', 'sa', NULL),
(20, 'San Francisco, CA, USA', 37.7749295, -122.4194155, NULL, NULL, NULL, NULL, 'Home', 'Aschaffenburg, Germany', 249091488, 67104036, 10, 'truck', NULL, 12, 'Immediate', NULL, NULL, NULL, '967142', 'Pending', NULL, '2022-05-27 18:01:48', '2022-05-27 18:01:48', NULL, 'as', 'sa', NULL),
(21, 'San Francisco, CA, USA', 37.7749295, -122.4194155, NULL, NULL, NULL, NULL, 'Home', 'Aschaffenburg, Germany', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '534484', 'Pending', NULL, '2022-05-27 18:01:57', '2022-05-27 18:01:57', NULL, 'as', 'sa', NULL),
(22, 'San Francisco, CA, USA', 37.7749295, -122.4194155, NULL, NULL, NULL, NULL, 'Home', 'Aschaffenburg, Germany', 249091488, 67104036, 1032, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '871783', 'Pending', NULL, '2022-05-27 18:02:01', '2022-05-27 18:02:01', NULL, 'as', 'sa', NULL),
(23, 'Adana, Reşatbey, Seyhan/Adana, Turkey', 36.9914194, 35.3308285, NULL, NULL, NULL, NULL, 'Home', '/55/61, 234 Lê Đức Thọ, Phường 6, Gò Vấp, Thành phố Hồ Chí Minh, Vietnam', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', 'fsd', 'dsfsdf', NULL, '689481', 'Pending', NULL, '2022-05-27 18:03:36', '2022-05-27 18:03:36', 12, 'dsad', 'sad', NULL),
(24, '111 Somerset Rd, Singapore 238164', 1.3001685, 103.8372952, NULL, NULL, NULL, NULL, 'Home', 'Montañeses 2171, C1428 CABA, Argentina', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '194524', 'Pending', NULL, '2022-05-27 18:14:31', '2022-05-27 18:14:31', NULL, '21', '22', NULL),
(25, '2801 N Jaycee Ln, Sioux Falls, SD 57104, USA', 43.5828065, -96.7402171, NULL, NULL, NULL, NULL, 'Home', '17-19 Mannings Heath Rd, Poole BH12 4NQ, UK', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '586189', 'Pending', NULL, '2022-05-27 18:26:36', '2022-05-27 18:26:36', NULL, 'fsd', 'fsd', NULL),
(26, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, NULL, NULL, NULL, NULL, 'Home', 'Sadarghat, Dhaka, Bangladesh', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '456922', 'Pending', NULL, '2022-05-27 18:27:04', '2022-05-27 18:27:04', NULL, 'dsa', 'sad', NULL),
(27, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, NULL, NULL, NULL, NULL, 'Home', 'Sadarghat, Dhaka, Bangladesh', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '518344', 'Pending', NULL, '2022-05-27 18:31:31', '2022-05-27 18:31:31', NULL, 'dsa', 'sad', NULL),
(28, '4V66+RCW, Andheri West, Mumbai, Maharashtra 400053, India', 19.1121049, 72.861073, 787, NULL, NULL, NULL, 'Home', 'Erode, Tamil Nadu, India', 249091488, 67104036, 32, 'truck', NULL, 12, 'Immediate', NULL, NULL, NULL, '989798', 'Pending', NULL, '2022-05-27 18:38:34', '2022-05-27 18:38:34', NULL, 'er', '43', NULL),
(29, '4V66+RCW, Andheri West, Mumbai, Maharashtra 400053, India', 19.1121049, 72.861073, 787, NULL, NULL, NULL, 'Home', 'Erode, Tamil Nadu, India', 249091488, 67104036, 32, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '588428', 'Pending', NULL, '2022-05-27 18:42:36', '2022-05-27 18:42:36', NULL, 'er', '43', NULL),
(30, 'Faisalabad - Multan Motorway, Punjab, Pakistan', 31.3659731, 72.8302535, NULL, NULL, NULL, NULL, 'Miscellaneous', 'GN-28, Street Number 18, GN Block, Sector V, Bidhannagar, Kolkata, West Bengal 700091, India', 249091488, 67104036, 10, 'truck', NULL, 12, 'Immediate', NULL, NULL, NULL, '959520', 'Pending', NULL, '2022-05-27 18:43:01', '2022-05-27 18:43:01', NULL, 'fsd', 'fsd', NULL),
(31, 'Faisalabad - Multan Motorway, Punjab, Pakistan', 31.3659731, 72.8302535, NULL, NULL, NULL, NULL, 'Miscellaneous', 'GN-28, Street Number 18, GN Block, Sector V, Bidhannagar, Kolkata, West Bengal 700091, India', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '486937', 'Pending', NULL, '2022-05-27 18:43:18', '2022-05-27 18:43:18', NULL, 'fsd', 'fsd', NULL),
(32, 'Faisalabad - Multan Motorway, Punjab, Pakistan', 31.3659731, 72.8302535, NULL, NULL, NULL, NULL, 'Miscellaneous', 'GN-28, Street Number 18, GN Block, Sector V, Bidhannagar, Kolkata, West Bengal 700091, India', 249091488, 67104036, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '891034', 'Pending', NULL, '2022-05-27 18:43:37', '2022-05-27 18:43:37', NULL, 'fsd', 'fsd', NULL),
(33, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '622849', 'Pending', NULL, '2022-05-27 18:44:02', '2022-05-27 18:44:02', NULL, 'dsa', 'dsa', NULL),
(34, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '944608', 'Pending', NULL, '2022-05-27 18:46:20', '2022-05-27 18:46:20', NULL, 'dsa', 'dsa', NULL),
(35, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '614835', 'Pending', NULL, '2022-05-27 18:46:57', '2022-05-27 18:46:57', NULL, 'dsa', 'dsa', NULL),
(36, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '151984', 'Pending', NULL, '2022-05-27 18:47:48', '2022-05-27 18:47:48', NULL, 'dsa', 'dsa', NULL),
(37, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '305558', 'Pending', NULL, '2022-05-27 18:47:55', '2022-05-27 18:47:55', NULL, 'dsa', 'dsa', NULL),
(38, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '607161', 'Pending', NULL, '2022-05-27 18:48:01', '2022-05-27 18:48:01', NULL, 'dsa', 'dsa', NULL),
(39, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '898229', 'Pending', NULL, '2022-05-27 18:48:59', '2022-05-27 18:48:59', NULL, 'dsa', 'dsa', NULL),
(40, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '378744', 'Pending', NULL, '2022-05-27 18:49:37', '2022-05-27 18:49:37', NULL, 'dsa', 'dsa', NULL),
(41, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '818744', 'Pending', NULL, '2022-05-27 18:51:05', '2022-05-27 18:51:05', NULL, 'dsa', 'dsa', NULL),
(42, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '713810', 'Pending', NULL, '2022-05-27 18:57:55', '2022-05-27 18:57:55', NULL, 'dsa', 'dsa', NULL),
(43, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '875349', 'Pending', NULL, '2022-05-27 18:59:03', '2022-05-27 18:59:03', NULL, 'dsa', 'dsa', NULL),
(44, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, 12, 'Immediate', NULL, NULL, NULL, '979527', 'Pending', NULL, '2022-05-27 18:59:27', '2022-05-27 18:59:27', NULL, 'dsa', 'dsa', NULL),
(45, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '808911', 'Pending', NULL, '2022-05-27 18:59:53', '2022-05-27 18:59:53', NULL, 'dsa', 'dsa', NULL),
(46, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '772179', 'Pending', NULL, '2022-05-27 19:00:41', '2022-05-27 19:00:41', NULL, 'dsa', 'dsa', NULL),
(47, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '385940', 'Pending', NULL, '2022-05-27 19:00:45', '2022-05-27 19:00:45', NULL, 'dsa', 'dsa', NULL),
(48, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '655961', 'Pending', NULL, '2022-05-27 19:02:57', '2022-05-27 19:02:57', NULL, 'dsa', 'dsa', NULL),
(49, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '926939', 'Pending', NULL, '2022-05-27 19:03:10', '2022-05-27 19:03:10', NULL, 'dsa', 'dsa', NULL),
(50, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '519578', 'Pending', NULL, '2022-05-27 19:03:17', '2022-05-27 19:03:17', NULL, 'dsa', 'dsa', NULL),
(51, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '676323', 'Pending', NULL, '2022-05-27 19:04:52', '2022-05-27 19:04:52', NULL, 'dsa', 'dsa', NULL),
(52, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '439976', 'Pending', NULL, '2022-05-27 19:07:31', '2022-05-27 19:07:31', NULL, 'dsa', 'dsa', NULL),
(53, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '227327', 'Pending', NULL, '2022-05-27 19:07:34', '2022-05-27 19:07:34', NULL, 'dsa', 'dsa', NULL),
(54, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249091488, 67104036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '777527', 'Pending', NULL, '2022-05-27 19:09:03', '2022-05-27 19:09:03', NULL, 'dsa', 'dsa', NULL),
(55, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '927728', 'Pending', NULL, '2022-05-27 19:10:04', '2022-05-27 19:10:04', NULL, 'dsa', 'dsa', NULL),
(56, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, '02125231231', NULL, '532506', 'Pending', NULL, '2022-05-27 19:11:57', '2022-05-27 19:11:57', 12, 'dsa', 'dsa', NULL),
(57, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, '02125231231', NULL, '827429', 'Pending', NULL, '2022-05-27 19:12:29', '2022-05-27 19:12:29', 12, 'dsa', 'dsa', NULL),
(58, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '830738', 'Pending', NULL, '2022-05-27 19:12:33', '2022-05-27 19:12:33', NULL, 'dsa', 'dsa', NULL),
(59, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '119490', 'Pending', NULL, '2022-05-27 19:13:41', '2022-05-27 19:13:41', NULL, 'dsa', 'dsa', NULL),
(60, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '550765', 'Pending', NULL, '2022-05-27 19:14:08', '2022-05-27 19:14:08', NULL, 'dsa', 'dsa', NULL),
(61, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '123102', 'Pending', NULL, '2022-05-27 19:14:09', '2022-05-27 19:14:09', NULL, 'dsa', 'dsa', NULL),
(62, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '869092', 'Pending', NULL, '2022-05-27 19:14:09', '2022-05-27 19:14:09', NULL, 'dsa', 'dsa', NULL),
(63, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '472346', 'Pending', NULL, '2022-05-27 19:14:10', '2022-05-27 19:14:10', NULL, 'dsa', 'dsa', NULL),
(64, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '611896', 'Pending', NULL, '2022-05-27 19:16:02', '2022-05-27 19:16:02', NULL, 'dsa', 'dsa', NULL),
(65, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '721514', 'Pending', NULL, '2022-05-27 19:16:11', '2022-05-27 19:16:11', NULL, 'dsa', 'dsa', NULL),
(66, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', -1801475808, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '333199', 'Pending', NULL, '2022-05-27 19:16:18', '2022-05-27 19:16:18', NULL, 'dsa', 'dsa', NULL),
(67, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, 1, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 249491488, 67154036, 2112, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '657188', 'Pending', NULL, '2022-05-27 19:36:20', '2022-05-27 19:36:20', NULL, 'dsa', 'dsa', NULL),
(68, 'Amsterdam, Netherlands', 52.3675734, 4.9041389, NULL, NULL, NULL, NULL, 'Miscellaneous', 'Sydney NSW, Australia', 338688197, 1512092955, 34342, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '172719', 'Pending', NULL, '2022-05-27 19:41:40', '2022-05-27 19:41:40', 12, 'sas', 'dsdsd', NULL),
(69, 'Amsterdam, Netherlands', 52.3675734, 4.9041389, NULL, NULL, NULL, NULL, 'Miscellaneous', 'Sydney NSW, Australia', 338688197, 1512092955, 34342, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '626118', 'Pending', NULL, '2022-05-27 20:30:18', '2022-05-27 20:30:18', 12, 'sas', 'dsdsd', NULL),
(70, 'Amsterdam, Netherlands', 52.3675734, 4.9041389, NULL, NULL, NULL, NULL, 'Miscellaneous', 'Sydney NSW, Australia', 338688197, 1512092955, 34342, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '825971', 'Pending', NULL, '2022-05-27 20:32:18', '2022-05-27 20:32:18', 12, 'sas', 'dsdsd', NULL),
(71, 'Amsterdam, Netherlands', 52.3675734, 4.9041389, NULL, NULL, NULL, NULL, 'Miscellaneous', 'Sydney NSW, Australia', 338688197, 1512092955, 34342, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '633110', 'Pending', NULL, '2022-05-27 20:33:05', '2022-05-27 20:33:05', 12, 'sas', 'dsdsd', NULL),
(72, 'Austin, TX, USA', 30.267153, -97.7430608, NULL, NULL, NULL, NULL, 'Home', 'Amsterdam, Netherlands', 249091488, 67104036, 1034, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '177181', 'Pending', NULL, '2022-05-27 20:35:35', '2022-05-27 20:35:35', 12, 'sas', 'sds', NULL),
(73, 'Dasmariñas, Cavite, Philippines', 14.2990183, 120.9589699, NULL, NULL, NULL, NULL, 'Home', '100 Princess Rd, Hulme, Manchester M15 5AS, UK', 53.4606974, -2.2457517, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '609264', 'Pending', NULL, '2022-05-27 20:37:53', '2022-05-27 20:37:53', 12, 'asd', 'dasd', NULL),
(74, 'Quito, Ecuador', -0.1806532, -78.4678382, NULL, NULL, NULL, NULL, 'Furniture', 'London HA9 0WS, UK', 51.5560247, -0.2796177, 105445, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '489990', 'Pending', NULL, '2022-05-27 20:42:40', '2022-05-27 20:42:40', 12, 'ds', 'sa', NULL),
(75, 'Europe', 54.5259614, 15.2551187, 904, NULL, NULL, NULL, 'Other', 'Uithoorn, Netherlands', 52.2446266, 4.8317337, 897897, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '885640', 'Pending', NULL, '2022-05-27 20:43:43', '2022-05-27 20:43:43', 12, 'erer', 'xzzx', NULL),
(76, '1500 Pattison Avenue &, S Broad St, Philadelphia, PA 19145, USA', 39.9021375, -75.1838075, NULL, NULL, NULL, NULL, 'Other', 'Ghaziabad, Uttar Pradesh, India', 28.6691565, 77.4537578, 104334, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '532175', 'Pending', NULL, '2022-05-27 20:47:20', '2022-05-27 20:47:20', 12, 'fgh', 'gffd', NULL),
(77, 'De Haar 9, 9405 TE Assen, Netherlands', 52.9583015, 6.522342, 4, NULL, NULL, NULL, 'Miscellaneous', 'Shahrah-e-Faisal, Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan', 24.8871232, 67.1346924, 534534, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '304785', 'Pending', NULL, '2022-05-27 21:45:53', '2022-05-27 21:45:53', 12, 'trtert', 'dsdsa', NULL),
(78, 'Dubai - United Arab Emirates', 25.2048493, 55.2707828, NULL, NULL, '2022-05-28 07:00:00', NULL, 'Electronic', 'Dallas, TX, USA', 32.7766642, -96.7969879, 105, 'sedan', NULL, NULL, 'Flexible', NULL, NULL, NULL, '681498', 'Pending', NULL, '2022-05-27 21:55:52', '2022-05-27 21:55:52', 12, 'sd', 'sdf', NULL),
(79, 'Tietgensgade 65, 1704 København V, Denmark', 55.6695423, 12.5646361, 774, NULL, NULL, NULL, 'Other', 'Waterloo Station, London SE1 8SR, UK', 51.5031217, -0.1129493, 78998, 'truck', NULL, 12, 'Immediate', NULL, NULL, NULL, '312930', 'Pending', NULL, '2022-05-27 22:03:55', '2022-05-27 22:03:55', 12, 'er', 'hj', NULL),
(80, 'X4XX+645, South Baluchar, Malda, West Bengal 732101, India', 24.9978805, 88.1485045, NULL, NULL, '2022-05-28 07:00:00', NULL, 'Home', 'San Diego, CA, USA', 32.715738, -117.1610838, 10, 'suv', NULL, NULL, 'Flexible', NULL, NULL, NULL, '181548', 'Pending', NULL, '2022-05-28 13:36:06', '2022-05-28 13:36:06', 12, 'asd', 'da', NULL),
(81, 'Faisalabad - Multan Motorway, Punjab, Pakistan', 31.3659731, 72.8302535, NULL, NULL, NULL, NULL, 'Home', '600 Terminal Dr, Louisville, KY 40209, USA', 38.175662, -85.7369231, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '335910', 'Pending', NULL, '2022-05-28 13:42:22', '2022-05-28 13:42:22', 12, 'dsf', 'dsf', NULL),
(82, 'Balasamudram, Hanamkonda, Telangana 506001, India', 18.0038513, 79.5605196, NULL, NULL, NULL, NULL, 'Home', 'Powsińska 31, 02-903 Warszawa, Poland', 52.1875648, 21.0609341, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '366737', 'Pending', NULL, '2022-05-28 13:43:49', '2022-05-28 13:43:49', 12, 'dsa', 'dsa', NULL),
(83, 'San Diego, CA, USA', 32.715738, -117.1610838, NULL, NULL, NULL, NULL, 'Home', 'Aschaffenburg, Germany', 49.9806625, 9.1355554, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '619298', 'Pending', NULL, '2022-05-28 13:44:36', '2022-05-28 13:44:36', 12, 'sa', 'sa', NULL),
(84, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, NULL, NULL, NULL, NULL, 'Home', 'Dasmariñas, Cavite, Philippines', 14.2990183, 120.9589699, 10, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '236776', 'Pending', NULL, '2022-05-28 13:46:25', '2022-05-28 13:46:25', 12, 'sad', 'dsa', NULL),
(85, '2801 N Jaycee Ln, Sioux Falls, SD 57104, USA', 43.5828065, -96.7402171, NULL, NULL, NULL, NULL, 'Home', 'Faisalabad, Punjab, Pakistan', 31.4503662, 73.1349605, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '493795', 'Pending', NULL, '2022-05-28 13:50:08', '2022-05-28 13:50:08', 12, 'sdf', 'fsd', NULL),
(86, '100 Princess Rd, Hulme, Manchester M15 5AS, UK', 53.4606974, -2.2457517, 249, NULL, NULL, NULL, 'Home', 'Adsdean, Chichester PO18 9DN, UK', 50.877119, -0.868771, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '400878', 'Pending', NULL, '2022-05-28 13:54:52', '2022-05-28 13:54:52', 12, 'sa', 'dsa', NULL),
(87, 'San Francisco, CA, USA', 37.7749295, -122.4194155, 502, NULL, NULL, NULL, 'Miscellaneous', 'San Diego, CA, USA', 32.715738, -117.1610838, 0, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '883265', 'Pending', NULL, '2022-05-28 13:56:23', '2022-05-28 13:56:23', 12, 'sa', 'sa', NULL),
(88, 'Sadarghat, Dhaka, Bangladesh', 23.705538, 90.4101155, 1, NULL, NULL, NULL, 'Home', 'Sadarghat, Dhaka, Bangladesh', 23.705538, 90.4101155, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '603884', 'Pending', NULL, '2022-05-28 13:57:25', '2022-05-28 13:57:25', 12, 'sad', 'ads', NULL),
(89, 'San Diego, CA, USA', 32.715738, -117.1610838, NULL, NULL, NULL, NULL, 'Home', '06081 Assisi, Province of Perugia, Italy', 43.0707017, 12.6195966, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '437286', 'Pending', NULL, '2022-05-28 13:59:38', '2022-05-28 13:59:38', 12, 'as', 'sa', NULL),
(90, 'First Ave, Doncaster saDN9 3RH, UK', 53.4778225, -1.0054727, 5, NULL, NULL, NULL, 'Home', 'Saddar, Rawalpindi, Punjab 46000, Pakistan', 33.5968788, 73.0528412, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '827373', 'Pending', NULL, '2022-05-28 14:00:15', '2022-05-28 14:00:15', 12, 'dsa', 'sad', NULL),
(91, '5500 Campanile Dr, San Diego, CA 92182, USA', 32.7774047, -117.0714068, 1, NULL, NULL, NULL, 'Home', 'Denver, CO, USA', 39.7392358, -104.990251, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '673812', 'Pending', NULL, '2022-05-28 14:02:11', '2022-05-28 14:02:11', 12, 'ds', 'ds', NULL),
(92, 'San Antonio, TX, USA', 29.4251905, -98.4945922, 1, NULL, NULL, NULL, 'Home', 'San Diego, CA, USA', 32.715738, -117.1610838, 0, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '471140', 'Pending', NULL, '2022-05-28 14:04:09', '2022-05-28 14:04:09', 12, 'sa', 'as', NULL),
(93, 'San Diego, CA, USA', 32.715738, -117.1610838, 502, NULL, NULL, NULL, 'Home', 'San Francisco, CA, USA', 37.7749295, -122.4194155, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '537155', 'Pending', NULL, '2022-05-28 14:10:50', '2022-05-28 14:10:50', 12, 'sa', 'sa', NULL),
(94, 'San Diego, CA, USA', 32.715738, -117.1610838, 1, NULL, NULL, NULL, 'Home', 'San Diego, CA, USA', 32.715738, -117.1610838, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '901127', 'Pending', NULL, '2022-05-28 14:13:30', '2022-05-28 14:13:30', 12, 'sa', 'sa', NULL),
(95, 'Sadarghat, Dhaka, Bangladesh', 23.705538, 90.4101155, NULL, NULL, NULL, NULL, 'Home', 'San Francisco, CA, USA', 37.7749295, -122.4194155, 60, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '443551', 'Pending', NULL, '2022-05-28 14:17:20', '2022-05-28 14:17:20', 12, 'sad', 'dsa', NULL),
(96, '06081 Assisi, Province of Perugia, Italy', 43.0707017, 12.6195966, NULL, NULL, NULL, NULL, 'Home', 'VPO-ALAMPUR VIA-DASYAL, DISTT, Moti Nagar, Hoshiarpur, Punjab 144251, India', 31.813226, 75.663738, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '680361', 'Pending', NULL, '2022-05-28 14:19:57', '2022-05-28 14:19:57', 12, 'as', 'sa', NULL),
(97, 'Sadar Bazaar, New Delhi, Delhi, India', 28.6585105, 77.2127305, NULL, NULL, NULL, NULL, 'Home', '100 Princess Rd, Hulme, Manchester M15 5AS, UK', 53.4606974, -2.2457517, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '195939', 'Pending', NULL, '2022-05-28 14:32:49', '2022-05-28 14:32:49', 12, 'sa', 'sad', NULL),
(98, '2-20 Western Rd, London NW10 7LW, UK', 51.52781, -0.26989, 1, NULL, NULL, NULL, 'Home', 'Powsińska 31, 02-903 Warszawa, Poland', 52.1875648, 21.0609341, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '491279', 'Pending', NULL, '2022-05-28 14:38:07', '2022-05-28 14:38:07', 12, 'sad', 'sd', NULL),
(99, 'Sadar Bazaar, New Delhi, Delhi, India', 28.6585105, 77.2127305, 1, NULL, NULL, NULL, 'Home', 'Sadarghat, Dhaka, Bangladesh', 23.705538, 90.4101155, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '677864', 'Pending', NULL, '2022-05-28 14:41:50', '2022-05-28 14:41:50', 12, 'sad', 'sa', NULL),
(100, 'Balasamudram, Hanamkonda, Telangana 506001, India', 18.0038513, 79.5605196, NULL, NULL, NULL, NULL, 'Home', '5500 Campanile Dr, San Diego, CA 92182, USA', 32.7774047, -117.0714068, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '388381', 'Pending', NULL, '2022-05-28 14:45:52', '2022-05-28 14:45:52', 12, 'das', 'dsa', NULL),
(101, '2801 N Jaycee Ln, Sioux Falls, SD 57104, USA', 43.5828065, -96.7402171, 867, NULL, NULL, NULL, 'Home', '600 Terminal Dr, Louisville, KY 40209, USA', 38.175662, -85.7369231, 0, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '648847', 'Pending', NULL, '2022-05-28 14:47:19', '2022-05-28 14:47:19', 12, 'fds', 'dsf', NULL),
(102, 'San Diego, CA, USA', 32.715738, -117.1610838, 1, NULL, NULL, NULL, 'Home', 'San Antonio, TX, USA', 29.4251905, -98.4945922, 0, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '334597', 'Pending', NULL, '2022-05-28 14:57:34', '2022-05-28 14:57:34', 12, 'sa', 'asd', NULL),
(103, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, NULL, NULL, NULL, NULL, 'Home', 'San Diego, CA, USA', 32.715738, -117.1610838, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '785988', 'Pending', NULL, '2022-05-28 14:58:19', '2022-05-28 14:58:19', 12, 'dsa', 'sad', NULL),
(104, 'Balasamudram, Hanamkonda, Telangana 506001, India', 18.0038513, 79.5605196, 1, NULL, NULL, NULL, 'Home', 'Sadarghat, Dhaka, Bangladesh', 23.705538, 90.4101155, 0, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '656431', 'Pending', NULL, '2022-05-28 14:59:43', '2022-05-28 14:59:43', 12, 'dsa', 'dsa', NULL),
(105, 'San Diego, CA, USA', 32.715738, -117.1610838, 1, NULL, NULL, NULL, 'Home', 'San Diego, CA, USA', 32.715738, -117.1610838, 0, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '231214', 'Pending', NULL, '2022-05-28 15:09:31', '2022-05-28 15:09:31', 12, 'sa', 'as', NULL),
(106, 'C-21, Block C 5, Hauz Khas Enclave, Hauz Khas, New Delhi, Delhi 110016, India', 28.5462079, 77.1966426, 937, NULL, NULL, NULL, 'Home', 'Balasamudram, Hanamkonda, Telangana 506001, India', 18.0038513, 79.5605196, 0, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '316454', 'Pending', NULL, '2022-05-28 15:15:09', '2022-05-28 15:15:09', 12, 'sad', 'dsa', NULL),
(107, '11, Jalan Pelabur B 23/B, Seksyen 23, 40300 Shah Alam, Selangor, Malaysia', 3.0449747, 101.5260519, 3, NULL, NULL, NULL, 'Miscellaneous', 'FDA Bhawan, Kotla Marg, Mandi House, New Delhi, Delhi 110002, India', 28.6325203, 77.2353961, 0, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '150852', 'Pending', NULL, '2022-05-28 15:15:56', '2022-05-28 15:15:56', 12, 'sad', 'dsa', NULL),
(108, 'Balasamudram, Hanamkonda, Telangana 506001, India', 18.0038513, 79.5605196, NULL, NULL, NULL, NULL, 'Home', 'Aschaffenburg, Germany', 49.9806625, 9.1355554, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '831619', 'Pending', NULL, '2022-05-28 15:17:42', '2022-05-28 15:17:42', 12, 'dsa', 'sad', NULL),
(109, 'First Ave, Doncaster DN9 3RH, UK', 53.4778225, -1.0054727, NULL, NULL, NULL, NULL, 'Home', 'Sadarghat, Dhaka, Bangladesh', 23.705538, 90.4101155, 10, 'suv', NULL, 12, 'Immediate', NULL, NULL, NULL, '969546', 'Pending', NULL, '2022-05-28 15:19:42', '2022-05-28 15:19:42', 12, 'dsa', 'sa', NULL),
(110, '06081 Assisi, Province of Perugia, Italy', 43.0707017, 12.6195966, 1, NULL, NULL, NULL, 'Miscellaneous', '06081 Assisi, Province of Perugia, Italy', 43.0707017, 12.6195966, 0, 'truck', NULL, 12, 'Immediate', NULL, NULL, NULL, '950458', 'Pending', NULL, '2022-05-28 16:07:05', '2022-05-28 16:07:05', 12, 'as', 'as', NULL),
(111, 'Seattle, WA, USA', 47.6062095, -122.3320708, NULL, NULL, '2022-05-31 07:00:00', NULL, 'Miscellaneous', 'Sydney NSW, Australia', -33.8688197, 151.2092955, 1888, 'suv', NULL, NULL, 'Flexible', NULL, NULL, NULL, '586907', 'Pending', NULL, '2022-05-28 16:58:54', '2022-05-28 16:58:54', 12, 'sa', 'asd', NULL),
(112, 'Shah Faisal Colony, Karachi, Karachi City, Sindh, Pakistan', 24.8773062, 67.1591053, 12.8, NULL, '2022-05-31 07:00:00', NULL, 'Home', 'Garden West Karachi, Karachi City, Sindh, Pakistan', 24.8734758, 67.0156785, 45, 'suv', NULL, NULL, 'Flexible', NULL, NULL, NULL, '170901', 'Pending', NULL, '2022-05-30 18:42:36', '2022-05-30 18:42:36', 13, '784', '123', NULL),
(113, 'Rome, Metropolitan City of Rome, Italy', 41.9027835, 12.4963655, 1, NULL, NULL, NULL, 'Home', 'Waterloo Station, London SE1 8SR, UK', 51.5031217, -0.1129493, 453, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '740964', 'Pending', NULL, '2022-06-01 15:58:39', '2022-06-01 15:58:39', 12, 'asd', 'ere', NULL),
(114, 'FDR Park, 1500 Pattison Avenue &, S Broad St, Philadelphia, PA 19145, USA', 39.9021375, -75.1838075, NULL, '', '2022-06-01 16:00:15', NULL, '2', '', 24.9180271, 67.0970916, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-01 16:00:15', '2022-06-01 16:00:15', NULL, NULL, NULL, NULL),
(115, 'Haridwar, Uttarakhand, India', 29.9456906, 78.1642478, NULL, NULL, '2022-06-16 07:00:00', NULL, 'Electronic', 'Hamburg, Germany', 53.5510846, 9.9936818, 23546, 'truck', NULL, NULL, 'Flexible', NULL, NULL, NULL, '627762', 'Pending', NULL, '2022-06-01 18:39:41', '2022-06-01 18:39:41', 12, 'Gjl', 'Hello', NULL),
(116, 'Dubai - United Arab Emirates', 25.2048493, 55.2707828, NULL, NULL, NULL, NULL, 'Electronic', 'Hyderabad, Telangana, India', 17.385044, 78.486671, 356, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '672647', 'Pending', NULL, '2022-06-01 18:42:39', '2022-06-01 18:42:39', 12, 'Hkj', 'Ghj', NULL),
(117, 'RK Hegde Nagar, Bengaluru, Karnataka, India', 13.0675728, 77.6335573, 205, NULL, NULL, NULL, 'Documents', '56, KB Dasan Rd, Teynampet, Chennai, Tamil Nadu 600018, India', 13.035992, 80.2477624, 0, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '942073', 'Pending', NULL, '2022-06-01 18:44:35', '2022-06-01 18:44:35', 12, 'Ggs', 'Hshdv', NULL),
(118, 'Plot E 11A, Block 10-A Block 10 A National Cement Employees CHS, Karachi, Karachi City, Sindh, Pakistan', 24.9090857, 67.1041092, NULL, '', '2022-06-01 18:45:54', NULL, '1', '', 24.9102336, 67.0564352, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-01 18:45:54', '2022-06-01 18:45:54', NULL, NULL, NULL, NULL),
(119, 'Seattle, WA, USA', 47.6062095, -122.3320708, 808, NULL, NULL, NULL, 'Home', 'San Francisco, CA, USA', 37.7749295, -122.4194155, 102, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '159237', 'Pending', NULL, '2022-06-01 21:54:38', '2022-06-01 21:54:38', 12, 'apart', 'asas', NULL),
(120, 'Via Aldo Manuzio, 66b, 00153 Roma RM, Italy', 41.8776847, 12.473844, 4, NULL, '2022-06-02 07:00:00', NULL, 'Furniture', 'Karachi, Karachi City, Sindh, Pakistan', 24.8607343, 67.0011364, 500, 'suv', NULL, NULL, 'Flexible', NULL, NULL, NULL, '112776', 'Pending', NULL, '2022-06-01 22:51:26', '2022-06-01 22:51:26', 12, 'Ssuit', 'Test', NULL),
(121, 'Plot E 11A, Block 10-A Block 10 A National Cement Employees CHS, Karachi, Karachi City, Sindh, Pakistan', 24.9090827, 67.1040892, NULL, '', '2022-06-01 22:52:15', NULL, '1', '', 24.936448, 67.0531584, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-01 22:52:15', '2022-06-01 22:52:15', NULL, NULL, NULL, NULL),
(122, 'Orlando, FL, USA', 28.5383832, -81.3789269, NULL, NULL, NULL, NULL, 'Home', 'Terminal 3, Indira Gandhi International Airport, New Delhi, Delhi 110037, India', 28.5550838, 77.0844015, 1666, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '249075', 'Pending', NULL, '2022-06-02 19:01:44', '2022-06-02 19:01:44', 12, 'hsis', 'shshs', NULL),
(123, 'New York, NY, USA', 40.7127753, -74.0059728, NULL, NULL, NULL, NULL, 'Home', 'Heidelberg, Germany', 49.3987524, 8.6724335, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '158787', 'Pending', NULL, '2022-06-02 20:50:41', '2022-06-02 20:50:41', 12, 'Hkk', 'Oiyb', NULL),
(124, 'J K LONE HOSPITAL, J K LONE HOSPITAL, Gangawal Park, Adarsh Nagar, Jaipur, Rajasthan 302004, India', 26.9013525, 75.8180774, 897, NULL, NULL, NULL, 'Electronic', 'Hyderabad, Telangana, India', 17.385044, 78.486671, 552, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '937886', 'Pending', NULL, '2022-06-02 23:22:30', '2022-06-02 23:22:30', 12, 'Hk', 'Hajbsv', NULL),
(125, 'Karachi, Karachi City, Sindh, Pakistan', 24.8607343, 67.0011364, 101, NULL, '2022-06-15 07:00:00', NULL, 'Home', 'Hyderabad, Sindh, Pakistan', 25.3959687, 68.357776, 56685, 'truck', NULL, NULL, 'Flexible', NULL, NULL, NULL, '328139', 'Pending', NULL, '2022-06-03 00:16:51', '2022-06-03 00:16:51', 12, 'Hk', 'Kr', NULL),
(126, '818 Shah Faisal, Green Town Asifabad Green Town Shah Faisal Colony, Karachi, Karachi City, Sindh 75200, Pakistan', 24.8834828, 67.161147, NULL, '', '2022-06-03 02:44:42', NULL, '31', '', 24.887913, 67.0583857, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-03 02:44:42', '2022-06-03 02:44:42', NULL, NULL, NULL, NULL),
(127, 'Florence, Metropolitan City of Florence, Italy', 43.7695604, 11.2558136, 609, NULL, NULL, NULL, 'Home', 'Frankfurt, Germany', 50.1109221, 8.6821267, 123, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '287978', 'Pending', NULL, '2022-06-03 18:28:19', '2022-06-03 18:28:19', 12, 'sds', 'sds', NULL),
(128, '2995 Atlas Rd, Richmond, CA 94806, USA', 38.0004826, -122.3507389, 51.3, NULL, NULL, NULL, 'Home', '807 N McCarthy Blvd, Milpitas, CA 95035, USA', 37.4390106, -121.9244903, 1000, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '647993', 'Pending', NULL, '2022-06-03 18:32:33', '2022-06-03 18:32:33', 12, 'sdf', 'dsf', NULL),
(129, 'Hakkâri, Hakkâri Merkez/Hakkari, Turkey', 37.577427, 43.736782, 2, NULL, NULL, NULL, 'Electronic', 'Hamburg, Germany', 53.5510846, 9.9936818, 693, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '270707', 'Pending', NULL, '2022-06-03 18:41:41', '2022-06-03 18:41:41', 12, 'Hjs', 'Haj', NULL),
(130, 'Hamburg, Germany', 53.5510846, 9.9936818, NULL, NULL, NULL, NULL, 'Electronic', 'Haridwar, Uttarakhand, India', 29.9456906, 78.1642478, 10646, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '671342', 'Pending', NULL, '2022-06-03 20:54:54', '2022-06-03 20:54:54', 12, 'Bsjbs', 'Hjshs', NULL),
(131, 'Sydney NSW, Australia', -33.8688197, 151.2092955, NULL, NULL, NULL, NULL, 'Home', 'Amsterdam, Netherlands', 52.3675734, 4.9041389, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '563122', 'Pending', NULL, '2022-06-03 21:12:18', '2022-06-03 21:12:18', 12, 'ass', 'sex', NULL),
(132, '100 Princess Rd, Hulme, Manchester M15 5AS, UK', 53.4606974, -2.2457517, 1, NULL, NULL, NULL, 'Home', '100 Princess Rd, Hulme, Manchester M15 5AS, UK', 53.4606974, -2.2457517, 1241, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '870184', 'Pending', NULL, '2022-06-03 21:17:33', '2022-06-03 21:17:33', 12, 'asd', 'das', NULL),
(133, 'Meestersweg 4, 7951 BS Staphorst, Netherlands', 52.6490136, 6.2080337, NULL, NULL, NULL, NULL, 'Home', 'Area 51, NV, USA', 37.2430548, -115.7930198, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '771229', 'Pending', NULL, '2022-06-03 21:21:34', '2022-06-03 21:21:34', 12, '445', '56', NULL),
(134, 'Hyderabad, Telangana, India', 17.385044, 78.486671, NULL, NULL, NULL, NULL, 'Electronic', 'Atlanta, GA, USA', 33.7489954, -84.3879824, 106, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '903513', 'Pending', NULL, '2022-06-03 22:04:02', '2022-06-03 22:04:02', 12, 'Apr', 'Ha', NULL),
(135, 'HSR Layout, Bengaluru, Karnataka, India', 12.9121181, 77.6445548, NULL, NULL, NULL, NULL, 'Electronic', 'Heidelberg, Germany', 49.3987524, 8.6724335, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '130335', 'Pending', NULL, '2022-06-03 23:51:59', '2022-06-03 23:52:00', 12, 'Hajs', 'Hskhs', NULL),
(136, 'Bolívar 466, C1066AAJ CABA, Argentina', -34.6133246, -58.3732276, NULL, NULL, NULL, NULL, 'Home', 'Meestersweg 4, 7951 BS Staphorst, Netherlands', 52.6490136, 6.2080337, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '431805', 'Pending', NULL, '2022-06-03 23:54:28', '2022-06-03 23:54:28', 12, '5', '4', NULL),
(137, '47 Đ. Nguyễn Tuân, Thanh Xuân Trung, Thanh Xuân, Hà Nội, Vietnam', 20.9944699, 105.8053163, NULL, NULL, NULL, NULL, 'Miscellaneous', '561 Sherbourne St, Toronto, ON M4X 0A1, Canada', 43.6693106, -79.3751182, 10, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '903202', 'Pending', NULL, '2022-06-03 23:55:55', '2022-06-03 23:55:55', 12, '789', '2', NULL),
(138, 'Nagpur, Maharashtra, India', 21.1458004, 79.0881546, 4, NULL, NULL, NULL, 'Electronic', 'Jakarta, Indonesia', -6.2087634, 106.845599, 6499, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '475853', 'Pending', NULL, '2022-06-04 00:04:24', '2022-06-04 00:04:24', 12, 'Jsk', 'Jan', NULL),
(139, 'Atlanta, GA, USA', 33.7489954, -84.3879824, NULL, NULL, NULL, NULL, 'Miscellaneous', 'Amsterdam, Netherlands', 52.3675734, 4.9041389, 456, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '532586', 'Pending', NULL, '2022-06-04 00:18:22', '2022-06-04 00:18:22', 12, 'as', 'sas', NULL),
(140, 'Indore, Madhya Pradesh, India', 22.7195687, 75.8577258, 393, NULL, NULL, NULL, 'Electronic', 'Jodhpur, Rajasthan, India', 26.2389469, 73.0243094, 65, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '217629', 'Pending', NULL, '2022-06-04 00:32:32', '2022-06-04 00:32:32', 12, 'J', 'Jabs', NULL),
(141, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-06-04 22:02:44', NULL, '1', '', 24.9102336, 67.0564352, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-04 22:02:44', '2022-06-04 22:02:44', NULL, NULL, NULL, NULL),
(142, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-06-04 22:03:28', NULL, '1', '', 24.9102336, 67.0564352, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-04 22:03:28', '2022-06-04 22:03:28', NULL, NULL, NULL, NULL),
(143, 'W39W+6R8, Block 6 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, NULL, '', '2022-06-04 23:14:25', NULL, '1', '', 24.936448, 67.0531584, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-04 23:14:25', '2022-06-04 23:14:25', NULL, NULL, NULL, NULL),
(144, 'Hamburg, Germany', 53.5510846, 9.9936818, NULL, NULL, NULL, NULL, 'Electronic', 'Hyderabad, Telangana, India', 17.385044, 78.486671, 1066, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '184228', 'Pending', NULL, '2022-06-06 18:59:35', '2022-06-06 18:59:35', 12, 'Haj', 'Haj', NULL),
(145, 'Plot E 11A, Block 10-A Block 10 A National Cement Employees CHS, Karachi, Karachi City, Sindh, Pakistan', 24.9090801, 67.1040918, NULL, '', '2022-06-06 19:00:42', NULL, '31', '', 24.887913, 67.0583857, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-06 19:00:42', '2022-06-06 19:00:42', NULL, NULL, NULL, NULL),
(146, 'Dublin, Ireland', 53.3498053, -6.2603097, NULL, NULL, NULL, NULL, 'Home', 'Khatu Shyam Ji Temple, Rajasthan 332404, India', 27.3622905, 75.5198166, 145, 'suv', NULL, NULL, 'Immediate', NULL, NULL, NULL, '225620', 'Pending', NULL, '2022-06-11 20:15:17', '2022-06-11 20:15:17', 12, 'sda', 'asa', NULL),
(147, 'Guadalajara, Jalisco, Mexico', 20.6596988, -103.3496092, NULL, NULL, NULL, NULL, 'Home', 'Hamburg, Germany', 53.5510846, 9.9936818, 10, 'truck', NULL, NULL, 'Immediate', NULL, NULL, NULL, '513930', 'Pending', NULL, '2022-06-14 20:14:22', '2022-06-14 20:14:22', 12, 'Hgsj', 'Bb', NULL),
(148, 'Frankfurt, Germany', 50.1109221, 8.6821267, NULL, NULL, '2022-06-15 07:00:00', NULL, 'Home', 'Golden Gate Bridge, San Francisco, CA, USA', 37.8199286, -122.4782551, 145, 'suv', NULL, NULL, 'Flexible', NULL, NULL, NULL, '955502', 'Pending', NULL, '2022-06-15 01:04:47', '2022-06-15 01:04:47', 14, 'Gff', 'Gf', NULL),
(149, '1800 Ellis St, San Francisco, CA 94115, USA', 37.785834, -122.406417, NULL, '', '2022-06-15 01:05:31', NULL, '31', '', 24.887913, 67.0583857, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-06-15 01:05:31', '2022-06-15 01:05:31', NULL, NULL, NULL, NULL),
(150, 'Frisco, TX, USA', 33.1506744, -96.8236116, 22.4, NULL, NULL, NULL, 'Home', '12569 Mercer Pkwy, Farmers Branch, TX 75234, USA', 32.9126016, -96.927038, 23, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '631840', 'Pending', NULL, '2022-06-16 01:40:08', '2022-06-16 01:40:08', 14, 'Test', 'Test', NULL),
(151, 'Frisco, TX, USA', 33.1506744, -96.8236116, 22.4, NULL, NULL, NULL, 'Home', '12569 Mercer Pkwy, Farmers Branch, TX 75234, USA', 32.9126016, -96.927038, 23, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '718739', 'Pending', NULL, '2022-06-16 01:40:16', '2022-06-16 01:40:16', 14, 'Test', 'Test', NULL),
(152, 'Delhi, India', 28.6862738, 77.2217831, 877, NULL, '2022-06-15 07:00:00', NULL, 'Furniture', 'Mumbai, Maharashtra, India', 19.0759837, 72.8776559, 23, 'truck', NULL, NULL, 'Flexible', NULL, NULL, NULL, '699659', 'Pending', NULL, '2022-06-16 01:46:33', '2022-06-16 01:46:33', 14, 'Test', 'Test', NULL),
(153, 'Delhi, India', 28.6862738, 77.2217831, 877, NULL, '2022-06-15 07:00:00', NULL, 'Furniture', 'Mumbai, Maharashtra, India', 19.0759837, 72.8776559, 23, 'truck', NULL, NULL, 'Flexible', NULL, NULL, NULL, '176306', 'Pending', NULL, '2022-06-16 01:46:45', '2022-06-16 01:46:45', 14, 'Test', 'Test', NULL),
(154, 'Calle del Gral. Pardiñas, 35, 28001 Madrid, Spain', 40.4273236, -3.6785559, 1, NULL, '2022-06-16 07:00:00', NULL, 'Other', 'Calle del Gral. Pardiñas, 35, 28001 Madrid, Spain', 40.4273236, -3.6785559, 23, 'truck', NULL, NULL, 'Flexible', NULL, NULL, NULL, '920537', 'Pending', NULL, '2022-06-16 22:47:30', '2022-06-16 22:47:30', 14, '12', 'Das', NULL);
INSERT INTO `jobs` (`id`, `delivery_address`, `delivery_latitude`, `delivery_longitude`, `distance`, `description`, `expected_delivery_time`, `geohash`, `item_category`, `job_address`, `job_latitude`, `job_longitude`, `job_price`, `package_size`, `posted_at`, `receiver_id`, `priority`, `receiver_name`, `receiver_contact`, `receiver_instructions`, `security_code`, `status`, `document_id`, `created_at`, `updated_at`, `sender_id`, `source_address_appartment`, `delivery_address_appartment`, `deleted_at`) VALUES
(155, 'Calle del Gral. Pardiñas, 35, 28001 Madrid, Spain', 40.4273236, -3.6785559, 1, NULL, '2022-06-16 07:00:00', NULL, 'Other', 'Calle del Gral. Pardiñas, 35, 28001 Madrid, Spain', 40.4273236, -3.6785559, 23, 'truck', NULL, NULL, 'Flexible', NULL, NULL, NULL, '536314', 'Pending', NULL, '2022-06-16 22:47:30', '2022-06-16 22:47:30', 14, '12', 'Das', NULL),
(156, 'Golden Gate Bridge, San Francisco, CA, USA', 37.8199286, -122.4782551, NULL, NULL, NULL, NULL, 'Home', 'Sahara Desert', 23.4162027, 25.66283, 28, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '697538', 'Pending', NULL, '2022-08-23 15:55:26', '2022-08-23 15:55:26', 14, 'fsd', 'dfd', NULL),
(157, '432 Park Ave, New York, NY 10022, USA', 40.761648, -73.9717602, NULL, NULL, NULL, NULL, 'Home', '12 Haji Ln, Singapore 189205', 1.3005148, 103.8593223, 23, 'sedan', NULL, NULL, 'Immediate', NULL, NULL, NULL, '670855', 'Pending', NULL, '2022-08-23 18:14:51', '2022-08-23 18:14:51', 14, '14', '18', NULL),
(158, 'Plot E 11A, Block 10-A Block 10 A National Cement Employees CHS, Karachi, Karachi City, Sindh, Pakistan', 24.9090024, 67.1040511, NULL, '', '2022-08-23 20:51:29', NULL, '1', '', 24.936448, 67.0531584, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-08-23 20:51:29', '2022-08-23 20:51:29', NULL, NULL, NULL, NULL),
(159, 'Plot E 11A, Block 10-A Block 10 A National Cement Employees CHS, Karachi, Karachi City, Sindh, Pakistan', 24.9090081, 67.1040477, NULL, '', '2022-08-23 21:03:06', NULL, '31', '', 24.887913, 67.0583857, 56, 'Sedan', NULL, NULL, 'Immediate', NULL, NULL, '', NULL, NULL, NULL, '2022-08-23 21:03:06', '2022-08-23 21:03:06', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobstatus`
--

CREATE TABLE `jobstatus` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobstatus`
--

INSERT INTO `jobstatus` (`id`, `status`, `job_id`, `receiver_id`, `latitude`, `longitude`, `updated_at`, `created_at`) VALUES
(1, 'Accepted', 2, 9, NULL, NULL, '2022-05-17 16:52:03', '2022-05-17 16:52:03'),
(2, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-17 16:52:10', '2022-05-17 16:52:10'),
(3, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-17 16:52:34', '2022-05-17 16:52:34'),
(4, 'Accepted', 2, 9, NULL, NULL, '2022-05-18 07:52:00', '2022-05-18 07:52:00'),
(5, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-18 07:52:06', '2022-05-18 07:52:06'),
(6, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 07:59:30', '2022-05-18 07:59:30'),
(7, 'Accepted', 2, 9, NULL, NULL, '2022-05-18 08:27:01', '2022-05-18 08:27:01'),
(8, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-18 08:53:36', '2022-05-18 08:53:36'),
(9, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 09:16:09', '2022-05-18 09:16:09'),
(10, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 09:21:29', '2022-05-18 09:21:29'),
(11, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 09:22:49', '2022-05-18 09:22:49'),
(12, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 10:41:38', '2022-05-18 10:41:38'),
(13, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 10:48:07', '2022-05-18 10:48:07'),
(14, 'Accepted', 1, 9, NULL, NULL, '2022-05-18 10:49:31', '2022-05-18 10:49:31'),
(15, 'Received', 1, 9, '24.9180271', '67.0970916', '2022-05-18 10:49:37', '2022-05-18 10:49:37'),
(16, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 10:50:04', '2022-05-18 10:50:04'),
(17, 'Accepted', 1, 9, NULL, NULL, '2022-05-18 16:22:50', '2022-05-18 16:22:50'),
(18, 'Received', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:23:21', '2022-05-18 16:23:21'),
(19, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:23:47', '2022-05-18 16:23:47'),
(20, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:24:20', '2022-05-18 16:24:20'),
(21, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:34:26', '2022-05-18 16:34:26'),
(22, 'Accepted', 2, 9, NULL, NULL, '2022-05-18 16:38:06', '2022-05-18 16:38:06'),
(23, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-18 16:38:11', '2022-05-18 16:38:11'),
(24, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 16:38:24', '2022-05-18 16:38:24'),
(25, 'Accepted', 3, 9, NULL, NULL, '2022-05-18 16:39:39', '2022-05-18 16:39:39'),
(26, 'Received', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:39:53', '2022-05-18 16:39:53'),
(27, 'Delivered', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:39:58', '2022-05-18 16:39:58'),
(28, 'Accepted', 3, 9, NULL, NULL, '2022-05-18 16:40:42', '2022-05-18 16:40:42'),
(29, 'Received', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:40:45', '2022-05-18 16:40:45'),
(30, 'Delivered', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:40:57', '2022-05-18 16:40:57'),
(31, 'Accepted', 4, 10, NULL, NULL, '2022-05-18 17:45:53', '2022-05-18 17:45:53'),
(32, 'Accepted', 4, 10, NULL, NULL, '2022-05-18 17:48:47', '2022-05-18 17:48:47'),
(33, 'Received', 4, 10, '24.9180271', '67.0970916', '2022-05-18 17:48:58', '2022-05-18 17:48:58'),
(34, 'Delivered', 4, 10, '24.9180271', '67.0970916', '2022-05-18 17:49:24', '2022-05-18 17:49:24'),
(35, 'Accepted', 3, 10, NULL, NULL, '2022-05-18 17:51:41', '2022-05-18 17:51:41'),
(36, 'Received', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:51:44', '2022-05-18 17:51:44'),
(37, 'Delivered', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:51:56', '2022-05-18 17:51:56'),
(38, 'Accepted', 3, 10, NULL, NULL, '2022-05-18 17:52:06', '2022-05-18 17:52:06'),
(39, 'Received', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:52:11', '2022-05-18 17:52:11'),
(40, 'Delivered', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:52:17', '2022-05-18 17:52:17'),
(41, 'Delivered', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:52:55', '2022-05-18 17:52:55'),
(42, 'Accepted', 1, 10, NULL, NULL, '2022-05-18 17:54:47', '2022-05-18 17:54:47'),
(43, 'Received', 1, 10, '24.9180271', '67.0970916', '2022-05-18 17:55:46', '2022-05-18 17:55:46'),
(44, 'Delivered', 1, 10, '24.9180271', '67.0970916', '2022-05-18 17:55:57', '2022-05-18 17:55:57'),
(45, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 09:26:20', '2022-05-19 09:26:20'),
(46, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-19 09:27:04', '2022-05-19 09:27:04'),
(47, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-19 09:27:17', '2022-05-19 09:27:17'),
(48, 'Accepted', 2, 13, NULL, NULL, '2022-05-19 09:33:46', '2022-05-19 09:33:46'),
(49, 'Received', 2, 13, '24.9180271', '67.0970916', '2022-05-19 09:34:06', '2022-05-19 09:34:06'),
(50, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-19 09:35:15', '2022-05-19 09:35:15'),
(51, 'Pending', 7, NULL, NULL, NULL, '2022-05-19 12:21:58', '2022-05-19 12:21:58'),
(52, 'Pending', 8, NULL, NULL, NULL, '2022-05-19 12:22:13', '2022-05-19 12:22:13'),
(53, 'Pending', 11, NULL, NULL, NULL, '2022-05-19 15:13:32', '2022-05-19 15:13:32'),
(54, 'Accepted', 3, 13, NULL, NULL, '2022-05-19 15:44:31', '2022-05-19 15:44:31'),
(55, 'Received', 3, 13, '24.9180271', '67.0970916', '2022-05-19 15:44:38', '2022-05-19 15:44:38'),
(56, 'Delivered', 3, 13, '24.9180271', '67.0970916', '2022-05-19 15:45:05', '2022-05-19 15:45:05'),
(57, 'Accepted', 4, 13, NULL, NULL, '2022-05-19 15:45:19', '2022-05-19 15:45:19'),
(58, 'Received', 4, 13, '24.9180271', '67.0970916', '2022-05-19 15:45:23', '2022-05-19 15:45:23'),
(59, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:31', '2022-05-19 17:23:31'),
(60, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:37', '2022-05-19 17:23:37'),
(61, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:54', '2022-05-19 17:23:54'),
(62, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:55', '2022-05-19 17:23:55'),
(63, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:24:34', '2022-05-19 17:24:34'),
(64, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:25:29', '2022-05-19 17:25:29'),
(65, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:26:33', '2022-05-19 17:26:33'),
(66, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:26:40', '2022-05-19 17:26:40'),
(67, NULL, 1, 13, '22.0212', '70.7709278', '2022-05-19 17:58:22', '2022-05-19 17:58:22'),
(68, NULL, 1, 13, '22.0212', '70.7709278', '2022-05-19 17:59:55', '2022-05-19 17:59:55'),
(69, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 07:53:18', '2022-05-20 07:53:18'),
(70, 'Received', 2, 13, '24.9180271', '67.0970916', '2022-05-20 07:53:37', '2022-05-20 07:53:37'),
(71, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-20 07:54:03', '2022-05-20 07:54:03'),
(72, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-20 07:54:49', '2022-05-20 07:54:49'),
(73, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 08:16:28', '2022-05-20 08:16:28'),
(74, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 08:20:22', '2022-05-20 08:20:22'),
(75, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 08:23:37', '2022-05-20 08:23:37'),
(76, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-20 08:23:52', '2022-05-20 08:23:52'),
(77, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 08:24:13', '2022-05-20 08:24:13'),
(78, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 08:26:57', '2022-05-20 08:26:57'),
(79, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 10:20:56', '2022-05-20 10:20:56'),
(80, NULL, 1, 13, '22.0212', '70.7709278', '2022-05-20 10:28:33', '2022-05-20 10:28:33'),
(81, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 10:30:19', '2022-05-20 10:30:19'),
(82, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 10:34:03', '2022-05-20 10:34:03'),
(83, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 11:41:09', '2022-05-20 11:41:09'),
(84, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:15:00', '2022-05-20 15:15:00'),
(85, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:18:18', '2022-05-20 15:18:18'),
(86, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:18:30', '2022-05-20 15:18:30'),
(87, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 15:22:10', '2022-05-20 15:22:10'),
(88, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 15:24:46', '2022-05-20 15:24:46'),
(89, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:28:52', '2022-05-20 15:28:52'),
(90, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:31:08', '2022-05-20 15:31:08'),
(91, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 15:32:36', '2022-05-20 15:32:36'),
(92, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:33:07', '2022-05-20 15:33:07'),
(93, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:40:07', '2022-05-20 15:40:07'),
(94, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:43:00', '2022-05-20 15:43:00'),
(95, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:43:24', '2022-05-20 15:43:24'),
(96, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 15:44:26', '2022-05-20 15:44:26'),
(97, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 15:45:12', '2022-05-20 15:45:12'),
(98, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-20 15:45:30', '2022-05-20 15:45:30'),
(99, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 15:45:43', '2022-05-20 15:45:43'),
(100, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:50:35', '2022-05-20 15:50:35'),
(101, 'Received', 3, 13, '24.9180271', '67.0970916', '2022-05-20 15:50:39', '2022-05-20 15:50:39'),
(102, 'Delivered', 3, 13, '24.9180271', '67.0970916', '2022-05-20 15:50:53', '2022-05-20 15:50:53'),
(103, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 16:50:29', '2022-05-20 16:50:29'),
(104, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-20 16:51:59', '2022-05-20 16:51:59'),
(105, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 16:52:39', '2022-05-20 16:52:39'),
(106, 'Pending', 13, NULL, NULL, NULL, '2022-05-20 17:15:04', '2022-05-20 17:15:04'),
(107, 'Pending', 14, NULL, NULL, NULL, '2022-05-20 17:15:16', '2022-05-20 17:15:16'),
(108, 'Pending', 15, NULL, NULL, NULL, '2022-05-20 17:19:17', '2022-05-20 17:19:17'),
(109, 'Pending', 16, NULL, NULL, NULL, '2022-05-20 17:19:31', '2022-05-20 17:19:31'),
(110, 'Pending', 17, NULL, NULL, NULL, '2022-05-20 17:23:29', '2022-05-20 17:23:29'),
(111, 'Pending', 18, NULL, NULL, NULL, '2022-05-20 17:23:31', '2022-05-20 17:23:31'),
(112, 'Pending', 19, NULL, NULL, NULL, '2022-05-20 17:23:32', '2022-05-20 17:23:32'),
(113, 'Pending', 20, NULL, NULL, NULL, '2022-05-20 17:23:33', '2022-05-20 17:23:33'),
(114, 'Pending', 21, NULL, NULL, NULL, '2022-05-20 17:23:33', '2022-05-20 17:23:33'),
(115, 'Pending', 22, NULL, NULL, NULL, '2022-05-20 17:23:34', '2022-05-20 17:23:34'),
(116, 'Pending', 23, NULL, NULL, NULL, '2022-05-20 17:23:34', '2022-05-20 17:23:34'),
(117, 'Pending', 24, NULL, NULL, NULL, '2022-05-20 17:23:34', '2022-05-20 17:23:34'),
(118, 'Pending', 25, NULL, NULL, NULL, '2022-05-20 17:23:35', '2022-05-20 17:23:35'),
(119, 'Pending', 26, NULL, NULL, NULL, '2022-05-20 17:23:35', '2022-05-20 17:23:35'),
(120, 'Pending', 27, NULL, NULL, NULL, '2022-05-20 17:25:35', '2022-05-20 17:25:35'),
(121, 'Pending', 28, NULL, NULL, NULL, '2022-05-20 17:52:13', '2022-05-20 17:52:13'),
(122, 'Pending', 29, NULL, NULL, NULL, '2022-05-20 17:52:58', '2022-05-20 17:52:58'),
(123, 'Pending', 30, NULL, NULL, NULL, '2022-05-20 17:53:06', '2022-05-20 17:53:06'),
(124, 'Pending', 31, NULL, NULL, NULL, '2022-05-20 17:53:10', '2022-05-20 17:53:10'),
(125, 'Pending', 32, NULL, NULL, NULL, '2022-05-20 17:53:11', '2022-05-20 17:53:11'),
(126, 'Pending', 33, NULL, NULL, NULL, '2022-05-20 17:53:11', '2022-05-20 17:53:11'),
(127, 'Pending', 34, NULL, NULL, NULL, '2022-05-20 17:53:12', '2022-05-20 17:53:12'),
(128, 'Pending', 35, NULL, NULL, NULL, '2022-05-20 17:53:12', '2022-05-20 17:53:12'),
(129, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 18:28:31', '2022-05-20 18:28:31'),
(130, 'Received', 2, 13, '24.9180271', '67.0970916', '2022-05-20 18:28:57', '2022-05-20 18:28:57'),
(131, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-20 18:29:27', '2022-05-20 18:29:27'),
(132, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 09:12:55', '2022-05-23 09:12:55'),
(133, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 09:13:24', '2022-05-23 09:13:24'),
(134, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 09:20:04', '2022-05-23 09:20:04'),
(135, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 09:20:09', '2022-05-23 09:20:09'),
(136, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 09:25:18', '2022-05-23 09:25:18'),
(137, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 09:30:43', '2022-05-23 09:30:43'),
(138, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 09:31:23', '2022-05-23 09:31:23'),
(139, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 10:33:47', '2022-05-23 10:33:47'),
(140, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 10:36:29', '2022-05-23 10:36:29'),
(141, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 10:54:57', '2022-05-23 10:54:57'),
(142, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 10:57:04', '2022-05-23 10:57:04'),
(143, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 10:58:17', '2022-05-23 10:58:17'),
(144, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 10:59:44', '2022-05-23 10:59:44'),
(145, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 11:14:15', '2022-05-23 11:14:15'),
(146, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 11:16:48', '2022-05-23 11:16:48'),
(147, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 11:19:27', '2022-05-23 11:19:27'),
(148, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 11:35:23', '2022-05-23 11:35:23'),
(149, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 11:37:00', '2022-05-23 11:37:00'),
(150, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 11:39:37', '2022-05-23 11:39:37'),
(151, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 11:46:52', '2022-05-23 11:46:52'),
(152, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 11:47:45', '2022-05-23 11:47:45'),
(153, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 11:48:05', '2022-05-23 11:48:05'),
(154, 'Delivered', 2, 12, '24.9180271', '67.0970916', '2022-05-23 11:48:46', '2022-05-23 11:48:46'),
(155, 'Delivered', 2, 12, '24.9180271', '67.0970916', '2022-05-23 11:50:48', '2022-05-23 11:50:48'),
(156, 'Delivered', 2, 12, '24.9180271', '67.0970916', '2022-05-23 12:02:30', '2022-05-23 12:02:30'),
(157, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 12:15:59', '2022-05-23 12:15:59'),
(158, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-23 12:16:31', '2022-05-23 12:16:31'),
(159, 'Delivered', 3, 12, '24.9180271', '67.0970916', '2022-05-23 12:16:54', '2022-05-23 12:16:54'),
(160, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 13:35:19', '2022-05-23 13:35:19'),
(161, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 13:35:22', '2022-05-23 13:35:22'),
(162, 'Delivered', 5, 12, '24.9180271', '67.0970916', '2022-05-23 13:36:06', '2022-05-23 13:36:06'),
(163, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 13:38:09', '2022-05-23 13:38:09'),
(164, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 13:38:13', '2022-05-23 13:38:13'),
(165, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 13:40:14', '2022-05-23 13:40:14'),
(166, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-23 13:40:20', '2022-05-23 13:40:20'),
(167, 'Delivered', 3, 12, '24.9180271', '67.0970916', '2022-05-23 13:40:35', '2022-05-23 13:40:35'),
(168, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 13:41:50', '2022-05-23 13:41:50'),
(169, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 13:41:53', '2022-05-23 13:41:53'),
(170, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 14:00:40', '2022-05-23 14:00:40'),
(171, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 14:00:45', '2022-05-23 14:00:45'),
(172, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 15:27:34', '2022-05-23 15:27:34'),
(173, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-23 15:27:39', '2022-05-23 15:27:39'),
(174, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 15:34:47', '2022-05-23 15:34:47'),
(175, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 15:34:52', '2022-05-23 15:34:52'),
(176, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 15:36:13', '2022-05-23 15:36:13'),
(177, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 15:36:21', '2022-05-23 15:36:21'),
(178, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 15:38:56', '2022-05-23 15:38:56'),
(179, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-23 15:38:59', '2022-05-23 15:38:59'),
(180, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 15:40:20', '2022-05-23 15:40:20'),
(181, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 15:40:26', '2022-05-23 15:40:26'),
(182, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 15:42:01', '2022-05-23 15:42:01'),
(183, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 15:45:51', '2022-05-23 15:45:51'),
(184, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-23 15:45:54', '2022-05-23 15:45:54'),
(185, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 15:47:57', '2022-05-23 15:47:57'),
(186, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 15:51:54', '2022-05-23 15:51:54'),
(187, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 15:51:58', '2022-05-23 15:51:58'),
(188, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 15:53:27', '2022-05-23 15:53:27'),
(189, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 15:53:32', '2022-05-23 15:53:32'),
(190, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 15:55:32', '2022-05-23 15:55:32'),
(191, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 15:55:37', '2022-05-23 15:55:37'),
(192, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 15:57:41', '2022-05-23 15:57:41'),
(193, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 15:57:51', '2022-05-23 15:57:51'),
(194, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 16:34:54', '2022-05-23 16:34:54'),
(195, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 16:37:00', '2022-05-23 16:37:00'),
(196, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 16:37:09', '2022-05-23 16:37:09'),
(197, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 16:46:59', '2022-05-23 16:46:59'),
(198, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 16:53:48', '2022-05-23 16:53:48'),
(199, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 16:53:58', '2022-05-23 16:53:58'),
(200, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 16:56:17', '2022-05-23 16:56:17'),
(201, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 16:56:23', '2022-05-23 16:56:23'),
(202, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 16:57:24', '2022-05-23 16:57:24'),
(203, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 16:58:00', '2022-05-23 16:58:00'),
(204, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 16:58:09', '2022-05-23 16:58:09'),
(205, 'Accepted', 3, 12, NULL, NULL, '2022-05-23 16:59:30', '2022-05-23 16:59:30'),
(206, 'Accepted', 5, 12, NULL, NULL, '2022-05-23 17:00:29', '2022-05-23 17:00:29'),
(207, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-23 17:00:32', '2022-05-23 17:00:32'),
(208, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 17:01:42', '2022-05-23 17:01:42'),
(209, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 17:01:50', '2022-05-23 17:01:50'),
(210, 'Accepted', 2, 12, NULL, NULL, '2022-05-23 17:06:42', '2022-05-23 17:06:42'),
(211, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-23 17:06:52', '2022-05-23 17:06:52'),
(212, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 09:56:53', '2022-05-24 09:56:53'),
(213, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-24 09:57:13', '2022-05-24 09:57:13'),
(214, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 09:59:59', '2022-05-24 09:59:59'),
(215, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-24 10:00:03', '2022-05-24 10:00:03'),
(216, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:04:03', '2022-05-24 10:04:03'),
(217, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-24 10:04:16', '2022-05-24 10:04:16'),
(218, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:04:39', '2022-05-24 10:04:39'),
(219, 'Accepted', 2, 12, NULL, NULL, '2022-05-24 10:05:31', '2022-05-24 10:05:31'),
(220, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-24 10:05:49', '2022-05-24 10:05:49'),
(221, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 10:08:34', '2022-05-24 10:08:34'),
(222, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-24 10:08:38', '2022-05-24 10:08:38'),
(223, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:21:46', '2022-05-24 10:21:46'),
(224, 'Accepted', 2, 12, NULL, NULL, '2022-05-24 10:22:45', '2022-05-24 10:22:45'),
(225, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-24 10:22:48', '2022-05-24 10:22:48'),
(226, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 10:26:27', '2022-05-24 10:26:27'),
(227, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-24 10:26:35', '2022-05-24 10:26:35'),
(228, 'Accepted', 2, 12, NULL, NULL, '2022-05-24 10:40:14', '2022-05-24 10:40:14'),
(229, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-24 10:40:27', '2022-05-24 10:40:27'),
(230, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:42:35', '2022-05-24 10:42:35'),
(231, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-24 10:42:39', '2022-05-24 10:42:39'),
(232, 'Accepted', 2, 12, NULL, NULL, '2022-05-24 10:44:37', '2022-05-24 10:44:37'),
(233, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:44:56', '2022-05-24 10:44:56'),
(234, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-24 10:45:01', '2022-05-24 10:45:01'),
(235, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:45:24', '2022-05-24 10:45:24'),
(236, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-24 10:45:34', '2022-05-24 10:45:34'),
(237, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 10:46:41', '2022-05-24 10:46:41'),
(238, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-24 10:46:44', '2022-05-24 10:46:44'),
(239, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 10:52:47', '2022-05-24 10:52:47'),
(240, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 10:53:49', '2022-05-24 10:53:49'),
(241, 'Accepted', 2, 12, NULL, NULL, '2022-05-24 11:05:10', '2022-05-24 11:05:10'),
(242, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-24 11:05:20', '2022-05-24 11:05:20'),
(243, 'Accepted', 2, 12, NULL, NULL, '2022-05-24 11:10:07', '2022-05-24 11:10:07'),
(244, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-24 11:10:11', '2022-05-24 11:10:11'),
(245, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 11:12:35', '2022-05-24 11:12:35'),
(246, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-24 11:12:39', '2022-05-24 11:12:39'),
(247, 'Accepted', 3, 12, NULL, NULL, '2022-05-24 11:15:51', '2022-05-24 11:15:51'),
(248, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-05-24 11:16:00', '2022-05-24 11:16:00'),
(249, 'Accepted', 5, 12, NULL, NULL, '2022-05-24 11:16:19', '2022-05-24 11:16:19'),
(250, 'Accepted', 2, 12, NULL, NULL, '2022-05-25 15:57:16', '2022-05-25 15:57:16'),
(251, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-25 15:57:25', '2022-05-25 15:57:25'),
(252, 'Accepted', 2, 12, NULL, NULL, '2022-05-26 13:51:07', '2022-05-26 13:51:07'),
(253, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-05-26 13:51:11', '2022-05-26 13:51:11'),
(254, 'Pending', 13, NULL, NULL, NULL, '2022-05-26 17:34:11', '2022-05-26 17:34:11'),
(255, 'Pending', 14, NULL, NULL, NULL, '2022-05-27 08:24:33', '2022-05-27 08:24:33'),
(256, 'Pending', 15, NULL, NULL, NULL, '2022-05-27 10:49:05', '2022-05-27 10:49:05'),
(257, 'Pending', 16, NULL, NULL, NULL, '2022-05-27 10:50:35', '2022-05-27 10:50:35'),
(258, 'Pending', 17, NULL, NULL, NULL, '2022-05-27 10:53:30', '2022-05-27 10:53:30'),
(259, 'Pending', 18, NULL, NULL, NULL, '2022-05-27 10:58:58', '2022-05-27 10:58:58'),
(260, 'Pending', 19, NULL, NULL, NULL, '2022-05-27 11:01:38', '2022-05-27 11:01:38'),
(261, 'Pending', 20, NULL, NULL, NULL, '2022-05-27 11:01:48', '2022-05-27 11:01:48'),
(262, 'Pending', 21, NULL, NULL, NULL, '2022-05-27 11:01:57', '2022-05-27 11:01:57'),
(263, 'Pending', 22, NULL, NULL, NULL, '2022-05-27 11:02:01', '2022-05-27 11:02:01'),
(264, 'Pending', 23, NULL, NULL, NULL, '2022-05-27 11:03:36', '2022-05-27 11:03:36'),
(265, 'Pending', 24, NULL, NULL, NULL, '2022-05-27 11:14:31', '2022-05-27 11:14:31'),
(266, 'Pending', 25, NULL, NULL, NULL, '2022-05-27 11:26:36', '2022-05-27 11:26:36'),
(267, 'Pending', 26, NULL, NULL, NULL, '2022-05-27 11:27:04', '2022-05-27 11:27:04'),
(268, 'Pending', 27, NULL, NULL, NULL, '2022-05-27 11:31:31', '2022-05-27 11:31:31'),
(269, 'Pending', 28, NULL, NULL, NULL, '2022-05-27 11:38:34', '2022-05-27 11:38:34'),
(270, 'Pending', 29, NULL, NULL, NULL, '2022-05-27 11:42:36', '2022-05-27 11:42:36'),
(271, 'Pending', 30, NULL, NULL, NULL, '2022-05-27 11:43:01', '2022-05-27 11:43:01'),
(272, 'Pending', 31, NULL, NULL, NULL, '2022-05-27 11:43:18', '2022-05-27 11:43:18'),
(273, 'Pending', 32, NULL, NULL, NULL, '2022-05-27 11:43:37', '2022-05-27 11:43:37'),
(274, 'Pending', 33, NULL, NULL, NULL, '2022-05-27 11:44:02', '2022-05-27 11:44:02'),
(275, 'Pending', 34, NULL, NULL, NULL, '2022-05-27 11:46:20', '2022-05-27 11:46:20'),
(276, 'Pending', 35, NULL, NULL, NULL, '2022-05-27 11:46:57', '2022-05-27 11:46:57'),
(277, 'Pending', 36, NULL, NULL, NULL, '2022-05-27 11:47:48', '2022-05-27 11:47:48'),
(278, 'Pending', 37, NULL, NULL, NULL, '2022-05-27 11:47:55', '2022-05-27 11:47:55'),
(279, 'Pending', 38, NULL, NULL, NULL, '2022-05-27 11:48:01', '2022-05-27 11:48:01'),
(280, 'Pending', 39, NULL, NULL, NULL, '2022-05-27 11:48:59', '2022-05-27 11:48:59'),
(281, 'Pending', 40, NULL, NULL, NULL, '2022-05-27 11:49:37', '2022-05-27 11:49:37'),
(282, 'Pending', 41, NULL, NULL, NULL, '2022-05-27 11:51:05', '2022-05-27 11:51:05'),
(283, 'Pending', 42, NULL, NULL, NULL, '2022-05-27 11:57:55', '2022-05-27 11:57:55'),
(284, 'Pending', 43, NULL, NULL, NULL, '2022-05-27 11:59:03', '2022-05-27 11:59:03'),
(285, 'Pending', 44, NULL, NULL, NULL, '2022-05-27 11:59:27', '2022-05-27 11:59:27'),
(286, 'Pending', 45, NULL, NULL, NULL, '2022-05-27 11:59:53', '2022-05-27 11:59:53'),
(287, 'Pending', 46, NULL, NULL, NULL, '2022-05-27 12:00:41', '2022-05-27 12:00:41'),
(288, 'Pending', 47, NULL, NULL, NULL, '2022-05-27 12:00:45', '2022-05-27 12:00:45'),
(289, 'Pending', 48, NULL, NULL, NULL, '2022-05-27 12:02:57', '2022-05-27 12:02:57'),
(290, 'Pending', 49, NULL, NULL, NULL, '2022-05-27 12:03:10', '2022-05-27 12:03:10'),
(291, 'Pending', 50, NULL, NULL, NULL, '2022-05-27 12:03:17', '2022-05-27 12:03:17'),
(292, 'Pending', 51, NULL, NULL, NULL, '2022-05-27 12:04:52', '2022-05-27 12:04:52'),
(293, 'Pending', 52, NULL, NULL, NULL, '2022-05-27 12:07:31', '2022-05-27 12:07:31'),
(294, 'Pending', 53, NULL, NULL, NULL, '2022-05-27 12:07:34', '2022-05-27 12:07:34'),
(295, 'Pending', 54, NULL, NULL, NULL, '2022-05-27 12:09:03', '2022-05-27 12:09:03'),
(296, 'Pending', 55, NULL, NULL, NULL, '2022-05-27 12:10:04', '2022-05-27 12:10:04'),
(297, 'Pending', 56, NULL, NULL, NULL, '2022-05-27 12:11:57', '2022-05-27 12:11:57'),
(298, 'Pending', 57, NULL, NULL, NULL, '2022-05-27 12:12:29', '2022-05-27 12:12:29'),
(299, 'Pending', 58, NULL, NULL, NULL, '2022-05-27 12:12:33', '2022-05-27 12:12:33'),
(300, 'Pending', 59, NULL, NULL, NULL, '2022-05-27 12:13:41', '2022-05-27 12:13:41'),
(301, 'Pending', 60, NULL, NULL, NULL, '2022-05-27 12:14:08', '2022-05-27 12:14:08'),
(302, 'Pending', 61, NULL, NULL, NULL, '2022-05-27 12:14:09', '2022-05-27 12:14:09'),
(303, 'Pending', 62, NULL, NULL, NULL, '2022-05-27 12:14:09', '2022-05-27 12:14:09'),
(304, 'Pending', 63, NULL, NULL, NULL, '2022-05-27 12:14:10', '2022-05-27 12:14:10'),
(305, 'Pending', 64, NULL, NULL, NULL, '2022-05-27 12:16:02', '2022-05-27 12:16:02'),
(306, 'Pending', 65, NULL, NULL, NULL, '2022-05-27 12:16:11', '2022-05-27 12:16:11'),
(307, 'Pending', 66, NULL, NULL, NULL, '2022-05-27 12:16:18', '2022-05-27 12:16:18'),
(308, 'Pending', 67, NULL, NULL, NULL, '2022-05-27 12:36:20', '2022-05-27 12:36:20'),
(309, 'Pending', 68, NULL, NULL, NULL, '2022-05-27 12:41:40', '2022-05-27 12:41:40'),
(310, 'Pending', 69, NULL, NULL, NULL, '2022-05-27 13:30:18', '2022-05-27 13:30:18'),
(311, 'Pending', 70, NULL, NULL, NULL, '2022-05-27 13:32:18', '2022-05-27 13:32:18'),
(312, 'Pending', 71, NULL, NULL, NULL, '2022-05-27 13:33:05', '2022-05-27 13:33:05'),
(313, 'Pending', 72, NULL, NULL, NULL, '2022-05-27 13:35:35', '2022-05-27 13:35:35'),
(314, 'Pending', 73, NULL, NULL, NULL, '2022-05-27 13:37:53', '2022-05-27 13:37:53'),
(315, 'Pending', 74, NULL, NULL, NULL, '2022-05-27 13:42:40', '2022-05-27 13:42:40'),
(316, 'Pending', 75, NULL, NULL, NULL, '2022-05-27 13:43:43', '2022-05-27 13:43:43'),
(317, 'Pending', 76, NULL, NULL, NULL, '2022-05-27 13:47:20', '2022-05-27 13:47:20'),
(318, 'Pending', 77, NULL, NULL, NULL, '2022-05-27 14:45:53', '2022-05-27 14:45:53'),
(319, 'Pending', 78, NULL, NULL, NULL, '2022-05-27 14:55:52', '2022-05-27 14:55:52'),
(320, 'Pending', 79, NULL, NULL, NULL, '2022-05-27 15:03:55', '2022-05-27 15:03:55'),
(321, 'Pending', 80, NULL, NULL, NULL, '2022-05-28 06:36:06', '2022-05-28 06:36:06'),
(322, 'Pending', 81, NULL, NULL, NULL, '2022-05-28 06:42:22', '2022-05-28 06:42:22'),
(323, 'Pending', 82, NULL, NULL, NULL, '2022-05-28 06:43:49', '2022-05-28 06:43:49'),
(324, 'Pending', 83, NULL, NULL, NULL, '2022-05-28 06:44:36', '2022-05-28 06:44:36'),
(325, 'Pending', 84, NULL, NULL, NULL, '2022-05-28 06:46:25', '2022-05-28 06:46:25'),
(326, 'Pending', 85, NULL, NULL, NULL, '2022-05-28 06:50:08', '2022-05-28 06:50:08'),
(327, 'Pending', 86, NULL, NULL, NULL, '2022-05-28 06:54:52', '2022-05-28 06:54:52'),
(328, 'Pending', 87, NULL, NULL, NULL, '2022-05-28 06:56:23', '2022-05-28 06:56:23'),
(329, 'Pending', 88, NULL, NULL, NULL, '2022-05-28 06:57:25', '2022-05-28 06:57:25'),
(330, 'Pending', 89, NULL, NULL, NULL, '2022-05-28 06:59:38', '2022-05-28 06:59:38'),
(331, 'Pending', 90, NULL, NULL, NULL, '2022-05-28 07:00:15', '2022-05-28 07:00:15'),
(332, 'Pending', 91, NULL, NULL, NULL, '2022-05-28 07:02:11', '2022-05-28 07:02:11'),
(333, 'Pending', 92, NULL, NULL, NULL, '2022-05-28 07:04:09', '2022-05-28 07:04:09'),
(334, 'Pending', 93, NULL, NULL, NULL, '2022-05-28 07:10:50', '2022-05-28 07:10:50'),
(335, 'Pending', 94, NULL, NULL, NULL, '2022-05-28 07:13:30', '2022-05-28 07:13:30'),
(336, 'Pending', 95, NULL, NULL, NULL, '2022-05-28 07:17:20', '2022-05-28 07:17:20'),
(337, 'Pending', 96, NULL, NULL, NULL, '2022-05-28 07:19:57', '2022-05-28 07:19:57'),
(338, 'Pending', 97, NULL, NULL, NULL, '2022-05-28 07:32:49', '2022-05-28 07:32:49'),
(339, 'Pending', 98, NULL, NULL, NULL, '2022-05-28 07:38:07', '2022-05-28 07:38:07'),
(340, 'Pending', 99, NULL, NULL, NULL, '2022-05-28 07:41:50', '2022-05-28 07:41:50'),
(341, 'Pending', 100, NULL, NULL, NULL, '2022-05-28 07:45:52', '2022-05-28 07:45:52'),
(342, 'Pending', 101, NULL, NULL, NULL, '2022-05-28 07:47:19', '2022-05-28 07:47:19'),
(343, 'Pending', 102, NULL, NULL, NULL, '2022-05-28 07:57:34', '2022-05-28 07:57:34'),
(344, 'Pending', 103, NULL, NULL, NULL, '2022-05-28 07:58:19', '2022-05-28 07:58:19'),
(345, 'Pending', 104, NULL, NULL, NULL, '2022-05-28 07:59:43', '2022-05-28 07:59:43'),
(346, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 08:02:11', '2022-05-28 08:02:11'),
(347, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-28 08:04:12', '2022-05-28 08:04:12'),
(348, 'Pending', 105, NULL, NULL, NULL, '2022-05-28 08:09:31', '2022-05-28 08:09:31'),
(349, 'Pending', 106, NULL, NULL, NULL, '2022-05-28 08:15:09', '2022-05-28 08:15:09'),
(350, 'Pending', 107, NULL, NULL, NULL, '2022-05-28 08:15:56', '2022-05-28 08:15:56'),
(351, 'Pending', 108, NULL, NULL, NULL, '2022-05-28 08:17:42', '2022-05-28 08:17:42'),
(352, 'Pending', 109, NULL, NULL, NULL, '2022-05-28 08:19:42', '2022-05-28 08:19:42'),
(353, 'Pending', 110, NULL, NULL, NULL, '2022-05-28 09:07:05', '2022-05-28 09:07:05'),
(354, 'Pending', 111, NULL, NULL, NULL, '2022-05-28 09:58:54', '2022-05-28 09:58:54'),
(355, 'Accepted', 3, 12, NULL, NULL, '2022-05-28 09:59:24', '2022-05-28 09:59:24'),
(356, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:05:45', '2022-05-28 10:05:45'),
(357, 'Accepted', 3, 12, NULL, NULL, '2022-05-28 10:07:55', '2022-05-28 10:07:55'),
(358, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:10:30', '2022-05-28 10:10:30'),
(359, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:11:54', '2022-05-28 10:11:54'),
(360, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:13:45', '2022-05-28 10:13:45'),
(361, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:15:06', '2022-05-28 10:15:06'),
(362, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:18:55', '2022-05-28 10:18:55'),
(363, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:20:49', '2022-05-28 10:20:49'),
(364, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:25:56', '2022-05-28 10:25:56'),
(365, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:28:18', '2022-05-28 10:28:18'),
(366, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:39:04', '2022-05-28 10:39:04'),
(367, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:44:35', '2022-05-28 10:44:35'),
(368, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:50:18', '2022-05-28 10:50:18'),
(369, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:51:46', '2022-05-28 10:51:46'),
(370, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:53:38', '2022-05-28 10:53:38'),
(371, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:53:49', '2022-05-28 10:53:49'),
(372, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:53:53', '2022-05-28 10:53:53'),
(373, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:55:17', '2022-05-28 10:55:17'),
(374, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 10:57:58', '2022-05-28 10:57:58'),
(375, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:02:40', '2022-05-28 11:02:40'),
(376, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:03:28', '2022-05-28 11:03:28'),
(377, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:04:55', '2022-05-28 11:04:55'),
(378, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:07:14', '2022-05-28 11:07:14'),
(379, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:07:14', '2022-05-28 11:07:14'),
(380, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:18:02', '2022-05-28 11:18:02'),
(381, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:21:37', '2022-05-28 11:21:37'),
(382, 'Accepted', 5, 12, NULL, NULL, '2022-05-28 11:25:48', '2022-05-28 11:25:48'),
(383, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 09:05:24', '2022-05-30 09:05:24'),
(384, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 09:28:38', '2022-05-30 09:28:38'),
(385, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-30 09:28:43', '2022-05-30 09:28:43'),
(386, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 09:40:36', '2022-05-30 09:40:36'),
(387, 'Received', 5, 12, '24.9180271', '67.0970916', '2022-05-30 09:40:39', '2022-05-30 09:40:39'),
(388, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 11:18:29', '2022-05-30 11:18:29'),
(389, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 11:21:49', '2022-05-30 11:21:49'),
(390, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 11:24:51', '2022-05-30 11:24:51'),
(391, 'Accepted', 5, 12, NULL, NULL, '2022-05-30 11:30:33', '2022-05-30 11:30:33'),
(392, 'Pending', 112, NULL, NULL, NULL, '2022-05-30 11:42:36', '2022-05-30 11:42:36'),
(393, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:08:44', '2022-05-30 12:08:44'),
(394, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:10:34', '2022-05-30 12:10:34'),
(395, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:14:56', '2022-05-30 12:14:56'),
(396, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:17:21', '2022-05-30 12:17:21'),
(397, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:19:19', '2022-05-30 12:19:19'),
(398, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:21:01', '2022-05-30 12:21:01'),
(399, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:26:08', '2022-05-30 12:26:08'),
(400, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:28:14', '2022-05-30 12:28:14'),
(401, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:32:20', '2022-05-30 12:32:20'),
(402, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:34:37', '2022-05-30 12:34:37'),
(403, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 12:41:27', '2022-05-30 12:41:27'),
(404, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 13:19:48', '2022-05-30 13:19:48'),
(405, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 13:29:13', '2022-05-30 13:29:13'),
(406, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 13:31:48', '2022-05-30 13:31:48'),
(407, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 13:32:13', '2022-05-30 13:32:13'),
(408, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 14:17:43', '2022-05-30 14:17:43'),
(409, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 14:18:42', '2022-05-30 14:18:42'),
(410, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 15:40:39', '2022-05-30 15:40:39'),
(411, 'Accepted', 3, 12, NULL, NULL, '2022-05-30 15:42:28', '2022-05-30 15:42:28'),
(412, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 08:14:06', '2022-06-01 08:14:06'),
(413, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 08:16:52', '2022-06-01 08:16:52'),
(414, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 08:21:23', '2022-06-01 08:21:23'),
(415, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 08:24:17', '2022-06-01 08:24:17'),
(416, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 08:25:47', '2022-06-01 08:25:47'),
(417, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 08:26:08', '2022-06-01 08:26:08'),
(418, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 08:26:57', '2022-06-01 08:26:57'),
(419, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 08:49:22', '2022-06-01 08:49:22'),
(420, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 08:50:58', '2022-06-01 08:50:58'),
(421, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 08:57:48', '2022-06-01 08:57:48'),
(422, 'Received', 2, 12, '24.9180271', '67.0970916', '2022-06-01 08:57:59', '2022-06-01 08:57:59'),
(423, 'Pending', 113, NULL, NULL, NULL, '2022-06-01 08:58:39', '2022-06-01 08:58:39'),
(424, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 11:36:21', '2022-06-01 11:36:21'),
(425, 'Received', 2, 12, '24.9090948', '67.1040864', '2022-06-01 11:37:15', '2022-06-01 11:37:15'),
(426, 'Pending', 115, NULL, NULL, NULL, '2022-06-01 11:39:41', '2022-06-01 11:39:41'),
(427, 'Pending', 116, NULL, NULL, NULL, '2022-06-01 11:42:39', '2022-06-01 11:42:39'),
(428, 'Pending', 117, NULL, NULL, NULL, '2022-06-01 11:44:35', '2022-06-01 11:44:35'),
(429, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 12:13:06', '2022-06-01 12:13:06'),
(430, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 12:30:18', '2022-06-01 12:30:18'),
(431, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 12:35:44', '2022-06-01 12:35:44'),
(432, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 12:37:01', '2022-06-01 12:37:01'),
(433, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 12:38:30', '2022-06-01 12:38:30'),
(434, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-06-01 13:19:59', '2022-06-01 13:19:59'),
(435, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-06-01 13:19:59', '2022-06-01 13:19:59'),
(436, 'Pending', 119, NULL, NULL, NULL, '2022-06-01 14:54:38', '2022-06-01 14:54:38'),
(437, 'Accepted', 2, 12, NULL, NULL, '2022-06-01 15:48:20', '2022-06-01 15:48:20'),
(438, 'Received', 2, 12, '24.909087', '67.1040987', '2022-06-01 15:48:35', '2022-06-01 15:48:35'),
(439, 'Accepted', 3, 12, NULL, NULL, '2022-06-01 15:49:35', '2022-06-01 15:49:35'),
(440, 'Received', 3, 12, '24.9090184', '67.1040614', '2022-06-01 15:49:49', '2022-06-01 15:49:49'),
(441, 'Received', 3, 12, '24.9089619', '67.1040421', '2022-06-01 15:49:55', '2022-06-01 15:49:55'),
(442, 'Pending', 120, NULL, NULL, NULL, '2022-06-01 15:51:26', '2022-06-01 15:51:26'),
(443, 'Accepted', 3, 12, NULL, NULL, '2022-06-02 06:38:19', '2022-06-02 06:38:19'),
(444, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-06-02 06:38:26', '2022-06-02 06:38:26'),
(445, 'Pending', 122, NULL, NULL, NULL, '2022-06-02 12:01:44', '2022-06-02 12:01:44'),
(446, 'Pending', 123, NULL, NULL, NULL, '2022-06-02 13:50:41', '2022-06-02 13:50:41'),
(447, 'Accepted', 2, 12, NULL, NULL, '2022-06-02 13:51:28', '2022-06-02 13:51:28'),
(448, 'Received', 2, 12, '24.9089643', '67.1040353', '2022-06-02 13:51:41', '2022-06-02 13:51:41'),
(449, 'Pending', 124, NULL, NULL, NULL, '2022-06-02 16:22:30', '2022-06-02 16:22:30'),
(450, 'Pending', 125, NULL, NULL, NULL, '2022-06-02 17:16:51', '2022-06-02 17:16:51'),
(451, 'Accepted', 4, 12, NULL, NULL, '2022-06-02 19:43:07', '2022-06-02 19:43:07'),
(452, 'Pending', 127, NULL, NULL, NULL, '2022-06-03 11:28:19', '2022-06-03 11:28:19'),
(453, 'Pending', 128, NULL, NULL, NULL, '2022-06-03 11:32:33', '2022-06-03 11:32:33'),
(454, 'Pending', 129, NULL, NULL, NULL, '2022-06-03 11:41:41', '2022-06-03 11:41:41'),
(455, 'Pending', 130, NULL, NULL, NULL, '2022-06-03 13:54:54', '2022-06-03 13:54:54'),
(456, 'Pending', 131, NULL, NULL, NULL, '2022-06-03 14:12:18', '2022-06-03 14:12:18'),
(457, 'Pending', 132, NULL, NULL, NULL, '2022-06-03 14:17:33', '2022-06-03 14:17:33'),
(458, 'Pending', 133, NULL, NULL, NULL, '2022-06-03 14:21:34', '2022-06-03 14:21:34'),
(459, 'Pending', 134, NULL, NULL, NULL, '2022-06-03 15:04:02', '2022-06-03 15:04:02'),
(460, 'Pending', 135, NULL, NULL, NULL, '2022-06-03 16:52:00', '2022-06-03 16:52:00'),
(461, 'Pending', 136, NULL, NULL, NULL, '2022-06-03 16:54:28', '2022-06-03 16:54:28'),
(462, 'Pending', 137, NULL, NULL, NULL, '2022-06-03 16:55:55', '2022-06-03 16:55:55'),
(463, 'Pending', 138, NULL, NULL, NULL, '2022-06-03 17:04:24', '2022-06-03 17:04:24'),
(464, 'Pending', 139, NULL, NULL, NULL, '2022-06-03 17:18:22', '2022-06-03 17:18:22'),
(465, 'Pending', 140, NULL, NULL, NULL, '2022-06-03 17:32:32', '2022-06-03 17:32:32'),
(466, 'Accepted', 2, 15, NULL, NULL, '2022-06-04 09:20:46', '2022-06-04 09:20:46'),
(467, 'Received', 2, 15, '24.9090848', '67.1040888', '2022-06-04 09:21:12', '2022-06-04 09:21:12'),
(468, 'Accepted', 3, 12, NULL, NULL, '2022-06-04 16:12:57', '2022-06-04 16:12:57'),
(469, 'Accepted', 4, 12, NULL, NULL, '2022-06-04 16:13:35', '2022-06-04 16:13:35'),
(470, 'Received', 4, 12, '24.9180271', '67.0970916', '2022-06-04 16:13:43', '2022-06-04 16:13:43'),
(471, 'Accepted', 6, 12, NULL, NULL, '2022-06-04 17:30:34', '2022-06-04 17:30:34'),
(472, 'Received', 6, 12, '24.908966', '67.104039', '2022-06-04 17:30:44', '2022-06-04 17:30:44'),
(473, 'Pending', 144, NULL, NULL, NULL, '2022-06-06 11:59:35', '2022-06-06 11:59:35'),
(474, 'Accepted', 2, 12, NULL, NULL, '2022-06-06 12:00:00', '2022-06-06 12:00:00'),
(475, 'Received', 2, 12, '24.9090774', '67.104091', '2022-06-06 12:00:09', '2022-06-06 12:00:09'),
(476, 'Accepted', 3, 12, NULL, NULL, '2022-06-06 15:20:52', '2022-06-06 15:20:52'),
(477, 'Received', 3, 12, '24.9180271', '67.0970916', '2022-06-06 15:21:37', '2022-06-06 15:21:37'),
(478, 'Accepted', 4, 12, NULL, NULL, '2022-06-08 14:23:16', '2022-06-08 14:23:16'),
(479, 'Received', 4, 12, '24.9090739', '67.1040883', '2022-06-08 14:23:24', '2022-06-08 14:23:24'),
(480, 'Pending', 146, NULL, NULL, NULL, '2022-06-11 13:15:17', '2022-06-11 13:15:17'),
(481, 'Accepted', 2, 12, NULL, NULL, '2022-06-14 13:09:16', '2022-06-14 13:09:16'),
(482, 'Accepted', 2, 12, NULL, NULL, '2022-06-14 13:09:22', '2022-06-14 13:09:22'),
(483, 'Accepted', 2, 12, NULL, NULL, '2022-06-14 13:09:23', '2022-06-14 13:09:23'),
(484, 'Received', 2, 12, '24.909072389985', '67.104022486464', '2022-06-14 13:09:49', '2022-06-14 13:09:49'),
(485, 'Received', 2, 12, '24.909072389985', '67.104022486464', '2022-06-14 13:09:51', '2022-06-14 13:09:51'),
(486, 'Received', 2, 12, '24.909072389985', '67.104022486464', '2022-06-14 13:10:01', '2022-06-14 13:10:01'),
(487, 'Pending', 147, NULL, NULL, NULL, '2022-06-14 13:14:22', '2022-06-14 13:14:22'),
(488, 'Accepted', 3, 14, NULL, NULL, '2022-06-14 18:03:17', '2022-06-14 18:03:17'),
(489, 'Received', 3, 14, '37.785834', '-122.406417', '2022-06-14 18:03:32', '2022-06-14 18:03:32'),
(490, 'Pending', 148, NULL, NULL, NULL, '2022-06-14 18:04:47', '2022-06-14 18:04:47'),
(491, 'Accepted', 4, 14, NULL, NULL, '2022-06-15 18:27:33', '2022-06-15 18:27:33'),
(492, 'Accepted', 4, 14, NULL, NULL, '2022-06-15 18:27:34', '2022-06-15 18:27:34'),
(493, 'Accepted', 4, 14, NULL, NULL, '2022-06-15 18:28:25', '2022-06-15 18:28:25'),
(494, 'Received', 4, 14, '37.785834', '-122.406417', '2022-06-15 18:28:50', '2022-06-15 18:28:50'),
(495, 'Received', 4, 14, '37.785834', '-122.406417', '2022-06-15 18:29:05', '2022-06-15 18:29:05'),
(496, 'Received', 4, 14, '37.785834', '-122.406417', '2022-06-15 18:29:05', '2022-06-15 18:29:05'),
(497, 'Pending', 150, NULL, NULL, NULL, '2022-06-15 18:40:08', '2022-06-15 18:40:08'),
(498, 'Pending', 151, NULL, NULL, NULL, '2022-06-15 18:40:16', '2022-06-15 18:40:16'),
(499, 'Pending', 152, NULL, NULL, NULL, '2022-06-15 18:46:33', '2022-06-15 18:46:33'),
(500, 'Pending', 153, NULL, NULL, NULL, '2022-06-15 18:46:45', '2022-06-15 18:46:45'),
(501, 'Accepted', 5, 14, NULL, NULL, '2022-06-16 14:10:51', '2022-06-16 14:10:51'),
(502, 'Received', 5, 14, '37.785834', '-122.406417', '2022-06-16 14:20:03', '2022-06-16 14:20:03'),
(503, 'Accepted', 6, 14, NULL, NULL, '2022-06-16 14:52:01', '2022-06-16 14:52:01'),
(504, 'Accepted', 6, 14, NULL, NULL, '2022-06-16 14:52:12', '2022-06-16 14:52:12'),
(505, 'Accepted', 7, 14, NULL, NULL, '2022-06-16 14:58:20', '2022-06-16 14:58:20'),
(506, 'Accepted', 8, 14, NULL, NULL, '2022-06-16 15:10:03', '2022-06-16 15:10:03'),
(507, 'Accepted', 13, 14, NULL, NULL, '2022-06-16 15:11:40', '2022-06-16 15:11:40'),
(508, 'Accepted', 2, 14, NULL, NULL, '2022-06-16 15:17:15', '2022-06-16 15:17:15'),
(509, 'Received', 2, 14, '37.785834', '-122.406417', '2022-06-16 15:18:44', '2022-06-16 15:18:44'),
(510, 'Pending', 154, NULL, NULL, NULL, '2022-06-16 15:47:30', '2022-06-16 15:47:30'),
(511, 'Pending', 155, NULL, NULL, NULL, '2022-06-16 15:47:30', '2022-06-16 15:47:30'),
(512, 'Pending', 156, NULL, NULL, NULL, '2022-08-23 08:55:26', '2022-08-23 08:55:26'),
(513, 'Accepted', 2, 22, NULL, NULL, '2022-08-23 10:41:24', '2022-08-23 10:41:24'),
(514, 'Accepted', 2, 22, NULL, NULL, '2022-08-23 10:41:45', '2022-08-23 10:41:45'),
(515, 'Accepted', 2, 22, NULL, NULL, '2022-08-23 10:48:29', '2022-08-23 10:48:29'),
(516, 'Accepted', 3, 22, NULL, NULL, '2022-08-23 10:54:41', '2022-08-23 10:54:41'),
(517, 'Accepted', 4, 22, NULL, NULL, '2022-08-23 10:55:56', '2022-08-23 10:55:56'),
(518, 'Accepted', 6, 22, NULL, NULL, '2022-08-23 10:59:52', '2022-08-23 10:59:52'),
(519, 'Accepted', 5, 22, NULL, NULL, '2022-08-23 11:02:00', '2022-08-23 11:02:00'),
(520, 'Accepted', 7, 22, NULL, NULL, '2022-08-23 11:03:47', '2022-08-23 11:03:47'),
(521, 'Accepted', 8, 22, NULL, NULL, '2022-08-23 11:06:17', '2022-08-23 11:06:17'),
(522, 'Accepted', 13, 22, NULL, NULL, '2022-08-23 11:10:01', '2022-08-23 11:10:01'),
(523, 'Pending', 157, NULL, NULL, NULL, '2022-08-23 11:14:51', '2022-08-23 11:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `job_prices`
--

CREATE TABLE `job_prices` (
  `id` int(11) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `customer_amount` int(11) NOT NULL,
  `driver_amount` int(11) NOT NULL,
  `admin_amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_reviews`
--

CREATE TABLE `job_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `review` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `client_reviewed` int(1) DEFAULT '0',
  `driver_reviewed` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `job_reviews`
--

INSERT INTO `job_reviews` (`id`, `job_id`, `sender_id`, `reviewer_id`, `rating`, `review`, `created_at`, `updated_at`, `client_reviewed`, `driver_reviewed`) VALUES
(1, 1, 9, 9, 5, 'Review From Consumer', '2020-12-03 15:00:28', '2020-12-03 15:00:28', 1, 0),
(2, 1, 9, 11, 5, 'Review From Driver', '2020-12-03 15:01:17', '2020-12-03 15:01:17', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_urls`
--

CREATE TABLE `job_urls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `owner_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `firebase_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `job_urls`
--

INSERT INTO `job_urls` (`id`, `owner_id`, `job_id`, `firebase_url`, `created_at`, `updated_at`) VALUES
(1, 9, 1, 'https://deliveryist.com/admin-laravel-6x/storage/app/images/products/03eea3d1c89302b5c5d5b52399d244ef.png', '2020-12-03 18:31:46', '2020-12-03 18:31:46'),
(2, 9, 1, 'https://deliveryist.com/admin-laravel-6x/storage/app/images/products/210f6a994043e19708df07090567f01e.png', '2020-12-03 18:31:46', '2020-12-03 18:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geohash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `distance` double DEFAULT '25'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `device_token`, `geohash`, `latitude`, `longitude`, `role_id`, `role`, `user_id`, `document_id`, `created_at`, `updated_at`, `distance`) VALUES
(1, 'f861911e5fb7317383978d', 'tkrtktu8r8u', 24.9728707, 67.0643315, NULL, 'Driver', 2, '3zkkGid4rZtaTt0aMQTV', '2020-08-26 18:37:41', '2020-09-15 16:51:29', 25),
(2, 'e2833d876557a98bb2ce09', 'tefqdryb6ez', 22.2789632, 70.7723264, NULL, 'Consumer', 32, 'DI8Q5xwiKpYnX2J58M1O', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(3, '4d5914a5c91b62944e2547', 'tg0pn1k0e5h', 18.1124372, 79.0192997, NULL, 'Driver', 22, 'FXerJyvpp5HmuXKjUMwo', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(4, '37d241cc9914d3cc34c52d', '9vg512zu3vw', 32.8759731, -96.9655921, NULL, 'Consumer', 24, 'bbBemZorT3o3C0RM0ILo', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(5, '391c8fa525e75d2475b02f', 'tefqdryb6ez', 22.2789632, 70.7723264, NULL, 'Consumer', 20, 'cEo042HGUu3GqduHsSKa', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(6, '4d5914a5c91b62944e2547', 'tg0pn1k0e5h', 18.1124372, 79.0192997, NULL, 'Consumer', 22, 'g1jXf1lkaHLVoLBBTR0x', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(7, '8e77089e8ab0e266c7d1a3', 'tefqdq5571v', 22.2698551, 70.7671948, NULL, 'Consumer', 30, 'jg42ZEDBTMWaV3tRMVAZ', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(8, 'e601e712dbcea16efaa8a1', 'wh0r0et9dk0', 23.7499741, 90.381185, NULL, 'Consumer', 19, 'lxqfT8ABjswc1kWTuypt', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(9, NULL, NULL, 22.2789632, 70.7854336, NULL, NULL, 41, NULL, '2020-09-16 09:23:55', '2020-09-16 09:23:55', 25);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-12-18 05:55:42', '2020-12-18 05:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-12-18 05:55:42', '2020-12-24 21:55:10', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 4, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 3, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2020-12-18 05:55:42', '2020-12-24 21:56:24', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 7, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.hooks', NULL),
(12, 1, 'Cashes', '', '_self', NULL, NULL, NULL, 8, '2020-12-18 09:08:59', '2020-12-24 21:56:18', 'voyager.cashes.index', NULL),
(13, 1, 'Categories', '', '_self', NULL, NULL, NULL, 9, '2020-12-18 09:09:58', '2020-12-24 21:56:18', 'voyager.categories.index', NULL),
(14, 1, 'Category Types', '', '_self', NULL, NULL, NULL, 10, '2020-12-18 09:10:15', '2020-12-24 21:56:18', 'voyager.category-types.index', NULL),
(15, 1, 'Conditions', '', '_self', NULL, NULL, NULL, 11, '2020-12-18 09:10:55', '2020-12-24 21:56:18', 'voyager.conditions.index', NULL),
(16, 1, 'Favorites', '', '_self', NULL, NULL, NULL, 12, '2020-12-18 09:11:53', '2020-12-24 21:56:18', 'voyager.favorites.index', NULL),
(17, 1, 'Invoices', '', '_self', NULL, NULL, NULL, 13, '2020-12-18 09:12:10', '2020-12-24 21:56:18', 'voyager.invoices.index', NULL),
(18, 1, 'Job Prices', '', '_self', NULL, NULL, NULL, 14, '2020-12-18 09:12:28', '2020-12-24 21:56:13', 'voyager.job-prices.index', NULL),
(20, 1, 'Jobs', '', '_self', NULL, NULL, NULL, 15, '2020-12-18 09:13:02', '2020-12-24 21:56:13', 'voyager.jobs.index', NULL),
(23, 1, 'Locations', '', '_self', NULL, NULL, NULL, 16, '2020-12-18 09:15:31', '2020-12-24 21:56:13', 'voyager.locations.index', NULL),
(24, 1, 'Otp Numbers', '', '_self', NULL, NULL, NULL, 17, '2020-12-18 09:16:06', '2020-12-24 21:56:13', 'voyager.otp-numbers.index', NULL),
(25, 1, 'Messages', '', '_self', NULL, NULL, NULL, 18, '2020-12-18 09:16:25', '2020-12-24 21:56:13', 'voyager.messages.index', NULL),
(27, 1, 'Products', '', '_self', NULL, NULL, NULL, 19, '2020-12-18 09:17:11', '2020-12-24 21:56:13', 'voyager.products.index', NULL),
(28, 1, 'Rooms', '', '_self', NULL, NULL, NULL, 20, '2020-12-18 09:17:34', '2020-12-24 21:56:13', 'voyager.rooms.index', NULL),
(30, 1, 'Vehicle Sizes', '', '_self', NULL, NULL, NULL, 21, '2020-12-18 09:20:14', '2020-12-24 21:56:13', 'voyager.vehicle-sizes.index', NULL),
(31, 1, 'Vehicles', '', '_self', NULL, NULL, NULL, 22, '2020-12-18 09:20:55', '2020-12-24 21:56:13', 'voyager.vehicles.index', NULL),
(32, 1, 'Wallets', '', '_self', NULL, NULL, NULL, 23, '2020-12-18 09:22:09', '2020-12-24 21:56:13', 'voyager.wallets.index', NULL),
(37, 1, 'Runtime Roles', '', '_self', NULL, NULL, NULL, 24, '2020-12-23 08:16:17', '2020-12-24 21:56:13', 'voyager.runtime-roles.index', NULL),
(42, 1, 'Job Urls', '', '_self', NULL, NULL, NULL, 25, '2020-12-23 08:42:31', '2020-12-24 21:56:13', 'voyager.job-urls.index', NULL),
(43, 1, 'Job Reviews', '', '_self', NULL, NULL, NULL, 26, '2020-12-23 08:52:25', '2020-12-24 21:56:13', 'voyager.job-reviews.index', NULL),
(44, 1, 'Admin Settings', '', '_self', NULL, NULL, NULL, 27, '2020-12-23 09:36:30', '2020-12-24 21:56:13', 'voyager.admin-settings.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `message`, `room_id`, `created_at`, `updated_at`) VALUES
(1, 2, 'bypass', 4, '2020-09-26 09:07:50', '2020-09-26 09:07:50'),
(2, 2, 'pure', 4, '2020-09-26 09:08:36', '2020-09-26 09:08:36'),
(3, 2, 'mainly', 4, '2020-09-26 09:08:43', '2020-09-26 09:08:43'),
(4, 9, 'ping', 1, '2020-11-28 12:14:34', '2020-11-28 12:14:34'),
(5, 9, 'hello ?', 1, '2020-11-28 14:08:04', '2020-11-28 14:08:04'),
(6, 9, 'What is Wrong?', 1, '2020-11-28 14:15:01', '2020-11-28 14:15:01'),
(7, 9, 'ytr', 1, '2020-11-28 14:26:40', '2020-11-28 14:26:40'),
(8, 9, 'scroll check', 1, '2020-11-28 14:28:21', '2020-11-28 14:28:21'),
(9, 9, 'hi', 1, '2020-12-03 22:36:39', '2020-12-03 22:36:39'),
(10, 9, 'hi', 1, '2020-12-17 11:47:05', '2020-12-17 11:47:05'),
(11, 12, 'kljsakldjsal', 2, '2022-05-21 20:27:59', '2022-05-21 20:27:59'),
(12, 12, 'sadssad', 2, '2022-05-24 21:43:00', '2022-05-24 21:43:00'),
(13, 12, 'hi, i am not interested..', 3, '2022-05-25 21:43:19', '2022-05-25 21:43:19'),
(14, 12, 'Hi', 4, '2022-06-01 18:45:38', '2022-06-01 18:45:38'),
(15, 12, 'Test', 4, '2022-06-01 22:52:07', '2022-06-01 22:52:07'),
(16, 12, 'Hello', 4, '2022-06-02 12:00:56', '2022-06-02 12:00:56'),
(17, 15, 'hgc', 5, '2022-06-04 16:27:47', '2022-06-04 16:27:47'),
(18, 15, 'gtigfdfc', 5, '2022-06-04 16:27:52', '2022-06-04 16:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(9, '2016_06_01_000004_create_oauth_clients_table', 1),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(11, '2016_10_21_190000_create_roles_table', 1),
(12, '2016_10_21_190000_create_settings_table', 1),
(13, '2016_11_30_135954_create_permission_table', 1),
(14, '2016_11_30_141208_create_permission_role_table', 1),
(15, '2016_12_26_201236_data_types__add__server_side', 1),
(16, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(17, '2017_01_14_005015_create_translations_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(21, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(22, '2017_08_05_000000_add_group_to_settings_table', 1),
(23, '2017_11_26_013050_add_user_role_relationship', 1),
(24, '2017_11_26_015000_create_user_roles_table', 1),
(25, '2018_03_11_000000_add_user_settings', 1),
(26, '2018_03_14_000000_add_details_to_data_types_table', 1),
(27, '2018_03_16_000000_make_settings_value_nullable', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\SuperAdmin', 1),
(2, 'App\\SuperAdmin', 1),
(7, 'App\\User', 19),
(7, 'App\\User', 20),
(7, 'App\\User', 21),
(7, 'App\\User', 22),
(7, 'App\\User', 23),
(7, 'App\\User', 24),
(7, 'App\\User', 25),
(7, 'App\\User', 26),
(7, 'App\\User', 27),
(7, 'App\\User', 28),
(7, 'App\\User', 29),
(7, 'App\\User', 30),
(7, 'App\\User', 31),
(7, 'App\\User', 32);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('004ffbe7842ca610442e8de7835926ec95b0ce886e2d2ad31b39528c096b33e118eccef7020e071a', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:52:55', '2022-06-15 00:52:55', '2023-06-14 17:52:55'),
('07b0550b58369850d8e63d4eeeb293ca1ba8960da8917d2a6be6d219d2be6b398e75f8cd56d5f41b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:00:23', '2022-06-06 22:00:23', '2023-06-06 15:00:23'),
('0a7ffc05dc5b92f4f2b8c223e1af18af3ffb4d0c691608840479173dfe22ab53a0e1c3fd96cdb3ac', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-30 22:40:05', '2022-05-30 22:40:05', '2023-05-30 15:40:05'),
('0a894d529653101a6c4f39504762c5669a1fd0be8b2715e38c5eb4888a70b695b33b5c03c8029805', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 22:37:20', '2022-06-13 22:37:20', '2023-06-13 15:37:20'),
('0b1109ff5af077f66835aa023423e32d8fb4b44765ae9be2d7f59371c4ca39c5708f1843d7e9e708', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 22:37:04', '2022-06-13 22:37:04', '2023-06-13 15:37:04'),
('0c6fbaf6ddcc840bfc5cf20af3adddbe738a814b32eced3b19b6b4feb2d9da7dc3f570d6552e7a64', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 01:02:29', '2022-06-15 01:02:29', '2023-06-14 18:02:29'),
('0e5c7c1f88bbad8c04cc0894fdcdc4f79e3a39a354f108af65e668be99dc285c7a76f10a37bc0f9a', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-12 19:17:22', '2022-05-12 19:17:22', '2023-05-12 12:17:22'),
('0e7df79e49251c8282d26c66db39add0ade55c8f8a59af93a38f3b1d3b562b9f048e753bf8d12291', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:55:10', '2022-06-01 16:55:10', '2023-06-01 09:55:10'),
('0ece42aaa03c861122b88059544b5d481f656d9c9b4e1affb7fbb183fa9ac6c1428e3daa5206e01b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:58:10', '2022-06-06 21:58:10', '2023-06-06 14:58:10'),
('0fb3c288aa0e38a9178eeae3dd2f6c6e087e2e5deea82b0a64e694dc9da6fdf7e454d6d306cf6869', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 22:36:28', '2022-06-13 22:36:28', '2023-06-13 15:36:28'),
('10fb72260564b0367eb5f947000e118abb10a656e934cc9baee59a9837bcb29c400df55dec95eb1c', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 13:39:50', '2022-08-23 13:39:50', '2023-08-23 06:39:50'),
('1386d1ac65f060373a4e4ab905198df80bf39414e3bd9e5531d25b241aec46be006d8443e79c9b95', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 22:00:41', '2022-06-04 22:00:41', '2023-06-04 15:00:41'),
('16f67bb708252549097e5707fdcd7730ef62db5ea6df49e529e9919cc1eb1cb0e8730d9fb465dfae', 22, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 16:36:13', '2022-08-23 16:36:13', '2023-08-23 09:36:13'),
('1a3f4716fa873abc27c05802371b041c2e8e6caf513a3820b16de7c2758b594c2faa30b2ee997136', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-30 18:48:30', '2022-05-30 18:48:30', '2023-05-30 11:48:30'),
('1a4fd19d1dd31ffd8bd873046d7589f811a3bbe509182521c30269983248d1ef2685f2291d715c27', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:14:23', '2022-06-02 20:14:23', '2023-06-02 13:14:23'),
('1b4e47f24cdc27c5d9f7d17fc163f1197f17b924bef38e36c822742acac19ad68961705ea8e816ce', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:48:47', '2022-06-01 16:48:47', '2023-06-01 09:48:47'),
('1dd344f5c33df58f43458f7c3962c2de39a1c306e7ee5933efe0890ca32ac837d78b58f019ec09bb', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:25:44', '2022-06-06 21:25:44', '2023-06-06 14:25:44'),
('1e60d62d8939a9139c9203451570130e33032ff98fc56583afb633e7eedd91da3ace0a8cacd10df1', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 22:25:42', '2022-05-19 22:25:42', '2023-05-19 15:25:42'),
('1f2f8f658a5877791098d9bad2c8603fcb56d75110b77d4b146e3c891ad3e378c5abe4be4c2a2261', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:03:40', '2022-06-02 20:03:40', '2023-06-02 13:03:40'),
('200269ec474edc46dcc4194962805c4bc2a75c81d3709942dd79735ea6d25b04b1f89622bd176cf7', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 19:06:14', '2022-06-03 19:06:14', '2023-06-03 12:06:14'),
('2007cd5145906b18a2014c0189146a75278b19ef661121fe4bd7fbb2486a7a24a35dfdf010c56362', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-11 19:39:51', '2022-05-11 19:39:51', '2023-05-11 12:39:51'),
('2154ef65b0c9f6f2ac6710f94de03eee812ef1cea9a34b72195e090352b8af0dff7d52cc91022a93', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 18:54:54', '2022-05-21 18:54:54', '2023-05-21 11:54:54'),
('2225d74ef521ce6465734608b78716c775b29cd65ec8297c26e96f425034453289ad6b6143dc1b8e', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 16:03:28', '2022-08-23 16:03:28', '2023-08-23 09:03:28'),
('227e23de30f036c579fd4bafcd1103778564ea5d257cc46e5f90c444f6957186618361ccd9a9ca6d', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 20:52:04', '2022-06-03 20:52:04', '2023-06-03 13:52:04'),
('231b78942cc4fceb9d7c461c5b75fbc7ddb3e34f0cbc8f3750936d795d82162a70267ea32ce3b6dd', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:27:12', '2022-06-14 23:27:12', '2023-06-14 16:27:12'),
('2397bee52b45154ca6fe556f43847ef80141fb373e4805ebd96d9ea32209d1f015f916e4cc3e40e2', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:53', '2022-06-14 23:47:53', '2023-06-14 16:47:53'),
('24f09729e5adae266e2df01d4bf0850ed28dad4c02da560794b1afe6aa6b0e4c1d77b7970924ffe8', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 18:27:59', '2022-06-02 18:27:59', '2023-06-02 11:27:59'),
('24fabec83529d3fb6419a7ec8f4b0647b222a23f61b24076d782dbce5167e54fc4d3edd0cb2651fd', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-09 11:51:41', '2022-05-09 11:51:41', '2023-05-09 16:51:41'),
('27816189f06a66f512a949f1d2a7ec373a002b4bdd14ba7b5564261683d5552fc908df46c2a01911', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 01:07:59', '2022-06-04 01:07:59', '2023-06-03 18:07:59'),
('2c8d48d6b38279dea1968f375b4f4c0f1812d8fe170091a82d1b77c40164668e927a72d80fcd3664', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 14:24:47', '2022-05-21 14:24:47', '2023-05-21 07:24:47'),
('2dfb8bb0850db54900c50091b23eb4507b22ec79971df0a1e77d209e654d22a30f23c59eedd1c90d', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:48:46', '2022-06-01 16:48:46', '2023-06-01 09:48:46'),
('30e65824eddba7ee63735d437e156279db26377229e0c1505b1a2f96050f582ab0b6f44d6a75dc91', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:43', '2022-06-14 23:47:43', '2023-06-14 16:47:43'),
('318f5199369d1a660735eb5d5cc1e7c91f777989fdebb67f6877f42d350d22e8125f6a16b8ebbf4f', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-11 19:45:47', '2022-05-11 19:45:47', '2023-05-11 12:45:47'),
('339aeec6f77e58a98342c469db12fb76bb706ecdc7eaa4b8ca4dc76b8227826591f05bcc61c3af97', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 19:03:53', '2022-06-02 19:03:53', '2023-06-02 12:03:53'),
('36b2547c6a9e87e49a4beafef7f4576c6b8e500b66a924a594839fb3ada6fb33ca876693d370496b', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 19:11:14', '2022-05-21 19:11:14', '2023-05-21 12:11:14'),
('38271fb250b5898f0d7df4d7a867c4a941c4de02e99b90dfe5ffe69139d5c3bbe53609cd30affbc2', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:49:31', '2022-06-01 16:49:31', '2023-06-01 09:49:31'),
('3bcc0da21ee9a4815ba935828388b5bbe181c23d9a3425dd677b6e1bf9f138003ad14ad375981a02', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 17:44:09', '2022-05-19 17:44:09', '2023-05-19 10:44:09'),
('3e16fd290a6d6a1973b6e3d0d9ddada4a9c3d40ba701d37e170d9bd4a31a8666e1275b013a1065f7', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 01:26:07', '2022-06-16 01:26:07', '2023-06-15 18:26:07'),
('3ecd072a8d9a231bc2260b8d4ccb6928f59c372dcd6c96f0962b8f78e33eddc793ceb4bb568d8d63', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-11 22:19:55', '2022-06-11 22:19:55', '2023-06-11 15:19:55'),
('3f00524c8d038ea50d2339b12074b1208685f333a671035d825a1afeba250ef43e1670dbddf1545d', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:00:23', '2022-06-06 22:00:23', '2023-06-06 15:00:23'),
('3f02667bc62cf71efad83f646fe30931d0be1ffdd98d5be654eb1c5b2ddcb8399b0c449b6bdc623b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 15:06:03', '2022-06-06 15:06:03', '2023-06-06 08:06:03'),
('3f0e3578ee4b1e624d9e25f2ac8de65820e2c4170a59e3c290be22a6338decbf1bff481ef571a677', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 16:03:23', '2022-08-23 16:03:23', '2023-08-23 09:03:23'),
('3f3b628be61e69313d64679daac40ff682ac6c25aaac67d6e0cd821f76b579787b4135a24945b45b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 00:26:34', '2022-06-04 00:26:34', '2023-06-03 17:26:34'),
('3f5e7dc74d033e3603517bbd7803faaa4585d395f6a80697530e0809f60ee274e697dcc15b2d35ab', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 01:02:57', '2022-06-15 01:02:57', '2023-06-14 18:02:57'),
('3f794866d465c136fcbd044ca6088699ba2b45db6f0ba5c5d8531ad22229288a7b80239b85a8b059', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 15:13:14', '2022-05-21 15:13:14', '2023-05-21 08:13:14'),
('41c0a51069fe22d12a37f5af29c34afbc258a3ae2d3f645d6a99c858e93c6cc776c69f0f5412e8e7', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:54', '2022-06-14 23:47:54', '2023-06-14 16:47:54'),
('435c14027eff9a31c7a2f9ba7fbc60b3651f9c9bd39697e86cd61ef8bf9c7824fc1b489f5a6c1411', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 22:35:28', '2022-06-14 22:35:28', '2023-06-14 15:35:28'),
('440ca4f6f296654aeacb37097e73c4e0ced54ed2878d26857ffe90b1636a37c8a315bf618276d9d6', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-12 19:17:23', '2022-05-12 19:17:23', '2023-05-12 12:17:23'),
('453e709b50192c392fca27746c40ecf4a8a04222d7a5ba3caef79e75a4ca8d00ddd975af44703179', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-07 00:27:52', '2022-06-07 00:27:52', '2023-06-06 17:27:52'),
('45756dc0707053bc1c0e07a49b5d21959c1c9ea8d709a3695f0d4d204a0936d6bc4f467dfce015c0', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 20:34:13', '2022-08-23 20:34:13', '2023-08-23 13:34:13'),
('468ab88b24c201504d4f7568b19c3c75328675e75d7d9e605a3573fef6a46a2945c5beeb47fafb02', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:23:17', '2022-06-16 18:23:17', '2023-06-16 11:23:17'),
('46a253c6ee392ab2159d077207b0437270e280eeb6c83cb19bab37939e46858749b7e8fc05ed38de', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:49', '2022-06-14 23:47:49', '2023-06-14 16:47:49'),
('47d797fd6906224ffcc7b2aa57feb58c4decea34210e0bb4c4eca1246cac1b459fbeb7ba7cd47f34', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 00:03:36', '2022-06-04 00:03:36', '2023-06-03 17:03:36'),
('493d686c4f239727ec6f72287849fd7442e82967b0fdd4c8a55f83d536bd9644dbe794b755c0bf39', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-08 21:22:06', '2022-06-08 21:22:06', '2023-06-08 14:22:06'),
('4c8d988056f4b20efe5782238ceb62db5f773f2b645cd88ae3d55d9dd3a87b1f2fdd454547d222da', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:54:51', '2022-06-06 21:54:51', '2023-06-06 14:54:51'),
('4e831808bbfb5454fd7abd916bd2e58e4c79af53246776ae4ea1b5738561fdc8b465f7790b0b38b8', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:44', '2022-06-14 23:47:44', '2023-06-14 16:47:44'),
('4ee80ebfb0d894abdc308162f9358ea4e4d249ee246b708987acb78758764a6fce983dd8d83d1374', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-08 21:22:09', '2022-06-08 21:22:09', '2023-06-08 14:22:09'),
('5156f478e1020bfe098036ee0cb870751eaafb0e41502fbebaeae316aa728522319c119facf11abb', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-11 22:03:16', '2022-06-11 22:03:16', '2023-06-11 15:03:16'),
('52fa967b316b357943eb77fb0c930dd5cd623640fad9e190293bb9952edca07caaf04f0cc337986c', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 00:59:45', '2022-06-04 00:59:45', '2023-06-03 17:59:45'),
('54430e692d4729e8ce551ba5c771137e354c89df3d5d63bab6abad7cae73e3efef56d655362396b1', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:36:39', '2022-06-16 18:36:39', '2023-06-16 11:36:39'),
('54706fae8e27e26220742a2d5889c7ebd7b7260c95eb37302a4aaf6d79854a8fd364bbf86f2a41b9', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 21:07:46', '2022-06-01 21:07:46', '2023-06-01 14:07:46'),
('54ecd819bcc2c7bc73e43e62a824158c08bd364dc9f07204d158277f16c6d2a8491de3cc9e140b74', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:25:09', '2022-06-16 18:25:09', '2023-06-16 11:25:09'),
('56733a82c6d53dd87d84476de10de7b24469b3db11ffb46fddbff0f5000ceba504a7ecd31cdac54c', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-13 16:58:50', '2022-05-13 16:58:50', '2023-05-13 09:58:50'),
('56af662baec0e3281b4a671fbd7aadc52f88189b70a49189c113c338785c73d4aaf4a328e1eafb46', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 21:51:04', '2022-06-14 21:51:04', '2023-06-14 14:51:04'),
('58181e6afe9a0d0b57ae2c61545778d1605e37cada273318f070be04ceb808e2b85ceea46504fc96', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 23:50:37', '2022-06-03 23:50:37', '2023-06-03 16:50:37'),
('5a10dd6d54ead263f13bf16eda6857818f167aca45a0a46c5867fde09eece4f1a3050bb390fe25d6', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 20:20:04', '2022-05-21 20:20:04', '2023-05-21 13:20:04'),
('5a8df17e263cdb7b7b7cf1b8edba36e6d9d34fde59845caa40af401176e2351ed19c28e0f94d59eb', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 22:39:01', '2022-06-13 22:39:01', '2023-06-13 15:39:01'),
('5b72cea29e3041bd94db59dc6ba5162cb2dd685ab20c3f2f143dcf2c706a1a428209a933b5b45b52', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:06:44', '2022-06-02 20:06:44', '2023-06-02 13:06:44'),
('5eb06782327f3921af0a2fcf4c2e1c8b0a7d89644b1098da7d4d218144606f54109021b2b4f38f59', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-08 21:22:06', '2022-06-08 21:22:06', '2023-06-08 14:22:06'),
('5fc93b726c0397d908bc0f45f22483747928bdcabff96870b37daa5494f5512e0e6fe67435dff681', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:04:37', '2022-06-15 00:04:37', '2023-06-14 17:04:37'),
('63023c941d6b9e7b3c99c62af31b68c7ff201ddb95d66bbf5e8b91bd18bc5da7ceee54ca29b388f3', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:48:47', '2022-06-01 16:48:47', '2023-06-01 09:48:47'),
('6568c9e4a4a7a8d3573ab2714b40deb3ffa034c5829009bf8c773a5bf6e180fd422e1144278e481d', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:52:44', '2022-06-15 00:52:44', '2023-06-14 17:52:44'),
('65a4f7599c304b710868a25be19b9ccc182ca77235dc905b4c647ada965662968ceefe97e1db9088', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:22:08', '2022-06-15 00:22:08', '2023-06-14 17:22:08'),
('6755059cb2f84905f594db4dc46e291fcbd2b13418973e4e31408386ea90486628a2dac92f27d3e2', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-11 20:14:29', '2022-06-11 20:14:29', '2023-06-11 13:14:29'),
('6a47470e3e04b73755b77ca2c9063479519e3cbf441ae0d75d1a48b18f26f1d68a1f7feb31838d94', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:07:33', '2022-06-02 20:07:33', '2023-06-02 13:07:33'),
('6af0bae9ff330e1a18fa50a264c2dace079cfe3f985578b8c451fad1eb6bc6bc571f67a91a89c789', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 17:54:11', '2022-07-02 17:54:11', '2023-07-02 10:54:11'),
('6cf5faaecba3079102e4f8c3730e9d85aecc6f8c5593a17200a2149e350092646abdd61e6c865d56', 20, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 20:02:14', '2022-06-16 20:02:14', '2023-06-16 13:02:14'),
('6e76efbbbdc1a404bc46eb6a831308f50256d770abd2713ee0794b092516cd2845ce43af3897897b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 21:06:15', '2022-06-03 21:06:15', '2023-06-03 14:06:15'),
('73a80185c772b1a416cc3b0f7f7c6dacc4b4b03fe4df75a01f3e106e68c412ab4d7ee643343ad3da', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 22:36:46', '2022-06-13 22:36:46', '2023-06-13 15:36:46'),
('754fab2833ac457ddc87e60e8c19123aa03c48dd9a6ce08dc73438f23e0b5c967b3cfa5b18077401', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-08 21:22:06', '2022-06-08 21:22:06', '2023-06-08 14:22:06'),
('75936e0d7abeea011d51d15c09f85d32717bb6b0d81951b967e2a4597a5af41f333a6129df10305e', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:26:32', '2022-06-06 21:26:32', '2023-06-06 14:26:32'),
('75b052594ea42aee348d351ae388d6f215f0e689a3d6108b247cabd9533c6fa810fd311f9fab6a2d', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:17:25', '2022-06-06 22:17:25', '2023-06-06 15:17:25'),
('76066e0863aedf33d38b300a742bb6aacb612cb52f13de220ddd51c3be33de46cf202f68d355c7fd', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 18:38:40', '2022-06-02 18:38:40', '2023-06-02 11:38:40'),
('764bbbcbc5d87dc086dc4100f280bba94364228dc2e6c2d845afe788d9078152729bbf3ce615252e', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:43:03', '2022-06-06 21:43:03', '2023-06-06 14:43:03'),
('77455e3b642c6856b862b827a038fb09eaf9ac4b1f867ac4787c98f337c550a9cf87a62aa7abeba0', 15, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 16:16:54', '2022-06-04 16:16:54', '2023-06-04 09:16:54'),
('7934bf6704aae2ac1718f2f166b945763b2f7e6e8218d2ab2c650f2f6cadc0b1ad55a6e1290c87f2', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:11:40', '2022-05-10 03:11:40', '2023-05-10 08:11:40'),
('7c1561e9a77ea35c84c97b91d875fcce7265bb5364981c770fbefddf0daa5168e6d8cf5ce9971b43', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:25:31', '2022-06-06 21:25:31', '2023-06-06 14:25:31'),
('7e0dcc91df66e8340301a2d2c5ecaca002305f1595878ce75b0d434d3343e91a885793d6996d7d18', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:57', '2022-06-14 23:47:57', '2023-06-14 16:47:57'),
('808cf0306f1c6525e27ce74103e248df46edc0c533d79b9c6007fadd395252c8b02017a898f3899c', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 23:15:34', '2022-06-02 23:15:34', '2023-06-02 16:15:34'),
('826acef12e39c04d2545046d8a9f63f8efcb4836d1a8a65e9e70e23c8c72ca694a2dcc56c172ab42', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:48:02', '2022-06-14 23:48:02', '2023-06-14 16:48:02'),
('83eb17ab72a5207d60e31c21ed13a746342f3c8e10aabf5123da6c4ba80d5e5c30876e5b25d466a9', 17, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-11 23:48:18', '2022-06-11 23:48:18', '2023-06-11 16:48:18'),
('84aae0401df75ef378290493c38bf7f38d64a27a60ebf637bf984ef92ecb8927cfd1921ea7068871', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:54:51', '2022-06-06 21:54:51', '2023-06-06 14:54:51'),
('85ae640ede075e9a82f3ec1b326149ee6606423f6bf4a46946f9a6c79bce31d6d956db263f267e99', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:00:23', '2022-06-06 22:00:23', '2023-06-06 15:00:23'),
('86271d4c15d8e632f4b9691cbc72fcbb1cb88d3fb49e8e39c76d336b5bcacf6d443e3d27164ac637', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:52:44', '2022-06-15 00:52:44', '2023-06-14 17:52:44'),
('8814bc8bab48349a7b40f6f9b6cee6baf3d604e3cdb62be6f44f7e536b0a53732902356161e7567d', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 17:40:30', '2022-06-16 17:40:30', '2023-06-16 10:40:30'),
('8818cfecb3133a9e6e966e2653d796c563fa643112f380d2c3d2cf79a2d0093f4230a092c3402276', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:43', '2022-06-14 23:47:43', '2023-06-14 16:47:43'),
('8ad4a50dfa9000dc3eeab8a78fbb84b64664d2cb97af46107880ce1f1e874fe73391d10333562e5f', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 00:31:28', '2022-06-04 00:31:28', '2023-06-03 17:31:28'),
('8b77448fa0d3846748bbf38907629a4f998e46a29dddb3095740632fc790062165e5d4b19b6b91d2', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:12:17', '2022-06-06 22:12:17', '2023-06-06 15:12:17'),
('8bcfae9d4e9b36169cdc21277ea7fef0aea90e238ce88a04dc09cf6d676bd99a1894519dc19c42b9', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 18:39:42', '2022-06-03 18:39:42', '2023-06-03 11:39:42'),
('8c02377bf9b75115f118807ded2bc226c84568dc59df5bcf25c041da617a4564d70e7ff7b05e7f85', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:12:18', '2022-05-10 03:12:18', '2023-05-10 08:12:18'),
('8c1acba8c94686876524e73271f3a5e478baf4c7fed288e0b5d30dbfa3138c7351caa7340c9c1dfb', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:38:25', '2022-06-16 18:38:25', '2023-06-16 11:38:25'),
('8cb8b196e0622d1922d8251b9f80a0e646dd6386bbdc89a4284eaca11c4b8b47ffd08c7c13619d77', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:22:28', '2022-05-10 03:22:28', '2023-05-10 08:22:28'),
('8ed510753ad413eb92ef9a747caaf3a1eaecc2a2ccfd0d5c63d39086ae08ce12d5ec523de0e98406', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-11 00:40:33', '2022-05-11 00:40:33', '2023-05-10 17:40:33'),
('901e095cf0ea79498b6106d0f34d86f1131e669abb7d60748e2ec8a9806d6966809f533f066d41ff', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:59:17', '2022-06-16 18:59:17', '2023-06-16 11:59:17'),
('90b94fe662de3130eda05008eceb8b0104d57a8e26d77b6eb26cb10ae335449282e6dce17021cef9', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:13:08', '2022-06-06 22:13:08', '2023-06-06 15:13:08'),
('918c746407d8c95c0592db439736b686610e533edf8ce8851432882654b6788528c545fafdf708e6', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 01:21:57', '2022-06-15 01:21:57', '2023-06-14 18:21:57'),
('928f7b4747dd9dd554b114e4e123525d143f98cd7354dc8d63e9982c4a472980a96a8da345e4ecfe', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 00:09:41', '2022-06-04 00:09:41', '2023-06-03 17:09:41'),
('931a7230d8e8c0fd250bb9432fd1102ce297eedafb9171ba615c901f88131077e5af87f82795ed8d', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 01:44:11', '2022-06-16 01:44:11', '2023-06-15 18:44:11'),
('93f521f6ca00fbbe4ff702a0a22fa09dce92d3b9e71e25593b4eff29e6c6354f79f7862c2714351d', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-09 11:47:19', '2022-05-09 11:47:19', '2023-05-09 16:47:19'),
('94f045d063c71efa25c1647846a1483346fe7cc00c129d886b788c24c8e13f754afd92f6861de6bc', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:48:47', '2022-06-01 16:48:47', '2023-06-01 09:48:47'),
('95d4f4a8d9b90f2ee40c13e49e0414cb71bbd0dfa03dde80b3ed3f76b24b99e83eac2f48e0eef8de', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 18:29:17', '2022-06-02 18:29:17', '2023-06-02 11:29:17'),
('96b3dacdd6d82f00da3f253e9b5d6b9501f7dfd0b9351a27c0cb25e5ce4485530ec694866aa2f692', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 21:02:57', '2022-06-01 21:02:57', '2023-06-01 14:02:57'),
('97cee39cd6582ee0ef73536b70544d7ce857ef287c5c3e46c72b46a56e7f6cad35cf82d7b851d134', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 18:25:18', '2022-06-02 18:25:18', '2023-06-02 11:25:18'),
('9b2f5815a54e11c0e1b860e882e9097c262ca98f320871a89536fc9db1adeb54c9827a4ffa664a43', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-08 21:22:06', '2022-06-08 21:22:06', '2023-06-08 14:22:06'),
('9b63d7a93677c351b00a4f674cc62128359dc531f8c48a1711c19926d10628ed83e97e5c162166d6', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 01:25:52', '2022-06-16 01:25:52', '2023-06-15 18:25:52'),
('9c3308690fcf738d5bdc6bfb80ea9e203c912122d9f68d1875c29a6b907d734abc95d64ac156e8c5', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-18 21:02:27', '2022-05-18 21:02:27', '2023-05-18 14:02:27'),
('a09a44c41233781bf0b5eec698c1e9a77c480a5055b4df696218d96b0ddd117105c5a5035c2ac5c2', 22, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 18:11:51', '2022-08-23 18:11:51', '2023-08-23 11:11:51'),
('a0edd91a26ca1494212b14bff9a39a8a6d70c9883b6d1cd00df867d5af9f58434e0eba6fb632863b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 17:21:05', '2022-06-06 17:21:05', '2023-06-06 10:21:05'),
('a10154fa80a79f3bc7224b45152b775ceefa5a5430ef2c1b70d5d6b7503422d588908bf219db865c', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 18:57:57', '2022-06-02 18:57:57', '2023-06-02 11:57:57'),
('a201c40f32cab07c0461ed1523f405b7d06485ebc83d310f44cf146b500e6ced40fe337562f3eb39', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 20:01:33', '2022-06-03 20:01:33', '2023-06-03 13:01:33'),
('a302b152cd06b344320a6fd277da96181c89efc9367209b81c0d0676d0af5c8588ba9ae1ff5e6123', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:22:37', '2022-05-10 03:22:37', '2023-05-10 08:22:37'),
('a3a99f1adcd2369fdc0218d5939a489a3df13d776b3a86dfc51efc5946f9f693fb17be10e8641a1c', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 14:37:52', '2022-08-23 14:37:52', '2023-08-23 07:37:52'),
('a47e48bd434ac4302a1acb50cf118b055b827673b53107a234b7237dee9e116200a2186f97ddd06c', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 18:56:58', '2022-06-14 18:56:58', '2023-06-14 11:56:58'),
('a4c1ef9012cb2381604051fb5c535d58bf237fe94635b6847a2f7e5a491f71dd4e374b5bce67f0ce', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:49:03', '2022-06-01 16:49:03', '2023-06-01 09:49:03'),
('a533f1859fb0c7dd2ebae4256ed187a40762579754d07c99c81bbff2fe7448f3c8deeda4167c58a7', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:52:37', '2022-06-15 00:52:37', '2023-06-14 17:52:37'),
('a63d661bc2e9f8cfab4afe34bfb47eaa828944bb21c697d2fc091921930ecc1a5fbca5bf442a273f', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 22:42:16', '2022-05-21 22:42:16', '2023-05-21 15:42:16'),
('a9984eb6f9c20e77cc510269f3697e652adc675202ab37cb7f4f3acdc73d618d269fa108b6fb88b0', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:29:11', '2022-06-15 00:29:11', '2023-06-14 17:29:11'),
('ac58ef470af73c9cd9a90beb8ea4479a5cb2156c22d9a502c6c35a13077d1c57adab935ff32f9cae', 11, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 16:46:28', '2022-05-21 16:46:28', '2023-05-21 09:46:28'),
('acd988c1fa746dc57cf1e02f0e17d042b6e0da67f37baf51e5bbaa396e02671c3ba3553f40265e80', 16, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-09 00:02:07', '2022-06-09 00:02:07', '2023-06-08 17:02:07'),
('ae4ef4ed8ac244b639cd24c3e58789d455f57451f8530aa541413d4abd72f3e00b31c7b9d31a3c2b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 20:29:00', '2022-06-13 20:29:00', '2023-06-13 13:29:00'),
('ae9855d27ab4b820b06d18910cabd7b8b4e444d7404935107a9029832ac1f2d8f444b9474a28a5e2', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:33:04', '2022-06-16 18:33:04', '2023-06-16 11:33:04'),
('b060f94e49050f39f5adbb50b60ac0e74d2452e123b8f66818315a8235a185b688223d7d10518740', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:28:16', '2022-06-06 21:28:16', '2023-06-06 14:28:16'),
('b0ea4304eab007294af5d35f29aa452c07303f1c13fdfa4477d41fab73ef95f5b4126e2ac6e3bf2e', 6, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-09 13:15:27', '2022-05-09 13:15:27', '2023-05-09 18:15:27'),
('b1fbc75ed6c886add8ca1d6edd5f90a4aed6ab910def92f9dba50c3efd1f0e83c726a101bf7fdd0a', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 23:47:43', '2022-06-14 23:47:43', '2023-06-14 16:47:43'),
('b2debcc289f059f2370ed488f90eabd27993d5db670ae867f1524ca4afd7fd57f0b8157c3da405cd', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:26:18', '2022-06-06 21:26:18', '2023-06-06 14:26:18'),
('b31633c9ead933891f2f68d3729ff8951727a3f238f98f3fc033715d8b1541a95f19054f3cfe6dcf', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:52:46', '2022-06-15 00:52:46', '2023-06-14 17:52:46'),
('b3b4f2a639decac0449a4c3d41a5810a233898fe55c21ea3e0f3dced4625cc3a1910f8049023eda5', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 18:14:38', '2022-06-06 18:14:38', '2023-06-06 11:14:38'),
('b3d8d6fa29b1ba3e79db3d374ad4df92b6cacc3c21683e71c8fca4a78e5f35ca2f67885410420b2b', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 18:12:55', '2022-08-23 18:12:55', '2023-08-23 11:12:55'),
('b5499b4274ba42f775de14ed7cee8c1bee8143d6708e14ac4e1a0434a9b27c6cc663c9965ad30b88', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:50:50', '2022-06-06 21:50:50', '2023-06-06 14:50:50'),
('b605a3835ea2c7fe70fb2d66c3fd91dfac13d07166b911d5a8704f72413fe1b0eeb1c1b173cbf780', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:28:29', '2022-06-06 21:28:29', '2023-06-06 14:28:29'),
('b6e4fc5b57ad618d5ad26b7d6b061c8d5c6caf4e901dec590027d15843def2de3c312d3325d1b89b', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:48:47', '2022-06-01 16:48:47', '2023-06-01 09:48:47'),
('bf530ffe24e51b4a51731bb0a57fdea82061c5400e1a18d4f4590819b93494919c55c05886c859cb', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 21:52:28', '2022-05-21 21:52:28', '2023-05-21 14:52:28'),
('c233dadbf48de8a43b7c923fdc6ab791cfe6ed4c02a9cb9fd9c47ab9c1dae5ada732ce247bef0f34', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-08-23 20:30:49', '2022-08-23 20:30:49', '2023-08-23 13:30:49'),
('c28120df7f01a4112c433723b4f81cbfdde6a8c7c95638a574a57385b10d215796321805a70fa4ce', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 18:34:03', '2022-06-16 18:34:03', '2023-06-16 11:34:03'),
('c2daeaf554a1f3a549ee2553567b61378f79513e13a29e9c4b8e355866de341672c417466bebe173', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 00:24:50', '2022-06-15 00:24:50', '2023-06-14 17:24:50'),
('c3527b207741d48d735689772b7ee0f9ff3abff511a9da7e7ac75128b55c82c38c2be2d4b9c5945c', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-30 18:40:34', '2022-05-30 18:40:34', '2023-05-30 11:40:34'),
('c70a797ebfab0569c4377f6bc31019eeea2eda5f8cc4a9b08e340df558088b4820e28b843a3a3d3f', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 01:25:49', '2022-06-16 01:25:49', '2023-06-15 18:25:49'),
('c77543e65d6a49a4a4d3c18b731918e274060c344bc4a88eda444e866516295463f02be85adc1b60', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:07:30', '2022-06-02 20:07:30', '2023-06-02 13:07:30'),
('ca3b826892193f67813d19c3cd005f5e1af3e340b7e7486a5d21476b73dc3f81966689c519713bff', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-27 19:28:36', '2022-05-27 19:28:36', '2023-05-27 12:28:36'),
('d08ba7f419a24793d02f39cadbe208aa7496787e89b053d5697687b331ebad60454e91ba29b247b4', 18, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-13 20:19:04', '2022-06-13 20:19:04', '2023-06-13 13:19:04'),
('d11412426dd9d72e4c81f68ab899704942f447214db21cb04260d6806945f15354dd9379240a4694', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 22:10:21', '2022-05-21 22:10:21', '2023-05-21 15:10:21'),
('d1bc2fc765a8e0062be19ae29c694a96c898d765f61131d6bb100087e0309e1cf07d3a5044409c34', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 16:48:23', '2022-06-01 16:48:23', '2023-06-01 09:48:23'),
('d216880a222a13b0ac57021b6606aaeb60aff4bf2b69d7a18726ef1f673f3a3398dbaaa1a177f699', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:52:38', '2022-06-06 21:52:38', '2023-06-06 14:52:38'),
('d27f16cf97748ce5ecdfd5331ccc1e53b0c99bfe548d165fd0455156f239af52b98b6488f19d908f', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-11 18:43:53', '2022-06-11 18:43:53', '2023-06-11 11:43:53'),
('d336225ea4c68154c1d855d8e4a1365a4cddcc48fa16ae2c53f06e48764d76779d80c66c8efe3ace', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-07 00:30:07', '2022-06-07 00:30:07', '2023-06-06 17:30:07'),
('d4e0b07696fc15a86e438e0c9fa25c6e1fadd93b63108526fc4f29f436d2fbe7d9967fd94058ec65', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-04 00:48:00', '2022-06-04 00:48:00', '2023-06-03 17:48:00'),
('d5b501c689ef7b890660637a3a96ca7c8922d0120f709b2a2a1bb3d87b0c2d99a4649ea5d542314a', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 16:38:26', '2022-05-21 16:38:26', '2023-05-21 09:38:26'),
('d6ef36aba099e0d612c27d7fc12e752f14068ed7a203d3630281aaba08fbb0910dce049c1b68a3de', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 21:46:03', '2022-06-06 21:46:03', '2023-06-06 14:46:03'),
('dd40cd8200c30d3794e81781520665908fccb8b131344a37c81e608250fc45e03f6c533f04f5e232', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:00:23', '2022-06-06 22:00:23', '2023-06-06 15:00:23'),
('dd6926769fbbd166d18b422e956e07f1fcbf6916d8cdc1ee16929ae785dff182810a94fb96620210', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 19:16:07', '2022-06-16 19:16:07', '2023-06-16 12:16:07'),
('e060808e6ce3e43424d854bba170a8a23785b15031dfdee21d96298510fd775225416edd3e9bb157', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 01:25:52', '2022-06-16 01:25:52', '2023-06-15 18:25:52'),
('e070406df626c94fcdd9c368c7d1ed67590c6752f60b057c1dbceefb4aae307376ff4204af2ca8b4', 19, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-15 01:00:29', '2022-06-15 01:00:29', '2023-06-14 18:00:29'),
('e137402c51148c0e2cdcb01d5c888ee36fde538ed30c05156565b83f066cb2d78c9a706d9dd94b06', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 20:03:52', '2022-05-21 20:03:52', '2023-05-21 13:03:52'),
('e1719ed6998f2733f2724139d5cf6f6828355e31d3588acd26da1d4a0bda75779b38b7ad90f1a478', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 01:21:55', '2022-06-16 01:21:55', '2023-06-15 18:21:55'),
('e1832c609322d70895cd11fd46aed15f88a961dd8c4248326f8acb706380a1119c1e7ad566c862f1', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 23:21:38', '2022-06-02 23:21:38', '2023-06-02 16:21:38'),
('e593f007b070cfaedf58a7dc8943d3579ec04a9cb04a684653ca37dff9ecdba6650074b95bacc95b', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-16 21:10:21', '2022-06-16 21:10:21', '2023-06-16 14:10:21'),
('e94583491dbcc0a3e65521472763247bab93233ec7110007b712e4100c5954aa8fba195affb56a69', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:12:41', '2022-06-02 20:12:41', '2023-06-02 13:12:41'),
('e9b39e1cb7fef8de9f0e9321b92b227f294474651e548cad22380cdf28ad601166f6f960edf72d08', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 18:35:45', '2022-06-01 18:35:45', '2023-06-01 11:35:45'),
('ea7c4279f711c316d21c77fb727d9107df3024f6a6e17932f30a37bedc7ccd899652a4436d92673f', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 22:04:58', '2022-05-19 22:04:58', '2023-05-19 15:04:58'),
('eb7ab2514ae3ab4b9abafc81491274c6776271f434663655e48efbe070ef9075aae683fe9414db49', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-03 22:02:44', '2022-06-03 22:02:44', '2023-06-03 15:02:44'),
('ebb9a55ceeba6d07dc72637ef0810f1e3b71e900bafb71f4ec35bf1b298b7bf9195f9e5f9bf86cb0', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 18:58:50', '2022-06-06 18:58:50', '2023-06-06 11:58:50'),
('ee683ad1e42c2b65dfc446f0a45323327b11b2d90f519154649ebf3c23a2ec075c8c06b063fe8b6c', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-14 19:39:04', '2022-06-14 19:39:04', '2023-06-14 12:39:04'),
('ee96a73c5e482e0ffb59e3882234c4c39887f3c3d1af7768109200c4d8cea84d7b8ae4f0490834cf', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-11 18:40:09', '2022-06-11 18:40:09', '2023-06-11 11:40:09'),
('efa49c6da6dfa80232fa6c083b5ad936bf2fa6cb24aa6d8251a654fbc45c9b2874c86b1b2bbbdd64', 21, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-03 01:45:24', '2022-07-03 01:45:24', '2023-07-02 18:45:24'),
('efc9db6fa912e4313c1a99b8f097448b24320208c71f82f67f0747c0ae6e297edae72b3455b36755', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 19:05:47', '2022-06-02 19:05:47', '2023-06-02 12:05:47'),
('f0403d2b66dfa2808d72a2b9c0eb9e995b2dc570a1a9fad4db36351db1db88c60f66ca5582e22acd', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-05 00:30:09', '2022-06-05 00:30:09', '2023-06-04 17:30:09'),
('f4dcc2b61c42e77e3a39460e46d46ccc553585261707ce395684f293d911da9046880b06b3209d75', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-01 21:04:11', '2022-06-01 21:04:11', '2023-06-01 14:04:11'),
('f50b37678beb961f28472321238badf347ccd8e93d203433db662aebb9797633739a95ff33bbdfac', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:09:41', '2022-06-06 22:09:41', '2023-06-06 15:09:41'),
('f637a42c15a5dbe15b3329eb5ef935c8012caa6188b1008bd40975c81b2a5c882c25c722b47787f3', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:06:18', '2022-06-02 20:06:18', '2023-06-02 13:06:18'),
('fa07aac7a24c03aa7bdac18c571c2b5b02b1463ccb681a2345fbe5ef1692fd459d43893af24aa855', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-06 22:00:23', '2022-06-06 22:00:23', '2023-06-06 15:00:23'),
('fb27cb78da63129abc0fd4a844ef67c1d807bf1252dac71b9dac05b0ba7f187b91b4a8f8c81edc29', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 13:43:19', '2022-06-02 13:43:19', '2023-06-02 06:43:19'),
('fb92a09d529d689ae17a4d49a046303102651a657a19845e17721503c738ca93657bd6713b295eab', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 19:13:19', '2022-05-21 19:13:19', '2023-05-21 12:13:19'),
('fcf3ab9319d378b461ed0bdcbf17a02b4a56cfa326946393f9a5bdb01a84c44b864fb7b0c8eabbaf', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-11 18:07:53', '2022-05-11 18:07:53', '2023-05-11 11:07:53'),
('fd008672d75203997d67ce4d0ba3f7f6a23e960d8cd7ed8d5ac084262c9eced10f282bf154b2e6ed', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-06-02 20:07:33', '2022-06-02 20:07:33', '2023-06-02 13:07:33'),
('fd1a066fa1c502cfb747113d3e95f00c450db4d4c9db5731e0f9fab7f7229e370df4751dfe3aaee9', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 18:51:15', '2022-05-21 18:51:15', '2023-05-21 11:51:15');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '0SCNZrVA6MxOav6GCEQIfphpzTaoVz0BOJp96GYT', NULL, 'http://localhost', 1, 0, 0, '2020-12-18 05:42:14', '2020-12-18 05:42:14'),
(2, NULL, 'Laravel Password Grant Client', 'xSoG1YOrG03D7VFgvD1lgmTAOGli10V1JP5LUx7P', 'users', 'http://localhost', 0, 1, 0, '2020-12-18 05:42:14', '2020-12-18 05:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-18 05:42:14', '2020-12-18 05:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otp_numbers`
--

CREATE TABLE `otp_numbers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otp_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '8769',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_verified` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `otp_numbers`
--

INSERT INTO `otp_numbers` (`id`, `phone_number`, `otp_code`, `email`, `is_verified`, `user_id`, `expires`, `created_at`, `updated_at`) VALUES
(4, '+923432322008', NULL, NULL, 1, NULL, NULL, '2020-10-11 05:44:35', '2022-06-04 16:16:06'),
(10, '+918780807379', NULL, 'gargi@mailinator.com', 1, 11, NULL, '2020-10-21 14:51:25', '2020-10-21 14:57:07'),
(22, NULL, '5560', NULL, 0, NULL, '2020-11-01 14:16:19', '2020-11-01 14:11:19', '2020-11-01 14:11:19'),
(23, '9925014279', '1021', NULL, 0, NULL, '2020-11-01 15:43:22', '2020-11-01 15:38:22', '2020-11-01 15:38:22'),
(25, '7041439950', '5823', NULL, 0, NULL, '2020-11-01 15:47:17', '2020-11-01 15:42:17', '2020-11-01 15:42:17'),
(29, '++918866765733', '7803', NULL, 0, NULL, '2020-11-11 10:48:48', '2020-11-10 18:12:34', '2020-11-11 10:43:48'),
(30, '+918866765733', NULL, NULL, 1, NULL, NULL, '2020-11-10 18:52:30', '2020-11-11 08:32:05'),
(31, '+91(886) 676-5733', '2265', NULL, 0, NULL, '2020-11-11 13:58:28', '2020-11-11 12:51:52', '2020-11-11 13:53:28'),
(32, '+923452433770', '3907', NULL, 0, NULL, '2022-05-13 18:00:21', '2022-05-07 04:36:15', '2022-05-13 17:55:21'),
(33, '+923310257603', '7530', NULL, 0, NULL, '2022-05-07 04:45:31', '2022-05-07 04:38:34', '2022-05-07 04:40:31'),
(34, '+923432322010', NULL, NULL, NULL, 6, NULL, '2022-05-09 11:47:19', '2022-05-09 13:15:27'),
(35, '+923340289059', NULL, NULL, 1, NULL, NULL, '2022-05-10 03:12:17', '2022-05-19 23:22:54'),
(36, '+923452455880', '2041', NULL, 0, NULL, '2022-05-13 17:51:57', '2022-05-13 17:46:57', '2022-05-13 17:46:57'),
(37, '+923428523609', '2171', NULL, 0, NULL, '2022-05-13 17:52:45', '2022-05-13 17:47:45', '2022-05-13 17:47:45'),
(38, '+923102678962', '7473', NULL, 0, NULL, '2022-05-13 17:54:49', '2022-05-13 17:49:09', '2022-05-13 17:49:49'),
(39, '+923452477880', '7264', NULL, 0, NULL, '2022-05-13 18:00:00', '2022-05-13 17:55:00', '2022-05-13 17:55:00'),
(40, '+923428238086', '8130', NULL, 0, NULL, '2022-05-13 18:02:13', '2022-05-13 17:56:41', '2022-05-13 17:57:13'),
(41, '+923452288901', '2179', NULL, 0, NULL, '2022-05-13 18:07:04', '2022-05-13 18:02:04', '2022-05-13 18:02:04'),
(42, '+923452288441', '4297', NULL, 0, NULL, '2022-05-13 18:10:16', '2022-05-13 18:05:16', '2022-05-13 18:05:16'),
(43, '+923423423423', '8388', NULL, 0, NULL, '2022-05-13 18:14:37', '2022-05-13 18:09:37', '2022-05-13 18:09:37'),
(44, '+923452433990', '3406', NULL, 0, NULL, '2022-05-13 18:25:25', '2022-05-13 18:20:25', '2022-05-13 18:20:25'),
(45, '+923158255454', '5536', NULL, 0, NULL, '2022-05-13 18:27:45', '2022-05-13 18:22:45', '2022-05-13 18:22:45'),
(46, '+923102095781', '7232', NULL, 0, NULL, '2022-05-13 18:45:11', '2022-05-13 18:40:11', '2022-05-13 18:40:11'),
(47, '+9203102678961', '3938', NULL, 0, NULL, '2022-05-13 19:05:59', '2022-05-13 18:58:54', '2022-05-13 19:00:59'),
(48, '+9203102678960', '3828', NULL, 0, NULL, '2022-05-13 19:14:50', '2022-05-13 19:09:50', '2022-05-13 19:09:50'),
(49, '+923452155104', '7047', NULL, 0, NULL, '2022-05-13 19:33:04', '2022-05-13 19:28:04', '2022-05-13 19:28:04'),
(50, '+923125612478', '2146', NULL, 0, NULL, '2022-05-13 20:09:40', '2022-05-13 20:04:40', '2022-05-13 20:04:40'),
(51, '+923362154122', '9657', NULL, 0, NULL, '2022-05-13 20:11:03', '2022-05-13 20:06:03', '2022-05-13 20:06:03'),
(52, '+923340289059', NULL, NULL, 1, 7, NULL, '2022-05-13 23:06:37', '2022-05-13 23:06:55'),
(53, '+923111964952', NULL, NULL, 1, NULL, NULL, '2022-05-18 21:00:56', '2022-05-18 21:01:01'),
(54, '+923102667812', NULL, NULL, 1, 8, NULL, '2022-05-18 21:02:27', '2022-05-21 19:13:01'),
(55, '+923340289059', NULL, NULL, 1, 9, NULL, '2022-05-19 17:44:09', '2022-05-19 18:02:35'),
(56, '+923102656545', NULL, NULL, 1, NULL, NULL, '2022-05-19 22:04:16', '2022-05-19 22:04:22'),
(57, '3102656545', '8769', NULL, NULL, 10, NULL, '2022-05-19 22:04:58', '2022-05-19 22:04:58'),
(58, '3111964952', '8769', NULL, NULL, 11, NULL, '2022-05-21 16:46:28', '2022-05-21 16:46:28'),
(59, 'PK3452698563', NULL, NULL, 1, 12, NULL, '2022-05-21 19:13:19', '2022-06-11 23:47:35'),
(60, '013213245445', '2273', NULL, 0, NULL, '2022-05-27 00:39:11', '2022-05-27 00:34:11', '2022-05-27 00:34:11'),
(61, 'bv', '7380', NULL, 0, NULL, '2022-05-27 15:29:34', '2022-05-27 15:24:34', '2022-05-27 15:24:34'),
(62, 'asd', '5062', NULL, 0, NULL, '2022-05-27 17:54:05', '2022-05-27 17:49:05', '2022-05-27 17:49:05'),
(63, 'dsad', '4192', NULL, 0, NULL, '2022-05-27 17:55:35', '2022-05-27 17:50:35', '2022-05-27 17:50:35'),
(64, 'sad', '3785', NULL, 0, NULL, '2022-05-27 17:58:30', '2022-05-27 17:53:30', '2022-05-27 17:53:30'),
(65, 'sas', '1792', NULL, 0, NULL, '2022-05-27 18:03:58', '2022-05-27 17:58:58', '2022-05-27 17:58:58'),
(66, 'dsfsdf', '2382', NULL, 0, NULL, '2022-05-27 18:08:36', '2022-05-27 18:03:36', '2022-05-27 18:03:36'),
(67, '02125231231', '3524', NULL, 0, NULL, '2022-05-27 19:16:57', '2022-05-27 19:11:57', '2022-05-27 19:11:57'),
(68, '3125454981', '8769', NULL, NULL, 13, NULL, '2022-05-30 18:40:34', '2022-05-30 18:40:34'),
(69, 'PK3368227469', NULL, NULL, 1, 14, NULL, '2022-06-04 01:07:59', '2022-08-23 16:33:13'),
(70, '3432322008', '8769', NULL, NULL, 15, NULL, '2022-06-04 16:16:53', '2022-06-04 16:16:53'),
(71, 'PK3102678695', '9318', NULL, 0, NULL, '2022-06-04 23:33:36', '2022-06-04 23:28:36', '2022-06-04 23:28:36'),
(72, 'PK3259647521', '7110', NULL, 0, NULL, '2022-06-04 23:37:31', '2022-06-04 23:32:31', '2022-06-04 23:32:31'),
(73, 'PK3102698742', '5492', NULL, 0, NULL, '2022-06-04 23:40:37', '2022-06-04 23:35:37', '2022-06-04 23:35:37'),
(74, 'PK3104589621', '3987', NULL, 0, NULL, '2022-06-04 23:43:43', '2022-06-04 23:38:43', '2022-06-04 23:38:43'),
(75, 'PK3124589666', '7474', NULL, 0, NULL, '2022-06-04 23:48:34', '2022-06-04 23:43:34', '2022-06-04 23:43:34'),
(76, 'PK3102556110', NULL, NULL, 1, NULL, NULL, '2022-06-04 23:45:42', '2022-06-04 23:51:32'),
(77, 'PK3102456691', NULL, NULL, 1, NULL, NULL, '2022-06-05 00:29:25', '2022-06-05 00:29:47'),
(78, 'PK3102698618', NULL, NULL, 1, NULL, NULL, '2022-06-06 15:04:41', '2022-06-06 15:05:09'),
(79, '+923102698745', '7840', NULL, 0, NULL, '2022-06-06 15:31:27', '2022-06-06 15:26:27', '2022-06-06 15:26:27'),
(80, '+923105621789', '3458', NULL, 0, NULL, '2022-06-06 15:53:27', '2022-06-06 15:48:27', '2022-06-06 15:48:27'),
(81, 'PK3452678941', NULL, NULL, 1, NULL, NULL, '2022-06-06 15:56:11', '2022-06-06 15:56:41'),
(82, 'PK3102897412', '9855', NULL, 0, NULL, '2022-06-06 16:05:43', '2022-06-06 16:00:43', '2022-06-06 16:00:43'),
(83, '+923102689561', NULL, NULL, 1, NULL, NULL, '2022-06-06 16:08:40', '2022-06-06 16:10:45'),
(84, 'PK3452678961', NULL, NULL, 1, NULL, NULL, '2022-06-06 16:14:58', '2022-06-06 16:15:04'),
(85, 'PK3106454574', '9993', NULL, 0, NULL, '2022-06-06 16:22:15', '2022-06-06 16:17:15', '2022-06-06 16:17:15'),
(86, '+923124569871', '9043', NULL, 0, NULL, '2022-06-06 16:25:52', '2022-06-06 16:20:52', '2022-06-06 16:20:52'),
(87, 'PK3102645378', NULL, NULL, 1, NULL, NULL, '2022-06-09 00:00:35', '2022-06-09 00:01:04'),
(88, '3102645378', '8769', NULL, NULL, 16, NULL, '2022-06-09 00:02:07', '2022-06-09 00:02:07'),
(89, '3452698563', '8769', NULL, NULL, 17, NULL, '2022-06-11 23:48:18', '2022-06-11 23:48:18'),
(90, 'PK3102647895', NULL, NULL, 1, NULL, NULL, '2022-06-13 20:18:08', '2022-06-13 20:18:16'),
(91, '3102647895', '8769', NULL, NULL, 18, NULL, '2022-06-13 20:19:03', '2022-06-13 20:19:03'),
(92, '3102698666', '8769', NULL, NULL, 19, NULL, '2022-06-15 01:00:29', '2022-06-15 01:00:29'),
(94, 'PK3002518623', NULL, NULL, 1, NULL, NULL, '2022-06-16 20:52:29', '2022-06-16 20:52:45'),
(95, 'PK3102698102', NULL, NULL, 1, NULL, NULL, '2022-06-16 20:57:35', '2022-06-16 21:03:07'),
(96, 'PK3452433770', NULL, NULL, 1, NULL, NULL, '2022-06-16 21:06:47', '2022-06-16 21:07:31'),
(97, 'PK3211234567', NULL, NULL, 1, NULL, NULL, '2022-06-18 00:24:57', '2022-06-18 00:26:10'),
(98, 'PK3131094717', '1077', NULL, 0, NULL, '2022-07-02 22:35:28', '2022-07-02 22:23:23', '2022-07-02 22:30:28'),
(99, 'PK3102699856', '6367', NULL, 0, NULL, '2022-07-03 01:28:45', '2022-07-03 01:23:45', '2022-07-03 01:23:45'),
(100, 'PK3109645312', NULL, NULL, 1, NULL, NULL, '2022-07-03 01:25:11', '2022-07-03 01:25:18'),
(101, 'PK3109866621', NULL, NULL, 1, NULL, NULL, '2022-07-03 01:27:04', '2022-07-03 01:27:14'),
(102, 'PK3102974612', '3930', NULL, 0, NULL, '2022-07-03 01:48:02', '2022-07-03 01:43:02', '2022-07-03 01:43:02'),
(103, 'PK3102697852', NULL, NULL, 1, NULL, NULL, '2022-07-03 01:45:04', '2022-07-03 01:45:09'),
(105, '3368227469', '8769', NULL, NULL, 22, NULL, '2022-08-23 16:36:12', '2022-08-23 16:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(2, 'browse_bread', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(3, 'browse_database', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(4, 'browse_media', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(5, 'browse_compass', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(6, 'browse_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(7, 'read_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(8, 'edit_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(9, 'add_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(10, 'delete_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(11, 'browse_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(12, 'read_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(13, 'edit_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(14, 'add_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(15, 'delete_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(16, 'browse_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(17, 'read_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(18, 'edit_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(19, 'add_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(20, 'delete_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(21, 'browse_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(22, 'read_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(23, 'edit_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(24, 'add_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(25, 'delete_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(26, 'browse_hooks', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(27, 'browse_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(28, 'read_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(29, 'edit_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(30, 'add_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(31, 'delete_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(32, 'browse_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(33, 'read_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(34, 'edit_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(35, 'add_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(36, 'delete_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(37, 'browse_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(38, 'read_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(39, 'edit_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(40, 'add_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(41, 'delete_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(42, 'browse_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(43, 'read_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(44, 'edit_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(45, 'add_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(46, 'delete_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(47, 'browse_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(48, 'read_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(49, 'edit_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(50, 'add_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(51, 'delete_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(52, 'browse_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(53, 'read_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(54, 'edit_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(55, 'add_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(56, 'delete_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(57, 'browse_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(58, 'read_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(59, 'edit_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(60, 'add_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(61, 'delete_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(62, 'browse_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(63, 'read_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(64, 'edit_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(65, 'add_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(66, 'delete_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(67, 'browse_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(68, 'read_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(69, 'edit_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(70, 'add_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(71, 'delete_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(82, 'browse_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(83, 'read_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(84, 'edit_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(85, 'add_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(86, 'delete_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(87, 'browse_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(88, 'read_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(89, 'edit_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(90, 'add_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(91, 'delete_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(92, 'browse_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(93, 'read_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(94, 'edit_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(95, 'add_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(96, 'delete_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(102, 'browse_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(103, 'read_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(104, 'edit_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(105, 'add_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(106, 'delete_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(107, 'browse_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(108, 'read_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(109, 'edit_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(110, 'add_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(111, 'delete_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(117, 'browse_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(118, 'read_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(119, 'edit_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(120, 'add_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(121, 'delete_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(122, 'browse_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(123, 'read_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(124, 'edit_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(125, 'add_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(126, 'delete_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(127, 'browse_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(128, 'read_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(129, 'edit_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(130, 'add_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(131, 'delete_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(152, 'browse_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(153, 'read_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(154, 'edit_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(155, 'add_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(156, 'delete_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(177, 'browse_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(178, 'read_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(179, 'edit_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(180, 'add_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(181, 'delete_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(182, 'browse_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(183, 'read_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(184, 'edit_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(185, 'add_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(186, 'delete_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(187, 'browse_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(188, 'read_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(189, 'edit_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(190, 'add_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(191, 'delete_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1);

-- --------------------------------------------------------

--
-- Table structure for table `photourls`
--

CREATE TABLE `photourls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `firebase_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `photourls`
--

INSERT INTO `photourls` (`id`, `name`, `type`, `owner_id`, `product_id`, `firebase_url`, `created_at`, `updated_at`) VALUES
(1, '1592682500000-img5.jpg', 'product', 27, 1, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1592682500000-img5.jpg?alt=media&token=c7ef9eeb-505a-4f9f-a33d-b2accb630532', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(2, '1594569524000-img7.jpg', 'product', 21, 2, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594569524000-img7.jpg?alt=media&token=bda55769-4cb9-4b22-8fc7-6c525013169d', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(3, '1594568287000-img4.jpg', 'product', 21, 3, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594568287000-img4.jpg?alt=media&token=de9352e6-e3c6-4c05-bbf7-281c8f2f2cd4', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(4, '1591299931000-6f3b215b6530bbd88dfc7f3d3b390aa1.jpg', 'product', 27, 4, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591299931000-6f3b215b6530bbd88dfc7f3d3b390aa1.jpg?alt=media&token=943e91ab-1057-45d7-b11d-e6f522016876', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(5, '1591526431000-0ef2d09faab8f4aa03a6a898472a54e1.jpg', 'product', 27, 5, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591526431000-0ef2d09faab8f4aa03a6a898472a54e1.jpg?alt=media&token=9e515153-1660-4a33-b286-1af04489456e', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(6, '1591268045000-1ec2838d5cd0cd53eaface83a1bfa921.jpg', 'product', 27, 6, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268045000-1ec2838d5cd0cd53eaface83a1bfa921.jpg?alt=media&token=0a9eb420-4204-4ed3-9f23-63a946b7c4f5', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(7, '1591278598000-5a24033a48ce0dfe82ca9ed68dff0d46.jpg', 'product', 27, 7, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591278598000-5a24033a48ce0dfe82ca9ed68dff0d46.jpg?alt=media&token=fed9533d-e763-4230-8933-2c3f924852ce', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(8, '1592682419000-img7.jpg', 'product', 27, 8, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1592682419000-img7.jpg?alt=media&token=4cbc3d8e-326b-4b8b-bfea-5e2f5a9c840c', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(9, '1591267801000-0bfcbc2ef78ad1ec040541ab72000bfe.jpg', 'product', 27, 9, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591267801000-0bfcbc2ef78ad1ec040541ab72000bfe.jpg?alt=media&token=27fe59d5-3cff-45b9-9aa9-d9dfc99d5fb8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(10, '1591698232000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 10, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591698232000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=991f9cd1-002b-40c0-a313-c8066df6a2ad', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(11, '1591278655000-5120 x 2880.jpg', 'product', 27, 11, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591278655000-5120%20x%202880.jpg?alt=media&token=2ae9d903-5cf8-4580-9620-ad87f7507019', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(12, '1591526165000-1 JAVA SCRIPT 2.png', 'product', 27, 12, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591526165000-1%20JAVA%20SCRIPT%202.png?alt=media&token=97ec6348-aac6-4c1d-8d0b-4c810c7ceba7', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(13, '1594569784000-img5.jpg', 'product', 21, 13, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594569784000-img5.jpg?alt=media&token=83c46e59-f287-49bb-8852-b971534f8908', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(14, '1591268493000-1c6249eced8071ae829575b0b59a9712.jpg', 'product', 27, 14, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268493000-1c6249eced8071ae829575b0b59a9712.jpg?alt=media&token=0de2601e-67c5-4ea3-a78c-11fcab3b64b9', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(15, '1591268423000-1c46e74a667b88260e699408d659ce38.jpg', 'product', 27, 15, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268423000-1c46e74a667b88260e699408d659ce38.jpg?alt=media&token=a8941c94-bd35-4936-9b71-3d0a9c79af54', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(16, '1591268341000-1b4a55d9d5fa19756be4c73500007934.jpg', 'product', 27, 16, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268341000-1b4a55d9d5fa19756be4c73500007934.jpg?alt=media&token=7904fb13-e194-43fd-a145-857f647c07e8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(17, '1591279007000-Wall92.jpg', 'product', 27, 17, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591279007000-Wall92.jpg?alt=media&token=8d3de557-9e62-4ed6-a5c6-2d431e51cf98', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(18, '1591268446000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 18, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268446000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=ded2709a-b822-46e3-97f7-be183348c711', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(19, '1591268182000-1d2430b7f93bd1867186c2f15b076f74.jpg', 'product', 27, 19, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268182000-1d2430b7f93bd1867186c2f15b076f74.jpg?alt=media&token=dcfda565-99e5-470c-8e2c-c3478400674b', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(20, '1591268186000-2a879d3c957727694634d9e5234bd94c.jpg', 'product', 27, 19, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268186000-2a879d3c957727694634d9e5234bd94c.jpg?alt=media&token=115d410f-8dd9-4b7c-9a79-a54b1a70bdd8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(21, '1591387090000-052ce910cc5b24a696b788d603407adb.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387090000-052ce910cc5b24a696b788d603407adb.jpg?alt=media&token=e783c8fc-c9d2-460e-8d45-3cae920495d8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(22, '1591387093000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387093000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=f91684b7-efa5-4050-895d-3fa4de6727f8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(23, '1591387096000-9aadf953cee378f14496675c7f23095a.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387096000-9aadf953cee378f14496675c7f23095a.jpg?alt=media&token=44aceea6-5098-43fb-9742-0316a2821bb8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(24, '1591379023000-2f409645a0f2d134c77cac7f1ea50cb9.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379023000-2f409645a0f2d134c77cac7f1ea50cb9.jpg?alt=media&token=00ed7ab9-2dbd-4fb3-bd1b-5abd095a6d64', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(25, '1591379024000-3aa0dd5eaffc4dddfd637990f2cead48.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379024000-3aa0dd5eaffc4dddfd637990f2cead48.jpg?alt=media&token=0fabe708-71de-4452-a0ca-f9471f63a7e2', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(26, '1591379026000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379026000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=f7805d43-2e51-4c32-97d9-b8d4a41fec06', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(27, '1591379028000-3edfbb5f58144180b9933e9df562e273.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379028000-3edfbb5f58144180b9933e9df562e273.jpg?alt=media&token=0c151a53-edf8-42f2-a2d7-2e771c754bd4', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(28, '1591268327000-3f44c357d1648815285bd5daaa629277.jpg', 'product', 27, 24, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268327000-3f44c357d1648815285bd5daaa629277.jpg?alt=media&token=a4748afa-3628-4b14-aa06-36f5856b74fd', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(37, 'ec34ead995bdd8ef37d8db98b199234a.png', 'product', 12, 37, 'http://test.waapsdeveloper.com/storage/app/images/products/ec34ead995bdd8ef37d8db98b199234a.png', '2022-05-25 19:34:53', '2022-05-25 19:34:53'),
(38, 'ec34ead995bdd8ef37d8db98b199234a.png', 'product', 12, 37, 'http://test.waapsdeveloper.com/storage/app/images/products/ec34ead995bdd8ef37d8db98b199234a.png', '2022-05-25 19:34:53', '2022-05-25 19:34:53'),
(39, 'b48efb5c233b8995433ed17bb9033641.png', 'product', 12, 38, 'http://test.waapsdeveloper.com/storage/app/images/products/b48efb5c233b8995433ed17bb9033641.png', '2022-05-25 20:20:07', '2022-05-25 20:20:07'),
(40, '41fa5000738f711bed1da832f92f83e3.png', 'product', 12, 39, 'http://test.waapsdeveloper.com/storage/app/images/products/41fa5000738f711bed1da832f92f83e3.png', '2022-06-01 15:59:32', '2022-06-01 15:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `added_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra_labels` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `geohash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insured` int(1) DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `quantities_available` double NOT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `added_by`, `category_id`, `description`, `extra_labels`, `geohash`, `insured`, `latitude`, `longitude`, `name`, `price`, `quantities_available`, `document_id`, `created_at`, `updated_at`, `deleted_at`, `condition_id`, `brand`, `vehicle_id`) VALUES
(1, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrtvgd97ff', 1, 24.936448, 67.0531584, 'Microsoft Os', 1500, 20, '3dyvREeXZgKq9jeB0Qp6', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(2, '21', '31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttfwqteh', 0, 24.887913, 67.0583857, 'Twilight', 855, 2, '3phqgpgMrIIYLqM93pPs', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(3, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrtktu8r8u', 1, 24.8607343, 67.0011364, 'Product 1', 44, 100, '7LhSKGikI792VLtRQww7', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(4, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 100058', 5000, 100, '7dZstmkdkRiJGHq7NiVn', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(5, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttwjve1k', 0, 24.9069568, 67.0466048, 'Product 1', 44, 100, 'BHjpKH27pHoAmjhYhwIo', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(6, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"fooo\", \"freeee\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'table', 5000, 110, 'G6uF27QOm0LaQQIsmnGs', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(7, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Ubuntu Dis', 4455, 1000, 'HCTrjwMd7Jrxw2eIsHFz', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(8, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"10%\"]', 'wy6u72v1c54', 1, 35.907757, 127.766922, 'Product Testing', 500, 50, 'OWQBdnyJSxv3pRb9z1Ho', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(9, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"boo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 1852', 500, 100, 'OovTWSiehRVSuDTAxzpb', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(10, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'ThyDIKrHsmSF1ZaA1wcl', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(11, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"limited\", \"75%\"]', 'tkrtwpywc9g', 1, 24.9167872, 67.0695424, 'Product 12345', 555, 100, 'UH9oVxe0QENFUKxjn6kj', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(12, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 500, 100, 'XL5dsPTLyOBSuvUwr6RQ', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(13, '21', '10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttfwqteh', 0, 24.887913, 67.0583857, 'just this', 4455, 10, 'XQC9CbK2iZGrAL4Qkczi', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(14, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"free\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'apsBwLrzjpwAE0HxH1lt', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(15, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 5005', 44, 100, 'eci1mVkIROjpijp4NInE', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(16, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'ehZNwDqI4zo9tjLAI0UR', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(17, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"freeee\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Testing 150', 44, 100, 'gOeXbBEwIx1hOEC86gNF', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(18, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'gb5vHGcjwmBliYMJAYBI', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(19, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"foo\"]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Computer Speakers', 50, 100, 'grOVNZPfozu6neSanTmA', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(20, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrwnnhj14y', 0, 24.994770260615, 67.066092302655, 'Product 1', 44, 100, 'juI5RrvyAnED5bEogv3I', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(21, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"ditro\", \"dsd\", \"asdas\"]', 'tkrtwpywc9g', 1, 24.9167872, 67.0695424, 'Linux DX', 44, 100, 'kmeQPn0BlPTGmkWVXcYC', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(22, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"fere\"]', 'tkrtvgd97ff', 0, 24.936448, 67.0531584, 'TESTING 128', 44, 100, 'lcAKxFTSAAkeyidpebq3', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(23, '21', '2', 'Rarely used, inflatable, good quality mattress', '[\"Furniture\", \"Mattress\"]', 'tusxf00d638', 0, 26.6752, 85.1668, 'Ezcomf Single Air Mattress', 45, 1, 'orTxKl0ERFmFWwTwtU0J', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(24, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"free\"]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Bed Sheet', 44, 100, 'xK4YvEcW3xs9ALzNvhod', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrator', NULL, '2020-12-18 05:55:42', '2020-12-26 02:37:24'),
(2, 'User', 'User', NULL, '2020-12-18 05:55:42', '2020-12-26 02:37:41'),
(3, 'Driver', 'Driver', NULL, '2020-12-26 00:58:36', '2020-12-26 00:58:36'),
(4, 'Consumer', 'Consumer', NULL, '2020-12-26 02:36:41', '2020-12-26 02:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-10-13 20:12:32', '2020-10-13 20:12:32'),
(2, 4, 1, NULL, NULL),
(3, 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `messeges_count` int(11) NOT NULL,
  `initiator_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recipient_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `messeges_count`, `initiator_id`, `recipient_id`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 1, '9', '21', NULL, '2020-11-28 12:14:33', '2020-11-28 12:14:34'),
(2, 2, '12', '9', NULL, '2022-05-21 20:27:59', '2022-05-24 21:43:00'),
(3, 1, '12', '12', NULL, '2022-05-25 21:43:19', '2022-05-25 21:43:19'),
(4, 3, '12', '21', NULL, '2022-06-01 18:45:38', '2022-06-02 12:00:56'),
(5, 2, '15', '21', NULL, '2022-06-04 16:27:47', '2022-06-04 16:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `runtime_roles`
--

CREATE TABLE `runtime_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `runtime_roles`
--

INSERT INTO `runtime_roles` (`id`, `user_id`, `role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 27, 0, 'Driver', '2020-09-11 03:08:37', '2020-09-11 03:08:37'),
(2, 27, 0, 'Driver', '2020-09-11 15:37:23', '2020-09-11 15:37:23'),
(3, 27, 0, 'Driver', '2020-09-11 16:39:03', '2020-09-11 16:39:03'),
(4, 27, 0, 'Driver', '2020-09-11 16:42:16', '2020-09-11 16:42:16'),
(5, 27, 0, 'Driver', '2020-09-11 16:48:06', '2020-09-11 16:48:06'),
(6, 27, 0, 'Driver', '2020-09-11 16:48:35', '2020-09-11 16:48:35'),
(7, 27, 0, 'Driver', '2020-09-11 16:54:39', '2020-09-11 16:54:39'),
(8, 27, 0, 'Driver', '2020-09-11 17:02:53', '2020-09-11 17:02:53'),
(9, 27, 0, 'Driver', '2020-09-11 17:04:21', '2020-09-11 17:04:21'),
(10, 27, 0, 'Driver', '2020-09-11 17:10:23', '2020-09-11 17:10:23'),
(11, 27, 0, 'Driver', '2020-09-11 17:13:12', '2020-09-11 17:13:12'),
(12, 27, 0, 'Driver', '2020-09-11 17:13:47', '2020-09-11 17:13:47'),
(13, 27, 0, 'Driver', '2020-09-11 17:18:08', '2020-09-11 17:18:08'),
(14, 27, 0, 'Driver', '2020-09-11 17:18:18', '2020-09-11 17:18:18'),
(15, 27, 0, 'Consumer', '2020-09-11 17:40:56', '2020-09-11 17:40:56'),
(16, 27, 0, 'Consumer', '2020-09-11 17:41:06', '2020-09-11 17:41:06'),
(17, 27, 0, 'Driver', '2020-09-12 12:56:58', '2020-09-12 12:56:58'),
(18, 27, 0, 'Consumer', '2020-09-12 12:59:08', '2020-09-12 12:59:08'),
(19, 27, 0, 'Driver', '2020-09-12 12:59:20', '2020-09-12 12:59:20'),
(20, 27, 0, 'Consumer', '2020-09-12 13:07:20', '2020-09-12 13:07:20'),
(21, 27, 0, 'Driver', '2020-09-12 13:34:13', '2020-09-12 13:34:13'),
(22, 27, 0, 'Driver', '2020-09-12 13:35:49', '2020-09-12 13:35:49'),
(23, 27, 0, 'Consumer', '2020-09-12 13:36:32', '2020-09-12 13:36:32'),
(24, 27, 0, 'Driver', '2020-09-15 16:37:42', '2020-09-15 16:37:42'),
(25, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(26, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(27, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(28, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(29, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(30, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(31, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(32, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(33, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(34, 41, 0, 'Driver', '2020-09-16 09:24:34', '2020-09-16 09:24:34'),
(35, 41, 0, 'Consumer', '2020-09-16 09:26:36', '2020-09-16 09:26:36'),
(36, 41, 0, 'Driver', '2020-09-19 05:08:44', '2020-09-19 05:08:44'),
(37, 27, 0, 'Driver', '2020-09-20 09:50:16', '2020-09-20 09:50:16'),
(38, 41, 0, 'Driver', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(39, 41, 0, 'Consumer', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(40, 41, 0, 'Consumer', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(41, 41, 0, 'Driver', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(42, 41, 0, 'Consumer', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(43, 41, 0, 'Driver', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(44, 41, 0, 'Driver', '2020-09-21 03:56:34', '2020-09-21 03:56:34'),
(45, 41, 0, 'Consumer', '2020-09-21 03:59:28', '2020-09-21 03:59:28'),
(46, 41, 0, 'Consumer', '2020-09-21 04:11:55', '2020-09-21 04:11:55'),
(47, 41, 0, 'Consumer', '2020-09-21 04:11:57', '2020-09-21 04:11:57'),
(48, 41, 0, 'Consumer', '2020-09-21 04:14:53', '2020-09-21 04:14:53'),
(49, 41, 0, 'Consumer', '2020-09-21 04:18:45', '2020-09-21 04:18:45'),
(50, 41, 0, 'Consumer', '2020-09-21 04:22:41', '2020-09-21 04:22:41'),
(51, 41, 0, 'Consumer', '2020-09-21 04:24:46', '2020-09-21 04:24:46'),
(52, 41, 0, 'Consumer', '2020-09-21 04:30:05', '2020-09-21 04:30:05'),
(53, 41, 0, 'Consumer', '2020-09-21 04:32:19', '2020-09-21 04:32:19'),
(54, 41, 0, 'Consumer', '2020-09-21 04:36:49', '2020-09-21 04:36:49'),
(55, 41, 0, 'Consumer', '2020-09-21 04:39:16', '2020-09-21 04:39:16'),
(56, 41, 0, 'Consumer', '2020-09-21 04:40:50', '2020-09-21 04:40:50'),
(57, 41, 0, 'Consumer', '2020-09-21 04:45:55', '2020-09-21 04:45:55'),
(58, 41, 0, 'Consumer', '2020-09-21 04:57:55', '2020-09-21 04:57:55'),
(59, 41, 0, 'Consumer', '2020-09-21 04:57:55', '2020-09-21 04:57:55'),
(60, 41, 0, 'Consumer', '2020-09-21 04:59:14', '2020-09-21 04:59:14'),
(61, 41, 0, 'Driver', '2020-09-21 05:00:20', '2020-09-21 05:00:20'),
(62, 41, 0, 'Consumer', '2020-09-21 05:00:24', '2020-09-21 05:00:24'),
(63, 41, 0, 'Consumer', '2020-09-21 05:09:01', '2020-09-21 05:09:01'),
(64, 41, 0, 'Consumer', '2020-09-21 05:10:01', '2020-09-21 05:10:01'),
(65, 41, 0, 'Consumer', '2020-09-21 05:10:50', '2020-09-21 05:10:50'),
(66, 41, 0, 'Consumer', '2020-09-21 05:13:15', '2020-09-21 05:13:15'),
(67, 41, 0, 'Consumer', '2020-09-21 05:15:21', '2020-09-21 05:15:21'),
(68, 41, 0, 'Consumer', '2020-09-21 05:18:26', '2020-09-21 05:18:26'),
(69, 41, 0, 'Consumer', '2020-09-21 05:20:53', '2020-09-21 05:20:53'),
(70, 41, 0, 'Consumer', '2020-09-21 05:21:38', '2020-09-21 05:21:38'),
(71, 41, 0, 'Consumer', '2020-09-21 05:23:52', '2020-09-21 05:23:52'),
(72, 41, 0, 'Consumer', '2020-09-21 05:25:30', '2020-09-21 05:25:30'),
(73, 41, 0, 'Consumer', '2020-09-21 05:27:16', '2020-09-21 05:27:16'),
(74, 41, 0, 'Consumer', '2020-09-21 05:28:15', '2020-09-21 05:28:15'),
(75, 41, 0, 'Consumer', '2020-09-21 05:30:27', '2020-09-21 05:30:27'),
(76, 41, 0, 'Driver', '2020-09-21 05:38:07', '2020-09-21 05:38:07'),
(77, 41, 0, 'Consumer', '2020-09-21 06:04:50', '2020-09-21 06:04:50'),
(78, 41, 0, 'Consumer', '2020-09-21 06:37:41', '2020-09-21 06:37:41'),
(79, 41, 0, 'Driver', '2020-09-21 06:37:48', '2020-09-21 06:37:48'),
(80, 41, 0, 'Consumer', '2020-09-21 06:38:03', '2020-09-21 06:38:03'),
(81, 41, 0, 'Consumer', '2020-09-21 06:59:12', '2020-09-21 06:59:12'),
(82, 41, 0, 'Consumer', '2020-09-21 07:18:03', '2020-09-21 07:18:03'),
(83, 41, 0, 'Driver', '2020-09-21 07:19:59', '2020-09-21 07:19:59'),
(84, 27, 0, 'Driver', '2020-09-21 18:59:09', '2020-09-21 18:59:09'),
(85, 41, 0, 'Consumer', '2020-09-23 10:35:14', '2020-09-23 10:35:14'),
(86, 41, 0, 'Consumer', '2020-09-23 11:15:17', '2020-09-23 11:15:17'),
(87, 2, 0, 'Consumer', '2020-09-25 15:02:16', '2020-09-25 15:02:16'),
(88, 2, 0, 'Driver', '2020-09-25 16:18:57', '2020-09-25 16:18:57'),
(89, 2, 0, 'Driver', '2020-09-25 16:23:06', '2020-09-25 16:23:06'),
(90, 2, 0, 'Driver', '2020-09-25 16:27:34', '2020-09-25 16:27:34'),
(91, 2, 0, 'Consumer', '2020-09-26 07:13:38', '2020-09-26 07:13:38'),
(92, 2, 0, 'Driver', '2020-09-26 07:17:53', '2020-09-26 07:17:53'),
(93, 2, 0, 'Driver', '2020-09-26 07:20:41', '2020-09-26 07:20:41'),
(94, 2, 0, 'Driver', '2020-09-26 07:23:54', '2020-09-26 07:23:54'),
(95, 2, 0, 'Driver', '2020-09-26 07:40:31', '2020-09-26 07:40:31'),
(96, 2, 0, 'Driver', '2020-09-26 07:51:57', '2020-09-26 07:51:57'),
(97, 2, 0, 'Driver', '2020-09-26 07:56:51', '2020-09-26 07:56:51'),
(98, 2, 0, 'Driver', '2020-09-26 08:16:22', '2020-09-26 08:16:22'),
(99, 2, 0, 'Driver', '2020-09-26 08:16:22', '2020-09-26 08:16:22'),
(100, 2, 0, 'Driver', '2020-09-26 08:31:37', '2020-09-26 08:31:37'),
(101, 2, 0, 'Driver', '2020-09-26 09:11:37', '2020-09-26 09:11:37'),
(102, 2, 0, 'Consumer', '2020-09-26 09:31:14', '2020-09-26 09:31:14'),
(103, 2, 0, 'Consumer', '2020-09-26 09:36:48', '2020-09-26 09:36:48'),
(104, 2, 0, 'Driver', '2020-09-26 09:37:08', '2020-09-26 09:37:08'),
(105, 2, 0, 'Consumer', '2020-09-26 09:38:17', '2020-09-26 09:38:17'),
(106, 2, 0, 'Driver', '2020-09-26 09:40:58', '2020-09-26 09:40:58'),
(107, 2, 0, 'Consumer', '2020-09-26 09:42:52', '2020-09-26 09:42:52'),
(108, 2, 0, 'Driver', '2020-09-26 09:51:58', '2020-09-26 09:51:58'),
(109, 2, 0, 'Driver', '2020-09-26 10:17:30', '2020-09-26 10:17:30'),
(110, 2, 0, 'Driver', '2020-09-26 10:29:47', '2020-09-26 10:29:47'),
(111, 2, 0, 'Driver', '2020-09-26 13:56:00', '2020-09-26 13:56:00'),
(112, 2, 0, 'Driver', '2020-09-26 14:20:50', '2020-09-26 14:20:50'),
(113, 2, 0, 'Driver', '2020-09-26 14:22:53', '2020-09-26 14:22:53'),
(114, 2, 0, 'Driver', '2020-09-26 14:28:06', '2020-09-26 14:28:06'),
(115, 2, 0, 'Driver', '2020-09-26 14:46:22', '2020-09-26 14:46:22'),
(116, 2, 0, 'Driver', '2020-09-26 14:53:30', '2020-09-26 14:53:30'),
(117, 2, 0, 'Consumer', '2020-09-26 15:30:55', '2020-09-26 15:30:55'),
(118, 2, 0, 'Driver', '2020-09-26 16:01:19', '2020-09-26 16:01:19'),
(119, 2, 0, 'Driver', '2020-09-26 16:17:38', '2020-09-26 16:17:38'),
(120, 2, 0, 'Driver', '2020-09-26 16:19:28', '2020-09-26 16:19:28'),
(121, 2, 0, 'Driver', '2020-09-28 16:22:13', '2020-09-28 16:22:13'),
(122, 2, 0, 'Driver', '2020-09-28 16:29:47', '2020-09-28 16:29:47'),
(123, 2, 0, 'Driver', '2020-09-28 16:35:23', '2020-09-28 16:35:23'),
(124, 2, 0, 'Driver', '2020-09-28 16:48:27', '2020-09-28 16:48:27'),
(125, 2, 0, 'Driver', '2020-09-28 16:57:07', '2020-09-28 16:57:07'),
(126, 2, 0, 'Driver', '2020-09-28 17:02:39', '2020-09-28 17:02:39'),
(127, 2, 0, 'Driver', '2020-09-28 17:02:39', '2020-09-28 17:02:39'),
(128, 2, 0, 'Driver', '2020-09-28 17:06:29', '2020-09-28 17:06:29'),
(129, 2, 0, 'Driver', '2020-09-28 17:08:33', '2020-09-28 17:08:33'),
(130, 2, 0, 'Driver', '2020-09-28 17:11:27', '2020-09-28 17:11:27'),
(131, 2, 0, 'Driver', '2020-09-28 17:22:19', '2020-09-28 17:22:19'),
(132, 2, 0, 'Driver', '2020-09-29 09:00:47', '2020-09-29 09:00:47'),
(133, 2, 0, 'Consumer', '2020-09-29 09:00:53', '2020-09-29 09:00:53'),
(134, 2, 0, 'Driver', '2020-09-29 15:37:47', '2020-09-29 15:37:47'),
(135, 2, 0, 'Driver', '2020-09-29 15:57:38', '2020-09-29 15:57:38'),
(136, 2, 0, 'Driver', '2020-09-29 16:02:01', '2020-09-29 16:02:01'),
(137, 2, 0, 'Driver', '2020-09-29 16:12:56', '2020-09-29 16:12:56'),
(138, 2, 0, 'Driver', '2020-09-29 16:25:21', '2020-09-29 16:25:21'),
(139, 2, 0, 'Driver', '2020-09-29 16:33:52', '2020-09-29 16:33:52'),
(140, 2, 0, 'Driver', '2020-09-29 16:42:14', '2020-09-29 16:42:14'),
(141, 2, 0, 'Driver', '2020-09-29 16:43:44', '2020-09-29 16:43:44'),
(142, 2, 0, 'Driver', '2020-09-29 17:08:09', '2020-09-29 17:08:09'),
(143, 2, 0, 'Driver', '2020-09-29 17:09:57', '2020-09-29 17:09:57'),
(144, 2, 0, 'Driver', '2020-09-30 16:44:12', '2020-09-30 16:44:12'),
(145, 2, 0, 'Consumer', '2020-10-01 06:34:41', '2020-10-01 06:34:41'),
(146, 2, 0, 'Consumer', '2020-10-02 15:34:49', '2020-10-02 15:34:49'),
(147, 2, 0, 'Consumer', '2020-10-02 15:34:49', '2020-10-02 15:34:49'),
(148, 2, 0, 'Consumer', '2020-10-02 16:23:01', '2020-10-02 16:23:01'),
(149, 2, 0, 'Driver', '2020-10-02 16:23:39', '2020-10-02 16:23:39'),
(150, 2, 0, 'Driver', '2020-10-02 16:23:58', '2020-10-02 16:23:58'),
(151, 2, 0, 'Driver', '2020-10-02 16:24:08', '2020-10-02 16:24:08'),
(152, 2, 0, 'Consumer', '2020-10-02 16:24:56', '2020-10-02 16:24:56'),
(153, 2, 0, 'Driver', '2020-10-02 16:25:21', '2020-10-02 16:25:21'),
(154, 2, 0, 'Consumer', '2020-10-02 16:25:37', '2020-10-02 16:25:37'),
(155, 2, 0, 'Consumer', '2020-10-02 18:12:02', '2020-10-02 18:12:02'),
(156, 2, 0, 'Consumer', '2020-10-02 18:14:48', '2020-10-02 18:14:48'),
(157, 2, 0, 'Consumer', '2020-10-02 18:20:37', '2020-10-02 18:20:37'),
(158, 2, 0, 'Consumer', '2020-10-02 18:25:16', '2020-10-02 18:25:16'),
(159, 2, 0, 'Driver', '2020-10-02 18:35:36', '2020-10-02 18:35:36'),
(160, 2, 0, 'Consumer', '2020-10-02 18:35:46', '2020-10-02 18:35:46'),
(161, 2, 0, 'Consumer', '2020-10-02 18:40:49', '2020-10-02 18:40:49'),
(162, 2, 0, 'Consumer', '2020-10-02 18:43:38', '2020-10-02 18:43:38'),
(163, 2, 0, 'Consumer', '2020-10-02 18:47:33', '2020-10-02 18:47:33'),
(164, 2, 0, 'Consumer', '2020-10-02 18:57:56', '2020-10-02 18:57:56'),
(165, 2, 0, 'Consumer', '2020-10-02 19:00:36', '2020-10-02 19:00:36'),
(166, 2, 0, 'Consumer', '2020-10-02 19:05:50', '2020-10-02 19:05:50'),
(167, 2, 0, 'Consumer', '2020-10-02 19:17:28', '2020-10-02 19:17:28'),
(168, 2, 0, 'Consumer', '2020-10-02 19:22:52', '2020-10-02 19:22:52'),
(169, 2, 0, 'Consumer', '2020-10-02 19:26:22', '2020-10-02 19:26:22'),
(170, 2, 0, 'Driver', '2020-10-02 19:29:02', '2020-10-02 19:29:02'),
(171, 2, 0, 'Consumer', '2020-10-02 19:29:07', '2020-10-02 19:29:07'),
(172, 2, 0, 'Consumer', '2020-10-02 19:34:05', '2020-10-02 19:34:05'),
(173, 2, 0, 'Consumer', '2020-10-02 19:38:31', '2020-10-02 19:38:31'),
(174, 2, 0, 'Consumer', '2020-10-02 19:39:51', '2020-10-02 19:39:51'),
(175, 2, 0, 'Consumer', '2020-10-02 19:41:34', '2020-10-02 19:41:34'),
(176, 2, 0, 'Consumer', '2020-10-02 19:44:58', '2020-10-02 19:44:58'),
(177, 2, 0, 'Consumer', '2020-10-02 19:48:57', '2020-10-02 19:48:57'),
(178, 2, 0, 'Consumer', '2020-10-03 13:01:13', '2020-10-03 13:01:13'),
(179, 2, 0, 'Driver', '2020-10-03 13:02:39', '2020-10-03 13:02:39'),
(180, 2, 0, 'Driver', '2020-10-03 13:03:40', '2020-10-03 13:03:40'),
(181, 2, 0, 'Consumer', '2020-10-03 13:09:18', '2020-10-03 13:09:18'),
(182, 2, 0, 'Driver', '2020-10-03 13:09:31', '2020-10-03 13:09:31'),
(183, 2, 0, 'Consumer', '2020-10-04 10:52:48', '2020-10-04 10:52:48'),
(184, 2, 0, 'Driver', '2020-10-05 15:49:28', '2020-10-05 15:49:28'),
(185, 2, 0, 'Driver', '2020-10-05 15:50:36', '2020-10-05 15:50:36'),
(186, 2, 0, 'Consumer', '2020-10-05 15:50:41', '2020-10-05 15:50:41'),
(187, 2, 0, 'Driver', '2020-10-05 16:03:56', '2020-10-05 16:03:56'),
(188, 2, 0, 'Driver', '2020-10-05 16:03:56', '2020-10-05 16:03:56'),
(189, 2, 0, 'Consumer', '2020-10-05 16:03:56', '2020-10-05 16:03:56'),
(190, 2, 0, 'Driver', '2020-10-05 16:04:09', '2020-10-05 16:04:09'),
(191, 2, 0, 'Consumer', '2020-10-05 16:05:00', '2020-10-05 16:05:00'),
(192, 2, 0, 'Driver', '2020-10-05 16:05:12', '2020-10-05 16:05:12'),
(193, 2, 0, 'Consumer', '2020-10-05 16:17:34', '2020-10-05 16:17:34'),
(194, 2, 0, 'Consumer', '2020-10-05 16:17:44', '2020-10-05 16:17:44'),
(195, 2, 0, 'Consumer', '2020-10-05 16:40:17', '2020-10-05 16:40:17'),
(196, 2, 0, 'Consumer', '2020-10-05 16:44:42', '2020-10-05 16:44:42'),
(197, 2, 0, 'Consumer', '2020-10-05 16:47:24', '2020-10-05 16:47:24'),
(198, 2, 0, 'Consumer', '2020-10-05 16:48:47', '2020-10-05 16:48:47'),
(199, 2, 0, 'Consumer', '2020-10-05 16:50:36', '2020-10-05 16:50:36'),
(200, 2, 0, 'Consumer', '2020-10-05 16:57:41', '2020-10-05 16:57:41'),
(201, 2, 0, 'Consumer', '2020-10-05 16:59:57', '2020-10-05 16:59:57'),
(202, 2, 0, 'Consumer', '2020-10-05 17:03:49', '2020-10-05 17:03:49'),
(203, 2, 0, 'Consumer', '2020-10-05 17:05:30', '2020-10-05 17:05:30'),
(204, 2, 0, 'Consumer', '2020-10-05 17:10:49', '2020-10-05 17:10:49'),
(205, 2, 0, 'Consumer', '2020-10-05 17:19:28', '2020-10-05 17:19:28'),
(206, 2, 0, 'Consumer', '2020-10-05 17:27:36', '2020-10-05 17:27:36'),
(207, 2, 0, 'Consumer', '2020-10-05 17:28:58', '2020-10-05 17:28:58'),
(208, 2, 0, 'Consumer', '2020-10-05 17:32:17', '2020-10-05 17:32:17'),
(209, 2, 0, 'Consumer', '2020-10-05 17:34:01', '2020-10-05 17:34:01'),
(210, 2, 0, 'Consumer', '2020-10-05 17:34:03', '2020-10-05 17:34:03'),
(211, 2, 0, 'Consumer', '2020-10-05 17:36:20', '2020-10-05 17:36:20'),
(212, 2, 0, 'Consumer', '2020-10-05 17:38:50', '2020-10-05 17:38:50'),
(213, 2, 0, 'Consumer', '2020-10-05 17:42:53', '2020-10-05 17:42:53'),
(214, 2, 0, 'Consumer', '2020-10-05 17:47:25', '2020-10-05 17:47:25'),
(215, 2, 0, 'Consumer', '2020-10-05 18:08:41', '2020-10-05 18:08:41'),
(216, 2, 0, 'Consumer', '2020-10-05 18:12:02', '2020-10-05 18:12:02'),
(217, 2, 0, 'Consumer', '2020-10-05 18:16:10', '2020-10-05 18:16:10'),
(218, 2, 0, 'Consumer', '2020-10-05 18:16:18', '2020-10-05 18:16:18'),
(219, 2, 0, 'Consumer', '2020-10-05 18:17:23', '2020-10-05 18:17:23'),
(220, 2, 0, 'Consumer', '2020-10-05 18:26:24', '2020-10-05 18:26:24'),
(221, 2, 0, 'Consumer', '2020-10-05 18:27:34', '2020-10-05 18:27:34'),
(222, 2, 0, 'Consumer', '2020-10-05 18:30:23', '2020-10-05 18:30:23'),
(223, 2, 0, 'Consumer', '2020-10-05 18:32:51', '2020-10-05 18:32:51'),
(224, 2, 0, 'Consumer', '2020-10-05 18:34:07', '2020-10-05 18:34:07'),
(225, 2, 0, 'Consumer', '2020-10-05 18:35:14', '2020-10-05 18:35:14'),
(226, 2, 0, 'Consumer', '2020-10-05 18:36:51', '2020-10-05 18:36:51'),
(227, 2, 0, 'Consumer', '2020-10-05 18:37:55', '2020-10-05 18:37:55'),
(228, 2, 0, 'Consumer', '2020-10-05 18:40:02', '2020-10-05 18:40:02'),
(229, 2, 0, 'Consumer', '2020-10-05 18:40:38', '2020-10-05 18:40:38'),
(230, 2, 0, 'Consumer', '2020-10-05 18:41:11', '2020-10-05 18:41:11'),
(231, 2, 0, 'Consumer', '2020-10-05 18:43:22', '2020-10-05 18:43:22'),
(232, 2, 0, 'Consumer', '2020-10-06 16:30:59', '2020-10-06 16:30:59'),
(233, 2, 0, 'Consumer', '2020-10-06 16:38:22', '2020-10-06 16:38:22'),
(234, 2, 0, 'Driver', '2020-10-06 16:40:23', '2020-10-06 16:40:23'),
(235, 2, 0, 'Driver', '2020-10-06 17:19:24', '2020-10-06 17:19:24'),
(236, 2, 0, 'Driver', '2020-10-06 17:29:25', '2020-10-06 17:29:25'),
(237, 2, 0, 'Driver', '2020-10-06 17:34:18', '2020-10-06 17:34:18'),
(238, 2, 0, 'Driver', '2020-10-06 17:34:19', '2020-10-06 17:34:19'),
(239, 2, 0, 'Consumer', '2020-10-06 18:10:06', '2020-10-06 18:10:06'),
(240, 2, 0, 'Driver', '2020-10-06 18:14:52', '2020-10-06 18:14:52'),
(241, 2, 0, 'Driver', '2020-10-06 18:16:30', '2020-10-06 18:16:30'),
(242, 2, 0, 'Consumer', '2020-10-06 18:16:40', '2020-10-06 18:16:40'),
(243, 7, 0, 'Consumer', '2020-10-07 06:58:19', '2020-10-07 06:58:19'),
(244, 7, 0, 'Consumer', '2020-10-07 07:00:51', '2020-10-07 07:00:51'),
(245, 7, 0, 'Driver', '2020-10-07 07:12:46', '2020-10-07 07:12:46'),
(246, 7, 0, 'Consumer', '2020-10-07 07:42:32', '2020-10-07 07:42:32'),
(247, 7, 0, 'Consumer', '2020-10-07 07:44:22', '2020-10-07 07:44:22'),
(248, 7, 0, 'Consumer', '2020-10-07 07:49:13', '2020-10-07 07:49:13'),
(249, 7, 0, 'Consumer', '2020-10-07 08:28:50', '2020-10-07 08:28:50'),
(250, 9, 0, 'Consumer', '2020-10-08 05:30:27', '2020-10-08 05:30:27'),
(251, 9, 0, 'Consumer', '2020-10-08 05:30:55', '2020-10-08 05:30:55'),
(252, 9, 0, 'Consumer', '2020-10-08 05:33:14', '2020-10-08 05:33:14'),
(253, 9, 0, 'Consumer', '2020-10-08 17:58:51', '2020-10-08 17:58:51'),
(254, 9, 0, 'Consumer', '2020-10-08 18:13:53', '2020-10-08 18:13:53'),
(255, 9, 0, 'Consumer', '2020-10-08 18:13:53', '2020-10-08 18:13:53'),
(256, 9, 0, 'Consumer', '2020-10-08 18:17:43', '2020-10-08 18:17:43'),
(257, 9, 0, 'Consumer', '2020-10-08 18:23:14', '2020-10-08 18:23:14'),
(258, 9, 0, 'Consumer', '2020-10-08 18:25:57', '2020-10-08 18:25:57'),
(259, 9, 0, 'Consumer', '2020-10-08 18:37:10', '2020-10-08 18:37:10'),
(260, 9, 0, 'Consumer', '2020-10-08 18:42:09', '2020-10-08 18:42:09'),
(261, 1, 0, 'Consumer', '2020-10-10 12:08:50', '2020-10-10 12:08:50'),
(262, 2, 0, 'Consumer', '2020-10-10 12:14:52', '2020-10-10 12:14:52'),
(263, 2, 0, 'Consumer', '2020-10-10 12:16:28', '2020-10-10 12:16:28'),
(264, 2, 0, 'Consumer', '2020-10-10 12:21:23', '2020-10-10 12:21:23'),
(265, 2, 0, 'Consumer', '2020-10-10 12:23:18', '2020-10-10 12:23:18'),
(266, 3, 0, 'Consumer', '2020-10-14 16:30:36', '2020-10-14 16:30:36'),
(267, 6, 0, 'Consumer', '2020-10-14 19:43:07', '2020-10-14 19:43:07'),
(268, 6, 0, 'Consumer', '2020-10-14 19:43:50', '2020-10-14 19:43:50'),
(269, 3, 0, 'Consumer', '2020-10-14 19:49:26', '2020-10-14 19:49:26'),
(270, 3, 0, 'Consumer', '2020-10-14 19:51:06', '2020-10-14 19:51:06'),
(271, 7, 0, 'Consumer', '2020-10-17 12:28:22', '2020-10-17 12:28:22'),
(272, 7, 0, 'Consumer', '2020-10-17 12:28:58', '2020-10-17 12:28:58'),
(273, 7, 0, 'Consumer', '2020-10-17 13:39:39', '2020-10-17 13:39:39'),
(274, 7, 0, 'Consumer', '2020-10-17 13:39:39', '2020-10-17 13:39:39'),
(275, 7, 0, 'Consumer', '2020-10-17 13:40:27', '2020-10-17 13:40:27'),
(276, 8, 0, 'Consumer', '2020-10-17 13:42:06', '2020-10-17 13:42:06'),
(277, 8, 0, 'Consumer', '2020-10-17 13:42:25', '2020-10-17 13:42:25'),
(278, 9, 0, 'Consumer', '2020-10-21 14:43:05', '2020-10-21 14:43:05'),
(279, 9, 0, 'Consumer', '2020-10-21 14:43:46', '2020-10-21 14:43:46'),
(280, 10, 0, 'Consumer', '2020-10-21 14:53:33', '2020-10-21 14:53:33'),
(281, 10, 0, 'Consumer', '2020-10-21 14:54:15', '2020-10-21 14:54:15'),
(282, 9, 0, 'Consumer', '2020-10-21 14:57:33', '2020-10-21 14:57:33'),
(283, 9, 0, 'Consumer', '2020-10-21 16:55:47', '2020-10-21 16:55:47'),
(284, 9, 0, 'Consumer', '2020-10-21 16:55:47', '2020-10-21 16:55:47'),
(285, 9, 0, 'Consumer', '2020-10-26 12:46:52', '2020-10-26 12:46:52'),
(286, 9, 0, 'Consumer', '2020-10-26 12:54:34', '2020-10-26 12:54:34'),
(287, 9, 0, 'Consumer', '2020-10-26 13:08:16', '2020-10-26 13:08:16'),
(288, 9, 0, 'Consumer', '2020-10-26 13:12:28', '2020-10-26 13:12:28'),
(289, 9, 0, 'Consumer', '2020-10-26 13:19:27', '2020-10-26 13:19:27'),
(290, 13, 0, 'Consumer', '2020-10-27 17:05:21', '2020-10-27 17:05:21'),
(291, 14, 0, 'Consumer', '2020-10-27 17:16:37', '2020-10-27 17:16:37'),
(292, 14, 0, 'Driver', '2020-10-27 17:18:06', '2020-10-27 17:18:06'),
(293, 14, 0, 'Consumer', '2020-10-27 17:18:26', '2020-10-27 17:18:26'),
(294, 14, 0, 'Consumer', '2020-10-27 17:18:26', '2020-10-27 17:18:26'),
(295, 13, 0, 'Consumer', '2020-10-27 17:28:26', '2020-10-27 17:28:26'),
(296, 13, 0, 'Driver', '2020-10-27 17:28:31', '2020-10-27 17:28:31'),
(297, 13, 0, 'Driver', '2020-10-27 20:29:50', '2020-10-27 20:29:50'),
(298, 13, 0, 'Driver', '2020-10-27 20:29:50', '2020-10-27 20:29:50'),
(299, 13, 0, 'Consumer', '2020-10-27 20:29:50', '2020-10-27 20:29:50'),
(300, 13, 0, 'Consumer', '2020-10-27 20:29:52', '2020-10-27 20:29:52'),
(301, 1, 0, 'Consumer', '2020-10-27 20:45:58', '2020-10-27 20:45:58'),
(302, 1, 0, 'Driver', '2020-10-27 20:46:23', '2020-10-27 20:46:23'),
(303, 1, 0, 'Consumer', '2020-10-27 21:24:59', '2020-10-27 21:24:59'),
(304, 1, 0, 'Consumer', '2020-10-27 21:33:44', '2020-10-27 21:33:44'),
(305, 1, 0, 'Consumer', '2020-10-27 21:33:47', '2020-10-27 21:33:47'),
(306, 1, 0, 'Driver', '2020-10-27 21:34:27', '2020-10-27 21:34:27'),
(307, 1, 0, 'Driver', '2020-10-27 21:35:30', '2020-10-27 21:35:30'),
(308, 13, 0, 'Consumer', '2020-10-27 21:36:55', '2020-10-27 21:36:55'),
(309, 13, 0, 'Consumer', '2020-10-27 23:34:12', '2020-10-27 23:34:12'),
(310, 13, 0, 'Consumer', '2020-10-27 23:51:11', '2020-10-27 23:51:11'),
(311, 13, 0, 'Consumer', '2020-10-28 00:01:10', '2020-10-28 00:01:10'),
(312, 13, 0, 'Consumer', '2020-10-28 13:53:04', '2020-10-28 13:53:04'),
(313, 1, 0, 'Consumer', '2020-10-28 13:58:05', '2020-10-28 13:58:05'),
(314, 1, 0, 'Consumer', '2020-10-28 13:58:18', '2020-10-28 13:58:18'),
(315, 1, 0, 'Consumer', '2020-10-28 13:58:45', '2020-10-28 13:58:45'),
(316, 1, 0, 'Consumer', '2020-10-29 05:12:02', '2020-10-29 05:12:02'),
(317, 1, 0, 'Consumer', '2020-10-30 01:44:21', '2020-10-30 01:44:21'),
(318, 11, 0, 'Consumer', '2020-10-31 10:11:15', '2020-10-31 10:11:15'),
(319, 11, 0, 'Consumer', '2020-10-31 10:21:30', '2020-10-31 10:21:30'),
(320, 11, 0, 'Consumer', '2020-10-31 10:24:00', '2020-10-31 10:24:00'),
(321, 11, 0, 'Consumer', '2020-10-31 10:48:29', '2020-10-31 10:48:29'),
(322, 11, 0, 'Consumer', '2020-10-31 10:55:52', '2020-10-31 10:55:52'),
(323, 1, 0, 'Consumer', '2020-11-01 13:49:25', '2020-11-01 13:49:25'),
(324, 1, 0, 'Consumer', '2020-11-01 13:49:28', '2020-11-01 13:49:28'),
(325, 1, 0, 'Consumer', '2020-11-01 13:58:06', '2020-11-01 13:58:06'),
(326, 1, 0, 'Consumer', '2020-11-01 14:00:28', '2020-11-01 14:00:28'),
(327, 1, 0, 'Consumer', '2020-11-01 14:00:42', '2020-11-01 14:00:42'),
(328, 1, 0, 'Consumer', '2020-11-01 14:07:56', '2020-11-01 14:07:56'),
(329, 1, 0, 'Driver', '2020-11-01 14:08:53', '2020-11-01 14:08:53'),
(330, 1, 0, 'Consumer', '2020-11-01 14:09:00', '2020-11-01 14:09:00'),
(331, 1, 0, 'Consumer', '2020-11-01 14:09:08', '2020-11-01 14:09:08'),
(332, 1, 0, 'Driver', '2020-11-01 14:11:55', '2020-11-01 14:11:55'),
(333, 1, 0, 'Consumer', '2020-11-01 14:16:11', '2020-11-01 14:16:11'),
(334, 1, 0, 'Consumer', '2020-11-01 14:16:54', '2020-11-01 14:16:54'),
(335, 1, 0, 'Consumer', '2020-11-01 14:17:05', '2020-11-01 14:17:05'),
(336, 1, 0, 'Consumer', '2020-11-01 14:20:06', '2020-11-01 14:20:06'),
(337, 1, 0, 'Consumer', '2020-11-01 14:21:04', '2020-11-01 14:21:04'),
(338, 1, 0, 'Consumer', '2020-11-01 14:25:19', '2020-11-01 14:25:19'),
(339, 1, 0, 'Consumer', '2020-11-01 14:26:24', '2020-11-01 14:26:24'),
(340, 1, 0, 'Consumer', '2020-11-01 14:26:27', '2020-11-01 14:26:27'),
(341, 1, 0, 'Consumer', '2020-11-01 14:28:01', '2020-11-01 14:28:01'),
(342, 1, 0, 'Consumer', '2020-11-01 14:28:38', '2020-11-01 14:28:38'),
(343, 1, 0, 'Consumer', '2020-11-01 14:51:11', '2020-11-01 14:51:11'),
(344, 11, 0, 'Consumer', '2020-11-01 15:36:55', '2020-11-01 15:36:55'),
(345, 11, 0, 'Consumer', '2020-11-01 15:41:37', '2020-11-01 15:41:37'),
(346, 1, 0, 'Consumer', '2020-11-01 15:41:42', '2020-11-01 15:41:42'),
(347, 18, 0, 'Consumer', '2020-11-01 15:46:41', '2020-11-01 15:46:41'),
(348, 18, 0, 'Driver', '2020-11-01 15:47:24', '2020-11-01 15:47:24'),
(349, 19, 0, 'Consumer', '2020-11-01 16:18:14', '2020-11-01 16:18:14'),
(350, 19, 0, 'Consumer', '2020-11-01 16:18:29', '2020-11-01 16:18:29'),
(351, 19, 0, 'Consumer', '2020-11-01 16:18:33', '2020-11-01 16:18:33'),
(352, 20, 0, 'Consumer', '2020-11-01 16:19:33', '2020-11-01 16:19:33'),
(353, 9, 0, 'Driver', '2020-11-04 06:49:48', '2020-11-04 06:49:48'),
(354, 9, 0, 'Driver', '2020-11-04 06:50:15', '2020-11-04 06:50:15'),
(355, 9, 0, 'Consumer', '2020-11-04 06:50:20', '2020-11-04 06:50:20'),
(356, 9, 0, 'Consumer', '2020-11-04 06:53:04', '2020-11-04 06:53:04'),
(357, 9, 0, 'Driver', '2020-11-04 06:53:10', '2020-11-04 06:53:10'),
(358, 9, 0, 'Driver', '2020-11-04 06:56:22', '2020-11-04 06:56:22'),
(359, 9, 0, 'Driver', '2020-11-04 06:58:45', '2020-11-04 06:58:45'),
(360, 9, 0, 'Driver', '2020-11-04 07:02:04', '2020-11-04 07:02:04'),
(361, 9, 0, 'Consumer', '2020-11-04 07:02:18', '2020-11-04 07:02:18'),
(362, 9, 0, 'Driver', '2020-11-04 07:03:13', '2020-11-04 07:03:13'),
(363, 9, 0, 'Consumer', '2020-11-04 07:04:19', '2020-11-04 07:04:19'),
(364, 9, 0, 'Driver', '2020-11-04 07:14:07', '2020-11-04 07:14:07'),
(365, 21, 0, 'Consumer', '2020-11-05 16:09:30', '2020-11-05 16:09:30'),
(366, 9, 0, 'Consumer', '2020-11-08 11:05:00', '2020-11-08 11:05:00'),
(367, 9, 0, 'Consumer', '2020-11-08 11:09:59', '2020-11-08 11:09:59'),
(368, 9, 0, 'Consumer', '2020-11-08 11:17:36', '2020-11-08 11:17:36'),
(369, 9, 0, 'Driver', '2020-11-08 12:23:17', '2020-11-08 12:23:17'),
(370, 9, 0, 'Consumer', '2020-11-08 12:31:03', '2020-11-08 12:31:03'),
(371, 9, 0, 'Driver', '2020-11-08 12:43:41', '2020-11-08 12:43:41'),
(372, 9, 0, 'Consumer', '2020-11-08 16:30:18', '2020-11-08 16:30:18'),
(373, 9, 0, 'Driver', '2020-11-08 16:32:38', '2020-11-08 16:32:38'),
(374, 9, 0, 'Driver', '2020-11-08 16:35:16', '2020-11-08 16:35:16'),
(375, 9, 0, 'Consumer', '2020-11-08 16:37:54', '2020-11-08 16:37:54'),
(376, 9, 0, 'Driver', '2020-11-08 16:38:18', '2020-11-08 16:38:18'),
(377, 9, 0, 'Driver', '2020-11-08 16:43:26', '2020-11-08 16:43:26'),
(378, 9, 0, 'Driver', '2020-11-08 16:49:49', '2020-11-08 16:49:49'),
(379, 9, 0, 'Driver', '2020-11-08 16:51:48', '2020-11-08 16:51:48'),
(380, 9, 0, 'Driver', '2020-11-08 16:53:08', '2020-11-08 16:53:08'),
(381, 9, 0, 'Driver', '2020-11-08 16:53:29', '2020-11-08 16:53:29'),
(382, 9, 0, 'Driver', '2020-11-08 16:55:29', '2020-11-08 16:55:29'),
(383, 9, 0, 'Driver', '2020-11-08 16:56:38', '2020-11-08 16:56:38'),
(384, 9, 0, 'Driver', '2020-11-08 17:02:27', '2020-11-08 17:02:27'),
(385, 9, 0, 'Driver', '2020-11-09 04:39:24', '2020-11-09 04:39:24'),
(386, 9, 0, 'Driver', '2020-11-09 04:41:51', '2020-11-09 04:41:51'),
(387, 9, 0, 'Driver', '2020-11-09 04:44:22', '2020-11-09 04:44:22'),
(388, 9, 0, 'Driver', '2020-11-09 05:00:14', '2020-11-09 05:00:14'),
(389, 9, 0, 'Driver', '2020-11-09 05:01:14', '2020-11-09 05:01:14'),
(390, 9, 0, 'Driver', '2020-11-09 05:01:54', '2020-11-09 05:01:54'),
(391, 9, 0, 'Driver', '2020-11-09 05:03:53', '2020-11-09 05:03:53'),
(392, 9, 0, 'Driver', '2020-11-09 05:05:56', '2020-11-09 05:05:56'),
(393, 9, 0, 'Driver', '2020-11-09 05:10:16', '2020-11-09 05:10:16'),
(394, 9, 0, 'Driver', '2020-11-09 05:10:36', '2020-11-09 05:10:36'),
(395, 9, 0, 'Driver', '2020-11-09 05:17:42', '2020-11-09 05:17:42'),
(396, 9, 0, 'Driver', '2020-11-09 06:11:55', '2020-11-09 06:11:55'),
(397, 9, 0, 'Driver', '2020-11-09 06:13:23', '2020-11-09 06:13:23'),
(398, 9, 0, 'Driver', '2020-11-09 06:14:53', '2020-11-09 06:14:53'),
(399, 9, 0, 'Driver', '2020-11-13 15:51:26', '2020-11-13 15:51:26'),
(400, 9, 0, 'Driver', '2020-11-13 15:59:43', '2020-11-13 15:59:43'),
(401, 9, 0, 'Consumer', '2020-11-13 16:02:18', '2020-11-13 16:02:18'),
(402, 9, 0, 'Driver', '2020-11-13 16:04:33', '2020-11-13 16:04:33'),
(403, 9, 0, 'Driver', '2020-11-13 16:10:04', '2020-11-13 16:10:04'),
(404, 9, 0, 'Driver', '2020-11-13 16:11:46', '2020-11-13 16:11:46'),
(405, 9, 0, 'Driver', '2020-11-13 16:12:23', '2020-11-13 16:12:23'),
(406, 9, 0, 'Consumer', '2020-11-13 16:13:14', '2020-11-13 16:13:14'),
(407, 9, 0, 'Driver', '2020-11-13 16:13:31', '2020-11-13 16:13:31'),
(408, 9, 0, 'Driver', '2020-11-13 16:15:02', '2020-11-13 16:15:02'),
(409, 9, 0, 'Driver', '2020-11-13 16:16:16', '2020-11-13 16:16:16'),
(410, 9, 0, 'Driver', '2020-11-13 16:23:38', '2020-11-13 16:23:38'),
(411, 9, 0, 'Driver', '2020-11-13 16:25:18', '2020-11-13 16:25:18'),
(412, 9, 0, 'Driver', '2020-11-13 16:25:51', '2020-11-13 16:25:51'),
(413, 9, 0, 'Driver', '2020-11-13 16:26:44', '2020-11-13 16:26:44'),
(414, 9, 0, 'Driver', '2020-11-13 16:27:21', '2020-11-13 16:27:21'),
(415, 9, 0, 'Driver', '2020-11-13 16:32:25', '2020-11-13 16:32:25'),
(416, 9, 0, 'Driver', '2020-11-13 16:32:40', '2020-11-13 16:32:40'),
(417, 9, 0, 'Driver', '2020-11-13 16:33:22', '2020-11-13 16:33:22'),
(418, 9, 0, 'Driver', '2020-11-13 16:34:00', '2020-11-13 16:34:00'),
(419, 9, 0, 'Driver', '2020-11-13 16:34:34', '2020-11-13 16:34:34'),
(420, 9, 0, 'Driver', '2020-11-13 16:35:21', '2020-11-13 16:35:21'),
(421, 9, 0, 'Driver', '2020-11-13 16:36:07', '2020-11-13 16:36:07'),
(422, 9, 0, 'Driver', '2020-11-13 16:40:07', '2020-11-13 16:40:07'),
(423, 9, 0, 'Driver', '2020-11-13 16:41:44', '2020-11-13 16:41:44'),
(424, 9, 0, 'Driver', '2020-11-13 16:43:05', '2020-11-13 16:43:05'),
(425, 9, 0, 'Driver', '2020-11-13 16:43:59', '2020-11-13 16:43:59'),
(426, 9, 0, 'Driver', '2020-11-13 16:46:22', '2020-11-13 16:46:22'),
(427, 9, 0, 'Consumer', '2020-11-13 16:47:38', '2020-11-13 16:47:38'),
(428, 9, 0, 'Consumer', '2020-11-13 16:50:56', '2020-11-13 16:50:56'),
(429, 9, 0, 'Driver', '2020-11-13 16:52:30', '2020-11-13 16:52:30'),
(430, 9, 0, 'Driver', '2020-11-13 16:53:33', '2020-11-13 16:53:33'),
(431, 9, 0, 'Driver', '2020-11-13 16:56:30', '2020-11-13 16:56:30'),
(432, 9, 0, 'Consumer', '2020-11-13 17:05:50', '2020-11-13 17:05:50'),
(433, 9, 0, 'Driver', '2020-11-13 17:06:14', '2020-11-13 17:06:14'),
(434, 9, 0, 'Driver', '2020-11-13 17:11:39', '2020-11-13 17:11:39'),
(435, 9, 0, 'Driver', '2020-11-13 17:12:45', '2020-11-13 17:12:45'),
(436, 9, 0, 'Driver', '2020-11-18 05:31:19', '2020-11-18 05:31:19'),
(437, 9, 0, 'Driver', '2020-11-24 08:06:27', '2020-11-24 08:06:27'),
(438, 9, 0, 'Driver', '2020-11-24 08:12:23', '2020-11-24 08:12:23'),
(439, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(440, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(441, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(442, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(443, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(444, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(445, 9, 0, 'Consumer', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(446, 9, 0, 'Driver', '2020-11-24 08:15:43', '2020-11-24 08:15:43'),
(447, 9, 0, 'Driver', '2020-11-24 08:16:36', '2020-11-24 08:16:36'),
(448, 9, 0, 'Driver', '2020-11-24 08:17:52', '2020-11-24 08:17:52'),
(449, 9, 0, 'Driver', '2020-11-24 08:17:57', '2020-11-24 08:17:57'),
(450, 9, 0, 'Driver', '2020-11-24 08:25:04', '2020-11-24 08:25:04'),
(451, 9, 0, 'Driver', '2020-11-24 08:27:29', '2020-11-24 08:27:29'),
(452, 9, 0, 'Driver', '2020-11-24 08:32:52', '2020-11-24 08:32:52'),
(453, 9, 0, 'Driver', '2020-11-24 09:09:13', '2020-11-24 09:09:13'),
(454, 9, 0, 'Driver', '2020-11-24 09:11:15', '2020-11-24 09:11:15'),
(455, 9, 0, 'Driver', '2020-11-24 09:16:47', '2020-11-24 09:16:47'),
(456, 9, 0, 'Driver', '2020-11-24 09:35:30', '2020-11-24 09:35:30'),
(457, 9, 0, 'Driver', '2020-11-24 09:39:34', '2020-11-24 09:39:34'),
(458, 9, 0, 'Driver', '2020-11-24 09:46:31', '2020-11-24 09:46:31'),
(459, 9, 0, 'Driver', '2020-11-24 09:47:27', '2020-11-24 09:47:27'),
(460, 9, 0, 'Driver', '2020-11-24 10:12:40', '2020-11-24 10:12:40'),
(461, 9, 0, 'Driver', '2020-11-25 15:41:51', '2020-11-25 15:41:51'),
(462, 9, 0, 'Driver', '2020-11-25 15:57:33', '2020-11-25 15:57:33'),
(463, 9, 0, 'Driver', '2020-11-25 16:02:31', '2020-11-25 16:02:31'),
(464, 9, 0, 'Driver', '2020-11-25 16:02:41', '2020-11-25 16:02:41'),
(465, 9, 0, 'Consumer', '2020-11-25 16:13:41', '2020-11-25 16:13:41'),
(466, 9, 0, 'Consumer', '2020-11-25 16:13:41', '2020-11-25 16:13:41'),
(467, 9, 0, 'Consumer', '2020-11-25 16:26:31', '2020-11-25 16:26:31'),
(468, 9, 0, 'Driver', '2020-11-25 16:33:07', '2020-11-25 16:33:07'),
(469, 9, 0, 'Driver', '2020-11-25 16:41:39', '2020-11-25 16:41:39'),
(470, 9, 0, 'Consumer', '2020-11-25 16:41:53', '2020-11-25 16:41:53'),
(471, 9, 0, 'Consumer', '2020-11-25 16:42:35', '2020-11-25 16:42:35'),
(472, 9, 0, 'Driver', '2020-11-25 16:46:42', '2020-11-25 16:46:42'),
(473, 9, 0, 'Driver', '2020-11-25 16:46:43', '2020-11-25 16:46:43'),
(474, 9, 0, 'Driver', '2020-11-25 16:54:11', '2020-11-25 16:54:11'),
(475, 9, 0, 'Consumer', '2020-11-25 16:54:19', '2020-11-25 16:54:19'),
(476, 9, 0, 'Driver', '2020-11-25 16:55:19', '2020-11-25 16:55:19'),
(477, 9, 0, 'Consumer', '2020-11-25 16:57:59', '2020-11-25 16:57:59'),
(478, 9, 0, 'Consumer', '2020-11-25 17:09:48', '2020-11-25 17:09:48'),
(479, 9, 0, 'Consumer', '2020-11-25 17:09:49', '2020-11-25 17:09:49'),
(480, 9, 0, 'Consumer', '2020-11-25 17:13:06', '2020-11-25 17:13:06'),
(481, 9, 0, 'Driver', '2020-11-25 17:16:38', '2020-11-25 17:16:38'),
(482, 9, 0, 'Consumer', '2020-11-25 17:16:38', '2020-11-25 17:16:38'),
(483, 9, 0, 'Driver', '2020-11-25 17:16:38', '2020-11-25 17:16:38'),
(484, 9, 0, 'Driver', '2020-11-25 17:16:39', '2020-11-25 17:16:39'),
(485, 9, 0, 'Driver', '2020-11-25 17:19:37', '2020-11-25 17:19:37'),
(486, 9, 0, 'Consumer', '2020-11-25 17:19:39', '2020-11-25 17:19:39'),
(487, 9, 0, 'Consumer', '2020-11-25 17:19:45', '2020-11-25 17:19:45'),
(488, 9, 0, 'Driver', '2020-11-25 17:20:03', '2020-11-25 17:20:03'),
(489, 9, 0, 'Driver', '2020-11-25 17:20:53', '2020-11-25 17:20:53'),
(490, 9, 0, 'Driver', '2020-11-25 17:27:58', '2020-11-25 17:27:58'),
(491, 9, 0, 'Driver', '2020-11-25 17:31:39', '2020-11-25 17:31:39'),
(492, 9, 0, 'Driver', '2020-11-25 17:31:41', '2020-11-25 17:31:41'),
(493, 9, 0, 'Driver', '2020-11-25 17:31:41', '2020-11-25 17:31:41'),
(494, 9, 0, 'Consumer', '2020-11-25 17:37:01', '2020-11-25 17:37:01'),
(495, 9, 0, 'Driver', '2020-11-25 18:54:45', '2020-11-25 18:54:45'),
(496, 9, 0, 'Consumer', '2020-11-26 16:14:27', '2020-11-26 16:14:27'),
(497, 9, 0, 'Consumer', '2020-11-26 16:52:40', '2020-11-26 16:52:40'),
(498, 9, 0, 'Driver', '2020-11-26 17:07:32', '2020-11-26 17:07:32'),
(499, 9, 0, 'Consumer', '2020-11-26 17:18:07', '2020-11-26 17:18:07'),
(500, 9, 0, 'Consumer', '2020-11-26 17:23:16', '2020-11-26 17:23:16'),
(501, 9, 0, 'Consumer', '2020-11-26 17:24:48', '2020-11-26 17:24:48'),
(502, 9, 0, 'Driver', '2020-11-28 11:39:32', '2020-11-28 11:39:32'),
(503, 9, 0, 'Driver', '2020-11-28 11:41:57', '2020-11-28 11:41:57'),
(504, 9, 0, 'Consumer', '2020-11-28 11:44:14', '2020-11-28 11:44:14'),
(505, 9, 0, 'Consumer', '2020-11-28 13:43:56', '2020-11-28 13:43:56'),
(506, 9, 0, 'Consumer', '2020-11-28 13:45:19', '2020-11-28 13:45:19'),
(507, 9, 0, 'Driver', '2020-11-28 13:46:07', '2020-11-28 13:46:07'),
(508, 9, 0, 'Driver', '2020-11-28 14:14:28', '2020-11-28 14:14:28'),
(509, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(510, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(511, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(512, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(513, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(514, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(515, 9, 0, 'Driver', '2020-11-28 15:02:12', '2020-11-28 15:02:12'),
(516, 9, 0, 'Driver', '2020-11-28 15:06:57', '2020-11-28 15:06:57'),
(517, 9, 0, 'Driver', '2020-11-28 15:10:52', '2020-11-28 15:10:52'),
(518, 9, 0, 'Consumer', '2020-11-28 15:44:49', '2020-11-28 15:44:49'),
(519, 9, 0, 'Consumer', '2020-11-29 05:01:49', '2020-11-29 05:01:49'),
(520, 9, 0, 'Driver', '2020-12-03 17:36:23', '2020-12-03 17:36:23'),
(521, 9, 0, 'Consumer', '2020-12-03 17:36:58', '2020-12-03 17:36:58'),
(522, 9, 0, 'Consumer', '2020-12-03 17:37:53', '2020-12-03 17:37:53'),
(523, 9, 0, 'Consumer', '2020-12-03 17:50:55', '2020-12-03 17:50:55'),
(524, 9, 0, 'Consumer', '2020-12-03 17:57:07', '2020-12-03 17:57:07'),
(525, 9, 0, 'Consumer', '2020-12-03 18:17:42', '2020-12-03 18:17:42'),
(526, 9, 0, 'Consumer', '2020-12-03 18:26:44', '2020-12-03 18:26:44'),
(527, 9, 0, 'Consumer', '2020-12-03 18:30:06', '2020-12-03 18:30:06'),
(528, 9, 0, 'Driver', '2020-12-03 18:33:44', '2020-12-03 18:33:44'),
(529, 9, 0, 'Driver', '2020-12-03 18:35:47', '2020-12-03 18:35:47'),
(530, 11, 0, 'Driver', '2020-12-03 18:37:20', '2020-12-03 18:37:20'),
(531, 9, 0, 'Consumer', '2020-12-03 19:03:23', '2020-12-03 19:03:23'),
(532, 9, 0, 'Consumer', '2020-12-03 19:11:23', '2020-12-03 19:11:23'),
(533, 11, 0, 'Driver', '2020-12-03 19:11:43', '2020-12-03 19:11:43'),
(534, 11, 0, 'Driver', '2020-12-03 19:14:47', '2020-12-03 19:14:47'),
(535, 11, 0, 'Driver', '2020-12-03 19:16:47', '2020-12-03 19:16:47'),
(536, 11, 0, 'Driver', '2020-12-03 19:21:22', '2020-12-03 19:21:22'),
(537, 11, 0, 'Driver', '2020-12-03 19:23:36', '2020-12-03 19:23:36'),
(538, 11, 0, 'Driver', '2020-12-03 19:31:54', '2020-12-03 19:31:54'),
(539, 11, 0, 'Driver', '2020-12-03 19:37:23', '2020-12-03 19:37:23'),
(540, 9, 0, 'Driver', '2020-12-03 19:39:01', '2020-12-03 19:39:01'),
(541, 9, 0, 'Driver', '2020-12-03 19:41:36', '2020-12-03 19:41:36'),
(542, 9, 0, 'Consumer', '2020-12-03 19:41:49', '2020-12-03 19:41:49'),
(543, 9, 0, 'Consumer', '2020-12-03 19:46:07', '2020-12-03 19:46:07'),
(544, 9, 0, 'Consumer', '2020-12-03 19:50:05', '2020-12-03 19:50:05'),
(545, 11, 0, 'Driver', '2020-12-03 20:03:23', '2020-12-03 20:03:23'),
(546, 11, 0, 'Driver', '2020-12-03 20:16:17', '2020-12-03 20:16:17'),
(547, 11, 0, 'Consumer', '2020-12-03 20:18:27', '2020-12-03 20:18:27'),
(548, 11, 0, 'Driver', '2020-12-03 20:22:19', '2020-12-03 20:22:19'),
(549, 11, 0, 'Driver', '2020-12-03 20:38:31', '2020-12-03 20:38:31'),
(550, 11, 0, 'Driver', '2020-12-03 20:41:31', '2020-12-03 20:41:31'),
(551, 11, 0, 'Consumer', '2020-12-03 20:49:46', '2020-12-03 20:49:46'),
(552, 11, 0, 'Driver', '2020-12-03 20:50:06', '2020-12-03 20:50:06'),
(553, 11, 0, 'Driver', '2020-12-03 20:52:52', '2020-12-03 20:52:52'),
(554, 11, 0, 'Consumer', '2020-12-03 20:54:59', '2020-12-03 20:54:59'),
(555, 11, 0, 'Driver', '2020-12-03 20:55:44', '2020-12-03 20:55:44'),
(556, 11, 0, 'Driver', '2020-12-03 20:57:44', '2020-12-03 20:57:44'),
(557, 11, 0, 'Driver', '2020-12-03 20:58:56', '2020-12-03 20:58:56'),
(558, 11, 0, 'Driver', '2020-12-03 21:00:49', '2020-12-03 21:00:49'),
(559, 11, 0, 'Consumer', '2020-12-03 21:13:10', '2020-12-03 21:13:10'),
(560, 11, 0, 'Driver', '2020-12-03 21:13:17', '2020-12-03 21:13:17'),
(561, 9, 0, 'Consumer', '2020-12-03 21:13:36', '2020-12-03 21:13:36'),
(562, 9, 0, 'Consumer', '2020-12-03 21:15:32', '2020-12-03 21:15:32'),
(563, 9, 0, 'Consumer', '2020-12-03 21:16:28', '2020-12-03 21:16:28'),
(564, 9, 0, 'Consumer', '2020-12-03 21:28:23', '2020-12-03 21:28:23'),
(565, 9, 0, 'Consumer', '2020-12-03 21:35:21', '2020-12-03 21:35:21'),
(566, 9, 0, 'Consumer', '2020-12-03 21:36:44', '2020-12-03 21:36:44'),
(567, 9, 0, 'Consumer', '2020-12-03 21:37:19', '2020-12-03 21:37:19'),
(568, 9, 0, 'Consumer', '2020-12-03 21:38:08', '2020-12-03 21:38:08'),
(569, 9, 0, 'Driver', '2020-12-03 21:41:49', '2020-12-03 21:41:49'),
(570, 9, 0, 'Consumer', '2020-12-03 21:41:57', '2020-12-03 21:41:57'),
(571, 9, 0, 'Consumer', '2020-12-03 21:42:36', '2020-12-03 21:42:36'),
(572, 9, 0, 'Consumer', '2020-12-03 21:43:32', '2020-12-03 21:43:32'),
(573, 9, 0, 'Consumer', '2020-12-03 21:50:23', '2020-12-03 21:50:23'),
(574, 9, 0, 'Consumer', '2020-12-03 21:51:19', '2020-12-03 21:51:19'),
(575, 9, 0, 'Consumer', '2020-12-03 21:52:24', '2020-12-03 21:52:24'),
(576, 9, 0, 'Consumer', '2020-12-03 21:55:56', '2020-12-03 21:55:56'),
(577, 9, 0, 'Consumer', '2020-12-03 21:57:17', '2020-12-03 21:57:17'),
(578, 9, 0, 'Consumer', '2020-12-03 22:02:28', '2020-12-03 22:02:28'),
(579, 9, 0, 'Consumer', '2020-12-03 22:03:13', '2020-12-03 22:03:13'),
(580, 9, 0, 'Consumer', '2020-12-03 22:04:56', '2020-12-03 22:04:56'),
(581, 9, 0, 'Consumer', '2020-12-03 22:17:15', '2020-12-03 22:17:15'),
(582, 9, 0, 'Consumer', '2020-12-03 22:18:03', '2020-12-03 22:18:03'),
(583, 9, 0, 'Consumer', '2020-12-03 22:23:17', '2020-12-03 22:23:17'),
(584, 9, 0, 'Consumer', '2020-12-03 22:24:08', '2020-12-03 22:24:08'),
(585, 9, 0, 'Consumer', '2020-12-03 22:24:51', '2020-12-03 22:24:51'),
(586, 9, 0, 'Consumer', '2020-12-03 22:25:23', '2020-12-03 22:25:23'),
(587, 9, 0, 'Consumer', '2020-12-03 22:26:53', '2020-12-03 22:26:53'),
(588, 9, 0, 'Consumer', '2020-12-03 22:27:23', '2020-12-03 22:27:23'),
(589, 9, 0, 'Consumer', '2020-12-03 22:28:32', '2020-12-03 22:28:32'),
(590, 9, 0, 'Consumer', '2020-12-03 22:31:28', '2020-12-03 22:31:28'),
(591, 9, 0, 'Consumer', '2020-12-03 22:34:48', '2020-12-03 22:34:48'),
(592, 9, 0, 'Consumer', '2020-12-03 22:37:34', '2020-12-03 22:37:34'),
(593, 9, 0, 'Consumer', '2020-12-03 22:37:47', '2020-12-03 22:37:47'),
(594, 9, 0, 'Consumer', '2020-12-17 10:17:14', '2020-12-17 10:17:14'),
(595, 9, 0, 'Consumer', '2020-12-17 10:21:09', '2020-12-17 10:21:09'),
(596, 9, 0, 'Consumer', '2020-12-17 10:21:55', '2020-12-17 10:21:55'),
(597, 9, 0, 'Consumer', '2020-12-17 10:23:52', '2020-12-17 10:23:52'),
(598, 9, 0, 'Consumer', '2020-12-17 10:25:55', '2020-12-17 10:25:55'),
(599, 9, 0, 'Consumer', '2020-12-17 10:27:06', '2020-12-17 10:27:06'),
(600, 9, 0, 'Consumer', '2020-12-17 10:27:56', '2020-12-17 10:27:56'),
(601, 9, 0, 'Consumer', '2020-12-17 10:32:12', '2020-12-17 10:32:12'),
(602, 9, 0, 'Consumer', '2020-12-17 10:35:15', '2020-12-17 10:35:15'),
(603, 9, 0, 'Consumer', '2020-12-17 10:37:49', '2020-12-17 10:37:49'),
(604, 9, 0, 'Consumer', '2020-12-17 10:39:50', '2020-12-17 10:39:50'),
(605, 9, 0, 'Consumer', '2020-12-17 10:44:48', '2020-12-17 10:44:48'),
(606, 9, 0, 'Consumer', '2020-12-17 10:47:14', '2020-12-17 10:47:14'),
(607, 9, 0, 'Consumer', '2020-12-17 10:48:09', '2020-12-17 10:48:09'),
(608, 9, 0, 'Consumer', '2020-12-17 10:50:28', '2020-12-17 10:50:28'),
(609, 9, 0, 'Consumer', '2020-12-17 10:51:34', '2020-12-17 10:51:34'),
(610, 9, 0, 'Consumer', '2020-12-17 11:04:01', '2020-12-17 11:04:01'),
(611, 9, 0, 'Consumer', '2020-12-17 11:09:35', '2020-12-17 11:09:35'),
(612, 9, 0, 'Consumer', '2020-12-17 11:11:29', '2020-12-17 11:11:29'),
(613, 9, 0, 'Consumer', '2020-12-17 11:12:57', '2020-12-17 11:12:57'),
(614, 9, 0, 'Consumer', '2020-12-17 11:17:56', '2020-12-17 11:17:56'),
(615, 9, 0, 'Consumer', '2020-12-17 11:20:43', '2020-12-17 11:20:43'),
(616, 9, 0, 'Consumer', '2020-12-17 11:22:18', '2020-12-17 11:22:18'),
(617, 9, 0, 'Driver', '2020-12-17 11:22:55', '2020-12-17 11:22:55'),
(618, 9, 0, 'Consumer', '2020-12-17 11:28:25', '2020-12-17 11:28:25'),
(619, 9, 0, 'Consumer', '2020-12-17 11:41:57', '2020-12-17 11:41:57'),
(620, 9, 0, 'Consumer', '2020-12-17 11:44:26', '2020-12-17 11:44:26'),
(621, 9, 0, 'Consumer', '2020-12-17 11:50:39', '2020-12-17 11:50:39'),
(622, 9, 0, 'Consumer', '2020-12-17 11:54:58', '2020-12-17 11:54:58'),
(623, 9, 0, 'Consumer', '2020-12-17 11:58:50', '2020-12-17 11:58:50'),
(624, 9, 0, 'Consumer', '2020-12-17 11:58:50', '2020-12-17 11:58:50'),
(625, 9, 0, 'Consumer', '2020-12-17 12:04:56', '2020-12-17 12:04:56'),
(626, 9, 0, 'Consumer', '2020-12-17 12:14:14', '2020-12-17 12:14:14'),
(627, 9, 0, 'Consumer', '2020-12-17 12:15:25', '2020-12-17 12:15:25'),
(628, 9, 0, 'Consumer', '2020-12-17 12:16:15', '2020-12-17 12:16:15'),
(629, 9, 0, 'Consumer', '2020-12-17 12:17:22', '2020-12-17 12:17:22'),
(630, 9, 0, 'Consumer', '2020-12-17 12:19:17', '2020-12-17 12:19:17'),
(631, 9, 0, 'Consumer', '2020-12-17 12:21:18', '2020-12-17 12:21:18'),
(632, 9, 0, 'Consumer', '2020-12-17 12:24:27', '2020-12-17 12:24:27'),
(633, 9, 0, 'Consumer', '2020-12-17 12:27:21', '2020-12-17 12:27:21'),
(634, 9, 0, 'Driver', '2020-12-17 12:28:45', '2020-12-17 12:28:45'),
(635, 9, 0, 'Consumer', '2020-12-17 12:29:02', '2020-12-17 12:29:02'),
(636, 9, 0, 'Consumer', '2020-12-17 12:33:31', '2020-12-17 12:33:31'),
(637, 9, 0, 'Consumer', '2020-12-17 12:36:36', '2020-12-17 12:36:36'),
(638, 9, 0, 'Consumer', '2020-12-17 12:37:42', '2020-12-17 12:37:42'),
(639, 7, 3, 'Driver', '2022-05-11 00:45:54', '2022-05-11 00:45:54'),
(640, 7, 3, 'Driver', '2022-05-11 00:45:55', '2022-05-11 00:45:55'),
(641, 7, 3, 'Driver', '2022-05-11 00:47:31', '2022-05-11 00:47:31'),
(642, 7, 3, 'Driver', '2022-05-11 00:48:01', '2022-05-11 00:48:01'),
(643, 7, 4, 'Consumer', '2022-05-11 00:48:23', '2022-05-11 00:48:23'),
(644, 7, 3, 'Driver', '2022-05-11 01:01:19', '2022-05-11 01:01:19'),
(645, 7, 3, 'Driver', '2022-05-11 15:01:10', '2022-05-11 15:01:10'),
(646, 7, 3, 'Driver', '2022-05-11 15:01:11', '2022-05-11 15:01:11'),
(647, 7, 3, 'Driver', '2022-05-11 15:01:30', '2022-05-11 15:01:30'),
(648, 7, 3, 'Driver', '2022-05-11 15:02:43', '2022-05-11 15:02:43'),
(649, 7, 4, 'Consumer', '2022-05-11 15:03:18', '2022-05-11 15:03:18'),
(650, 7, 3, 'Driver', '2022-05-11 15:09:15', '2022-05-11 15:09:15'),
(651, 7, 3, 'Driver', '2022-05-11 15:16:54', '2022-05-11 15:16:54'),
(652, 7, 3, 'Driver', '2022-05-11 15:19:18', '2022-05-11 15:19:18'),
(653, 7, 3, 'Driver', '2022-05-11 16:01:19', '2022-05-11 16:01:19'),
(654, 7, 3, 'Driver', '2022-05-11 16:11:08', '2022-05-11 16:11:08'),
(655, 7, 4, 'Consumer', '2022-05-11 16:19:15', '2022-05-11 16:19:15'),
(656, 7, 3, 'Driver', '2022-05-11 17:18:03', '2022-05-11 17:18:03'),
(657, 7, 4, 'Consumer', '2022-05-11 17:18:10', '2022-05-11 17:18:10'),
(658, 7, 4, 'Consumer', '2022-05-11 17:36:26', '2022-05-11 17:36:26'),
(659, 7, 4, 'Consumer', '2022-05-11 17:36:32', '2022-05-11 17:36:32'),
(660, 7, 4, 'Consumer', '2022-05-11 18:07:59', '2022-05-11 18:07:59'),
(661, 7, 3, 'Driver', '2022-05-11 19:37:31', '2022-05-11 19:37:31'),
(662, 7, 3, 'Driver', '2022-05-11 20:11:08', '2022-05-11 20:11:08'),
(663, 7, 3, 'Driver', '2022-05-11 20:11:09', '2022-05-11 20:11:09'),
(664, 7, 3, 'Driver', '2022-05-11 20:12:21', '2022-05-11 20:12:21'),
(665, 7, 3, 'Driver', '2022-05-11 20:49:07', '2022-05-11 20:49:07'),
(666, 7, 4, 'Consumer', '2022-05-11 20:50:14', '2022-05-11 20:50:14'),
(667, 7, 4, 'Consumer', '2022-05-11 20:50:19', '2022-05-11 20:50:19'),
(668, 7, 4, 'Consumer', '2022-05-11 20:50:35', '2022-05-11 20:50:35'),
(669, 7, 4, 'Consumer', '2022-05-11 20:50:37', '2022-05-11 20:50:37'),
(670, 7, 4, 'Consumer', '2022-05-11 20:51:01', '2022-05-11 20:51:01'),
(671, 7, 4, 'Consumer', '2022-05-11 20:51:02', '2022-05-11 20:51:02'),
(672, 7, 3, 'Driver', '2022-05-11 22:46:54', '2022-05-11 22:46:54'),
(673, 7, 3, 'Driver', '2022-05-11 22:46:55', '2022-05-11 22:46:55'),
(674, 7, 4, 'Consumer', '2022-05-11 22:48:42', '2022-05-11 22:48:42'),
(675, 7, 4, 'Consumer', '2022-05-11 22:48:46', '2022-05-11 22:48:46'),
(676, 7, 4, 'Consumer', '2022-05-11 22:48:48', '2022-05-11 22:48:48'),
(677, 7, 3, 'Driver', '2022-05-11 22:49:04', '2022-05-11 22:49:04'),
(678, 7, 3, 'Driver', '2022-05-11 22:50:19', '2022-05-11 22:50:19'),
(679, 7, 4, 'Consumer', '2022-05-11 22:56:25', '2022-05-11 22:56:25'),
(680, 7, 3, 'Driver', '2022-05-11 22:56:26', '2022-05-11 22:56:26'),
(681, 7, 3, 'Driver', '2022-05-11 22:56:34', '2022-05-11 22:56:34'),
(682, 7, 3, 'Driver', '2022-05-12 19:17:28', '2022-05-12 19:17:28'),
(683, 7, 3, 'Driver', '2022-05-12 19:17:29', '2022-05-12 19:17:29'),
(684, 7, 3, 'Driver', '2022-05-12 19:17:29', '2022-05-12 19:17:29'),
(685, 7, 3, 'Driver', '2022-05-12 19:17:43', '2022-05-12 19:17:43'),
(686, 7, 3, 'Driver', '2022-05-12 19:18:32', '2022-05-12 19:18:32'),
(687, 7, 3, 'Driver', '2022-05-12 19:18:33', '2022-05-12 19:18:33'),
(688, 7, 4, 'Consumer', '2022-05-12 19:18:46', '2022-05-12 19:18:46'),
(689, 7, 3, 'Driver', '2022-05-12 19:19:05', '2022-05-12 19:19:05'),
(690, 7, 4, 'Consumer', '2022-05-12 19:19:31', '2022-05-12 19:19:31'),
(691, 7, 3, 'Driver', '2022-05-12 19:19:38', '2022-05-12 19:19:38'),
(692, 7, 4, 'Consumer', '2022-05-12 19:20:04', '2022-05-12 19:20:04'),
(693, 7, 4, 'Consumer', '2022-05-12 19:20:22', '2022-05-12 19:20:22'),
(694, 7, 4, 'Consumer', '2022-05-12 19:20:22', '2022-05-12 19:20:22'),
(695, 7, 4, 'Consumer', '2022-05-12 19:20:23', '2022-05-12 19:20:23'),
(696, 7, 4, 'Consumer', '2022-05-12 19:20:24', '2022-05-12 19:20:24'),
(697, 7, 4, 'Consumer', '2022-05-12 19:20:25', '2022-05-12 19:20:25'),
(698, 7, 4, 'Consumer', '2022-05-12 19:20:31', '2022-05-12 19:20:31'),
(699, 7, 3, 'Driver', '2022-05-12 19:20:32', '2022-05-12 19:20:32'),
(700, 7, 4, 'Consumer', '2022-05-12 19:20:43', '2022-05-12 19:20:43'),
(701, 7, 4, 'Consumer', '2022-05-12 19:20:45', '2022-05-12 19:20:45'),
(702, 7, 4, 'Consumer', '2022-05-12 19:21:28', '2022-05-12 19:21:28'),
(703, 7, 4, 'Consumer', '2022-05-12 19:21:31', '2022-05-12 19:21:31'),
(704, 7, 4, 'Consumer', '2022-05-12 19:21:39', '2022-05-12 19:21:39'),
(705, 7, 4, 'Consumer', '2022-05-12 19:21:39', '2022-05-12 19:21:39'),
(706, 7, 4, 'Consumer', '2022-05-12 19:21:39', '2022-05-12 19:21:39'),
(707, 7, 3, 'Driver', '2022-05-12 19:21:39', '2022-05-12 19:21:39'),
(708, 7, 3, 'Driver', '2022-05-13 15:29:49', '2022-05-13 15:29:49'),
(709, 7, 3, 'Driver', '2022-05-13 16:24:13', '2022-05-13 16:24:13'),
(710, 7, 3, 'Driver', '2022-05-13 16:25:11', '2022-05-13 16:25:11'),
(711, 7, 4, 'Consumer', '2022-05-13 16:32:57', '2022-05-13 16:32:57'),
(712, 7, 3, 'Driver', '2022-05-13 16:35:02', '2022-05-13 16:35:02'),
(713, 7, 3, 'Driver', '2022-05-13 16:37:25', '2022-05-13 16:37:25'),
(714, 7, 3, 'Driver', '2022-05-13 17:15:07', '2022-05-13 17:15:07'),
(715, 7, 3, 'Driver', '2022-05-13 17:15:07', '2022-05-13 17:15:07'),
(716, 7, 3, 'Driver', '2022-05-13 17:15:08', '2022-05-13 17:15:08'),
(717, 7, 4, 'Consumer', '2022-05-13 17:15:26', '2022-05-13 17:15:26'),
(718, 7, 4, 'Consumer', '2022-05-13 17:15:29', '2022-05-13 17:15:29'),
(719, 7, 3, 'Driver', '2022-05-13 17:15:42', '2022-05-13 17:15:42'),
(720, 7, 3, 'Driver', '2022-05-13 17:27:22', '2022-05-13 17:27:22'),
(721, 7, 3, 'Driver', '2022-05-13 17:28:42', '2022-05-13 17:28:42'),
(722, 7, 3, 'Driver', '2022-05-13 17:31:02', '2022-05-13 17:31:02'),
(723, 7, 3, 'Driver', '2022-05-13 17:31:54', '2022-05-13 17:31:54'),
(724, 7, 3, 'Driver', '2022-05-13 17:33:03', '2022-05-13 17:33:03'),
(725, 7, 3, 'Driver', '2022-05-13 17:33:05', '2022-05-13 17:33:05'),
(726, 7, 3, 'Driver', '2022-05-13 17:33:49', '2022-05-13 17:33:49'),
(727, 7, 3, 'Driver', '2022-05-13 17:34:53', '2022-05-13 17:34:53'),
(728, 7, 3, 'Driver', '2022-05-13 23:06:26', '2022-05-13 23:06:26'),
(729, 7, 3, 'Driver', '2022-05-13 23:41:19', '2022-05-13 23:41:19'),
(730, 7, 3, 'Driver', '2022-05-13 23:42:16', '2022-05-13 23:42:16'),
(731, 7, 3, 'Driver', '2022-05-13 23:43:09', '2022-05-13 23:43:09'),
(732, 7, 3, 'Driver', '2022-05-13 23:44:22', '2022-05-13 23:44:22');
INSERT INTO `runtime_roles` (`id`, `user_id`, `role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(733, 7, 3, 'Driver', '2022-05-13 23:47:45', '2022-05-13 23:47:45'),
(734, 7, 3, 'Driver', '2022-05-13 23:49:11', '2022-05-13 23:49:11'),
(735, 7, 3, 'Driver', '2022-05-13 23:49:33', '2022-05-13 23:49:33'),
(736, 7, 3, 'Driver', '2022-05-13 23:50:20', '2022-05-13 23:50:20'),
(737, 7, 3, 'Driver', '2022-05-13 23:50:54', '2022-05-13 23:50:54'),
(738, 7, 3, 'Driver', '2022-05-13 23:55:38', '2022-05-13 23:55:38'),
(739, 7, 3, 'Driver', '2022-05-16 14:01:29', '2022-05-16 14:01:29'),
(740, 7, 3, 'Driver', '2022-05-16 14:01:30', '2022-05-16 14:01:30'),
(741, 7, 3, 'Driver', '2022-05-16 14:02:12', '2022-05-16 14:02:12'),
(742, 7, 3, 'Driver', '2022-05-16 14:05:28', '2022-05-16 14:05:28'),
(743, 7, 3, 'Driver', '2022-05-16 14:06:29', '2022-05-16 14:06:29'),
(744, 7, 3, 'Driver', '2022-05-16 14:06:56', '2022-05-16 14:06:56'),
(745, 7, 3, 'Driver', '2022-05-16 14:18:14', '2022-05-16 14:18:14'),
(746, 7, 3, 'Driver', '2022-05-16 14:18:15', '2022-05-16 14:18:15'),
(747, 7, 3, 'Driver', '2022-05-16 14:19:49', '2022-05-16 14:19:49'),
(748, 7, 3, 'Driver', '2022-05-16 14:21:41', '2022-05-16 14:21:41'),
(749, 7, 3, 'Driver', '2022-05-16 14:22:19', '2022-05-16 14:22:19'),
(750, 7, 3, 'Driver', '2022-05-16 14:32:07', '2022-05-16 14:32:07'),
(751, 7, 3, 'Driver', '2022-05-16 14:34:34', '2022-05-16 14:34:34'),
(752, 7, 4, 'Consumer', '2022-05-16 14:37:16', '2022-05-16 14:37:16'),
(753, 7, 3, 'Driver', '2022-05-16 14:37:46', '2022-05-16 14:37:46'),
(754, 7, 3, 'Driver', '2022-05-16 14:41:09', '2022-05-16 14:41:09'),
(755, 7, 3, 'Driver', '2022-05-16 14:41:25', '2022-05-16 14:41:25'),
(756, 7, 4, 'Consumer', '2022-05-16 14:43:34', '2022-05-16 14:43:34'),
(757, 7, 3, 'Driver', '2022-05-16 14:44:02', '2022-05-16 14:44:02'),
(758, 7, 3, 'Driver', '2022-05-16 14:45:05', '2022-05-16 14:45:05'),
(759, 7, 3, 'Driver', '2022-05-16 14:47:30', '2022-05-16 14:47:30'),
(760, 7, 3, 'Driver', '2022-05-16 14:48:22', '2022-05-16 14:48:22'),
(761, 7, 4, 'Consumer', '2022-05-16 14:50:40', '2022-05-16 14:50:40'),
(762, 7, 4, 'Consumer', '2022-05-16 14:52:04', '2022-05-16 14:52:04'),
(763, 7, 3, 'Driver', '2022-05-16 14:52:44', '2022-05-16 14:52:44'),
(764, 7, 3, 'Driver', '2022-05-16 15:05:14', '2022-05-16 15:05:14'),
(765, 7, 3, 'Driver', '2022-05-16 15:06:38', '2022-05-16 15:06:38'),
(766, 7, 3, 'Driver', '2022-05-16 15:12:40', '2022-05-16 15:12:40'),
(767, 7, 4, 'Consumer', '2022-05-16 15:12:48', '2022-05-16 15:12:48'),
(768, 7, 3, 'Driver', '2022-05-16 15:13:09', '2022-05-16 15:13:09'),
(769, 7, 3, 'Driver', '2022-05-16 15:14:21', '2022-05-16 15:14:21'),
(770, 7, 3, 'Driver', '2022-05-16 15:16:31', '2022-05-16 15:16:31'),
(771, 7, 3, 'Driver', '2022-05-16 15:17:11', '2022-05-16 15:17:11'),
(772, 7, 3, 'Driver', '2022-05-16 15:17:12', '2022-05-16 15:17:12'),
(773, 7, 3, 'Driver', '2022-05-16 15:17:32', '2022-05-16 15:17:32'),
(774, 7, 3, 'Driver', '2022-05-16 15:18:24', '2022-05-16 15:18:24'),
(775, 7, 3, 'Driver', '2022-05-16 15:21:13', '2022-05-16 15:21:13'),
(776, 7, 3, 'Driver', '2022-05-16 15:21:47', '2022-05-16 15:21:47'),
(777, 7, 3, 'Driver', '2022-05-16 15:23:19', '2022-05-16 15:23:19'),
(778, 7, 3, 'Driver', '2022-05-16 15:23:20', '2022-05-16 15:23:20'),
(779, 7, 3, 'Driver', '2022-05-16 16:02:05', '2022-05-16 16:02:05'),
(780, 7, 3, 'Driver', '2022-05-16 16:02:06', '2022-05-16 16:02:06'),
(781, 7, 3, 'Driver', '2022-05-16 16:02:54', '2022-05-16 16:02:54'),
(782, 7, 3, 'Driver', '2022-05-16 16:03:57', '2022-05-16 16:03:57'),
(783, 7, 3, 'Driver', '2022-05-16 16:03:58', '2022-05-16 16:03:58'),
(784, 7, 3, 'Driver', '2022-05-16 16:05:19', '2022-05-16 16:05:19'),
(785, 7, 3, 'Driver', '2022-05-16 16:09:48', '2022-05-16 16:09:48'),
(786, 7, 3, 'Driver', '2022-05-16 16:10:31', '2022-05-16 16:10:31'),
(787, 7, 3, 'Driver', '2022-05-16 16:49:52', '2022-05-16 16:49:52'),
(788, 7, 3, 'Driver', '2022-05-16 16:58:35', '2022-05-16 16:58:35'),
(789, 7, 3, 'Driver', '2022-05-16 17:01:02', '2022-05-16 17:01:02'),
(790, 7, 3, 'Driver', '2022-05-16 17:02:10', '2022-05-16 17:02:10'),
(791, 7, 3, 'Driver', '2022-05-16 18:48:11', '2022-05-16 18:48:11'),
(792, 7, 3, 'Driver', '2022-05-16 18:54:44', '2022-05-16 18:54:44'),
(793, 7, 3, 'Driver', '2022-05-16 18:59:22', '2022-05-16 18:59:22'),
(794, 7, 3, 'Driver', '2022-05-16 19:00:26', '2022-05-16 19:00:26'),
(795, 7, 3, 'Driver', '2022-05-16 19:01:55', '2022-05-16 19:01:55'),
(796, 7, 3, 'Driver', '2022-05-16 19:01:55', '2022-05-16 19:01:55'),
(797, 7, 3, 'Driver', '2022-05-16 19:01:56', '2022-05-16 19:01:56'),
(798, 7, 3, 'Driver', '2022-05-16 19:03:43', '2022-05-16 19:03:43'),
(799, 7, 3, 'Driver', '2022-05-16 19:03:45', '2022-05-16 19:03:45'),
(800, 7, 3, 'Driver', '2022-05-16 19:03:52', '2022-05-16 19:03:52'),
(801, 7, 3, 'Driver', '2022-05-16 19:04:12', '2022-05-16 19:04:12'),
(802, 7, 3, 'Driver', '2022-05-16 19:04:55', '2022-05-16 19:04:55'),
(803, 7, 3, 'Driver', '2022-05-16 19:05:46', '2022-05-16 19:05:46'),
(804, 7, 3, 'Driver', '2022-05-16 19:06:25', '2022-05-16 19:06:25'),
(805, 7, 3, 'Driver', '2022-05-16 19:07:37', '2022-05-16 19:07:37'),
(806, 7, 3, 'Driver', '2022-05-16 19:09:08', '2022-05-16 19:09:08'),
(807, 7, 3, 'Driver', '2022-05-16 19:09:08', '2022-05-16 19:09:08'),
(808, 7, 3, 'Driver', '2022-05-16 19:10:18', '2022-05-16 19:10:18'),
(809, 7, 3, 'Driver', '2022-05-16 19:11:31', '2022-05-16 19:11:31'),
(810, 7, 3, 'Driver', '2022-05-16 19:16:52', '2022-05-16 19:16:52'),
(811, 7, 3, 'Driver', '2022-05-16 19:17:48', '2022-05-16 19:17:48'),
(812, 7, 3, 'Driver', '2022-05-16 19:21:01', '2022-05-16 19:21:01'),
(813, 7, 3, 'Driver', '2022-05-16 19:21:41', '2022-05-16 19:21:41'),
(814, 7, 3, 'Driver', '2022-05-16 19:22:22', '2022-05-16 19:22:22'),
(815, 7, 3, 'Driver', '2022-05-16 19:22:56', '2022-05-16 19:22:56'),
(816, 7, 3, 'Driver', '2022-05-16 19:23:30', '2022-05-16 19:23:30'),
(817, 7, 3, 'Driver', '2022-05-16 19:23:30', '2022-05-16 19:23:30'),
(818, 7, 3, 'Driver', '2022-05-16 19:23:31', '2022-05-16 19:23:31'),
(819, 7, 3, 'Driver', '2022-05-16 19:24:16', '2022-05-16 19:24:16'),
(820, 8, 3, 'Driver', '2022-05-18 21:09:03', '2022-05-18 21:09:03'),
(821, 8, 3, 'Driver', '2022-05-18 21:12:58', '2022-05-18 21:12:58'),
(822, 8, 4, 'Consumer', '2022-05-18 21:13:43', '2022-05-18 21:13:43'),
(823, 8, 3, 'Driver', '2022-05-18 21:42:03', '2022-05-18 21:42:03'),
(824, 8, 3, 'Driver', '2022-05-18 21:43:01', '2022-05-18 21:43:01'),
(825, 8, 3, 'Driver', '2022-05-18 21:44:33', '2022-05-18 21:44:33'),
(826, 8, 3, 'Driver', '2022-05-18 21:47:32', '2022-05-18 21:47:32'),
(827, 8, 3, 'Driver', '2022-05-18 21:48:07', '2022-05-18 21:48:07'),
(828, 8, 3, 'Driver', '2022-05-18 21:50:40', '2022-05-18 21:50:40'),
(829, 8, 3, 'Driver', '2022-05-18 21:51:47', '2022-05-18 21:51:47'),
(830, 8, 3, 'Driver', '2022-05-18 21:52:22', '2022-05-18 21:52:22'),
(831, 8, 3, 'Driver', '2022-05-18 21:52:44', '2022-05-18 21:52:44'),
(832, 8, 3, 'Driver', '2022-05-18 21:53:15', '2022-05-18 21:53:15'),
(833, 8, 3, 'Driver', '2022-05-18 21:53:51', '2022-05-18 21:53:51'),
(834, 8, 3, 'Driver', '2022-05-18 21:53:59', '2022-05-18 21:53:59'),
(835, 8, 3, 'Driver', '2022-05-18 21:55:51', '2022-05-18 21:55:51'),
(836, 8, 3, 'Driver', '2022-05-18 21:57:29', '2022-05-18 21:57:29'),
(837, 8, 3, 'Driver', '2022-05-18 21:59:11', '2022-05-18 21:59:11'),
(838, 8, 3, 'Driver', '2022-05-18 22:01:01', '2022-05-18 22:01:01'),
(839, 8, 3, 'Driver', '2022-05-18 22:01:20', '2022-05-18 22:01:20'),
(840, 8, 3, 'Driver', '2022-05-18 22:01:42', '2022-05-18 22:01:42'),
(841, 8, 3, 'Driver', '2022-05-18 22:02:15', '2022-05-18 22:02:15'),
(842, 8, 3, 'Driver', '2022-05-18 22:02:51', '2022-05-18 22:02:51'),
(843, 8, 3, 'Driver', '2022-05-18 22:03:19', '2022-05-18 22:03:19'),
(844, 8, 3, 'Driver', '2022-05-18 22:04:10', '2022-05-18 22:04:10'),
(845, 8, 3, 'Driver', '2022-05-18 22:05:47', '2022-05-18 22:05:47'),
(846, 8, 3, 'Driver', '2022-05-18 22:06:57', '2022-05-18 22:06:57'),
(847, 8, 3, 'Driver', '2022-05-18 22:07:34', '2022-05-18 22:07:34'),
(848, 8, 3, 'Driver', '2022-05-18 22:09:38', '2022-05-18 22:09:38'),
(849, 8, 3, 'Driver', '2022-05-18 22:09:52', '2022-05-18 22:09:52'),
(850, 8, 3, 'Driver', '2022-05-18 22:10:13', '2022-05-18 22:10:13'),
(851, 8, 3, 'Driver', '2022-05-18 22:10:41', '2022-05-18 22:10:41'),
(852, 8, 3, 'Driver', '2022-05-18 22:11:09', '2022-05-18 22:11:09'),
(853, 8, 3, 'Driver', '2022-05-18 22:12:06', '2022-05-18 22:12:06'),
(854, 8, 3, 'Driver', '2022-05-18 22:12:39', '2022-05-18 22:12:39'),
(855, 8, 3, 'Driver', '2022-05-18 22:13:08', '2022-05-18 22:13:08'),
(856, 8, 3, 'Driver', '2022-05-18 22:13:50', '2022-05-18 22:13:50'),
(857, 8, 3, 'Driver', '2022-05-18 22:14:29', '2022-05-18 22:14:29'),
(858, 8, 3, 'Driver', '2022-05-18 22:14:43', '2022-05-18 22:14:43'),
(859, 8, 3, 'Driver', '2022-05-18 22:15:31', '2022-05-18 22:15:31'),
(860, 8, 3, 'Driver', '2022-05-18 22:16:02', '2022-05-18 22:16:02'),
(861, 8, 3, 'Driver', '2022-05-18 22:16:26', '2022-05-18 22:16:26'),
(862, 8, 3, 'Driver', '2022-05-18 22:18:34', '2022-05-18 22:18:34'),
(863, 8, 3, 'Driver', '2022-05-18 22:19:24', '2022-05-18 22:19:24'),
(864, 8, 3, 'Driver', '2022-05-18 22:20:14', '2022-05-18 22:20:14'),
(865, 8, 3, 'Driver', '2022-05-18 22:21:13', '2022-05-18 22:21:13'),
(866, 8, 3, 'Driver', '2022-05-18 22:21:52', '2022-05-18 22:21:52'),
(867, 8, 3, 'Driver', '2022-05-18 22:22:50', '2022-05-18 22:22:50'),
(868, 8, 3, 'Driver', '2022-05-18 22:23:27', '2022-05-18 22:23:27'),
(869, 8, 3, 'Driver', '2022-05-18 22:24:06', '2022-05-18 22:24:06'),
(870, 8, 3, 'Driver', '2022-05-18 22:24:37', '2022-05-18 22:24:37'),
(871, 8, 3, 'Driver', '2022-05-18 22:25:14', '2022-05-18 22:25:14'),
(872, 8, 3, 'Driver', '2022-05-18 22:25:31', '2022-05-18 22:25:31'),
(873, 8, 3, 'Driver', '2022-05-18 22:26:53', '2022-05-18 22:26:53'),
(874, 8, 3, 'Driver', '2022-05-18 22:27:09', '2022-05-18 22:27:09'),
(875, 8, 3, 'Driver', '2022-05-18 22:29:19', '2022-05-18 22:29:19'),
(876, 8, 3, 'Driver', '2022-05-18 22:30:35', '2022-05-18 22:30:35'),
(877, 8, 3, 'Driver', '2022-05-18 22:31:12', '2022-05-18 22:31:12'),
(878, 8, 3, 'Driver', '2022-05-18 22:32:00', '2022-05-18 22:32:00'),
(879, 8, 3, 'Driver', '2022-05-18 22:32:11', '2022-05-18 22:32:11'),
(880, 8, 3, 'Driver', '2022-05-18 22:32:28', '2022-05-18 22:32:28'),
(881, 8, 3, 'Driver', '2022-05-18 22:32:48', '2022-05-18 22:32:48'),
(882, 8, 3, 'Driver', '2022-05-18 22:33:01', '2022-05-18 22:33:01'),
(883, 8, 3, 'Driver', '2022-05-18 22:33:35', '2022-05-18 22:33:35'),
(884, 9, 3, 'Driver', '2022-05-19 17:49:28', '2022-05-19 17:49:28'),
(885, 9, 3, 'Driver', '2022-05-19 19:11:06', '2022-05-19 19:11:06'),
(886, 8, 3, 'Driver', '2022-05-19 21:57:50', '2022-05-19 21:57:50'),
(887, 8, 3, 'Driver', '2022-05-19 21:59:56', '2022-05-19 21:59:56'),
(888, 8, 3, 'Driver', '2022-05-19 22:02:44', '2022-05-19 22:02:44'),
(889, 8, 3, 'Driver', '2022-05-19 22:03:26', '2022-05-19 22:03:26'),
(890, 10, 3, 'Driver', '2022-05-19 22:05:00', '2022-05-19 22:05:00'),
(891, 8, 3, 'Driver', '2022-05-19 22:08:05', '2022-05-19 22:08:05'),
(892, 8, 3, 'Driver', '2022-05-19 22:13:23', '2022-05-19 22:13:23'),
(893, 8, 3, 'Driver', '2022-05-19 22:13:26', '2022-05-19 22:13:26'),
(894, 8, 3, 'Driver', '2022-05-19 22:13:44', '2022-05-19 22:13:44'),
(895, 8, 3, 'Driver', '2022-05-19 22:13:47', '2022-05-19 22:13:47'),
(896, 8, 3, 'Driver', '2022-05-19 22:20:31', '2022-05-19 22:20:31'),
(897, 8, 3, 'Driver', '2022-05-19 22:21:02', '2022-05-19 22:21:02'),
(898, 8, 3, 'Driver', '2022-05-19 22:23:09', '2022-05-19 22:23:09'),
(899, 8, 3, 'Driver', '2022-05-19 22:23:39', '2022-05-19 22:23:39'),
(900, 8, 3, 'Driver', '2022-05-19 22:23:48', '2022-05-19 22:23:48'),
(901, 8, 3, 'Driver', '2022-05-19 22:24:02', '2022-05-19 22:24:02'),
(902, 8, 3, 'Driver', '2022-05-19 22:24:51', '2022-05-19 22:24:51'),
(903, 8, 3, 'Driver', '2022-05-19 22:25:02', '2022-05-19 22:25:02'),
(904, 8, 3, 'Driver', '2022-05-19 22:25:48', '2022-05-19 22:25:48'),
(905, 8, 3, 'Driver', '2022-05-19 22:26:06', '2022-05-19 22:26:06'),
(906, 8, 3, 'Driver', '2022-05-19 22:27:12', '2022-05-19 22:27:12'),
(907, 8, 3, 'Driver', '2022-05-19 22:28:25', '2022-05-19 22:28:25'),
(908, 8, 3, 'Driver', '2022-05-19 22:28:27', '2022-05-19 22:28:27'),
(909, 8, 3, 'Driver', '2022-05-19 22:30:06', '2022-05-19 22:30:06'),
(910, 8, 3, 'Driver', '2022-05-19 22:33:36', '2022-05-19 22:33:36'),
(911, 8, 3, 'Driver', '2022-05-19 22:33:49', '2022-05-19 22:33:49'),
(912, 8, 3, 'Driver', '2022-05-19 22:34:10', '2022-05-19 22:34:10'),
(913, 8, 3, 'Driver', '2022-05-21 14:24:53', '2022-05-21 14:24:53'),
(914, 8, 3, 'Driver', '2022-05-21 15:13:29', '2022-05-21 15:13:29'),
(915, 8, 3, 'Driver', '2022-05-21 16:40:47', '2022-05-21 16:40:47'),
(916, 8, 4, 'Consumer', '2022-05-21 16:41:49', '2022-05-21 16:41:49'),
(917, 8, 4, 'Consumer', '2022-05-21 16:42:13', '2022-05-21 16:42:13'),
(918, 8, 4, 'Consumer', '2022-05-21 16:42:17', '2022-05-21 16:42:17'),
(919, 8, 4, 'Consumer', '2022-05-21 16:42:18', '2022-05-21 16:42:18'),
(920, 8, 4, 'Consumer', '2022-05-21 16:43:35', '2022-05-21 16:43:35'),
(921, 8, 3, 'Driver', '2022-05-21 16:43:53', '2022-05-21 16:43:53'),
(922, 11, 3, 'Driver', '2022-05-21 16:46:31', '2022-05-21 16:46:31'),
(923, 11, 3, 'Driver', '2022-05-21 16:46:34', '2022-05-21 16:46:34'),
(924, 11, 3, 'Driver', '2022-05-21 16:47:20', '2022-05-21 16:47:20'),
(925, 11, 3, 'Driver', '2022-05-21 18:30:12', '2022-05-21 18:30:12'),
(926, 11, 3, 'Driver', '2022-05-21 18:30:13', '2022-05-21 18:30:13'),
(927, 11, 3, 'Driver', '2022-05-21 18:30:17', '2022-05-21 18:30:17'),
(928, 11, 4, 'Consumer', '2022-05-21 18:40:53', '2022-05-21 18:40:53'),
(929, 11, 4, 'Consumer', '2022-05-21 18:42:54', '2022-05-21 18:42:54'),
(930, 11, 4, 'Consumer', '2022-05-21 18:43:21', '2022-05-21 18:43:21'),
(931, 11, 4, 'Consumer', '2022-05-21 18:44:40', '2022-05-21 18:44:40'),
(932, 11, 4, 'Consumer', '2022-05-21 18:45:55', '2022-05-21 18:45:55'),
(933, 11, 4, 'Consumer', '2022-05-21 18:46:26', '2022-05-21 18:46:26'),
(934, 11, 4, 'Consumer', '2022-05-21 18:48:00', '2022-05-21 18:48:00'),
(935, 8, 3, 'Driver', '2022-05-21 18:51:18', '2022-05-21 18:51:18'),
(936, 8, 3, 'Driver', '2022-05-21 18:51:19', '2022-05-21 18:51:19'),
(937, 8, 3, 'Driver', '2022-05-21 18:51:31', '2022-05-21 18:51:31'),
(938, 8, 3, 'Driver', '2022-05-21 18:53:02', '2022-05-21 18:53:02'),
(939, 8, 3, 'Driver', '2022-05-21 18:54:22', '2022-05-21 18:54:22'),
(940, 8, 3, 'Driver', '2022-05-21 18:54:59', '2022-05-21 18:54:59'),
(941, 8, 3, 'Driver', '2022-05-21 19:00:12', '2022-05-21 19:00:12'),
(942, 8, 3, 'Driver', '2022-05-21 19:04:10', '2022-05-21 19:04:10'),
(943, 8, 3, 'Driver', '2022-05-21 19:05:39', '2022-05-21 19:05:39'),
(944, 8, 3, 'Driver', '2022-05-21 19:08:21', '2022-05-21 19:08:21'),
(945, 8, 3, 'Driver', '2022-05-21 19:09:25', '2022-05-21 19:09:25'),
(946, 8, 3, 'Driver', '2022-05-21 19:11:20', '2022-05-21 19:11:20'),
(947, 8, 3, 'Driver', '2022-05-21 19:12:34', '2022-05-21 19:12:34'),
(948, 12, 3, 'Driver', '2022-05-21 19:13:22', '2022-05-21 19:13:22'),
(949, 12, 3, 'Driver', '2022-05-21 19:13:24', '2022-05-21 19:13:24'),
(950, 12, 3, 'Driver', '2022-05-21 19:13:26', '2022-05-21 19:13:26'),
(951, 8, 3, 'Driver', '2022-05-21 19:16:48', '2022-05-21 19:16:48'),
(952, 8, 3, 'Driver', '2022-05-21 19:18:18', '2022-05-21 19:18:18'),
(953, 8, 4, 'Consumer', '2022-05-21 20:03:55', '2022-05-21 20:03:55'),
(954, 8, 4, 'Consumer', '2022-05-21 20:07:59', '2022-05-21 20:07:59'),
(955, 8, 4, 'Consumer', '2022-05-21 20:09:07', '2022-05-21 20:09:07'),
(956, 8, 4, 'Consumer', '2022-05-21 20:10:56', '2022-05-21 20:10:56'),
(957, 8, 4, 'Consumer', '2022-05-21 20:16:11', '2022-05-21 20:16:11'),
(958, 8, 4, 'Consumer', '2022-05-21 20:16:56', '2022-05-21 20:16:56'),
(959, 8, 4, 'Consumer', '2022-05-21 20:18:03', '2022-05-21 20:18:03'),
(960, 12, 3, 'Driver', '2022-05-21 20:20:06', '2022-05-21 20:20:06'),
(961, 12, 4, 'Consumer', '2022-05-21 20:20:30', '2022-05-21 20:20:30'),
(962, 12, 4, 'Consumer', '2022-05-21 20:20:36', '2022-05-21 20:20:36'),
(963, 12, 4, 'Consumer', '2022-05-21 20:20:36', '2022-05-21 20:20:36'),
(964, 8, 4, 'Consumer', '2022-05-21 20:20:42', '2022-05-21 20:20:42'),
(965, 8, 4, 'Consumer', '2022-05-21 20:21:14', '2022-05-21 20:21:14'),
(966, 8, 4, 'Consumer', '2022-05-21 20:21:47', '2022-05-21 20:21:47'),
(967, 8, 4, 'Consumer', '2022-05-21 20:22:26', '2022-05-21 20:22:26'),
(968, 12, 3, 'Driver', '2022-05-21 20:22:45', '2022-05-21 20:22:45'),
(969, 8, 4, 'Consumer', '2022-05-21 20:28:00', '2022-05-21 20:28:00'),
(970, 8, 3, 'Driver', '2022-05-21 20:30:56', '2022-05-21 20:30:56'),
(971, 8, 3, 'Driver', '2022-05-21 20:32:06', '2022-05-21 20:32:06'),
(972, 8, 3, 'Driver', '2022-05-21 20:32:53', '2022-05-21 20:32:53'),
(973, 8, 4, 'Consumer', '2022-05-21 20:33:15', '2022-05-21 20:33:15'),
(974, 8, 3, 'Driver', '2022-05-21 20:35:05', '2022-05-21 20:35:05'),
(975, 8, 3, 'Driver', '2022-05-21 20:37:46', '2022-05-21 20:37:46'),
(976, 8, 4, 'Consumer', '2022-05-21 20:38:08', '2022-05-21 20:38:08'),
(977, 8, 4, 'Consumer', '2022-05-21 20:43:36', '2022-05-21 20:43:36'),
(978, 8, 4, 'Consumer', '2022-05-21 20:47:23', '2022-05-21 20:47:23'),
(979, 8, 4, 'Consumer', '2022-05-21 20:48:31', '2022-05-21 20:48:31'),
(980, 8, 4, 'Consumer', '2022-05-21 20:48:55', '2022-05-21 20:48:55'),
(981, 8, 3, 'Driver', '2022-05-21 20:50:37', '2022-05-21 20:50:37'),
(982, 8, 4, 'Consumer', '2022-05-21 20:50:53', '2022-05-21 20:50:53'),
(983, 8, 4, 'Consumer', '2022-05-21 20:51:06', '2022-05-21 20:51:06'),
(984, 8, 3, 'Driver', '2022-05-21 20:51:16', '2022-05-21 20:51:16'),
(985, 8, 4, 'Consumer', '2022-05-21 20:51:44', '2022-05-21 20:51:44'),
(986, 8, 3, 'Driver', '2022-05-21 20:51:59', '2022-05-21 20:51:59'),
(987, 8, 3, 'Driver', '2022-05-21 20:53:03', '2022-05-21 20:53:03'),
(988, 8, 3, 'Driver', '2022-05-21 20:53:24', '2022-05-21 20:53:24'),
(989, 8, 4, 'Consumer', '2022-05-21 20:55:44', '2022-05-21 20:55:44'),
(990, 12, 3, 'Driver', '2022-05-21 20:55:48', '2022-05-21 20:55:48'),
(991, 8, 3, 'Driver', '2022-05-21 20:59:16', '2022-05-21 20:59:16'),
(992, 8, 3, 'Driver', '2022-05-21 20:59:39', '2022-05-21 20:59:39'),
(993, 8, 3, 'Driver', '2022-05-21 21:07:09', '2022-05-21 21:07:09'),
(994, 8, 3, 'Driver', '2022-05-21 21:08:37', '2022-05-21 21:08:37'),
(995, 8, 3, 'Driver', '2022-05-21 21:09:45', '2022-05-21 21:09:45'),
(996, 8, 3, 'Driver', '2022-05-21 21:11:00', '2022-05-21 21:11:00'),
(997, 8, 3, 'Driver', '2022-05-21 21:12:03', '2022-05-21 21:12:03'),
(998, 8, 3, 'Driver', '2022-05-21 21:45:57', '2022-05-21 21:45:57'),
(999, 8, 3, 'Driver', '2022-05-21 21:51:37', '2022-05-21 21:51:37'),
(1000, 12, 3, 'Driver', '2022-05-21 21:52:31', '2022-05-21 21:52:31'),
(1001, 12, 4, 'Consumer', '2022-05-21 21:53:23', '2022-05-21 21:53:23'),
(1002, 12, 4, 'Consumer', '2022-05-21 21:54:32', '2022-05-21 21:54:32'),
(1003, 12, 3, 'Driver', '2022-05-21 21:55:16', '2022-05-21 21:55:16'),
(1004, 12, 4, 'Consumer', '2022-05-21 21:56:02', '2022-05-21 21:56:02'),
(1005, 12, 4, 'Consumer', '2022-05-21 21:57:53', '2022-05-21 21:57:53'),
(1006, 12, 4, 'Consumer', '2022-05-21 21:58:55', '2022-05-21 21:58:55'),
(1007, 12, 4, 'Consumer', '2022-05-21 21:59:38', '2022-05-21 21:59:38'),
(1008, 12, 3, 'Driver', '2022-05-21 22:01:12', '2022-05-21 22:01:12'),
(1009, 12, 4, 'Consumer', '2022-05-21 22:01:39', '2022-05-21 22:01:39'),
(1010, 12, 3, 'Driver', '2022-05-21 22:01:48', '2022-05-21 22:01:48'),
(1011, 12, 3, 'Driver', '2022-05-21 22:02:29', '2022-05-21 22:02:29'),
(1012, 12, 3, 'Driver', '2022-05-21 22:03:56', '2022-05-21 22:03:56'),
(1013, 12, 4, 'Consumer', '2022-05-21 22:04:03', '2022-05-21 22:04:03'),
(1014, 12, 3, 'Driver', '2022-05-21 22:04:29', '2022-05-21 22:04:29'),
(1015, 12, 4, 'Consumer', '2022-05-21 22:04:36', '2022-05-21 22:04:36'),
(1016, 12, 3, 'Driver', '2022-05-21 22:04:53', '2022-05-21 22:04:53'),
(1017, 12, 4, 'Consumer', '2022-05-21 22:05:00', '2022-05-21 22:05:00'),
(1018, 12, 3, 'Driver', '2022-05-21 22:05:46', '2022-05-21 22:05:46'),
(1019, 12, 4, 'Consumer', '2022-05-21 22:05:47', '2022-05-21 22:05:47'),
(1020, 12, 3, 'Driver', '2022-05-21 22:07:59', '2022-05-21 22:07:59'),
(1021, 12, 4, 'Consumer', '2022-05-21 22:09:01', '2022-05-21 22:09:01'),
(1022, 12, 4, 'Consumer', '2022-05-21 22:09:51', '2022-05-21 22:09:51'),
(1023, 12, 3, 'Driver', '2022-05-21 22:10:25', '2022-05-21 22:10:25'),
(1024, 12, 3, 'Driver', '2022-05-21 22:10:39', '2022-05-21 22:10:39'),
(1025, 12, 3, 'Driver', '2022-05-21 22:14:26', '2022-05-21 22:14:26'),
(1026, 12, 3, 'Driver', '2022-05-21 22:15:27', '2022-05-21 22:15:27'),
(1027, 12, 4, 'Consumer', '2022-05-21 22:16:09', '2022-05-21 22:16:09'),
(1028, 12, 4, 'Consumer', '2022-05-21 22:16:33', '2022-05-21 22:16:33'),
(1029, 12, 3, 'Driver', '2022-05-21 22:16:40', '2022-05-21 22:16:40'),
(1030, 12, 3, 'Driver', '2022-05-21 22:17:51', '2022-05-21 22:17:51'),
(1031, 12, 4, 'Consumer', '2022-05-21 22:19:09', '2022-05-21 22:19:09'),
(1032, 12, 4, 'Consumer', '2022-05-21 22:19:43', '2022-05-21 22:19:43'),
(1033, 12, 3, 'Driver', '2022-05-21 22:20:07', '2022-05-21 22:20:07'),
(1034, 12, 4, 'Consumer', '2022-05-21 22:20:20', '2022-05-21 22:20:20'),
(1035, 12, 3, 'Driver', '2022-05-21 22:20:30', '2022-05-21 22:20:30'),
(1036, 12, 4, 'Consumer', '2022-05-21 22:21:37', '2022-05-21 22:21:37'),
(1037, 12, 3, 'Driver', '2022-05-21 22:22:13', '2022-05-21 22:22:13'),
(1038, 12, 3, 'Driver', '2022-05-21 22:22:49', '2022-05-21 22:22:49'),
(1039, 12, 3, 'Driver', '2022-05-21 22:23:49', '2022-05-21 22:23:49'),
(1040, 12, 4, 'Consumer', '2022-05-21 22:23:55', '2022-05-21 22:23:55'),
(1041, 12, 3, 'Driver', '2022-05-21 22:26:11', '2022-05-21 22:26:11'),
(1042, 12, 3, 'Driver', '2022-05-21 22:26:44', '2022-05-21 22:26:44'),
(1043, 12, 3, 'Driver', '2022-05-21 22:27:18', '2022-05-21 22:27:18'),
(1044, 12, 3, 'Driver', '2022-05-21 22:27:58', '2022-05-21 22:27:58'),
(1045, 12, 3, 'Driver', '2022-05-21 22:33:05', '2022-05-21 22:33:05'),
(1046, 12, 3, 'Driver', '2022-05-21 22:33:55', '2022-05-21 22:33:55'),
(1047, 12, 4, 'Consumer', '2022-05-21 22:34:30', '2022-05-21 22:34:30'),
(1048, 12, 3, 'Driver', '2022-05-21 22:36:34', '2022-05-21 22:36:34'),
(1049, 12, 4, 'Consumer', '2022-05-21 22:37:20', '2022-05-21 22:37:20'),
(1050, 12, 3, 'Driver', '2022-05-21 22:37:31', '2022-05-21 22:37:31'),
(1051, 12, 4, 'Consumer', '2022-05-21 22:37:53', '2022-05-21 22:37:53'),
(1052, 12, 3, 'Driver', '2022-05-21 22:38:03', '2022-05-21 22:38:03'),
(1053, 12, 3, 'Driver', '2022-05-21 22:38:25', '2022-05-21 22:38:25'),
(1054, 12, 4, 'Consumer', '2022-05-21 22:39:13', '2022-05-21 22:39:13'),
(1055, 12, 3, 'Driver', '2022-05-21 22:41:33', '2022-05-21 22:41:33'),
(1056, 12, 3, 'Driver', '2022-05-21 22:42:21', '2022-05-21 22:42:21'),
(1057, 12, 3, 'Driver', '2022-05-21 22:43:16', '2022-05-21 22:43:16'),
(1058, 12, 4, 'Consumer', '2022-05-21 22:43:31', '2022-05-21 22:43:31'),
(1059, 12, 4, 'Consumer', '2022-05-21 22:44:40', '2022-05-21 22:44:40'),
(1060, 12, 4, 'Consumer', '2022-05-21 22:45:37', '2022-05-21 22:45:37'),
(1061, 12, 4, 'Consumer', '2022-05-21 22:46:42', '2022-05-21 22:46:42'),
(1062, 12, 4, 'Consumer', '2022-05-21 22:48:10', '2022-05-21 22:48:10'),
(1063, 12, 4, 'Consumer', '2022-05-21 22:49:57', '2022-05-21 22:49:57'),
(1064, 12, 4, 'Consumer', '2022-05-21 22:51:43', '2022-05-21 22:51:43'),
(1065, 12, 4, 'Consumer', '2022-05-23 15:53:00', '2022-05-23 15:53:00'),
(1066, 12, 3, 'Driver', '2022-05-23 15:54:15', '2022-05-23 15:54:15'),
(1067, 12, 3, 'Driver', '2022-05-23 16:09:32', '2022-05-23 16:09:32'),
(1068, 12, 3, 'Driver', '2022-05-23 16:12:15', '2022-05-23 16:12:15'),
(1069, 12, 3, 'Driver', '2022-05-23 16:19:55', '2022-05-23 16:19:55'),
(1070, 12, 3, 'Driver', '2022-05-23 16:25:14', '2022-05-23 16:25:14'),
(1071, 12, 3, 'Driver', '2022-05-23 16:30:28', '2022-05-23 16:30:28'),
(1072, 12, 3, 'Driver', '2022-05-23 16:53:36', '2022-05-23 16:53:36'),
(1073, 12, 3, 'Driver', '2022-05-23 16:54:02', '2022-05-23 16:54:02'),
(1074, 12, 3, 'Driver', '2022-05-23 17:23:44', '2022-05-23 17:23:44'),
(1075, 12, 3, 'Driver', '2022-05-23 17:31:42', '2022-05-23 17:31:42'),
(1076, 12, 3, 'Driver', '2022-05-23 17:33:43', '2022-05-23 17:33:43'),
(1077, 12, 3, 'Driver', '2022-05-23 17:36:23', '2022-05-23 17:36:23'),
(1078, 12, 3, 'Driver', '2022-05-23 17:42:49', '2022-05-23 17:42:49'),
(1079, 12, 4, 'Consumer', '2022-05-23 17:42:58', '2022-05-23 17:42:58'),
(1080, 12, 3, 'Driver', '2022-05-23 17:52:40', '2022-05-23 17:52:40'),
(1081, 12, 3, 'Driver', '2022-05-23 17:54:53', '2022-05-23 17:54:53'),
(1082, 12, 3, 'Driver', '2022-05-23 17:56:56', '2022-05-23 17:56:56'),
(1083, 12, 3, 'Driver', '2022-05-23 17:58:13', '2022-05-23 17:58:13'),
(1084, 12, 3, 'Driver', '2022-05-23 17:59:30', '2022-05-23 17:59:30'),
(1085, 12, 3, 'Driver', '2022-05-23 18:06:27', '2022-05-23 18:06:27'),
(1086, 12, 3, 'Driver', '2022-05-23 18:07:09', '2022-05-23 18:07:09'),
(1087, 12, 3, 'Driver', '2022-05-23 18:07:51', '2022-05-23 18:07:51'),
(1088, 12, 3, 'Driver', '2022-05-23 18:08:29', '2022-05-23 18:08:29'),
(1089, 12, 3, 'Driver', '2022-05-23 18:11:00', '2022-05-23 18:11:00'),
(1090, 12, 3, 'Driver', '2022-05-23 18:11:39', '2022-05-23 18:11:39'),
(1091, 12, 3, 'Driver', '2022-05-23 18:12:33', '2022-05-23 18:12:33'),
(1092, 12, 3, 'Driver', '2022-05-23 18:13:30', '2022-05-23 18:13:30'),
(1093, 12, 3, 'Driver', '2022-05-23 18:14:10', '2022-05-23 18:14:10'),
(1094, 12, 3, 'Driver', '2022-05-23 18:36:53', '2022-05-23 18:36:53'),
(1095, 12, 3, 'Driver', '2022-05-23 18:36:55', '2022-05-23 18:36:55'),
(1096, 12, 3, 'Driver', '2022-05-23 18:38:37', '2022-05-23 18:38:37'),
(1097, 12, 3, 'Driver', '2022-05-23 18:39:31', '2022-05-23 18:39:31'),
(1098, 12, 3, 'Driver', '2022-05-23 18:45:32', '2022-05-23 18:45:32'),
(1099, 12, 3, 'Driver', '2022-05-23 18:46:48', '2022-05-23 18:46:48'),
(1100, 12, 3, 'Driver', '2022-05-23 18:47:40', '2022-05-23 18:47:40'),
(1101, 12, 3, 'Driver', '2022-05-23 19:15:49', '2022-05-23 19:15:49'),
(1102, 12, 3, 'Driver', '2022-05-23 20:34:25', '2022-05-23 20:34:25'),
(1103, 12, 3, 'Driver', '2022-05-23 20:35:15', '2022-05-23 20:35:15'),
(1104, 12, 3, 'Driver', '2022-05-23 20:38:05', '2022-05-23 20:38:05'),
(1105, 12, 3, 'Driver', '2022-05-23 20:40:10', '2022-05-23 20:40:10'),
(1106, 12, 3, 'Driver', '2022-05-23 20:41:45', '2022-05-23 20:41:45'),
(1107, 12, 3, 'Driver', '2022-05-23 20:44:25', '2022-05-23 20:44:25'),
(1108, 12, 4, 'Consumer', '2022-05-23 21:00:07', '2022-05-23 21:00:07'),
(1109, 12, 3, 'Driver', '2022-05-23 21:00:33', '2022-05-23 21:00:33'),
(1110, 12, 3, 'Driver', '2022-05-23 22:27:29', '2022-05-23 22:27:29'),
(1111, 12, 3, 'Driver', '2022-05-23 22:34:37', '2022-05-23 22:34:37'),
(1112, 12, 3, 'Driver', '2022-05-23 22:36:10', '2022-05-23 22:36:10'),
(1113, 12, 3, 'Driver', '2022-05-23 22:38:52', '2022-05-23 22:38:52'),
(1114, 12, 3, 'Driver', '2022-05-23 22:40:15', '2022-05-23 22:40:15'),
(1115, 12, 3, 'Driver', '2022-05-23 22:41:56', '2022-05-23 22:41:56'),
(1116, 12, 3, 'Driver', '2022-05-23 22:45:47', '2022-05-23 22:45:47'),
(1117, 12, 4, 'Consumer', '2022-05-23 22:47:36', '2022-05-23 22:47:36'),
(1118, 12, 3, 'Driver', '2022-05-23 22:47:46', '2022-05-23 22:47:46'),
(1119, 12, 3, 'Driver', '2022-05-23 22:51:49', '2022-05-23 22:51:49'),
(1120, 12, 3, 'Driver', '2022-05-23 22:53:23', '2022-05-23 22:53:23'),
(1121, 12, 3, 'Driver', '2022-05-23 22:55:27', '2022-05-23 22:55:27'),
(1122, 12, 3, 'Driver', '2022-05-23 22:57:15', '2022-05-23 22:57:15'),
(1123, 12, 3, 'Driver', '2022-05-23 22:57:34', '2022-05-23 22:57:34'),
(1124, 12, 4, 'Consumer', '2022-05-23 23:20:36', '2022-05-23 23:20:36'),
(1125, 12, 4, 'Consumer', '2022-05-23 23:22:41', '2022-05-23 23:22:41'),
(1126, 12, 4, 'Consumer', '2022-05-23 23:24:26', '2022-05-23 23:24:26'),
(1127, 12, 4, 'Consumer', '2022-05-23 23:25:49', '2022-05-23 23:25:49'),
(1128, 12, 4, 'Consumer', '2022-05-23 23:26:19', '2022-05-23 23:26:19'),
(1129, 12, 4, 'Consumer', '2022-05-23 23:27:19', '2022-05-23 23:27:19'),
(1130, 12, 4, 'Consumer', '2022-05-23 23:31:22', '2022-05-23 23:31:22'),
(1131, 12, 3, 'Driver', '2022-05-23 23:34:50', '2022-05-23 23:34:50'),
(1132, 12, 4, 'Consumer', '2022-05-23 23:36:06', '2022-05-23 23:36:06'),
(1133, 12, 4, 'Consumer', '2022-05-23 23:36:30', '2022-05-23 23:36:30'),
(1134, 12, 3, 'Driver', '2022-05-23 23:36:54', '2022-05-23 23:36:54'),
(1135, 12, 4, 'Consumer', '2022-05-23 23:40:13', '2022-05-23 23:40:13'),
(1136, 12, 4, 'Consumer', '2022-05-23 23:41:51', '2022-05-23 23:41:51'),
(1137, 12, 4, 'Consumer', '2022-05-23 23:42:37', '2022-05-23 23:42:37'),
(1138, 12, 3, 'Driver', '2022-05-23 23:46:46', '2022-05-23 23:46:46'),
(1139, 12, 4, 'Consumer', '2022-05-23 23:47:52', '2022-05-23 23:47:52'),
(1140, 12, 4, 'Consumer', '2022-05-23 23:48:35', '2022-05-23 23:48:35'),
(1141, 12, 4, 'Consumer', '2022-05-23 23:49:03', '2022-05-23 23:49:03'),
(1142, 12, 4, 'Consumer', '2022-05-23 23:49:44', '2022-05-23 23:49:44'),
(1143, 12, 4, 'Consumer', '2022-05-23 23:50:23', '2022-05-23 23:50:23'),
(1144, 12, 4, 'Consumer', '2022-05-23 23:53:43', '2022-05-23 23:53:43'),
(1145, 12, 3, 'Driver', '2022-05-23 23:53:43', '2022-05-23 23:53:43'),
(1146, 12, 4, 'Consumer', '2022-05-23 23:55:57', '2022-05-23 23:55:57'),
(1147, 12, 3, 'Driver', '2022-05-23 23:56:07', '2022-05-23 23:56:07'),
(1148, 12, 4, 'Consumer', '2022-05-23 23:56:27', '2022-05-23 23:56:27'),
(1149, 12, 3, 'Driver', '2022-05-23 23:57:11', '2022-05-23 23:57:11'),
(1150, 12, 4, 'Consumer', '2022-05-23 23:57:32', '2022-05-23 23:57:32'),
(1151, 12, 3, 'Driver', '2022-05-23 23:57:55', '2022-05-23 23:57:55'),
(1152, 12, 4, 'Consumer', '2022-05-23 23:57:56', '2022-05-23 23:57:56'),
(1153, 12, 3, 'Driver', '2022-05-23 23:58:48', '2022-05-23 23:58:48'),
(1154, 12, 3, 'Driver', '2022-05-23 23:59:21', '2022-05-23 23:59:21'),
(1155, 12, 3, 'Driver', '2022-05-24 00:00:19', '2022-05-24 00:00:19'),
(1156, 12, 3, 'Driver', '2022-05-24 00:01:25', '2022-05-24 00:01:25'),
(1157, 12, 4, 'Consumer', '2022-05-24 00:01:28', '2022-05-24 00:01:28'),
(1158, 12, 3, 'Driver', '2022-05-24 00:02:32', '2022-05-24 00:02:32'),
(1159, 12, 4, 'Consumer', '2022-05-24 00:03:06', '2022-05-24 00:03:06'),
(1160, 12, 4, 'Consumer', '2022-05-24 00:04:18', '2022-05-24 00:04:18'),
(1161, 12, 3, 'Driver', '2022-05-24 00:06:35', '2022-05-24 00:06:35'),
(1162, 12, 4, 'Consumer', '2022-05-24 00:11:00', '2022-05-24 00:11:00'),
(1163, 12, 4, 'Consumer', '2022-05-24 00:11:55', '2022-05-24 00:11:55'),
(1164, 8, 3, 'Driver', '2022-05-24 15:18:35', '2022-05-24 15:18:35'),
(1165, 8, 4, 'Consumer', '2022-05-24 15:18:41', '2022-05-24 15:18:41'),
(1166, 8, 4, 'Consumer', '2022-05-24 15:52:09', '2022-05-24 15:52:09'),
(1167, 8, 4, 'Consumer', '2022-05-24 15:52:54', '2022-05-24 15:52:54'),
(1168, 8, 4, 'Consumer', '2022-05-24 15:54:17', '2022-05-24 15:54:17'),
(1169, 8, 4, 'Consumer', '2022-05-24 15:55:39', '2022-05-24 15:55:39'),
(1170, 8, 4, 'Consumer', '2022-05-24 15:56:24', '2022-05-24 15:56:24'),
(1171, 8, 4, 'Consumer', '2022-05-24 16:48:28', '2022-05-24 16:48:28'),
(1172, 8, 4, 'Consumer', '2022-05-24 16:50:24', '2022-05-24 16:50:24'),
(1173, 8, 4, 'Consumer', '2022-05-24 16:51:39', '2022-05-24 16:51:39'),
(1174, 8, 4, 'Consumer', '2022-05-24 16:54:00', '2022-05-24 16:54:00'),
(1175, 12, 3, 'Driver', '2022-05-24 16:56:46', '2022-05-24 16:56:46'),
(1176, 12, 3, 'Driver', '2022-05-24 16:59:51', '2022-05-24 16:59:51'),
(1177, 8, 4, 'Consumer', '2022-05-24 17:01:26', '2022-05-24 17:01:26'),
(1178, 12, 3, 'Driver', '2022-05-24 17:03:59', '2022-05-24 17:03:59'),
(1179, 12, 3, 'Driver', '2022-05-24 17:05:25', '2022-05-24 17:05:25'),
(1180, 12, 3, 'Driver', '2022-05-24 17:08:30', '2022-05-24 17:08:30'),
(1181, 12, 3, 'Driver', '2022-05-24 17:10:24', '2022-05-24 17:10:24'),
(1182, 12, 3, 'Driver', '2022-05-24 17:11:33', '2022-05-24 17:11:33'),
(1183, 12, 3, 'Driver', '2022-05-24 17:21:41', '2022-05-24 17:21:41'),
(1184, 12, 3, 'Driver', '2022-05-24 17:22:34', '2022-05-24 17:22:34'),
(1185, 12, 3, 'Driver', '2022-05-24 17:26:19', '2022-05-24 17:26:19'),
(1186, 12, 3, 'Driver', '2022-05-24 17:26:48', '2022-05-24 17:26:48'),
(1187, 12, 3, 'Driver', '2022-05-24 17:28:12', '2022-05-24 17:28:12'),
(1188, 12, 3, 'Driver', '2022-05-24 17:40:05', '2022-05-24 17:40:05'),
(1189, 12, 3, 'Driver', '2022-05-24 17:42:29', '2022-05-24 17:42:29'),
(1190, 12, 3, 'Driver', '2022-05-24 17:43:57', '2022-05-24 17:43:57'),
(1191, 12, 3, 'Driver', '2022-05-24 17:46:18', '2022-05-24 17:46:18'),
(1192, 12, 3, 'Driver', '2022-05-24 17:53:37', '2022-05-24 17:53:37'),
(1193, 12, 3, 'Driver', '2022-05-24 18:05:04', '2022-05-24 18:05:04'),
(1194, 12, 3, 'Driver', '2022-05-24 18:12:29', '2022-05-24 18:12:29'),
(1195, 12, 3, 'Driver', '2022-05-24 18:15:38', '2022-05-24 18:15:38'),
(1196, 12, 4, 'Consumer', '2022-05-24 18:16:49', '2022-05-24 18:16:49'),
(1197, 12, 4, 'Consumer', '2022-05-24 21:44:06', '2022-05-24 21:44:06'),
(1198, 12, 4, 'Consumer', '2022-05-24 23:34:05', '2022-05-24 23:34:05'),
(1199, 12, 4, 'Consumer', '2022-05-24 23:35:01', '2022-05-24 23:35:01'),
(1200, 12, 4, 'Consumer', '2022-05-24 23:35:01', '2022-05-24 23:35:01'),
(1201, 8, 4, 'Consumer', '2022-05-25 18:21:23', '2022-05-25 18:21:23'),
(1202, 8, 4, 'Consumer', '2022-05-25 18:26:49', '2022-05-25 18:26:49'),
(1203, 8, 4, 'Consumer', '2022-05-25 18:30:52', '2022-05-25 18:30:52'),
(1204, 8, 4, 'Consumer', '2022-05-25 18:38:55', '2022-05-25 18:38:55'),
(1205, 8, 4, 'Consumer', '2022-05-25 18:39:57', '2022-05-25 18:39:57'),
(1206, 8, 4, 'Consumer', '2022-05-25 18:44:09', '2022-05-25 18:44:09'),
(1207, 8, 4, 'Consumer', '2022-05-25 18:46:34', '2022-05-25 18:46:34'),
(1208, 8, 4, 'Consumer', '2022-05-25 18:48:49', '2022-05-25 18:48:49'),
(1209, 8, 3, 'Driver', '2022-05-25 18:49:36', '2022-05-25 18:49:36'),
(1210, 8, 4, 'Consumer', '2022-05-25 18:49:40', '2022-05-25 18:49:40'),
(1211, 8, 4, 'Consumer', '2022-05-25 18:51:12', '2022-05-25 18:51:12'),
(1212, 8, 4, 'Consumer', '2022-05-25 18:52:07', '2022-05-25 18:52:07'),
(1213, 8, 4, 'Consumer', '2022-05-25 18:53:00', '2022-05-25 18:53:00'),
(1214, 8, 4, 'Consumer', '2022-05-25 18:59:30', '2022-05-25 18:59:30'),
(1215, 8, 4, 'Consumer', '2022-05-25 19:00:04', '2022-05-25 19:00:04'),
(1216, 8, 4, 'Consumer', '2022-05-25 19:03:44', '2022-05-25 19:03:44'),
(1217, 8, 4, 'Consumer', '2022-05-25 19:07:27', '2022-05-25 19:07:27'),
(1218, 8, 4, 'Consumer', '2022-05-25 19:08:00', '2022-05-25 19:08:00'),
(1219, 8, 3, 'Driver', '2022-05-25 19:09:34', '2022-05-25 19:09:34'),
(1220, 8, 4, 'Consumer', '2022-05-25 19:09:35', '2022-05-25 19:09:35'),
(1221, 8, 4, 'Consumer', '2022-05-25 19:11:15', '2022-05-25 19:11:15'),
(1222, 8, 4, 'Consumer', '2022-05-25 19:13:11', '2022-05-25 19:13:11'),
(1223, 8, 4, 'Consumer', '2022-05-25 19:13:56', '2022-05-25 19:13:56'),
(1224, 8, 4, 'Consumer', '2022-05-25 19:15:08', '2022-05-25 19:15:08'),
(1225, 8, 4, 'Consumer', '2022-05-25 19:18:41', '2022-05-25 19:18:41'),
(1226, 8, 4, 'Consumer', '2022-05-25 19:20:13', '2022-05-25 19:20:13'),
(1227, 12, 3, 'Driver', '2022-05-25 19:33:14', '2022-05-25 19:33:14'),
(1228, 12, 4, 'Consumer', '2022-05-25 22:15:08', '2022-05-25 22:15:08'),
(1229, 12, 4, 'Consumer', '2022-05-25 22:26:29', '2022-05-25 22:26:29'),
(1230, 12, 4, 'Consumer', '2022-05-25 22:27:15', '2022-05-25 22:27:15'),
(1231, 12, 4, 'Consumer', '2022-05-25 22:28:45', '2022-05-25 22:28:45'),
(1232, 12, 4, 'Consumer', '2022-05-25 22:29:52', '2022-05-25 22:29:52'),
(1233, 12, 4, 'Consumer', '2022-05-25 22:33:50', '2022-05-25 22:33:50'),
(1234, 12, 4, 'Consumer', '2022-05-25 22:34:40', '2022-05-25 22:34:40'),
(1235, 12, 4, 'Consumer', '2022-05-25 22:35:45', '2022-05-25 22:35:45'),
(1236, 12, 4, 'Consumer', '2022-05-25 22:36:44', '2022-05-25 22:36:44'),
(1237, 12, 4, 'Consumer', '2022-05-25 22:37:08', '2022-05-25 22:37:08'),
(1238, 12, 4, 'Consumer', '2022-05-25 22:38:44', '2022-05-25 22:38:44'),
(1239, 12, 4, 'Consumer', '2022-05-25 22:39:06', '2022-05-25 22:39:06'),
(1240, 12, 4, 'Consumer', '2022-05-25 22:40:22', '2022-05-25 22:40:22'),
(1241, 12, 4, 'Consumer', '2022-05-25 22:41:07', '2022-05-25 22:41:07'),
(1242, 12, 4, 'Consumer', '2022-05-25 22:41:24', '2022-05-25 22:41:24'),
(1243, 12, 4, 'Consumer', '2022-05-25 22:42:58', '2022-05-25 22:42:58'),
(1244, 12, 4, 'Consumer', '2022-05-25 22:44:34', '2022-05-25 22:44:34'),
(1245, 12, 4, 'Consumer', '2022-05-25 22:46:54', '2022-05-25 22:46:54'),
(1246, 12, 4, 'Consumer', '2022-05-25 22:47:34', '2022-05-25 22:47:34'),
(1247, 12, 4, 'Consumer', '2022-05-25 22:48:30', '2022-05-25 22:48:30'),
(1248, 12, 4, 'Consumer', '2022-05-25 22:52:45', '2022-05-25 22:52:45'),
(1249, 12, 4, 'Consumer', '2022-05-25 22:54:21', '2022-05-25 22:54:21'),
(1250, 12, 3, 'Driver', '2022-05-25 22:57:05', '2022-05-25 22:57:05'),
(1251, 12, 3, 'Driver', '2022-05-25 22:59:31', '2022-05-25 22:59:31'),
(1252, 12, 4, 'Consumer', '2022-05-25 23:07:32', '2022-05-25 23:07:32'),
(1253, 12, 4, 'Consumer', '2022-05-25 23:10:27', '2022-05-25 23:10:27'),
(1254, 12, 4, 'Consumer', '2022-05-25 23:13:25', '2022-05-25 23:13:25'),
(1255, 12, 4, 'Consumer', '2022-05-25 23:14:30', '2022-05-25 23:14:30'),
(1256, 12, 4, 'Consumer', '2022-05-25 23:15:01', '2022-05-25 23:15:01'),
(1257, 12, 4, 'Consumer', '2022-05-25 23:37:21', '2022-05-25 23:37:21'),
(1258, 12, 4, 'Consumer', '2022-05-25 23:39:00', '2022-05-25 23:39:00'),
(1259, 12, 4, 'Consumer', '2022-05-25 23:39:10', '2022-05-25 23:39:10'),
(1260, 12, 4, 'Consumer', '2022-05-25 23:40:44', '2022-05-25 23:40:44'),
(1261, 12, 4, 'Consumer', '2022-05-25 23:43:56', '2022-05-25 23:43:56'),
(1262, 12, 4, 'Consumer', '2022-05-25 23:46:10', '2022-05-25 23:46:10'),
(1263, 12, 4, 'Consumer', '2022-05-25 23:49:38', '2022-05-25 23:49:38'),
(1264, 12, 4, 'Consumer', '2022-05-25 23:50:36', '2022-05-25 23:50:36'),
(1265, 12, 4, 'Consumer', '2022-05-25 23:55:43', '2022-05-25 23:55:43'),
(1266, 12, 4, 'Consumer', '2022-05-25 23:56:04', '2022-05-25 23:56:04'),
(1267, 12, 4, 'Consumer', '2022-05-25 23:58:28', '2022-05-25 23:58:28'),
(1268, 12, 4, 'Consumer', '2022-05-25 23:59:22', '2022-05-25 23:59:22'),
(1269, 12, 4, 'Consumer', '2022-05-26 00:01:37', '2022-05-26 00:01:37'),
(1270, 12, 4, 'Consumer', '2022-05-26 00:05:15', '2022-05-26 00:05:15'),
(1271, 12, 4, 'Consumer', '2022-05-26 16:00:33', '2022-05-26 16:00:33'),
(1272, 12, 4, 'Consumer', '2022-05-26 16:10:42', '2022-05-26 16:10:42'),
(1273, 12, 3, 'Driver', '2022-05-26 18:00:48', '2022-05-26 18:00:48'),
(1274, 12, 4, 'Consumer', '2022-05-26 19:17:16', '2022-05-26 19:17:16'),
(1275, 12, 4, 'Consumer', '2022-05-26 20:41:36', '2022-05-26 20:41:36'),
(1276, 12, 4, 'Consumer', '2022-05-26 20:42:02', '2022-05-26 20:42:02'),
(1277, 12, 4, 'Consumer', '2022-05-26 20:42:11', '2022-05-26 20:42:11'),
(1278, 12, 3, 'Driver', '2022-05-26 20:46:00', '2022-05-26 20:46:00'),
(1279, 12, 3, 'Driver', '2022-05-26 20:50:09', '2022-05-26 20:50:09'),
(1280, 12, 3, 'Driver', '2022-05-26 20:51:00', '2022-05-26 20:51:00'),
(1281, 12, 4, 'Consumer', '2022-05-26 22:44:29', '2022-05-26 22:44:29'),
(1282, 12, 4, 'Consumer', '2022-05-26 22:55:32', '2022-05-26 22:55:32'),
(1283, 12, 4, 'Consumer', '2022-05-26 22:56:20', '2022-05-26 22:56:20'),
(1284, 12, 4, 'Consumer', '2022-05-26 22:58:44', '2022-05-26 22:58:44'),
(1285, 12, 4, 'Consumer', '2022-05-26 23:02:03', '2022-05-26 23:02:03'),
(1286, 12, 4, 'Consumer', '2022-05-26 23:34:14', '2022-05-26 23:34:14'),
(1287, 12, 4, 'Consumer', '2022-05-26 23:35:33', '2022-05-26 23:35:33'),
(1288, 12, 4, 'Consumer', '2022-05-26 23:36:33', '2022-05-26 23:36:33'),
(1289, 12, 4, 'Consumer', '2022-05-26 23:44:39', '2022-05-26 23:44:39'),
(1290, 12, 4, 'Consumer', '2022-05-26 23:47:15', '2022-05-26 23:47:15'),
(1291, 12, 4, 'Consumer', '2022-05-26 23:48:03', '2022-05-26 23:48:03'),
(1292, 12, 4, 'Consumer', '2022-05-26 23:52:07', '2022-05-26 23:52:07'),
(1293, 12, 4, 'Consumer', '2022-05-26 23:56:59', '2022-05-26 23:56:59'),
(1294, 12, 4, 'Consumer', '2022-05-26 23:57:35', '2022-05-26 23:57:35'),
(1295, 12, 4, 'Consumer', '2022-05-27 00:04:17', '2022-05-27 00:04:17'),
(1296, 12, 4, 'Consumer', '2022-05-27 00:04:47', '2022-05-27 00:04:47'),
(1297, 12, 4, 'Consumer', '2022-05-27 00:04:58', '2022-05-27 00:04:58'),
(1298, 12, 4, 'Consumer', '2022-05-27 00:05:42', '2022-05-27 00:05:42'),
(1299, 12, 4, 'Consumer', '2022-05-27 00:06:45', '2022-05-27 00:06:45'),
(1300, 12, 4, 'Consumer', '2022-05-27 00:16:38', '2022-05-27 00:16:38'),
(1301, 12, 4, 'Consumer', '2022-05-27 00:17:05', '2022-05-27 00:17:05'),
(1302, 12, 4, 'Consumer', '2022-05-27 13:39:51', '2022-05-27 13:39:51'),
(1303, 12, 4, 'Consumer', '2022-05-27 13:43:24', '2022-05-27 13:43:24'),
(1304, 12, 4, 'Consumer', '2022-05-27 13:45:07', '2022-05-27 13:45:07'),
(1305, 12, 4, 'Consumer', '2022-05-27 13:48:31', '2022-05-27 13:48:31'),
(1306, 12, 4, 'Consumer', '2022-05-27 13:49:21', '2022-05-27 13:49:21'),
(1307, 12, 4, 'Consumer', '2022-05-27 13:50:19', '2022-05-27 13:50:19'),
(1308, 12, 4, 'Consumer', '2022-05-27 13:51:56', '2022-05-27 13:51:56'),
(1309, 12, 4, 'Consumer', '2022-05-27 13:56:36', '2022-05-27 13:56:36'),
(1310, 12, 4, 'Consumer', '2022-05-27 14:04:13', '2022-05-27 14:04:13'),
(1311, 12, 4, 'Consumer', '2022-05-27 14:11:33', '2022-05-27 14:11:33'),
(1312, 12, 4, 'Consumer', '2022-05-27 14:12:11', '2022-05-27 14:12:11'),
(1313, 12, 4, 'Consumer', '2022-05-27 14:14:50', '2022-05-27 14:14:50'),
(1314, 12, 4, 'Consumer', '2022-05-27 14:16:26', '2022-05-27 14:16:26'),
(1315, 12, 4, 'Consumer', '2022-05-27 14:22:58', '2022-05-27 14:22:58'),
(1316, 12, 4, 'Consumer', '2022-05-27 14:27:23', '2022-05-27 14:27:23'),
(1317, 12, 4, 'Consumer', '2022-05-27 14:30:01', '2022-05-27 14:30:01'),
(1318, 12, 4, 'Consumer', '2022-05-27 14:30:51', '2022-05-27 14:30:51'),
(1319, 12, 4, 'Consumer', '2022-05-27 14:35:00', '2022-05-27 14:35:00'),
(1320, 12, 4, 'Consumer', '2022-05-27 14:35:51', '2022-05-27 14:35:51'),
(1321, 12, 4, 'Consumer', '2022-05-27 14:36:42', '2022-05-27 14:36:42'),
(1322, 12, 4, 'Consumer', '2022-05-27 14:38:17', '2022-05-27 14:38:17'),
(1323, 12, 4, 'Consumer', '2022-05-27 14:41:33', '2022-05-27 14:41:33'),
(1324, 12, 4, 'Consumer', '2022-05-27 14:43:54', '2022-05-27 14:43:54'),
(1325, 12, 4, 'Consumer', '2022-05-27 14:49:08', '2022-05-27 14:49:08'),
(1326, 12, 4, 'Consumer', '2022-05-27 14:51:14', '2022-05-27 14:51:14'),
(1327, 12, 4, 'Consumer', '2022-05-27 14:53:35', '2022-05-27 14:53:35'),
(1328, 12, 4, 'Consumer', '2022-05-27 14:53:48', '2022-05-27 14:53:48'),
(1329, 12, 4, 'Consumer', '2022-05-27 14:55:07', '2022-05-27 14:55:07'),
(1330, 12, 4, 'Consumer', '2022-05-27 15:00:42', '2022-05-27 15:00:42'),
(1331, 12, 4, 'Consumer', '2022-05-27 15:02:18', '2022-05-27 15:02:18'),
(1332, 12, 4, 'Consumer', '2022-05-27 15:04:05', '2022-05-27 15:04:05'),
(1333, 12, 4, 'Consumer', '2022-05-27 15:22:01', '2022-05-27 15:22:01'),
(1334, 12, 4, 'Consumer', '2022-05-27 15:24:12', '2022-05-27 15:24:12'),
(1335, 12, 4, 'Consumer', '2022-05-27 16:11:49', '2022-05-27 16:11:49'),
(1336, 12, 4, 'Consumer', '2022-05-27 16:15:33', '2022-05-27 16:15:33'),
(1337, 12, 4, 'Consumer', '2022-05-27 17:23:53', '2022-05-27 17:23:53'),
(1338, 12, 4, 'Consumer', '2022-05-27 17:26:47', '2022-05-27 17:26:47'),
(1339, 12, 4, 'Consumer', '2022-05-27 17:29:51', '2022-05-27 17:29:51'),
(1340, 12, 4, 'Consumer', '2022-05-27 17:34:07', '2022-05-27 17:34:07'),
(1341, 12, 4, 'Consumer', '2022-05-27 17:35:38', '2022-05-27 17:35:38'),
(1342, 12, 4, 'Consumer', '2022-05-27 17:37:09', '2022-05-27 17:37:09'),
(1343, 12, 3, 'Driver', '2022-05-27 17:39:47', '2022-05-27 17:39:47'),
(1344, 12, 4, 'Consumer', '2022-05-27 17:40:04', '2022-05-27 17:40:04'),
(1345, 12, 4, 'Consumer', '2022-05-27 17:42:42', '2022-05-27 17:42:42'),
(1346, 12, 4, 'Consumer', '2022-05-27 17:44:06', '2022-05-27 17:44:06'),
(1347, 12, 4, 'Consumer', '2022-05-27 17:47:51', '2022-05-27 17:47:51'),
(1348, 12, 4, 'Consumer', '2022-05-27 17:48:40', '2022-05-27 17:48:40'),
(1349, 12, 4, 'Consumer', '2022-05-27 17:50:07', '2022-05-27 17:50:07'),
(1350, 12, 4, 'Consumer', '2022-05-27 17:50:50', '2022-05-27 17:50:50'),
(1351, 12, 4, 'Consumer', '2022-05-27 17:51:54', '2022-05-27 17:51:54'),
(1352, 12, 4, 'Consumer', '2022-05-27 17:53:07', '2022-05-27 17:53:07'),
(1353, 12, 4, 'Consumer', '2022-05-27 17:54:56', '2022-05-27 17:54:56'),
(1354, 12, 4, 'Consumer', '2022-05-27 17:56:55', '2022-05-27 17:56:55'),
(1355, 12, 4, 'Consumer', '2022-05-27 17:58:25', '2022-05-27 17:58:25'),
(1356, 12, 4, 'Consumer', '2022-05-27 18:01:13', '2022-05-27 18:01:13'),
(1357, 12, 4, 'Consumer', '2022-05-27 18:03:12', '2022-05-27 18:03:12'),
(1358, 12, 4, 'Consumer', '2022-05-27 18:04:21', '2022-05-27 18:04:21'),
(1359, 12, 4, 'Consumer', '2022-05-27 18:08:51', '2022-05-27 18:08:51'),
(1360, 12, 4, 'Consumer', '2022-05-27 18:09:29', '2022-05-27 18:09:29'),
(1361, 12, 4, 'Consumer', '2022-05-27 18:11:07', '2022-05-27 18:11:07'),
(1362, 12, 4, 'Consumer', '2022-05-27 18:14:11', '2022-05-27 18:14:11'),
(1363, 12, 4, 'Consumer', '2022-05-27 18:26:23', '2022-05-27 18:26:23'),
(1364, 12, 4, 'Consumer', '2022-05-27 18:26:54', '2022-05-27 18:26:54'),
(1365, 12, 4, 'Consumer', '2022-05-27 18:38:10', '2022-05-27 18:38:10'),
(1366, 12, 4, 'Consumer', '2022-05-27 18:42:48', '2022-05-27 18:42:48'),
(1367, 12, 4, 'Consumer', '2022-05-27 18:43:48', '2022-05-27 18:43:48'),
(1368, 12, 4, 'Consumer', '2022-05-27 18:47:22', '2022-05-27 18:47:22'),
(1369, 12, 4, 'Consumer', '2022-05-27 18:48:36', '2022-05-27 18:48:36'),
(1370, 12, 4, 'Consumer', '2022-05-27 18:51:59', '2022-05-27 18:51:59'),
(1371, 12, 4, 'Consumer', '2022-05-27 18:53:53', '2022-05-27 18:53:53'),
(1372, 12, 4, 'Consumer', '2022-05-27 18:57:13', '2022-05-27 18:57:13'),
(1373, 12, 4, 'Consumer', '2022-05-27 18:59:43', '2022-05-27 18:59:43'),
(1374, 12, 4, 'Consumer', '2022-05-27 19:02:21', '2022-05-27 19:02:21'),
(1375, 12, 4, 'Consumer', '2022-05-27 19:05:19', '2022-05-27 19:05:19'),
(1376, 12, 4, 'Consumer', '2022-05-27 19:06:01', '2022-05-27 19:06:01'),
(1377, 12, 4, 'Consumer', '2022-05-27 19:07:24', '2022-05-27 19:07:24'),
(1378, 12, 4, 'Consumer', '2022-05-27 19:10:43', '2022-05-27 19:10:43'),
(1379, 12, 4, 'Consumer', '2022-05-27 19:12:38', '2022-05-27 19:12:38'),
(1380, 12, 4, 'Consumer', '2022-05-27 19:13:15', '2022-05-27 19:13:15'),
(1381, 12, 4, 'Consumer', '2022-05-27 19:15:40', '2022-05-27 19:15:40'),
(1382, 12, 4, 'Consumer', '2022-05-27 19:18:17', '2022-05-27 19:18:17'),
(1383, 12, 4, 'Consumer', '2022-05-27 19:21:00', '2022-05-27 19:21:00'),
(1384, 12, 4, 'Consumer', '2022-05-27 19:23:03', '2022-05-27 19:23:03'),
(1385, 12, 4, 'Consumer', '2022-05-27 19:25:03', '2022-05-27 19:25:03'),
(1386, 12, 4, 'Consumer', '2022-05-27 19:25:55', '2022-05-27 19:25:55'),
(1387, 12, 4, 'Consumer', '2022-05-27 19:27:06', '2022-05-27 19:27:06'),
(1388, 12, 4, 'Consumer', '2022-05-27 19:27:32', '2022-05-27 19:27:32'),
(1389, 12, 4, 'Consumer', '2022-05-27 19:28:39', '2022-05-27 19:28:39'),
(1390, 12, 4, 'Consumer', '2022-05-27 19:28:46', '2022-05-27 19:28:46'),
(1391, 12, 4, 'Consumer', '2022-05-27 19:28:49', '2022-05-27 19:28:49'),
(1392, 12, 4, 'Consumer', '2022-05-27 19:28:50', '2022-05-27 19:28:50'),
(1393, 12, 4, 'Consumer', '2022-05-27 19:28:51', '2022-05-27 19:28:51'),
(1394, 12, 4, 'Consumer', '2022-05-27 19:28:51', '2022-05-27 19:28:51'),
(1395, 12, 4, 'Consumer', '2022-05-27 19:28:51', '2022-05-27 19:28:51'),
(1396, 12, 4, 'Consumer', '2022-05-27 19:28:52', '2022-05-27 19:28:52'),
(1397, 12, 4, 'Consumer', '2022-05-27 19:28:52', '2022-05-27 19:28:52'),
(1398, 12, 4, 'Consumer', '2022-05-27 19:28:52', '2022-05-27 19:28:52'),
(1399, 12, 4, 'Consumer', '2022-05-27 19:28:52', '2022-05-27 19:28:52'),
(1400, 12, 4, 'Consumer', '2022-05-27 19:28:53', '2022-05-27 19:28:53'),
(1401, 12, 4, 'Consumer', '2022-05-27 19:28:54', '2022-05-27 19:28:54'),
(1402, 12, 4, 'Consumer', '2022-05-27 19:28:54', '2022-05-27 19:28:54'),
(1403, 12, 4, 'Consumer', '2022-05-27 19:28:55', '2022-05-27 19:28:55'),
(1404, 12, 4, 'Consumer', '2022-05-27 19:28:56', '2022-05-27 19:28:56'),
(1405, 12, 4, 'Consumer', '2022-05-27 19:28:56', '2022-05-27 19:28:56'),
(1406, 12, 4, 'Consumer', '2022-05-27 19:28:56', '2022-05-27 19:28:56'),
(1407, 12, 4, 'Consumer', '2022-05-27 19:28:56', '2022-05-27 19:28:56'),
(1408, 12, 4, 'Consumer', '2022-05-27 19:28:57', '2022-05-27 19:28:57'),
(1409, 12, 4, 'Consumer', '2022-05-27 19:30:16', '2022-05-27 19:30:16'),
(1410, 12, 4, 'Consumer', '2022-05-27 19:30:40', '2022-05-27 19:30:40'),
(1411, 12, 4, 'Consumer', '2022-05-27 19:30:41', '2022-05-27 19:30:41'),
(1412, 12, 4, 'Consumer', '2022-05-27 19:30:42', '2022-05-27 19:30:42'),
(1413, 12, 4, 'Consumer', '2022-05-27 19:30:42', '2022-05-27 19:30:42'),
(1414, 12, 4, 'Consumer', '2022-05-27 19:30:43', '2022-05-27 19:30:43'),
(1415, 12, 4, 'Consumer', '2022-05-27 19:30:43', '2022-05-27 19:30:43'),
(1416, 12, 4, 'Consumer', '2022-05-27 19:30:43', '2022-05-27 19:30:43'),
(1417, 12, 4, 'Consumer', '2022-05-27 19:30:52', '2022-05-27 19:30:52'),
(1418, 12, 4, 'Consumer', '2022-05-27 19:31:06', '2022-05-27 19:31:06'),
(1419, 12, 3, 'Driver', '2022-05-27 19:31:18', '2022-05-27 19:31:18'),
(1420, 12, 4, 'Consumer', '2022-05-27 19:31:43', '2022-05-27 19:31:43'),
(1421, 12, 4, 'Consumer', '2022-05-27 19:31:48', '2022-05-27 19:31:48'),
(1422, 12, 4, 'Consumer', '2022-05-27 19:31:49', '2022-05-27 19:31:49'),
(1423, 12, 4, 'Consumer', '2022-05-27 19:33:15', '2022-05-27 19:33:15'),
(1424, 12, 4, 'Consumer', '2022-05-27 20:27:58', '2022-05-27 20:27:58'),
(1425, 12, 4, 'Consumer', '2022-05-27 20:33:55', '2022-05-27 20:33:55'),
(1426, 12, 4, 'Consumer', '2022-05-27 20:35:14', '2022-05-27 20:35:14'),
(1427, 12, 4, 'Consumer', '2022-05-27 20:37:28', '2022-05-27 20:37:28'),
(1428, 12, 4, 'Consumer', '2022-05-27 20:38:15', '2022-05-27 20:38:15'),
(1429, 12, 4, 'Consumer', '2022-05-27 20:38:32', '2022-05-27 20:38:32'),
(1430, 12, 4, 'Consumer', '2022-05-27 20:42:09', '2022-05-27 20:42:09'),
(1431, 12, 4, 'Consumer', '2022-05-27 20:42:54', '2022-05-27 20:42:54'),
(1432, 12, 4, 'Consumer', '2022-05-27 20:44:07', '2022-05-27 20:44:07'),
(1433, 12, 4, 'Consumer', '2022-05-27 20:46:49', '2022-05-27 20:46:49'),
(1434, 12, 4, 'Consumer', '2022-05-27 21:16:42', '2022-05-27 21:16:42'),
(1435, 12, 4, 'Consumer', '2022-05-27 21:44:41', '2022-05-27 21:44:41'),
(1436, 12, 3, 'Driver', '2022-05-27 21:45:18', '2022-05-27 21:45:18'),
(1437, 12, 4, 'Consumer', '2022-05-27 21:45:22', '2022-05-27 21:45:22'),
(1438, 12, 4, 'Consumer', '2022-05-27 21:51:53', '2022-05-27 21:51:53'),
(1439, 12, 3, 'Driver', '2022-05-27 21:55:23', '2022-05-27 21:55:23'),
(1440, 12, 4, 'Consumer', '2022-05-27 21:55:27', '2022-05-27 21:55:27'),
(1441, 12, 4, 'Consumer', '2022-05-27 22:03:02', '2022-05-27 22:03:02'),
(1442, 12, 3, 'Driver', '2022-05-27 22:06:11', '2022-05-27 22:06:11'),
(1443, 12, 3, 'Driver', '2022-05-27 22:06:25', '2022-05-27 22:06:25'),
(1444, 12, 4, 'Consumer', '2022-05-27 22:09:56', '2022-05-27 22:09:56'),
(1445, 12, 3, 'Driver', '2022-05-27 22:10:39', '2022-05-27 22:10:39'),
(1446, 12, 4, 'Consumer', '2022-05-28 13:32:24', '2022-05-28 13:32:24'),
(1447, 12, 4, 'Consumer', '2022-05-28 13:35:46', '2022-05-28 13:35:46'),
(1448, 12, 4, 'Consumer', '2022-05-28 13:42:07', '2022-05-28 13:42:07'),
(1449, 12, 4, 'Consumer', '2022-05-28 13:43:33', '2022-05-28 13:43:33'),
(1450, 12, 4, 'Consumer', '2022-05-28 13:44:24', '2022-05-28 13:44:24'),
(1451, 12, 4, 'Consumer', '2022-05-28 13:46:13', '2022-05-28 13:46:13'),
(1452, 12, 4, 'Consumer', '2022-05-28 13:49:56', '2022-05-28 13:49:56');
INSERT INTO `runtime_roles` (`id`, `user_id`, `role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(1453, 12, 4, 'Consumer', '2022-05-28 13:54:39', '2022-05-28 13:54:39'),
(1454, 12, 4, 'Consumer', '2022-05-28 13:56:06', '2022-05-28 13:56:06'),
(1455, 12, 4, 'Consumer', '2022-05-28 13:57:12', '2022-05-28 13:57:12'),
(1456, 12, 4, 'Consumer', '2022-05-28 13:59:28', '2022-05-28 13:59:28'),
(1457, 12, 4, 'Consumer', '2022-05-28 14:00:01', '2022-05-28 14:00:01'),
(1458, 12, 4, 'Consumer', '2022-05-28 14:01:58', '2022-05-28 14:01:58'),
(1459, 12, 4, 'Consumer', '2022-05-28 14:03:58', '2022-05-28 14:03:58'),
(1460, 12, 4, 'Consumer', '2022-05-28 14:10:37', '2022-05-28 14:10:37'),
(1461, 12, 4, 'Consumer', '2022-05-28 14:12:57', '2022-05-28 14:12:57'),
(1462, 12, 4, 'Consumer', '2022-05-28 14:13:17', '2022-05-28 14:13:17'),
(1463, 12, 4, 'Consumer', '2022-05-28 14:17:05', '2022-05-28 14:17:05'),
(1464, 12, 4, 'Consumer', '2022-05-28 14:19:42', '2022-05-28 14:19:42'),
(1465, 12, 4, 'Consumer', '2022-05-28 14:32:37', '2022-05-28 14:32:37'),
(1466, 12, 4, 'Consumer', '2022-05-28 14:35:53', '2022-05-28 14:35:53'),
(1467, 12, 4, 'Consumer', '2022-05-28 14:41:26', '2022-05-28 14:41:26'),
(1468, 12, 4, 'Consumer', '2022-05-28 14:45:38', '2022-05-28 14:45:38'),
(1469, 12, 4, 'Consumer', '2022-05-28 14:46:33', '2022-05-28 14:46:33'),
(1470, 12, 4, 'Consumer', '2022-05-28 14:51:56', '2022-05-28 14:51:56'),
(1471, 12, 4, 'Consumer', '2022-05-28 14:53:07', '2022-05-28 14:53:07'),
(1472, 12, 4, 'Consumer', '2022-05-28 14:54:37', '2022-05-28 14:54:37'),
(1473, 12, 4, 'Consumer', '2022-05-28 14:57:19', '2022-05-28 14:57:19'),
(1474, 12, 4, 'Consumer', '2022-05-28 14:58:06', '2022-05-28 14:58:06'),
(1475, 12, 4, 'Consumer', '2022-05-28 14:59:22', '2022-05-28 14:59:22'),
(1476, 12, 3, 'Driver', '2022-05-28 15:00:43', '2022-05-28 15:00:43'),
(1477, 12, 4, 'Consumer', '2022-05-28 15:06:41', '2022-05-28 15:06:41'),
(1478, 12, 4, 'Consumer', '2022-05-28 15:09:18', '2022-05-28 15:09:18'),
(1479, 12, 4, 'Consumer', '2022-05-28 15:14:58', '2022-05-28 15:14:58'),
(1480, 12, 4, 'Consumer', '2022-05-28 15:15:41', '2022-05-28 15:15:41'),
(1481, 12, 4, 'Consumer', '2022-05-28 15:17:29', '2022-05-28 15:17:29'),
(1482, 12, 4, 'Consumer', '2022-05-28 15:19:30', '2022-05-28 15:19:30'),
(1483, 12, 4, 'Consumer', '2022-05-28 15:20:52', '2022-05-28 15:20:52'),
(1484, 12, 4, 'Consumer', '2022-05-28 15:24:48', '2022-05-28 15:24:48'),
(1485, 12, 4, 'Consumer', '2022-05-28 15:28:00', '2022-05-28 15:28:00'),
(1486, 12, 4, 'Consumer', '2022-05-28 15:53:41', '2022-05-28 15:53:41'),
(1487, 12, 4, 'Consumer', '2022-05-28 15:55:56', '2022-05-28 15:55:56'),
(1488, 12, 4, 'Consumer', '2022-05-28 16:04:16', '2022-05-28 16:04:16'),
(1489, 12, 4, 'Consumer', '2022-05-28 16:06:28', '2022-05-28 16:06:28'),
(1490, 12, 3, 'Driver', '2022-05-28 16:07:31', '2022-05-28 16:07:31'),
(1491, 12, 3, 'Driver', '2022-05-28 16:55:43', '2022-05-28 16:55:43'),
(1492, 12, 4, 'Consumer', '2022-05-28 16:58:13', '2022-05-28 16:58:13'),
(1493, 12, 3, 'Driver', '2022-05-28 16:59:17', '2022-05-28 16:59:17'),
(1494, 12, 3, 'Driver', '2022-05-28 17:02:21', '2022-05-28 17:02:21'),
(1495, 12, 3, 'Driver', '2022-05-28 17:02:22', '2022-05-28 17:02:22'),
(1496, 12, 3, 'Driver', '2022-05-28 17:05:25', '2022-05-28 17:05:25'),
(1497, 12, 3, 'Driver', '2022-05-28 17:07:49', '2022-05-28 17:07:49'),
(1498, 12, 3, 'Driver', '2022-05-28 17:10:26', '2022-05-28 17:10:26'),
(1499, 12, 3, 'Driver', '2022-05-28 17:11:48', '2022-05-28 17:11:48'),
(1500, 12, 3, 'Driver', '2022-05-28 17:13:38', '2022-05-28 17:13:38'),
(1501, 12, 3, 'Driver', '2022-05-28 17:13:39', '2022-05-28 17:13:39'),
(1502, 12, 3, 'Driver', '2022-05-28 17:14:58', '2022-05-28 17:14:58'),
(1503, 12, 3, 'Driver', '2022-05-28 17:15:02', '2022-05-28 17:15:02'),
(1504, 12, 3, 'Driver', '2022-05-28 17:15:02', '2022-05-28 17:15:02'),
(1505, 12, 3, 'Driver', '2022-05-28 17:15:03', '2022-05-28 17:15:03'),
(1506, 12, 3, 'Driver', '2022-05-28 17:15:03', '2022-05-28 17:15:03'),
(1507, 12, 3, 'Driver', '2022-05-28 17:15:03', '2022-05-28 17:15:03'),
(1508, 12, 3, 'Driver', '2022-05-28 17:18:50', '2022-05-28 17:18:50'),
(1509, 12, 3, 'Driver', '2022-05-28 17:20:44', '2022-05-28 17:20:44'),
(1510, 12, 3, 'Driver', '2022-05-28 17:25:52', '2022-05-28 17:25:52'),
(1511, 12, 3, 'Driver', '2022-05-28 17:28:12', '2022-05-28 17:28:12'),
(1512, 12, 3, 'Driver', '2022-05-28 17:38:57', '2022-05-28 17:38:57'),
(1513, 12, 4, 'Consumer', '2022-05-28 17:44:08', '2022-05-28 17:44:08'),
(1514, 12, 3, 'Driver', '2022-05-28 17:44:31', '2022-05-28 17:44:31'),
(1515, 12, 3, 'Driver', '2022-05-28 17:50:13', '2022-05-28 17:50:13'),
(1516, 12, 3, 'Driver', '2022-05-28 17:51:43', '2022-05-28 17:51:43'),
(1517, 12, 3, 'Driver', '2022-05-28 17:51:43', '2022-05-28 17:51:43'),
(1518, 12, 3, 'Driver', '2022-05-28 17:53:34', '2022-05-28 17:53:34'),
(1519, 12, 4, 'Consumer', '2022-05-28 17:55:04', '2022-05-28 17:55:04'),
(1520, 12, 3, 'Driver', '2022-05-28 17:55:13', '2022-05-28 17:55:13'),
(1521, 12, 3, 'Driver', '2022-05-28 17:57:54', '2022-05-28 17:57:54'),
(1522, 12, 3, 'Driver', '2022-05-28 18:02:34', '2022-05-28 18:02:34'),
(1523, 12, 3, 'Driver', '2022-05-28 18:03:23', '2022-05-28 18:03:23'),
(1524, 12, 3, 'Driver', '2022-05-28 18:04:50', '2022-05-28 18:04:50'),
(1525, 12, 3, 'Driver', '2022-05-28 18:07:05', '2022-05-28 18:07:05'),
(1526, 12, 3, 'Driver', '2022-05-28 18:17:59', '2022-05-28 18:17:59'),
(1527, 12, 3, 'Driver', '2022-05-28 18:21:32', '2022-05-28 18:21:32'),
(1528, 12, 3, 'Driver', '2022-05-28 18:25:44', '2022-05-28 18:25:44'),
(1529, 12, 3, 'Driver', '2022-05-30 15:20:18', '2022-05-30 15:20:18'),
(1530, 12, 4, 'Consumer', '2022-05-30 15:24:38', '2022-05-30 15:24:38'),
(1531, 12, 3, 'Driver', '2022-05-30 16:03:33', '2022-05-30 16:03:33'),
(1532, 12, 3, 'Driver', '2022-05-30 16:23:02', '2022-05-30 16:23:02'),
(1533, 12, 3, 'Driver', '2022-05-30 16:28:33', '2022-05-30 16:28:33'),
(1534, 12, 3, 'Driver', '2022-05-30 16:40:30', '2022-05-30 16:40:30'),
(1535, 12, 3, 'Driver', '2022-05-30 16:40:33', '2022-05-30 16:40:33'),
(1536, 12, 3, 'Driver', '2022-05-30 18:18:04', '2022-05-30 18:18:04'),
(1537, 12, 3, 'Driver', '2022-05-30 18:18:24', '2022-05-30 18:18:24'),
(1538, 12, 3, 'Driver', '2022-05-30 18:21:27', '2022-05-30 18:21:27'),
(1539, 12, 3, 'Driver', '2022-05-30 18:21:45', '2022-05-30 18:21:45'),
(1540, 12, 3, 'Driver', '2022-05-30 18:23:57', '2022-05-30 18:23:57'),
(1541, 12, 3, 'Driver', '2022-05-30 18:30:29', '2022-05-30 18:30:29'),
(1542, 12, 3, 'Driver', '2022-05-30 18:34:56', '2022-05-30 18:34:56'),
(1543, 12, 3, 'Driver', '2022-05-30 18:35:08', '2022-05-30 18:35:08'),
(1544, 13, 3, 'Driver', '2022-05-30 18:40:49', '2022-05-30 18:40:49'),
(1545, 13, 3, 'Driver', '2022-05-30 18:40:53', '2022-05-30 18:40:53'),
(1546, 13, 4, 'Consumer', '2022-05-30 18:41:24', '2022-05-30 18:41:24'),
(1547, 13, 3, 'Driver', '2022-05-30 18:42:44', '2022-05-30 18:42:44'),
(1548, 13, 3, 'Driver', '2022-05-30 18:47:56', '2022-05-30 18:47:56'),
(1549, 12, 3, 'Driver', '2022-05-30 18:48:35', '2022-05-30 18:48:35'),
(1550, 12, 3, 'Driver', '2022-05-30 19:08:33', '2022-05-30 19:08:33'),
(1551, 12, 3, 'Driver', '2022-05-30 19:10:30', '2022-05-30 19:10:30'),
(1552, 12, 3, 'Driver', '2022-05-30 19:14:52', '2022-05-30 19:14:52'),
(1553, 12, 3, 'Driver', '2022-05-30 19:17:11', '2022-05-30 19:17:11'),
(1554, 12, 3, 'Driver', '2022-05-30 19:19:12', '2022-05-30 19:19:12'),
(1555, 12, 3, 'Driver', '2022-05-30 19:20:54', '2022-05-30 19:20:54'),
(1556, 12, 3, 'Driver', '2022-05-30 19:26:04', '2022-05-30 19:26:04'),
(1557, 12, 3, 'Driver', '2022-05-30 19:27:13', '2022-05-30 19:27:13'),
(1558, 12, 3, 'Driver', '2022-05-30 19:28:08', '2022-05-30 19:28:08'),
(1559, 12, 3, 'Driver', '2022-05-30 19:32:14', '2022-05-30 19:32:14'),
(1560, 12, 3, 'Driver', '2022-05-30 19:34:05', '2022-05-30 19:34:05'),
(1561, 12, 3, 'Driver', '2022-05-30 19:34:31', '2022-05-30 19:34:31'),
(1562, 12, 3, 'Driver', '2022-05-30 20:19:27', '2022-05-30 20:19:27'),
(1563, 12, 3, 'Driver', '2022-05-30 20:29:09', '2022-05-30 20:29:09'),
(1564, 12, 3, 'Driver', '2022-05-30 20:31:37', '2022-05-30 20:31:37'),
(1565, 12, 3, 'Driver', '2022-05-30 21:17:39', '2022-05-30 21:17:39'),
(1566, 12, 3, 'Driver', '2022-05-30 22:39:43', '2022-05-30 22:39:43'),
(1567, 12, 3, 'Driver', '2022-05-30 22:40:34', '2022-05-30 22:40:34'),
(1568, 12, 3, 'Driver', '2022-05-30 22:42:23', '2022-05-30 22:42:23'),
(1569, 12, 3, 'Driver', '2022-06-01 15:11:58', '2022-06-01 15:11:58'),
(1570, 12, 3, 'Driver', '2022-06-01 15:13:58', '2022-06-01 15:13:58'),
(1571, 12, 3, 'Driver', '2022-06-01 15:16:46', '2022-06-01 15:16:46'),
(1572, 12, 3, 'Driver', '2022-06-01 15:19:34', '2022-06-01 15:19:34'),
(1573, 12, 3, 'Driver', '2022-06-01 15:21:15', '2022-06-01 15:21:15'),
(1574, 12, 3, 'Driver', '2022-06-01 15:24:10', '2022-06-01 15:24:10'),
(1575, 12, 3, 'Driver', '2022-06-01 15:25:42', '2022-06-01 15:25:42'),
(1576, 12, 3, 'Driver', '2022-06-01 15:26:49', '2022-06-01 15:26:49'),
(1577, 12, 3, 'Driver', '2022-06-01 15:47:43', '2022-06-01 15:47:43'),
(1578, 12, 3, 'Driver', '2022-06-01 15:48:40', '2022-06-01 15:48:40'),
(1579, 12, 3, 'Driver', '2022-06-01 15:50:50', '2022-06-01 15:50:50'),
(1580, 12, 3, 'Driver', '2022-06-01 15:56:53', '2022-06-01 15:56:53'),
(1581, 12, 3, 'Driver', '2022-06-01 15:57:38', '2022-06-01 15:57:38'),
(1582, 12, 4, 'Consumer', '2022-06-01 15:58:16', '2022-06-01 15:58:16'),
(1583, 12, 4, 'Consumer', '2022-06-01 16:00:15', '2022-06-01 16:00:15'),
(1584, 12, 3, 'Driver', '2022-06-01 16:00:23', '2022-06-01 16:00:23'),
(1585, 12, 3, 'Driver', '2022-06-01 16:14:28', '2022-06-01 16:14:28'),
(1586, 12, 4, 'Consumer', '2022-06-01 16:14:42', '2022-06-01 16:14:42'),
(1587, 12, 4, 'Consumer', '2022-06-01 16:14:43', '2022-06-01 16:14:43'),
(1588, 12, 3, 'Driver', '2022-06-01 16:50:24', '2022-06-01 16:50:24'),
(1589, 12, 3, 'Driver', '2022-06-01 16:50:35', '2022-06-01 16:50:35'),
(1590, 12, 3, 'Driver', '2022-06-01 16:51:17', '2022-06-01 16:51:17'),
(1591, 12, 3, 'Driver', '2022-06-01 16:52:34', '2022-06-01 16:52:34'),
(1592, 12, 3, 'Driver', '2022-06-01 16:52:50', '2022-06-01 16:52:50'),
(1593, 12, 3, 'Driver', '2022-06-01 16:54:12', '2022-06-01 16:54:12'),
(1594, 12, 3, 'Driver', '2022-06-01 16:54:12', '2022-06-01 16:54:12'),
(1595, 12, 3, 'Driver', '2022-06-01 16:54:12', '2022-06-01 16:54:12'),
(1596, 12, 3, 'Driver', '2022-06-01 16:54:13', '2022-06-01 16:54:13'),
(1597, 12, 3, 'Driver', '2022-06-01 16:54:13', '2022-06-01 16:54:13'),
(1598, 12, 3, 'Driver', '2022-06-01 16:54:13', '2022-06-01 16:54:13'),
(1599, 12, 3, 'Driver', '2022-06-01 16:54:13', '2022-06-01 16:54:13'),
(1600, 12, 3, 'Driver', '2022-06-01 16:54:14', '2022-06-01 16:54:14'),
(1601, 12, 3, 'Driver', '2022-06-01 16:54:14', '2022-06-01 16:54:14'),
(1602, 12, 3, 'Driver', '2022-06-01 16:54:14', '2022-06-01 16:54:14'),
(1603, 12, 3, 'Driver', '2022-06-01 16:54:14', '2022-06-01 16:54:14'),
(1604, 12, 3, 'Driver', '2022-06-01 16:54:14', '2022-06-01 16:54:14'),
(1605, 12, 3, 'Driver', '2022-06-01 16:54:14', '2022-06-01 16:54:14'),
(1606, 12, 3, 'Driver', '2022-06-01 16:54:15', '2022-06-01 16:54:15'),
(1607, 12, 3, 'Driver', '2022-06-01 16:54:15', '2022-06-01 16:54:15'),
(1608, 12, 3, 'Driver', '2022-06-01 16:54:15', '2022-06-01 16:54:15'),
(1609, 12, 3, 'Driver', '2022-06-01 16:54:15', '2022-06-01 16:54:15'),
(1610, 12, 3, 'Driver', '2022-06-01 16:54:15', '2022-06-01 16:54:15'),
(1611, 12, 3, 'Driver', '2022-06-01 16:54:15', '2022-06-01 16:54:15'),
(1612, 12, 3, 'Driver', '2022-06-01 16:54:16', '2022-06-01 16:54:16'),
(1613, 12, 3, 'Driver', '2022-06-01 16:54:16', '2022-06-01 16:54:16'),
(1614, 12, 4, 'Consumer', '2022-06-01 16:54:16', '2022-06-01 16:54:16'),
(1615, 12, 3, 'Driver', '2022-06-01 16:54:16', '2022-06-01 16:54:16'),
(1616, 12, 4, 'Consumer', '2022-06-01 16:54:18', '2022-06-01 16:54:18'),
(1617, 12, 4, 'Consumer', '2022-06-01 16:54:18', '2022-06-01 16:54:18'),
(1618, 12, 3, 'Driver', '2022-06-01 16:55:07', '2022-06-01 16:55:07'),
(1619, 12, 3, 'Driver', '2022-06-01 16:55:55', '2022-06-01 16:55:55'),
(1620, 12, 3, 'Driver', '2022-06-01 18:35:56', '2022-06-01 18:35:56'),
(1621, 12, 4, 'Consumer', '2022-06-01 18:37:38', '2022-06-01 18:37:38'),
(1622, 12, 4, 'Consumer', '2022-06-01 18:41:32', '2022-06-01 18:41:32'),
(1623, 12, 4, 'Consumer', '2022-06-01 18:43:04', '2022-06-01 18:43:04'),
(1624, 12, 4, 'Consumer', '2022-06-01 18:44:51', '2022-06-01 18:44:51'),
(1625, 12, 4, 'Consumer', '2022-06-01 18:45:54', '2022-06-01 18:45:54'),
(1626, 12, 3, 'Driver', '2022-06-01 19:11:07', '2022-06-01 19:11:07'),
(1627, 12, 3, 'Driver', '2022-06-01 19:13:01', '2022-06-01 19:13:01'),
(1628, 12, 3, 'Driver', '2022-06-01 19:30:10', '2022-06-01 19:30:10'),
(1629, 12, 3, 'Driver', '2022-06-01 19:35:33', '2022-06-01 19:35:33'),
(1630, 12, 3, 'Driver', '2022-06-01 19:36:53', '2022-06-01 19:36:53'),
(1631, 12, 3, 'Driver', '2022-06-01 19:38:21', '2022-06-01 19:38:21'),
(1632, 12, 3, 'Driver', '2022-06-01 19:38:23', '2022-06-01 19:38:23'),
(1633, 12, 4, 'Consumer', '2022-06-01 20:20:12', '2022-06-01 20:20:12'),
(1634, 12, 4, 'Consumer', '2022-06-01 20:29:10', '2022-06-01 20:29:10'),
(1635, 12, 4, 'Consumer', '2022-06-01 20:33:27', '2022-06-01 20:33:27'),
(1636, 12, 3, 'Driver', '2022-06-01 20:35:28', '2022-06-01 20:35:28'),
(1637, 12, 4, 'Consumer', '2022-06-01 20:35:35', '2022-06-01 20:35:35'),
(1638, 12, 4, 'Consumer', '2022-06-01 20:35:52', '2022-06-01 20:35:52'),
(1639, 12, 4, 'Consumer', '2022-06-01 20:38:06', '2022-06-01 20:38:06'),
(1640, 12, 4, 'Consumer', '2022-06-01 20:47:50', '2022-06-01 20:47:50'),
(1641, 12, 3, 'Driver', '2022-06-01 20:48:56', '2022-06-01 20:48:56'),
(1642, 12, 4, 'Consumer', '2022-06-01 20:49:03', '2022-06-01 20:49:03'),
(1643, 12, 4, 'Consumer', '2022-06-01 21:00:00', '2022-06-01 21:00:00'),
(1644, 12, 4, 'Consumer', '2022-06-01 21:03:04', '2022-06-01 21:03:04'),
(1645, 12, 4, 'Consumer', '2022-06-01 21:04:17', '2022-06-01 21:04:17'),
(1646, 12, 4, 'Consumer', '2022-06-01 21:05:26', '2022-06-01 21:05:26'),
(1647, 12, 3, 'Driver', '2022-06-01 21:07:07', '2022-06-01 21:07:07'),
(1648, 12, 4, 'Consumer', '2022-06-01 21:24:23', '2022-06-01 21:24:23'),
(1649, 12, 4, 'Consumer', '2022-06-01 21:25:05', '2022-06-01 21:25:05'),
(1650, 12, 4, 'Consumer', '2022-06-01 21:43:14', '2022-06-01 21:43:14'),
(1651, 12, 4, 'Consumer', '2022-06-01 21:45:49', '2022-06-01 21:45:49'),
(1652, 12, 3, 'Driver', '2022-06-01 21:48:49', '2022-06-01 21:48:49'),
(1653, 12, 4, 'Consumer', '2022-06-01 21:48:53', '2022-06-01 21:48:53'),
(1654, 12, 4, 'Consumer', '2022-06-01 21:53:55', '2022-06-01 21:53:55'),
(1655, 12, 3, 'Driver', '2022-06-01 21:54:54', '2022-06-01 21:54:54'),
(1656, 12, 3, 'Driver', '2022-06-01 22:33:18', '2022-06-01 22:33:18'),
(1657, 12, 3, 'Driver', '2022-06-01 22:35:06', '2022-06-01 22:35:06'),
(1658, 12, 3, 'Driver', '2022-06-01 22:36:50', '2022-06-01 22:36:50'),
(1659, 12, 3, 'Driver', '2022-06-01 22:46:23', '2022-06-01 22:46:23'),
(1660, 12, 4, 'Consumer', '2022-06-01 22:46:31', '2022-06-01 22:46:31'),
(1661, 12, 4, 'Consumer', '2022-06-01 22:48:02', '2022-06-01 22:48:02'),
(1662, 12, 3, 'Driver', '2022-06-01 22:48:09', '2022-06-01 22:48:09'),
(1663, 12, 3, 'Driver', '2022-06-01 22:49:26', '2022-06-01 22:49:26'),
(1664, 12, 4, 'Consumer', '2022-06-01 22:50:10', '2022-06-01 22:50:10'),
(1665, 12, 3, 'Driver', '2022-06-01 22:51:41', '2022-06-01 22:51:41'),
(1666, 12, 3, 'Driver', '2022-06-01 22:51:45', '2022-06-01 22:51:45'),
(1667, 12, 4, 'Consumer', '2022-06-01 22:52:16', '2022-06-01 22:52:16'),
(1668, 12, 4, 'Consumer', '2022-06-02 12:01:16', '2022-06-02 12:01:16'),
(1669, 12, 3, 'Driver', '2022-06-02 12:02:14', '2022-06-02 12:02:14'),
(1670, 12, 3, 'Driver', '2022-06-02 13:27:22', '2022-06-02 13:27:22'),
(1671, 12, 3, 'Driver', '2022-06-02 13:28:24', '2022-06-02 13:28:24'),
(1672, 12, 3, 'Driver', '2022-06-02 13:35:39', '2022-06-02 13:35:39'),
(1673, 12, 3, 'Driver', '2022-06-02 13:37:00', '2022-06-02 13:37:00'),
(1674, 12, 4, 'Consumer', '2022-06-02 13:38:44', '2022-06-02 13:38:44'),
(1675, 12, 3, 'Driver', '2022-06-02 13:43:42', '2022-06-02 13:43:42'),
(1676, 12, 4, 'Consumer', '2022-06-02 14:14:49', '2022-06-02 14:14:49'),
(1677, 12, 4, 'Consumer', '2022-06-02 14:15:06', '2022-06-02 14:15:06'),
(1678, 12, 4, 'Consumer', '2022-06-02 14:21:40', '2022-06-02 14:21:40'),
(1679, 12, 4, 'Consumer', '2022-06-02 14:22:45', '2022-06-02 14:22:45'),
(1680, 12, 4, 'Consumer', '2022-06-02 14:23:33', '2022-06-02 14:23:33'),
(1681, 12, 4, 'Consumer', '2022-06-02 14:24:57', '2022-06-02 14:24:57'),
(1682, 12, 4, 'Consumer', '2022-06-02 14:25:37', '2022-06-02 14:25:37'),
(1683, 12, 4, 'Consumer', '2022-06-02 14:25:40', '2022-06-02 14:25:40'),
(1684, 12, 4, 'Consumer', '2022-06-02 14:25:41', '2022-06-02 14:25:41'),
(1685, 12, 4, 'Consumer', '2022-06-02 14:25:41', '2022-06-02 14:25:41'),
(1686, 12, 4, 'Consumer', '2022-06-02 14:25:42', '2022-06-02 14:25:42'),
(1687, 12, 4, 'Consumer', '2022-06-02 14:25:42', '2022-06-02 14:25:42'),
(1688, 12, 4, 'Consumer', '2022-06-02 14:25:42', '2022-06-02 14:25:42'),
(1689, 12, 4, 'Consumer', '2022-06-02 14:25:42', '2022-06-02 14:25:42'),
(1690, 12, 4, 'Consumer', '2022-06-02 14:25:42', '2022-06-02 14:25:42'),
(1691, 12, 4, 'Consumer', '2022-06-02 14:25:43', '2022-06-02 14:25:43'),
(1692, 12, 4, 'Consumer', '2022-06-02 14:25:43', '2022-06-02 14:25:43'),
(1693, 12, 4, 'Consumer', '2022-06-02 14:25:43', '2022-06-02 14:25:43'),
(1694, 12, 3, 'Driver', '2022-06-02 17:32:26', '2022-06-02 17:32:26'),
(1695, 12, 4, 'Consumer', '2022-06-02 17:32:31', '2022-06-02 17:32:31'),
(1696, 12, 4, 'Consumer', '2022-06-02 17:48:53', '2022-06-02 17:48:53'),
(1697, 12, 4, 'Consumer', '2022-06-02 17:52:19', '2022-06-02 17:52:19'),
(1698, 12, 4, 'Consumer', '2022-06-02 17:55:18', '2022-06-02 17:55:18'),
(1699, 12, 4, 'Consumer', '2022-06-02 18:00:16', '2022-06-02 18:00:16'),
(1700, 12, 4, 'Consumer', '2022-06-02 18:01:06', '2022-06-02 18:01:06'),
(1701, 12, 4, 'Consumer', '2022-06-02 18:01:43', '2022-06-02 18:01:43'),
(1702, 12, 4, 'Consumer', '2022-06-02 18:12:22', '2022-06-02 18:12:22'),
(1703, 12, 4, 'Consumer', '2022-06-02 18:12:32', '2022-06-02 18:12:32'),
(1704, 12, 4, 'Consumer', '2022-06-02 18:12:46', '2022-06-02 18:12:46'),
(1705, 12, 4, 'Consumer', '2022-06-02 18:12:53', '2022-06-02 18:12:53'),
(1706, 12, 3, 'Driver', '2022-06-02 18:12:55', '2022-06-02 18:12:55'),
(1707, 12, 4, 'Consumer', '2022-06-02 18:13:02', '2022-06-02 18:13:02'),
(1708, 12, 4, 'Consumer', '2022-06-02 18:13:28', '2022-06-02 18:13:28'),
(1709, 12, 4, 'Consumer', '2022-06-02 18:13:34', '2022-06-02 18:13:34'),
(1710, 12, 3, 'Driver', '2022-06-02 18:13:41', '2022-06-02 18:13:41'),
(1711, 12, 4, 'Consumer', '2022-06-02 18:13:47', '2022-06-02 18:13:47'),
(1712, 12, 4, 'Consumer', '2022-06-02 18:13:51', '2022-06-02 18:13:51'),
(1713, 12, 4, 'Consumer', '2022-06-02 18:13:52', '2022-06-02 18:13:52'),
(1714, 12, 4, 'Consumer', '2022-06-02 18:13:52', '2022-06-02 18:13:52'),
(1715, 12, 4, 'Consumer', '2022-06-02 18:13:52', '2022-06-02 18:13:52'),
(1716, 12, 4, 'Consumer', '2022-06-02 18:13:52', '2022-06-02 18:13:52'),
(1717, 12, 4, 'Consumer', '2022-06-02 18:13:53', '2022-06-02 18:13:53'),
(1718, 12, 4, 'Consumer', '2022-06-02 18:13:53', '2022-06-02 18:13:53'),
(1719, 12, 4, 'Consumer', '2022-06-02 18:13:53', '2022-06-02 18:13:53'),
(1720, 12, 4, 'Consumer', '2022-06-02 18:13:53', '2022-06-02 18:13:53'),
(1721, 12, 4, 'Consumer', '2022-06-02 18:13:53', '2022-06-02 18:13:53'),
(1722, 12, 4, 'Consumer', '2022-06-02 18:13:54', '2022-06-02 18:13:54'),
(1723, 12, 4, 'Consumer', '2022-06-02 18:25:23', '2022-06-02 18:25:23'),
(1724, 12, 4, 'Consumer', '2022-06-02 18:25:34', '2022-06-02 18:25:34'),
(1725, 12, 3, 'Driver', '2022-06-02 18:25:35', '2022-06-02 18:25:35'),
(1726, 12, 4, 'Consumer', '2022-06-02 18:25:44', '2022-06-02 18:25:44'),
(1727, 12, 4, 'Consumer', '2022-06-02 18:25:57', '2022-06-02 18:25:57'),
(1728, 12, 4, 'Consumer', '2022-06-02 18:27:06', '2022-06-02 18:27:06'),
(1729, 12, 4, 'Consumer', '2022-06-02 18:28:06', '2022-06-02 18:28:06'),
(1730, 12, 4, 'Consumer', '2022-06-02 18:28:20', '2022-06-02 18:28:20'),
(1731, 12, 4, 'Consumer', '2022-06-02 18:28:21', '2022-06-02 18:28:21'),
(1732, 12, 4, 'Consumer', '2022-06-02 18:28:21', '2022-06-02 18:28:21'),
(1733, 12, 4, 'Consumer', '2022-06-02 18:28:21', '2022-06-02 18:28:21'),
(1734, 12, 4, 'Consumer', '2022-06-02 18:28:22', '2022-06-02 18:28:22'),
(1735, 12, 4, 'Consumer', '2022-06-02 18:28:22', '2022-06-02 18:28:22'),
(1736, 12, 3, 'Driver', '2022-06-02 18:29:01', '2022-06-02 18:29:01'),
(1737, 12, 4, 'Consumer', '2022-06-02 18:29:09', '2022-06-02 18:29:09'),
(1738, 12, 4, 'Consumer', '2022-06-02 18:29:09', '2022-06-02 18:29:09'),
(1739, 12, 4, 'Consumer', '2022-06-02 18:29:09', '2022-06-02 18:29:09'),
(1740, 12, 4, 'Consumer', '2022-06-02 18:29:10', '2022-06-02 18:29:10'),
(1741, 12, 4, 'Consumer', '2022-06-02 18:29:10', '2022-06-02 18:29:10'),
(1742, 12, 4, 'Consumer', '2022-06-02 18:29:10', '2022-06-02 18:29:10'),
(1743, 12, 4, 'Consumer', '2022-06-02 18:29:19', '2022-06-02 18:29:19'),
(1744, 12, 4, 'Consumer', '2022-06-02 18:29:23', '2022-06-02 18:29:23'),
(1745, 12, 4, 'Consumer', '2022-06-02 18:29:24', '2022-06-02 18:29:24'),
(1746, 12, 4, 'Consumer', '2022-06-02 18:29:25', '2022-06-02 18:29:25'),
(1747, 12, 4, 'Consumer', '2022-06-02 18:29:25', '2022-06-02 18:29:25'),
(1748, 12, 4, 'Consumer', '2022-06-02 18:29:25', '2022-06-02 18:29:25'),
(1749, 12, 4, 'Consumer', '2022-06-02 18:29:26', '2022-06-02 18:29:26'),
(1750, 12, 4, 'Consumer', '2022-06-02 18:29:26', '2022-06-02 18:29:26'),
(1751, 12, 4, 'Consumer', '2022-06-02 18:29:27', '2022-06-02 18:29:27'),
(1752, 12, 4, 'Consumer', '2022-06-02 18:29:27', '2022-06-02 18:29:27'),
(1753, 12, 4, 'Consumer', '2022-06-02 18:29:31', '2022-06-02 18:29:31'),
(1754, 12, 4, 'Consumer', '2022-06-02 18:29:31', '2022-06-02 18:29:31'),
(1755, 12, 4, 'Consumer', '2022-06-02 18:29:32', '2022-06-02 18:29:32'),
(1756, 12, 4, 'Consumer', '2022-06-02 18:29:32', '2022-06-02 18:29:32'),
(1757, 12, 4, 'Consumer', '2022-06-02 18:29:32', '2022-06-02 18:29:32'),
(1758, 12, 4, 'Consumer', '2022-06-02 18:29:56', '2022-06-02 18:29:56'),
(1759, 12, 4, 'Consumer', '2022-06-02 18:30:29', '2022-06-02 18:30:29'),
(1760, 12, 4, 'Consumer', '2022-06-02 18:30:42', '2022-06-02 18:30:42'),
(1761, 12, 3, 'Driver', '2022-06-02 18:32:21', '2022-06-02 18:32:21'),
(1762, 12, 4, 'Consumer', '2022-06-02 18:32:26', '2022-06-02 18:32:26'),
(1763, 12, 4, 'Consumer', '2022-06-02 18:32:32', '2022-06-02 18:32:32'),
(1764, 12, 4, 'Consumer', '2022-06-02 18:38:47', '2022-06-02 18:38:47'),
(1765, 12, 4, 'Consumer', '2022-06-02 18:38:51', '2022-06-02 18:38:51'),
(1766, 12, 3, 'Driver', '2022-06-02 18:38:53', '2022-06-02 18:38:53'),
(1767, 12, 4, 'Consumer', '2022-06-02 18:40:21', '2022-06-02 18:40:21'),
(1768, 12, 4, 'Consumer', '2022-06-02 18:40:33', '2022-06-02 18:40:33'),
(1769, 12, 4, 'Consumer', '2022-06-02 18:40:33', '2022-06-02 18:40:33'),
(1770, 12, 4, 'Consumer', '2022-06-02 18:40:33', '2022-06-02 18:40:33'),
(1771, 12, 4, 'Consumer', '2022-06-02 18:40:33', '2022-06-02 18:40:33'),
(1772, 12, 4, 'Consumer', '2022-06-02 18:40:33', '2022-06-02 18:40:33'),
(1773, 12, 4, 'Consumer', '2022-06-02 18:40:37', '2022-06-02 18:40:37'),
(1774, 12, 4, 'Consumer', '2022-06-02 18:40:37', '2022-06-02 18:40:37'),
(1775, 12, 4, 'Consumer', '2022-06-02 18:40:39', '2022-06-02 18:40:39'),
(1776, 12, 4, 'Consumer', '2022-06-02 18:40:39', '2022-06-02 18:40:39'),
(1777, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1778, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1779, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1780, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1781, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1782, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1783, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1784, 12, 4, 'Consumer', '2022-06-02 18:43:16', '2022-06-02 18:43:16'),
(1785, 12, 4, 'Consumer', '2022-06-02 18:43:17', '2022-06-02 18:43:17'),
(1786, 12, 4, 'Consumer', '2022-06-02 18:43:17', '2022-06-02 18:43:17'),
(1787, 12, 4, 'Consumer', '2022-06-02 18:43:17', '2022-06-02 18:43:17'),
(1788, 12, 4, 'Consumer', '2022-06-02 18:43:17', '2022-06-02 18:43:17'),
(1789, 12, 4, 'Consumer', '2022-06-02 18:43:17', '2022-06-02 18:43:17'),
(1790, 12, 4, 'Consumer', '2022-06-02 18:43:22', '2022-06-02 18:43:22'),
(1791, 12, 4, 'Consumer', '2022-06-02 18:43:23', '2022-06-02 18:43:23'),
(1792, 12, 4, 'Consumer', '2022-06-02 18:46:39', '2022-06-02 18:46:39'),
(1793, 12, 4, 'Consumer', '2022-06-02 18:58:06', '2022-06-02 18:58:06'),
(1794, 12, 4, 'Consumer', '2022-06-02 18:59:53', '2022-06-02 18:59:53'),
(1795, 12, 4, 'Consumer', '2022-06-02 19:01:12', '2022-06-02 19:01:12'),
(1796, 12, 4, 'Consumer', '2022-06-02 19:03:59', '2022-06-02 19:03:59'),
(1797, 12, 4, 'Consumer', '2022-06-02 19:04:16', '2022-06-02 19:04:16'),
(1798, 12, 3, 'Driver', '2022-06-02 19:04:24', '2022-06-02 19:04:24'),
(1799, 12, 4, 'Consumer', '2022-06-02 19:04:48', '2022-06-02 19:04:48'),
(1800, 12, 4, 'Consumer', '2022-06-02 19:05:16', '2022-06-02 19:05:16'),
(1801, 12, 4, 'Consumer', '2022-06-02 19:05:18', '2022-06-02 19:05:18'),
(1802, 12, 4, 'Consumer', '2022-06-02 19:05:21', '2022-06-02 19:05:21'),
(1803, 12, 4, 'Consumer', '2022-06-02 19:05:22', '2022-06-02 19:05:22'),
(1804, 12, 4, 'Consumer', '2022-06-02 19:05:22', '2022-06-02 19:05:22'),
(1805, 12, 4, 'Consumer', '2022-06-02 19:05:22', '2022-06-02 19:05:22'),
(1806, 12, 4, 'Consumer', '2022-06-02 19:05:23', '2022-06-02 19:05:23'),
(1807, 12, 4, 'Consumer', '2022-06-02 19:05:23', '2022-06-02 19:05:23'),
(1808, 12, 4, 'Consumer', '2022-06-02 19:05:23', '2022-06-02 19:05:23'),
(1809, 12, 4, 'Consumer', '2022-06-02 19:05:23', '2022-06-02 19:05:23'),
(1810, 12, 4, 'Consumer', '2022-06-02 19:05:23', '2022-06-02 19:05:23'),
(1811, 12, 4, 'Consumer', '2022-06-02 19:05:24', '2022-06-02 19:05:24'),
(1812, 12, 4, 'Consumer', '2022-06-02 19:05:24', '2022-06-02 19:05:24'),
(1813, 12, 4, 'Consumer', '2022-06-02 19:05:24', '2022-06-02 19:05:24'),
(1814, 12, 4, 'Consumer', '2022-06-02 19:05:26', '2022-06-02 19:05:26'),
(1815, 12, 4, 'Consumer', '2022-06-02 19:05:27', '2022-06-02 19:05:27'),
(1816, 12, 4, 'Consumer', '2022-06-02 19:05:28', '2022-06-02 19:05:28'),
(1817, 12, 4, 'Consumer', '2022-06-02 19:05:33', '2022-06-02 19:05:33'),
(1818, 12, 4, 'Consumer', '2022-06-02 19:05:34', '2022-06-02 19:05:34'),
(1819, 12, 4, 'Consumer', '2022-06-02 19:05:34', '2022-06-02 19:05:34'),
(1820, 12, 4, 'Consumer', '2022-06-02 19:05:35', '2022-06-02 19:05:35'),
(1821, 12, 4, 'Consumer', '2022-06-02 19:05:35', '2022-06-02 19:05:35'),
(1822, 12, 4, 'Consumer', '2022-06-02 19:05:35', '2022-06-02 19:05:35'),
(1823, 12, 4, 'Consumer', '2022-06-02 19:05:36', '2022-06-02 19:05:36'),
(1824, 12, 4, 'Consumer', '2022-06-02 19:05:36', '2022-06-02 19:05:36'),
(1825, 12, 4, 'Consumer', '2022-06-02 19:05:36', '2022-06-02 19:05:36'),
(1826, 12, 4, 'Consumer', '2022-06-02 19:05:36', '2022-06-02 19:05:36'),
(1827, 12, 4, 'Consumer', '2022-06-02 19:05:36', '2022-06-02 19:05:36'),
(1828, 12, 4, 'Consumer', '2022-06-02 19:05:39', '2022-06-02 19:05:39'),
(1829, 12, 4, 'Consumer', '2022-06-02 19:05:40', '2022-06-02 19:05:40'),
(1830, 12, 4, 'Consumer', '2022-06-02 19:05:41', '2022-06-02 19:05:41'),
(1831, 12, 4, 'Consumer', '2022-06-02 19:05:42', '2022-06-02 19:05:42'),
(1832, 12, 4, 'Consumer', '2022-06-02 19:05:43', '2022-06-02 19:05:43'),
(1833, 12, 4, 'Consumer', '2022-06-02 19:05:43', '2022-06-02 19:05:43'),
(1834, 12, 4, 'Consumer', '2022-06-02 19:05:43', '2022-06-02 19:05:43'),
(1835, 12, 4, 'Consumer', '2022-06-02 19:05:44', '2022-06-02 19:05:44'),
(1836, 12, 4, 'Consumer', '2022-06-02 19:05:44', '2022-06-02 19:05:44'),
(1837, 12, 4, 'Consumer', '2022-06-02 19:05:44', '2022-06-02 19:05:44'),
(1838, 12, 4, 'Consumer', '2022-06-02 19:05:44', '2022-06-02 19:05:44'),
(1839, 12, 4, 'Consumer', '2022-06-02 19:05:52', '2022-06-02 19:05:52'),
(1840, 12, 4, 'Consumer', '2022-06-02 19:06:21', '2022-06-02 19:06:21'),
(1841, 12, 4, 'Consumer', '2022-06-02 19:06:23', '2022-06-02 19:06:23'),
(1842, 12, 4, 'Consumer', '2022-06-02 19:06:23', '2022-06-02 19:06:23'),
(1843, 12, 4, 'Consumer', '2022-06-02 19:06:24', '2022-06-02 19:06:24'),
(1844, 12, 4, 'Consumer', '2022-06-02 19:06:24', '2022-06-02 19:06:24'),
(1845, 12, 4, 'Consumer', '2022-06-02 19:06:24', '2022-06-02 19:06:24'),
(1846, 12, 4, 'Consumer', '2022-06-02 19:06:24', '2022-06-02 19:06:24'),
(1847, 12, 4, 'Consumer', '2022-06-02 19:06:25', '2022-06-02 19:06:25'),
(1848, 12, 4, 'Consumer', '2022-06-02 19:06:25', '2022-06-02 19:06:25'),
(1849, 12, 4, 'Consumer', '2022-06-02 19:06:25', '2022-06-02 19:06:25'),
(1850, 12, 4, 'Consumer', '2022-06-02 19:06:25', '2022-06-02 19:06:25'),
(1851, 12, 4, 'Consumer', '2022-06-02 19:06:25', '2022-06-02 19:06:25'),
(1852, 12, 4, 'Consumer', '2022-06-02 19:06:26', '2022-06-02 19:06:26'),
(1853, 12, 4, 'Consumer', '2022-06-02 19:06:26', '2022-06-02 19:06:26'),
(1854, 12, 4, 'Consumer', '2022-06-02 19:06:59', '2022-06-02 19:06:59'),
(1855, 12, 4, 'Consumer', '2022-06-02 19:07:00', '2022-06-02 19:07:00'),
(1856, 12, 4, 'Consumer', '2022-06-02 19:07:00', '2022-06-02 19:07:00'),
(1857, 12, 4, 'Consumer', '2022-06-02 19:07:00', '2022-06-02 19:07:00'),
(1858, 12, 4, 'Consumer', '2022-06-02 19:07:01', '2022-06-02 19:07:01'),
(1859, 12, 4, 'Consumer', '2022-06-02 19:07:01', '2022-06-02 19:07:01'),
(1860, 12, 4, 'Consumer', '2022-06-02 19:07:01', '2022-06-02 19:07:01'),
(1861, 12, 4, 'Consumer', '2022-06-02 19:07:01', '2022-06-02 19:07:01'),
(1862, 12, 4, 'Consumer', '2022-06-02 19:07:02', '2022-06-02 19:07:02'),
(1863, 12, 4, 'Consumer', '2022-06-02 19:07:02', '2022-06-02 19:07:02'),
(1864, 12, 4, 'Consumer', '2022-06-02 19:07:02', '2022-06-02 19:07:02'),
(1865, 12, 4, 'Consumer', '2022-06-02 19:07:03', '2022-06-02 19:07:03'),
(1866, 12, 4, 'Consumer', '2022-06-02 19:07:03', '2022-06-02 19:07:03'),
(1867, 12, 4, 'Consumer', '2022-06-02 19:07:03', '2022-06-02 19:07:03'),
(1868, 12, 4, 'Consumer', '2022-06-02 19:07:03', '2022-06-02 19:07:03'),
(1869, 12, 4, 'Consumer', '2022-06-02 19:07:03', '2022-06-02 19:07:03'),
(1870, 12, 4, 'Consumer', '2022-06-02 19:07:03', '2022-06-02 19:07:03'),
(1871, 12, 4, 'Consumer', '2022-06-02 19:07:04', '2022-06-02 19:07:04'),
(1872, 12, 4, 'Consumer', '2022-06-02 19:07:06', '2022-06-02 19:07:06'),
(1873, 12, 4, 'Consumer', '2022-06-02 19:07:07', '2022-06-02 19:07:07'),
(1874, 12, 4, 'Consumer', '2022-06-02 19:07:07', '2022-06-02 19:07:07'),
(1875, 12, 4, 'Consumer', '2022-06-02 19:07:07', '2022-06-02 19:07:07'),
(1876, 12, 4, 'Consumer', '2022-06-02 19:07:07', '2022-06-02 19:07:07'),
(1877, 12, 4, 'Consumer', '2022-06-02 19:07:08', '2022-06-02 19:07:08'),
(1878, 12, 4, 'Consumer', '2022-06-02 19:07:08', '2022-06-02 19:07:08'),
(1879, 12, 4, 'Consumer', '2022-06-02 19:07:08', '2022-06-02 19:07:08'),
(1880, 12, 4, 'Consumer', '2022-06-02 19:07:08', '2022-06-02 19:07:08'),
(1881, 12, 4, 'Consumer', '2022-06-02 19:07:11', '2022-06-02 19:07:11'),
(1882, 12, 4, 'Consumer', '2022-06-02 19:07:12', '2022-06-02 19:07:12'),
(1883, 12, 4, 'Consumer', '2022-06-02 19:07:12', '2022-06-02 19:07:12'),
(1884, 12, 4, 'Consumer', '2022-06-02 19:07:12', '2022-06-02 19:07:12'),
(1885, 12, 4, 'Consumer', '2022-06-02 19:07:12', '2022-06-02 19:07:12'),
(1886, 12, 4, 'Consumer', '2022-06-02 19:08:19', '2022-06-02 19:08:19'),
(1887, 12, 4, 'Consumer', '2022-06-02 20:03:44', '2022-06-02 20:03:44'),
(1888, 12, 4, 'Consumer', '2022-06-02 20:04:08', '2022-06-02 20:04:08'),
(1889, 12, 4, 'Consumer', '2022-06-02 20:04:26', '2022-06-02 20:04:26'),
(1890, 12, 3, 'Driver', '2022-06-02 20:05:32', '2022-06-02 20:05:32'),
(1891, 12, 4, 'Consumer', '2022-06-02 20:05:51', '2022-06-02 20:05:51'),
(1892, 12, 4, 'Consumer', '2022-06-02 20:06:21', '2022-06-02 20:06:21'),
(1893, 12, 4, 'Consumer', '2022-06-02 20:06:25', '2022-06-02 20:06:25'),
(1894, 12, 4, 'Consumer', '2022-06-02 20:06:25', '2022-06-02 20:06:25'),
(1895, 12, 4, 'Consumer', '2022-06-02 20:06:26', '2022-06-02 20:06:26'),
(1896, 12, 4, 'Consumer', '2022-06-02 20:06:26', '2022-06-02 20:06:26'),
(1897, 12, 4, 'Consumer', '2022-06-02 20:06:26', '2022-06-02 20:06:26'),
(1898, 12, 4, 'Consumer', '2022-06-02 20:06:31', '2022-06-02 20:06:31'),
(1899, 12, 4, 'Consumer', '2022-06-02 20:06:31', '2022-06-02 20:06:31'),
(1900, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1901, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1902, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1903, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1904, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1905, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1906, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1907, 12, 4, 'Consumer', '2022-06-02 20:06:32', '2022-06-02 20:06:32'),
(1908, 12, 4, 'Consumer', '2022-06-02 20:06:46', '2022-06-02 20:06:46'),
(1909, 12, 4, 'Consumer', '2022-06-02 20:06:48', '2022-06-02 20:06:48'),
(1910, 12, 4, 'Consumer', '2022-06-02 20:06:48', '2022-06-02 20:06:48'),
(1911, 12, 4, 'Consumer', '2022-06-02 20:06:48', '2022-06-02 20:06:48'),
(1912, 12, 3, 'Driver', '2022-06-02 20:06:49', '2022-06-02 20:06:49'),
(1913, 12, 4, 'Consumer', '2022-06-02 20:07:21', '2022-06-02 20:07:21'),
(1914, 12, 4, 'Consumer', '2022-06-02 20:07:21', '2022-06-02 20:07:21'),
(1915, 12, 4, 'Consumer', '2022-06-02 20:07:21', '2022-06-02 20:07:21'),
(1916, 12, 4, 'Consumer', '2022-06-02 20:07:22', '2022-06-02 20:07:22'),
(1917, 12, 4, 'Consumer', '2022-06-02 20:07:22', '2022-06-02 20:07:22'),
(1918, 12, 4, 'Consumer', '2022-06-02 20:07:23', '2022-06-02 20:07:23'),
(1919, 12, 4, 'Consumer', '2022-06-02 20:07:23', '2022-06-02 20:07:23'),
(1920, 12, 4, 'Consumer', '2022-06-02 20:07:23', '2022-06-02 20:07:23'),
(1921, 12, 4, 'Consumer', '2022-06-02 20:07:24', '2022-06-02 20:07:24'),
(1922, 12, 4, 'Consumer', '2022-06-02 20:07:24', '2022-06-02 20:07:24'),
(1923, 12, 4, 'Consumer', '2022-06-02 20:07:24', '2022-06-02 20:07:24'),
(1924, 12, 4, 'Consumer', '2022-06-02 20:07:24', '2022-06-02 20:07:24'),
(1925, 12, 4, 'Consumer', '2022-06-02 20:07:24', '2022-06-02 20:07:24'),
(1926, 12, 4, 'Consumer', '2022-06-02 20:07:25', '2022-06-02 20:07:25'),
(1927, 12, 4, 'Consumer', '2022-06-02 20:07:25', '2022-06-02 20:07:25'),
(1928, 12, 4, 'Consumer', '2022-06-02 20:07:25', '2022-06-02 20:07:25'),
(1929, 12, 4, 'Consumer', '2022-06-02 20:07:26', '2022-06-02 20:07:26'),
(1930, 12, 4, 'Consumer', '2022-06-02 20:07:26', '2022-06-02 20:07:26'),
(1931, 12, 4, 'Consumer', '2022-06-02 20:07:26', '2022-06-02 20:07:26'),
(1932, 12, 4, 'Consumer', '2022-06-02 20:07:27', '2022-06-02 20:07:27'),
(1933, 12, 4, 'Consumer', '2022-06-02 20:07:27', '2022-06-02 20:07:27'),
(1934, 12, 4, 'Consumer', '2022-06-02 20:07:27', '2022-06-02 20:07:27'),
(1935, 12, 4, 'Consumer', '2022-06-02 20:07:28', '2022-06-02 20:07:28'),
(1936, 12, 4, 'Consumer', '2022-06-02 20:07:28', '2022-06-02 20:07:28'),
(1937, 12, 4, 'Consumer', '2022-06-02 20:07:28', '2022-06-02 20:07:28'),
(1938, 12, 4, 'Consumer', '2022-06-02 20:07:28', '2022-06-02 20:07:28'),
(1939, 12, 3, 'Driver', '2022-06-02 20:07:40', '2022-06-02 20:07:40'),
(1940, 12, 4, 'Consumer', '2022-06-02 20:07:45', '2022-06-02 20:07:45'),
(1941, 12, 4, 'Consumer', '2022-06-02 20:07:49', '2022-06-02 20:07:49'),
(1942, 12, 4, 'Consumer', '2022-06-02 20:07:52', '2022-06-02 20:07:52'),
(1943, 12, 4, 'Consumer', '2022-06-02 20:08:11', '2022-06-02 20:08:11'),
(1944, 12, 4, 'Consumer', '2022-06-02 20:08:22', '2022-06-02 20:08:22'),
(1945, 12, 4, 'Consumer', '2022-06-02 20:12:45', '2022-06-02 20:12:45'),
(1946, 12, 4, 'Consumer', '2022-06-02 20:13:11', '2022-06-02 20:13:11'),
(1947, 12, 4, 'Consumer', '2022-06-02 20:13:13', '2022-06-02 20:13:13'),
(1948, 12, 4, 'Consumer', '2022-06-02 20:14:29', '2022-06-02 20:14:29'),
(1949, 12, 4, 'Consumer', '2022-06-02 20:14:32', '2022-06-02 20:14:32'),
(1950, 12, 4, 'Consumer', '2022-06-02 20:14:33', '2022-06-02 20:14:33'),
(1951, 12, 4, 'Consumer', '2022-06-02 20:14:34', '2022-06-02 20:14:34'),
(1952, 12, 4, 'Consumer', '2022-06-02 20:14:35', '2022-06-02 20:14:35'),
(1953, 12, 4, 'Consumer', '2022-06-02 20:14:35', '2022-06-02 20:14:35'),
(1954, 12, 4, 'Consumer', '2022-06-02 20:14:36', '2022-06-02 20:14:36'),
(1955, 12, 4, 'Consumer', '2022-06-02 20:14:37', '2022-06-02 20:14:37'),
(1956, 12, 4, 'Consumer', '2022-06-02 20:14:37', '2022-06-02 20:14:37'),
(1957, 12, 4, 'Consumer', '2022-06-02 20:14:37', '2022-06-02 20:14:37'),
(1958, 12, 4, 'Consumer', '2022-06-02 20:14:37', '2022-06-02 20:14:37'),
(1959, 12, 4, 'Consumer', '2022-06-02 20:14:38', '2022-06-02 20:14:38'),
(1960, 12, 4, 'Consumer', '2022-06-02 20:14:38', '2022-06-02 20:14:38'),
(1961, 12, 4, 'Consumer', '2022-06-02 20:14:38', '2022-06-02 20:14:38'),
(1962, 12, 4, 'Consumer', '2022-06-02 20:14:38', '2022-06-02 20:14:38'),
(1963, 12, 3, 'Driver', '2022-06-02 20:14:39', '2022-06-02 20:14:39'),
(1964, 12, 4, 'Consumer', '2022-06-02 20:14:49', '2022-06-02 20:14:49'),
(1965, 12, 4, 'Consumer', '2022-06-02 20:15:51', '2022-06-02 20:15:51'),
(1966, 12, 4, 'Consumer', '2022-06-02 20:16:27', '2022-06-02 20:16:27'),
(1967, 12, 4, 'Consumer', '2022-06-02 20:22:33', '2022-06-02 20:22:33'),
(1968, 12, 4, 'Consumer', '2022-06-02 20:25:19', '2022-06-02 20:25:19'),
(1969, 12, 3, 'Driver', '2022-06-02 20:26:52', '2022-06-02 20:26:52'),
(1970, 12, 4, 'Consumer', '2022-06-02 20:26:59', '2022-06-02 20:26:59'),
(1971, 12, 4, 'Consumer', '2022-06-02 20:27:58', '2022-06-02 20:27:58'),
(1972, 12, 4, 'Consumer', '2022-06-02 20:32:21', '2022-06-02 20:32:21'),
(1973, 12, 4, 'Consumer', '2022-06-02 20:41:27', '2022-06-02 20:41:27'),
(1974, 12, 4, 'Consumer', '2022-06-02 20:41:47', '2022-06-02 20:41:47'),
(1975, 12, 4, 'Consumer', '2022-06-02 20:42:35', '2022-06-02 20:42:35'),
(1976, 12, 4, 'Consumer', '2022-06-02 20:44:33', '2022-06-02 20:44:33'),
(1977, 12, 4, 'Consumer', '2022-06-02 20:44:44', '2022-06-02 20:44:44'),
(1978, 12, 4, 'Consumer', '2022-06-02 20:45:11', '2022-06-02 20:45:11'),
(1979, 12, 4, 'Consumer', '2022-06-02 20:49:13', '2022-06-02 20:49:13'),
(1980, 12, 4, 'Consumer', '2022-06-02 20:49:24', '2022-06-02 20:49:24'),
(1981, 12, 3, 'Driver', '2022-06-02 20:51:10', '2022-06-02 20:51:10'),
(1982, 12, 4, 'Consumer', '2022-06-02 20:52:46', '2022-06-02 20:52:46'),
(1983, 12, 4, 'Consumer', '2022-06-02 20:58:56', '2022-06-02 20:58:56'),
(1984, 12, 4, 'Consumer', '2022-06-02 21:04:59', '2022-06-02 21:04:59'),
(1985, 12, 4, 'Consumer', '2022-06-02 21:06:15', '2022-06-02 21:06:15'),
(1986, 12, 4, 'Consumer', '2022-06-02 21:07:38', '2022-06-02 21:07:38'),
(1987, 12, 4, 'Consumer', '2022-06-02 21:08:09', '2022-06-02 21:08:09'),
(1988, 12, 4, 'Consumer', '2022-06-02 21:08:47', '2022-06-02 21:08:47'),
(1989, 12, 4, 'Consumer', '2022-06-02 21:23:23', '2022-06-02 21:23:23'),
(1990, 12, 4, 'Consumer', '2022-06-02 21:48:47', '2022-06-02 21:48:47'),
(1991, 12, 4, 'Consumer', '2022-06-02 22:20:05', '2022-06-02 22:20:05'),
(1992, 12, 4, 'Consumer', '2022-06-02 22:22:00', '2022-06-02 22:22:00'),
(1993, 12, 4, 'Consumer', '2022-06-02 22:28:11', '2022-06-02 22:28:11'),
(1994, 12, 4, 'Consumer', '2022-06-02 22:49:01', '2022-06-02 22:49:01'),
(1995, 12, 4, 'Consumer', '2022-06-02 22:51:35', '2022-06-02 22:51:35'),
(1996, 12, 4, 'Consumer', '2022-06-02 22:56:57', '2022-06-02 22:56:57'),
(1997, 12, 4, 'Consumer', '2022-06-02 22:58:33', '2022-06-02 22:58:33'),
(1998, 12, 4, 'Consumer', '2022-06-02 23:04:40', '2022-06-02 23:04:40'),
(1999, 12, 4, 'Consumer', '2022-06-02 23:15:42', '2022-06-02 23:15:42'),
(2000, 12, 4, 'Consumer', '2022-06-02 23:21:44', '2022-06-02 23:21:44'),
(2001, 12, 4, 'Consumer', '2022-06-03 00:13:09', '2022-06-03 00:13:09'),
(2002, 12, 3, 'Driver', '2022-06-03 00:13:39', '2022-06-03 00:13:39'),
(2003, 12, 4, 'Consumer', '2022-06-03 00:14:07', '2022-06-03 00:14:07'),
(2004, 12, 4, 'Consumer', '2022-06-03 00:14:07', '2022-06-03 00:14:07'),
(2005, 12, 4, 'Consumer', '2022-06-03 00:14:07', '2022-06-03 00:14:07'),
(2006, 12, 4, 'Consumer', '2022-06-03 00:14:07', '2022-06-03 00:14:07'),
(2007, 12, 4, 'Consumer', '2022-06-03 00:14:07', '2022-06-03 00:14:07'),
(2008, 12, 3, 'Driver', '2022-06-03 00:14:10', '2022-06-03 00:14:10'),
(2009, 12, 4, 'Consumer', '2022-06-03 00:14:23', '2022-06-03 00:14:23'),
(2010, 12, 4, 'Consumer', '2022-06-03 00:15:45', '2022-06-03 00:15:45'),
(2011, 12, 4, 'Consumer', '2022-06-03 02:40:16', '2022-06-03 02:40:16'),
(2012, 12, 4, 'Consumer', '2022-06-03 02:40:16', '2022-06-03 02:40:16'),
(2013, 12, 4, 'Consumer', '2022-06-03 02:40:20', '2022-06-03 02:40:20'),
(2014, 12, 3, 'Driver', '2022-06-03 02:42:29', '2022-06-03 02:42:29'),
(2015, 12, 3, 'Driver', '2022-06-03 02:42:31', '2022-06-03 02:42:31'),
(2016, 12, 4, 'Consumer', '2022-06-03 02:44:43', '2022-06-03 02:44:43'),
(2017, 12, 4, 'Consumer', '2022-06-03 18:11:08', '2022-06-03 18:11:08'),
(2018, 12, 4, 'Consumer', '2022-06-03 18:12:44', '2022-06-03 18:12:44'),
(2019, 12, 4, 'Consumer', '2022-06-03 18:14:40', '2022-06-03 18:14:40'),
(2020, 12, 4, 'Consumer', '2022-06-03 18:27:26', '2022-06-03 18:27:26'),
(2021, 12, 4, 'Consumer', '2022-06-03 18:27:48', '2022-06-03 18:27:48'),
(2022, 12, 4, 'Consumer', '2022-06-03 18:27:59', '2022-06-03 18:27:59'),
(2023, 12, 4, 'Consumer', '2022-06-03 18:31:47', '2022-06-03 18:31:47'),
(2024, 12, 4, 'Consumer', '2022-06-03 18:32:15', '2022-06-03 18:32:15'),
(2025, 12, 3, 'Driver', '2022-06-03 18:40:01', '2022-06-03 18:40:01'),
(2026, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2027, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2028, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2029, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2030, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2031, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2032, 12, 4, 'Consumer', '2022-06-03 18:40:20', '2022-06-03 18:40:20'),
(2033, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2034, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2035, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2036, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2037, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2038, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2039, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2040, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2041, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2042, 12, 4, 'Consumer', '2022-06-03 18:40:21', '2022-06-03 18:40:21'),
(2043, 12, 4, 'Consumer', '2022-06-03 18:40:53', '2022-06-03 18:40:53'),
(2044, 12, 4, 'Consumer', '2022-06-03 18:42:12', '2022-06-03 18:42:12'),
(2045, 12, 4, 'Consumer', '2022-06-03 18:42:32', '2022-06-03 18:42:32'),
(2046, 12, 4, 'Consumer', '2022-06-03 18:46:32', '2022-06-03 18:46:32'),
(2047, 12, 4, 'Consumer', '2022-06-03 18:56:58', '2022-06-03 18:56:58'),
(2048, 12, 4, 'Consumer', '2022-06-03 18:57:46', '2022-06-03 18:57:46'),
(2049, 12, 4, 'Consumer', '2022-06-03 18:58:10', '2022-06-03 18:58:10'),
(2050, 12, 4, 'Consumer', '2022-06-03 19:00:34', '2022-06-03 19:00:34'),
(2051, 12, 4, 'Consumer', '2022-06-03 19:01:34', '2022-06-03 19:01:34'),
(2052, 12, 4, 'Consumer', '2022-06-03 19:01:43', '2022-06-03 19:01:43'),
(2053, 12, 4, 'Consumer', '2022-06-03 19:06:20', '2022-06-03 19:06:20'),
(2054, 12, 4, 'Consumer', '2022-06-03 19:07:22', '2022-06-03 19:07:22'),
(2055, 12, 4, 'Consumer', '2022-06-03 19:10:57', '2022-06-03 19:10:57'),
(2056, 12, 4, 'Consumer', '2022-06-03 19:24:45', '2022-06-03 19:24:45'),
(2057, 12, 4, 'Consumer', '2022-06-03 19:26:09', '2022-06-03 19:26:09'),
(2058, 12, 4, 'Consumer', '2022-06-03 19:30:19', '2022-06-03 19:30:19'),
(2059, 12, 4, 'Consumer', '2022-06-03 19:32:04', '2022-06-03 19:32:04'),
(2060, 12, 4, 'Consumer', '2022-06-03 19:33:33', '2022-06-03 19:33:33'),
(2061, 12, 4, 'Consumer', '2022-06-03 19:37:22', '2022-06-03 19:37:22'),
(2062, 12, 4, 'Consumer', '2022-06-03 20:01:38', '2022-06-03 20:01:38'),
(2063, 12, 4, 'Consumer', '2022-06-03 20:02:38', '2022-06-03 20:02:38'),
(2064, 12, 4, 'Consumer', '2022-06-03 20:46:06', '2022-06-03 20:46:06'),
(2065, 12, 4, 'Consumer', '2022-06-03 20:53:04', '2022-06-03 20:53:04'),
(2066, 12, 4, 'Consumer', '2022-06-03 20:54:11', '2022-06-03 20:54:11'),
(2067, 12, 3, 'Driver', '2022-06-03 20:55:32', '2022-06-03 20:55:32'),
(2068, 12, 4, 'Consumer', '2022-06-03 21:06:25', '2022-06-03 21:06:25'),
(2069, 12, 4, 'Consumer', '2022-06-03 21:09:15', '2022-06-03 21:09:15'),
(2070, 12, 4, 'Consumer', '2022-06-03 21:11:33', '2022-06-03 21:11:33'),
(2071, 12, 3, 'Driver', '2022-06-03 21:11:49', '2022-06-03 21:11:49'),
(2072, 12, 4, 'Consumer', '2022-06-03 21:11:53', '2022-06-03 21:11:53'),
(2073, 12, 4, 'Consumer', '2022-06-03 21:17:16', '2022-06-03 21:17:16'),
(2074, 12, 4, 'Consumer', '2022-06-03 21:20:56', '2022-06-03 21:20:56'),
(2075, 12, 4, 'Consumer', '2022-06-03 21:21:10', '2022-06-03 21:21:10'),
(2076, 12, 4, 'Consumer', '2022-06-03 22:02:53', '2022-06-03 22:02:53'),
(2077, 12, 4, 'Consumer', '2022-06-03 22:03:23', '2022-06-03 22:03:23'),
(2078, 12, 4, 'Consumer', '2022-06-03 22:05:27', '2022-06-03 22:05:27'),
(2079, 12, 4, 'Consumer', '2022-06-03 22:05:35', '2022-06-03 22:05:35'),
(2080, 12, 4, 'Consumer', '2022-06-03 22:05:35', '2022-06-03 22:05:35'),
(2081, 12, 4, 'Consumer', '2022-06-03 22:07:16', '2022-06-03 22:07:16'),
(2082, 12, 4, 'Consumer', '2022-06-03 22:14:27', '2022-06-03 22:14:27'),
(2083, 12, 4, 'Consumer', '2022-06-03 22:15:20', '2022-06-03 22:15:20'),
(2084, 12, 4, 'Consumer', '2022-06-03 22:41:30', '2022-06-03 22:41:30'),
(2085, 12, 4, 'Consumer', '2022-06-03 22:47:41', '2022-06-03 22:47:41'),
(2086, 12, 4, 'Consumer', '2022-06-03 22:50:46', '2022-06-03 22:50:46'),
(2087, 12, 4, 'Consumer', '2022-06-03 22:51:30', '2022-06-03 22:51:30'),
(2088, 12, 4, 'Consumer', '2022-06-03 22:52:38', '2022-06-03 22:52:38'),
(2089, 12, 4, 'Consumer', '2022-06-03 22:56:09', '2022-06-03 22:56:09'),
(2090, 12, 4, 'Consumer', '2022-06-03 22:58:49', '2022-06-03 22:58:49'),
(2091, 12, 4, 'Consumer', '2022-06-03 22:59:53', '2022-06-03 22:59:53'),
(2092, 12, 4, 'Consumer', '2022-06-03 23:01:39', '2022-06-03 23:01:39'),
(2093, 12, 4, 'Consumer', '2022-06-03 23:02:24', '2022-06-03 23:02:24'),
(2094, 12, 4, 'Consumer', '2022-06-03 23:02:53', '2022-06-03 23:02:53'),
(2095, 12, 4, 'Consumer', '2022-06-03 23:03:58', '2022-06-03 23:03:58'),
(2096, 12, 4, 'Consumer', '2022-06-03 23:07:23', '2022-06-03 23:07:23'),
(2097, 12, 4, 'Consumer', '2022-06-03 23:08:45', '2022-06-03 23:08:45'),
(2098, 12, 4, 'Consumer', '2022-06-03 23:09:43', '2022-06-03 23:09:43'),
(2099, 12, 4, 'Consumer', '2022-06-03 23:12:05', '2022-06-03 23:12:05'),
(2100, 12, 4, 'Consumer', '2022-06-03 23:13:58', '2022-06-03 23:13:58'),
(2101, 12, 4, 'Consumer', '2022-06-03 23:14:30', '2022-06-03 23:14:30'),
(2102, 12, 4, 'Consumer', '2022-06-03 23:18:12', '2022-06-03 23:18:12'),
(2103, 12, 4, 'Consumer', '2022-06-03 23:18:54', '2022-06-03 23:18:54'),
(2104, 12, 4, 'Consumer', '2022-06-03 23:50:47', '2022-06-03 23:50:47'),
(2105, 12, 4, 'Consumer', '2022-06-03 23:51:24', '2022-06-03 23:51:24'),
(2106, 12, 4, 'Consumer', '2022-06-03 23:54:04', '2022-06-03 23:54:04'),
(2107, 12, 4, 'Consumer', '2022-06-03 23:55:35', '2022-06-03 23:55:35'),
(2108, 12, 4, 'Consumer', '2022-06-04 00:03:41', '2022-06-04 00:03:41'),
(2109, 12, 4, 'Consumer', '2022-06-04 00:09:46', '2022-06-04 00:09:46'),
(2110, 12, 4, 'Consumer', '2022-06-04 00:11:56', '2022-06-04 00:11:56'),
(2111, 12, 4, 'Consumer', '2022-06-04 00:12:17', '2022-06-04 00:12:17'),
(2112, 12, 4, 'Consumer', '2022-06-04 00:18:02', '2022-06-04 00:18:02'),
(2113, 12, 4, 'Consumer', '2022-06-04 00:31:33', '2022-06-04 00:31:33'),
(2114, 12, 4, 'Consumer', '2022-06-04 00:33:36', '2022-06-04 00:33:36'),
(2115, 12, 4, 'Consumer', '2022-06-04 00:35:28', '2022-06-04 00:35:28'),
(2116, 12, 4, 'Consumer', '2022-06-04 00:37:37', '2022-06-04 00:37:37'),
(2117, 12, 4, 'Consumer', '2022-06-04 00:48:08', '2022-06-04 00:48:08'),
(2118, 12, 4, 'Consumer', '2022-06-04 00:50:34', '2022-06-04 00:50:34'),
(2119, 12, 4, 'Consumer', '2022-06-04 00:51:24', '2022-06-04 00:51:24'),
(2120, 12, 4, 'Consumer', '2022-06-04 00:52:33', '2022-06-04 00:52:33'),
(2121, 12, 4, 'Consumer', '2022-06-04 00:59:51', '2022-06-04 00:59:51'),
(2122, 14, 3, 'Driver', '2022-06-04 01:08:16', '2022-06-04 01:08:16'),
(2123, 14, 3, 'Driver', '2022-06-04 01:08:19', '2022-06-04 01:08:19'),
(2124, 14, 4, 'Consumer', '2022-06-04 01:08:53', '2022-06-04 01:08:53'),
(2125, 14, 3, 'Driver', '2022-06-04 01:11:41', '2022-06-04 01:11:41'),
(2126, 14, 3, 'Driver', '2022-06-04 01:12:19', '2022-06-04 01:12:19'),
(2127, 14, 3, 'Driver', '2022-06-04 01:13:09', '2022-06-04 01:13:09'),
(2128, 14, 4, 'Consumer', '2022-06-04 01:14:16', '2022-06-04 01:14:16'),
(2129, 14, 3, 'Driver', '2022-06-04 01:14:20', '2022-06-04 01:14:20'),
(2130, 14, 3, 'Driver', '2022-06-04 01:14:52', '2022-06-04 01:14:52'),
(2131, 14, 3, 'Driver', '2022-06-04 01:15:48', '2022-06-04 01:15:48'),
(2132, 14, 3, 'Driver', '2022-06-04 01:17:53', '2022-06-04 01:17:53'),
(2133, 14, 3, 'Driver', '2022-06-04 01:18:52', '2022-06-04 01:18:52'),
(2134, 14, 3, 'Driver', '2022-06-04 01:19:49', '2022-06-04 01:19:49'),
(2135, 14, 3, 'Driver', '2022-06-04 01:22:16', '2022-06-04 01:22:16'),
(2136, 14, 3, 'Driver', '2022-06-04 01:22:54', '2022-06-04 01:22:54'),
(2137, 14, 3, 'Driver', '2022-06-04 01:25:36', '2022-06-04 01:25:36'),
(2138, 12, 4, 'Consumer', '2022-06-04 01:27:36', '2022-06-04 01:27:36'),
(2139, 12, 3, 'Driver', '2022-06-04 01:27:58', '2022-06-04 01:27:58'),
(2140, 14, 4, 'Consumer', '2022-06-04 01:30:53', '2022-06-04 01:30:53'),
(2141, 12, 4, 'Consumer', '2022-06-04 14:50:41', '2022-06-04 14:50:41'),
(2142, 12, 3, 'Driver', '2022-06-04 14:50:56', '2022-06-04 14:50:56'),
(2143, 12, 4, 'Consumer', '2022-06-04 14:51:19', '2022-06-04 14:51:19'),
(2144, 12, 4, 'Consumer', '2022-06-04 15:12:01', '2022-06-04 15:12:01'),
(2145, 12, 4, 'Consumer', '2022-06-04 15:12:45', '2022-06-04 15:12:45'),
(2146, 12, 4, 'Consumer', '2022-06-04 15:13:34', '2022-06-04 15:13:34'),
(2147, 12, 4, 'Consumer', '2022-06-04 15:13:56', '2022-06-04 15:13:56'),
(2148, 12, 4, 'Consumer', '2022-06-04 15:16:46', '2022-06-04 15:16:46'),
(2149, 12, 4, 'Consumer', '2022-06-04 15:19:27', '2022-06-04 15:19:27'),
(2150, 14, 3, 'Driver', '2022-06-04 16:13:39', '2022-06-04 16:13:39'),
(2151, 14, 4, 'Consumer', '2022-06-04 16:13:55', '2022-06-04 16:13:55'),
(2152, 15, 3, 'Driver', '2022-06-04 16:18:04', '2022-06-04 16:18:04'),
(2153, 15, 3, 'Driver', '2022-06-04 16:18:40', '2022-06-04 16:18:40'),
(2154, 15, 4, 'Consumer', '2022-06-04 16:21:56', '2022-06-04 16:21:56'),
(2155, 15, 4, 'Consumer', '2022-06-04 16:28:53', '2022-06-04 16:28:53'),
(2156, 15, 4, 'Consumer', '2022-06-04 16:28:59', '2022-06-04 16:28:59'),
(2157, 12, 4, 'Consumer', '2022-06-04 22:02:44', '2022-06-04 22:02:44'),
(2158, 12, 4, 'Consumer', '2022-06-04 22:03:28', '2022-06-04 22:03:28'),
(2159, 12, 3, 'Driver', '2022-06-04 22:47:05', '2022-06-04 22:47:05'),
(2160, 12, 3, 'Driver', '2022-06-04 22:49:02', '2022-06-04 22:49:02');
INSERT INTO `runtime_roles` (`id`, `user_id`, `role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(2161, 12, 3, 'Driver', '2022-06-04 22:49:35', '2022-06-04 22:49:35'),
(2162, 12, 3, 'Driver', '2022-06-04 22:53:27', '2022-06-04 22:53:27'),
(2163, 12, 3, 'Driver', '2022-06-04 22:53:33', '2022-06-04 22:53:33'),
(2164, 12, 3, 'Driver', '2022-06-04 22:54:06', '2022-06-04 22:54:06'),
(2165, 14, 3, 'Driver', '2022-06-04 22:56:42', '2022-06-04 22:56:42'),
(2166, 14, 4, 'Consumer', '2022-06-04 22:57:00', '2022-06-04 22:57:00'),
(2167, 12, 3, 'Driver', '2022-06-04 23:02:16', '2022-06-04 23:02:16'),
(2168, 12, 3, 'Driver', '2022-06-04 23:03:57', '2022-06-04 23:03:57'),
(2169, 12, 3, 'Driver', '2022-06-04 23:06:39', '2022-06-04 23:06:39'),
(2170, 12, 3, 'Driver', '2022-06-04 23:07:01', '2022-06-04 23:07:01'),
(2171, 12, 3, 'Driver', '2022-06-04 23:09:17', '2022-06-04 23:09:17'),
(2172, 12, 3, 'Driver', '2022-06-04 23:09:53', '2022-06-04 23:09:53'),
(2173, 12, 3, 'Driver', '2022-06-04 23:12:39', '2022-06-04 23:12:39'),
(2174, 12, 3, 'Driver', '2022-06-04 23:12:51', '2022-06-04 23:12:51'),
(2175, 12, 3, 'Driver', '2022-06-04 23:13:26', '2022-06-04 23:13:26'),
(2176, 12, 3, 'Driver', '2022-06-04 23:13:27', '2022-06-04 23:13:27'),
(2177, 12, 4, 'Consumer', '2022-06-04 23:13:57', '2022-06-04 23:13:57'),
(2178, 12, 4, 'Consumer', '2022-06-04 23:14:25', '2022-06-04 23:14:25'),
(2179, 12, 3, 'Driver', '2022-06-04 23:18:15', '2022-06-04 23:18:15'),
(2180, 12, 3, 'Driver', '2022-06-04 23:18:56', '2022-06-04 23:18:56'),
(2181, 12, 3, 'Driver', '2022-06-04 23:19:35', '2022-06-04 23:19:35'),
(2182, 12, 4, 'Consumer', '2022-06-04 23:21:05', '2022-06-04 23:21:05'),
(2183, 12, 3, 'Driver', '2022-06-04 23:21:16', '2022-06-04 23:21:16'),
(2184, 12, 3, 'Driver', '2022-06-04 23:22:25', '2022-06-04 23:22:25'),
(2185, 12, 4, 'Consumer', '2022-06-04 23:22:33', '2022-06-04 23:22:33'),
(2186, 12, 4, 'Consumer', '2022-06-04 23:23:30', '2022-06-04 23:23:30'),
(2187, 12, 3, 'Driver', '2022-06-04 23:23:55', '2022-06-04 23:23:55'),
(2188, 12, 4, 'Consumer', '2022-06-04 23:24:08', '2022-06-04 23:24:08'),
(2189, 12, 3, 'Driver', '2022-06-04 23:27:02', '2022-06-04 23:27:02'),
(2190, 12, 4, 'Consumer', '2022-06-04 23:27:05', '2022-06-04 23:27:05'),
(2191, 12, 3, 'Driver', '2022-06-04 23:27:57', '2022-06-04 23:27:57'),
(2192, 12, 4, 'Consumer', '2022-06-04 23:28:01', '2022-06-04 23:28:01'),
(2193, 12, 3, 'Driver', '2022-06-05 00:30:17', '2022-06-05 00:30:17'),
(2194, 12, 4, 'Consumer', '2022-06-05 00:31:00', '2022-06-05 00:31:00'),
(2195, 12, 3, 'Driver', '2022-06-05 00:31:44', '2022-06-05 00:31:44'),
(2196, 12, 3, 'Driver', '2022-06-05 00:57:03', '2022-06-05 00:57:03'),
(2197, 12, 4, 'Consumer', '2022-06-05 00:57:12', '2022-06-05 00:57:12'),
(2198, 12, 3, 'Driver', '2022-06-06 15:06:16', '2022-06-06 15:06:16'),
(2199, 12, 4, 'Consumer', '2022-06-06 15:10:47', '2022-06-06 15:10:47'),
(2200, 12, 4, 'Consumer', '2022-06-06 15:11:02', '2022-06-06 15:11:02'),
(2201, 12, 3, 'Driver', '2022-06-06 18:14:45', '2022-06-06 18:14:45'),
(2202, 12, 4, 'Consumer', '2022-06-06 18:23:31', '2022-06-06 18:23:31'),
(2203, 12, 4, 'Consumer', '2022-06-06 18:23:41', '2022-06-06 18:23:41'),
(2204, 12, 4, 'Consumer', '2022-06-06 18:23:47', '2022-06-06 18:23:47'),
(2205, 12, 4, 'Consumer', '2022-06-06 18:23:48', '2022-06-06 18:23:48'),
(2206, 12, 4, 'Consumer', '2022-06-06 18:24:19', '2022-06-06 18:24:19'),
(2207, 12, 4, 'Consumer', '2022-06-06 18:24:23', '2022-06-06 18:24:23'),
(2208, 12, 4, 'Consumer', '2022-06-06 18:24:25', '2022-06-06 18:24:25'),
(2209, 12, 4, 'Consumer', '2022-06-06 18:24:39', '2022-06-06 18:24:39'),
(2210, 12, 4, 'Consumer', '2022-06-06 18:25:27', '2022-06-06 18:25:27'),
(2211, 12, 4, 'Consumer', '2022-06-06 18:26:16', '2022-06-06 18:26:16'),
(2212, 12, 4, 'Consumer', '2022-06-06 18:28:41', '2022-06-06 18:28:41'),
(2213, 12, 4, 'Consumer', '2022-06-06 18:29:41', '2022-06-06 18:29:41'),
(2214, 12, 4, 'Consumer', '2022-06-06 18:35:57', '2022-06-06 18:35:57'),
(2215, 12, 4, 'Consumer', '2022-06-06 18:37:36', '2022-06-06 18:37:36'),
(2216, 12, 4, 'Consumer', '2022-06-06 18:37:48', '2022-06-06 18:37:48'),
(2217, 12, 4, 'Consumer', '2022-06-06 18:37:48', '2022-06-06 18:37:48'),
(2218, 12, 4, 'Consumer', '2022-06-06 18:37:48', '2022-06-06 18:37:48'),
(2219, 12, 4, 'Consumer', '2022-06-06 18:37:48', '2022-06-06 18:37:48'),
(2220, 12, 4, 'Consumer', '2022-06-06 18:40:10', '2022-06-06 18:40:10'),
(2221, 12, 4, 'Consumer', '2022-06-06 18:40:11', '2022-06-06 18:40:11'),
(2222, 12, 4, 'Consumer', '2022-06-06 18:40:16', '2022-06-06 18:40:16'),
(2223, 12, 4, 'Consumer', '2022-06-06 18:40:18', '2022-06-06 18:40:18'),
(2224, 12, 4, 'Consumer', '2022-06-06 18:40:19', '2022-06-06 18:40:19'),
(2225, 12, 4, 'Consumer', '2022-06-06 18:43:32', '2022-06-06 18:43:32'),
(2226, 12, 4, 'Consumer', '2022-06-06 18:44:47', '2022-06-06 18:44:47'),
(2227, 12, 4, 'Consumer', '2022-06-06 18:46:18', '2022-06-06 18:46:18'),
(2228, 12, 4, 'Consumer', '2022-06-06 18:58:53', '2022-06-06 18:58:53'),
(2229, 12, 3, 'Driver', '2022-06-06 18:59:46', '2022-06-06 18:59:46'),
(2230, 12, 4, 'Consumer', '2022-06-06 19:00:43', '2022-06-06 19:00:43'),
(2231, 12, 3, 'Driver', '2022-06-06 19:00:51', '2022-06-06 19:00:51'),
(2232, 12, 3, 'Driver', '2022-06-06 19:00:57', '2022-06-06 19:00:57'),
(2233, 12, 3, 'Driver', '2022-06-06 19:01:10', '2022-06-06 19:01:10'),
(2234, 12, 3, 'Driver', '2022-06-06 22:17:12', '2022-06-06 22:17:12'),
(2235, 12, 3, 'Driver', '2022-06-06 22:17:12', '2022-06-06 22:17:12'),
(2236, 12, 3, 'Driver', '2022-06-06 22:17:12', '2022-06-06 22:17:12'),
(2237, 12, 3, 'Driver', '2022-06-06 22:17:47', '2022-06-06 22:17:47'),
(2238, 12, 3, 'Driver', '2022-06-06 22:18:21', '2022-06-06 22:18:21'),
(2239, 12, 3, 'Driver', '2022-06-06 22:19:54', '2022-06-06 22:19:54'),
(2240, 12, 4, 'Consumer', '2022-06-06 22:20:15', '2022-06-06 22:20:15'),
(2241, 12, 4, 'Consumer', '2022-06-06 22:20:27', '2022-06-06 22:20:27'),
(2242, 12, 4, 'Consumer', '2022-06-06 22:20:38', '2022-06-06 22:20:38'),
(2243, 12, 3, 'Driver', '2022-06-06 22:20:47', '2022-06-06 22:20:47'),
(2244, 12, 3, 'Driver', '2022-06-06 22:22:50', '2022-06-06 22:22:50'),
(2245, 12, 3, 'Driver', '2022-06-06 22:22:55', '2022-06-06 22:22:55'),
(2246, 12, 3, 'Driver', '2022-06-06 22:28:15', '2022-06-06 22:28:15'),
(2247, 12, 3, 'Driver', '2022-06-06 22:33:14', '2022-06-06 22:33:14'),
(2248, 12, 3, 'Driver', '2022-06-06 22:33:17', '2022-06-06 22:33:17'),
(2249, 12, 3, 'Driver', '2022-06-06 22:37:30', '2022-06-06 22:37:30'),
(2250, 12, 3, 'Driver', '2022-06-06 22:39:16', '2022-06-06 22:39:16'),
(2251, 12, 3, 'Driver', '2022-06-06 22:50:55', '2022-06-06 22:50:55'),
(2252, 12, 3, 'Driver', '2022-06-06 22:50:59', '2022-06-06 22:50:59'),
(2253, 12, 3, 'Driver', '2022-06-06 22:51:55', '2022-06-06 22:51:55'),
(2254, 12, 4, 'Consumer', '2022-06-08 21:22:27', '2022-06-08 21:22:27'),
(2255, 12, 3, 'Driver', '2022-06-08 21:23:04', '2022-06-08 21:23:04'),
(2256, 12, 3, 'Driver', '2022-06-08 22:36:30', '2022-06-08 22:36:30'),
(2257, 16, 3, 'Driver', '2022-06-09 00:02:13', '2022-06-09 00:02:13'),
(2258, 12, 3, 'Driver', '2022-06-11 20:14:35', '2022-06-11 20:14:35'),
(2259, 12, 4, 'Consumer', '2022-06-11 20:14:40', '2022-06-11 20:14:40'),
(2260, 12, 4, 'Consumer', '2022-06-11 20:14:54', '2022-06-11 20:14:54'),
(2261, 12, 3, 'Driver', '2022-06-11 21:45:31', '2022-06-11 21:45:31'),
(2262, 12, 3, 'Driver', '2022-06-11 21:45:40', '2022-06-11 21:45:40'),
(2263, 12, 3, 'Driver', '2022-06-11 22:03:25', '2022-06-11 22:03:25'),
(2264, 12, 3, 'Driver', '2022-06-11 22:08:51', '2022-06-11 22:08:51'),
(2265, 12, 3, 'Driver', '2022-06-11 22:20:04', '2022-06-11 22:20:04'),
(2266, 12, 3, 'Driver', '2022-06-11 23:46:40', '2022-06-11 23:46:40'),
(2267, 12, 3, 'Driver', '2022-06-11 23:46:59', '2022-06-11 23:46:59'),
(2268, 17, 3, 'Driver', '2022-06-11 23:48:37', '2022-06-11 23:48:37'),
(2269, 17, 3, 'Driver', '2022-06-11 23:53:54', '2022-06-11 23:53:54'),
(2270, 17, 3, 'Driver', '2022-06-11 23:54:10', '2022-06-11 23:54:10'),
(2271, 18, 3, 'Driver', '2022-06-13 20:19:13', '2022-06-13 20:19:13'),
(2272, 18, 3, 'Driver', '2022-06-13 20:20:12', '2022-06-13 20:20:12'),
(2273, 18, 3, 'Driver', '2022-06-13 20:25:42', '2022-06-13 20:25:42'),
(2274, 18, 3, 'Driver', '2022-06-13 20:27:19', '2022-06-13 20:27:19'),
(2275, 18, 3, 'Driver', '2022-06-13 20:28:37', '2022-06-13 20:28:37'),
(2276, 12, 3, 'Driver', '2022-06-13 20:39:46', '2022-06-13 20:39:46'),
(2277, 12, 3, 'Driver', '2022-06-13 20:39:52', '2022-06-13 20:39:52'),
(2278, 12, 3, 'Driver', '2022-06-13 20:41:25', '2022-06-13 20:41:25'),
(2279, 12, 3, 'Driver', '2022-06-13 20:41:30', '2022-06-13 20:41:30'),
(2280, 12, 3, 'Driver', '2022-06-14 18:57:04', '2022-06-14 18:57:04'),
(2281, 12, 4, 'Consumer', '2022-06-14 19:41:45', '2022-06-14 19:41:45'),
(2282, 12, 3, 'Driver', '2022-06-14 20:08:47', '2022-06-14 20:08:47'),
(2283, 12, 4, 'Consumer', '2022-06-14 20:12:47', '2022-06-14 20:12:47'),
(2284, 12, 4, 'Consumer', '2022-06-14 20:13:07', '2022-06-14 20:13:07'),
(2285, 12, 4, 'Consumer', '2022-06-14 21:51:09', '2022-06-14 21:51:09'),
(2286, 12, 4, 'Consumer', '2022-06-14 22:35:31', '2022-06-14 22:35:31'),
(2287, 12, 3, 'Driver', '2022-06-14 22:35:59', '2022-06-14 22:35:59'),
(2288, 12, 3, 'Driver', '2022-06-14 22:38:27', '2022-06-14 22:38:27'),
(2289, 12, 3, 'Driver', '2022-06-14 22:46:25', '2022-06-14 22:46:25'),
(2290, 12, 3, 'Driver', '2022-06-14 22:47:47', '2022-06-14 22:47:47'),
(2291, 12, 3, 'Driver', '2022-06-14 22:49:15', '2022-06-14 22:49:15'),
(2292, 12, 3, 'Driver', '2022-06-14 22:49:33', '2022-06-14 22:49:33'),
(2293, 12, 4, 'Consumer', '2022-06-14 22:50:14', '2022-06-14 22:50:14'),
(2294, 12, 3, 'Driver', '2022-06-14 22:51:14', '2022-06-14 22:51:14'),
(2295, 12, 4, 'Consumer', '2022-06-14 22:51:22', '2022-06-14 22:51:22'),
(2296, 12, 3, 'Driver', '2022-06-14 23:06:23', '2022-06-14 23:06:23'),
(2297, 12, 4, 'Consumer', '2022-06-14 23:06:27', '2022-06-14 23:06:27'),
(2298, 12, 3, 'Driver', '2022-06-14 23:07:31', '2022-06-14 23:07:31'),
(2299, 12, 4, 'Consumer', '2022-06-14 23:07:35', '2022-06-14 23:07:35'),
(2300, 12, 3, 'Driver', '2022-06-14 23:07:57', '2022-06-14 23:07:57'),
(2301, 12, 4, 'Consumer', '2022-06-14 23:08:01', '2022-06-14 23:08:01'),
(2302, 12, 4, 'Consumer', '2022-06-14 23:09:18', '2022-06-14 23:09:18'),
(2303, 12, 4, 'Consumer', '2022-06-14 23:10:41', '2022-06-14 23:10:41'),
(2304, 12, 4, 'Consumer', '2022-06-14 23:10:49', '2022-06-14 23:10:49'),
(2305, 12, 4, 'Consumer', '2022-06-14 23:12:00', '2022-06-14 23:12:00'),
(2306, 12, 4, 'Consumer', '2022-06-14 23:13:08', '2022-06-14 23:13:08'),
(2307, 12, 4, 'Consumer', '2022-06-14 23:13:09', '2022-06-14 23:13:09'),
(2308, 12, 4, 'Consumer', '2022-06-14 23:27:15', '2022-06-14 23:27:15'),
(2309, 12, 3, 'Driver', '2022-06-14 23:40:33', '2022-06-14 23:40:33'),
(2310, 12, 4, 'Consumer', '2022-06-14 23:40:49', '2022-06-14 23:40:49'),
(2311, 12, 4, 'Consumer', '2022-06-14 23:41:00', '2022-06-14 23:41:00'),
(2312, 12, 4, 'Consumer', '2022-06-14 23:41:00', '2022-06-14 23:41:00'),
(2313, 12, 4, 'Consumer', '2022-06-14 23:41:00', '2022-06-14 23:41:00'),
(2314, 12, 4, 'Consumer', '2022-06-14 23:41:09', '2022-06-14 23:41:09'),
(2315, 12, 4, 'Consumer', '2022-06-14 23:41:14', '2022-06-14 23:41:14'),
(2316, 12, 4, 'Consumer', '2022-06-14 23:41:25', '2022-06-14 23:41:25'),
(2317, 12, 4, 'Consumer', '2022-06-14 23:43:54', '2022-06-14 23:43:54'),
(2318, 12, 4, 'Consumer', '2022-06-14 23:45:47', '2022-06-14 23:45:47'),
(2319, 12, 4, 'Consumer', '2022-06-14 23:47:47', '2022-06-14 23:47:47'),
(2320, 12, 4, 'Consumer', '2022-06-14 23:47:52', '2022-06-14 23:47:52'),
(2321, 12, 4, 'Consumer', '2022-06-14 23:48:09', '2022-06-14 23:48:09'),
(2322, 12, 4, 'Consumer', '2022-06-14 23:48:28', '2022-06-14 23:48:28'),
(2323, 12, 4, 'Consumer', '2022-06-14 23:49:03', '2022-06-14 23:49:03'),
(2324, 12, 4, 'Consumer', '2022-06-14 23:51:58', '2022-06-14 23:51:58'),
(2325, 12, 4, 'Consumer', '2022-06-14 23:56:32', '2022-06-14 23:56:32'),
(2326, 12, 4, 'Consumer', '2022-06-14 23:58:12', '2022-06-14 23:58:12'),
(2327, 12, 4, 'Consumer', '2022-06-14 23:58:19', '2022-06-14 23:58:19'),
(2328, 12, 4, 'Consumer', '2022-06-15 00:00:37', '2022-06-15 00:00:37'),
(2329, 12, 4, 'Consumer', '2022-06-15 00:02:38', '2022-06-15 00:02:38'),
(2330, 12, 4, 'Consumer', '2022-06-15 00:04:43', '2022-06-15 00:04:43'),
(2331, 12, 4, 'Consumer', '2022-06-15 00:06:22', '2022-06-15 00:06:22'),
(2332, 12, 4, 'Consumer', '2022-06-15 00:12:49', '2022-06-15 00:12:49'),
(2333, 12, 4, 'Consumer', '2022-06-15 00:15:25', '2022-06-15 00:15:25'),
(2334, 12, 4, 'Consumer', '2022-06-15 00:17:45', '2022-06-15 00:17:45'),
(2335, 12, 4, 'Consumer', '2022-06-15 00:19:26', '2022-06-15 00:19:26'),
(2336, 12, 4, 'Consumer', '2022-06-15 00:20:39', '2022-06-15 00:20:39'),
(2337, 12, 4, 'Consumer', '2022-06-15 00:22:13', '2022-06-15 00:22:13'),
(2338, 12, 4, 'Consumer', '2022-06-15 00:24:52', '2022-06-15 00:24:52'),
(2339, 12, 4, 'Consumer', '2022-06-15 00:29:13', '2022-06-15 00:29:13'),
(2340, 12, 4, 'Consumer', '2022-06-15 00:31:07', '2022-06-15 00:31:07'),
(2341, 12, 4, 'Consumer', '2022-06-15 00:36:06', '2022-06-15 00:36:06'),
(2342, 12, 4, 'Consumer', '2022-06-15 00:37:51', '2022-06-15 00:37:51'),
(2343, 12, 4, 'Consumer', '2022-06-15 00:38:30', '2022-06-15 00:38:30'),
(2344, 12, 4, 'Consumer', '2022-06-15 00:39:51', '2022-06-15 00:39:51'),
(2345, 12, 4, 'Consumer', '2022-06-15 00:43:59', '2022-06-15 00:43:59'),
(2346, 12, 4, 'Consumer', '2022-06-15 00:45:08', '2022-06-15 00:45:08'),
(2347, 12, 4, 'Consumer', '2022-06-15 00:46:39', '2022-06-15 00:46:39'),
(2348, 12, 4, 'Consumer', '2022-06-15 00:49:01', '2022-06-15 00:49:01'),
(2349, 12, 3, 'Driver', '2022-06-15 00:49:25', '2022-06-15 00:49:25'),
(2350, 12, 4, 'Consumer', '2022-06-15 00:51:53', '2022-06-15 00:51:53'),
(2351, 14, 4, 'Consumer', '2022-06-15 00:52:45', '2022-06-15 00:52:45'),
(2352, 14, 3, 'Driver', '2022-06-15 00:58:11', '2022-06-15 00:58:11'),
(2353, 19, 3, 'Driver', '2022-06-15 01:00:36', '2022-06-15 01:00:36'),
(2354, 19, 3, 'Driver', '2022-06-15 01:02:19', '2022-06-15 01:02:19'),
(2355, 19, 3, 'Driver', '2022-06-15 01:02:31', '2022-06-15 01:02:31'),
(2356, 14, 3, 'Driver', '2022-06-15 01:02:32', '2022-06-15 01:02:32'),
(2357, 14, 3, 'Driver', '2022-06-15 01:03:02', '2022-06-15 01:03:02'),
(2358, 14, 4, 'Consumer', '2022-06-15 01:03:52', '2022-06-15 01:03:52'),
(2359, 14, 4, 'Consumer', '2022-06-15 01:05:31', '2022-06-15 01:05:31'),
(2360, 14, 4, 'Consumer', '2022-06-15 01:19:14', '2022-06-15 01:19:14'),
(2361, 14, 4, 'Consumer', '2022-06-16 01:23:52', '2022-06-16 01:23:52'),
(2362, 14, 4, 'Consumer', '2022-06-16 01:24:41', '2022-06-16 01:24:41'),
(2363, 14, 3, 'Driver', '2022-06-16 01:24:54', '2022-06-16 01:24:54'),
(2364, 14, 3, 'Driver', '2022-06-16 01:26:26', '2022-06-16 01:26:26'),
(2365, 14, 4, 'Consumer', '2022-06-16 01:30:24', '2022-06-16 01:30:24'),
(2366, 14, 3, 'Driver', '2022-06-16 01:40:56', '2022-06-16 01:40:56'),
(2367, 14, 4, 'Consumer', '2022-06-16 01:42:16', '2022-06-16 01:42:16'),
(2368, 14, 4, 'Consumer', '2022-06-16 01:44:20', '2022-06-16 01:44:20'),
(2369, 14, 3, 'Driver', '2022-06-16 01:46:54', '2022-06-16 01:46:54'),
(2370, 14, 3, 'Driver', '2022-06-16 18:23:07', '2022-06-16 18:23:07'),
(2371, 14, 4, 'Consumer', '2022-06-16 18:23:40', '2022-06-16 18:23:40'),
(2372, 14, 3, 'Driver', '2022-06-16 18:23:42', '2022-06-16 18:23:42'),
(2373, 14, 3, 'Driver', '2022-06-16 18:23:48', '2022-06-16 18:23:48'),
(2374, 14, 3, 'Driver', '2022-06-16 18:23:48', '2022-06-16 18:23:48'),
(2375, 14, 3, 'Driver', '2022-06-16 18:24:57', '2022-06-16 18:24:57'),
(2376, 14, 3, 'Driver', '2022-06-16 18:25:12', '2022-06-16 18:25:12'),
(2377, 14, 3, 'Driver', '2022-06-16 18:25:33', '2022-06-16 18:25:33'),
(2378, 14, 3, 'Driver', '2022-06-16 18:26:42', '2022-06-16 18:26:42'),
(2379, 14, 4, 'Consumer', '2022-06-16 18:31:58', '2022-06-16 18:31:58'),
(2380, 14, 3, 'Driver', '2022-06-16 18:33:14', '2022-06-16 18:33:14'),
(2381, 14, 3, 'Driver', '2022-06-16 18:33:39', '2022-06-16 18:33:39'),
(2382, 14, 3, 'Driver', '2022-06-16 18:34:06', '2022-06-16 18:34:06'),
(2383, 14, 3, 'Driver', '2022-06-16 18:36:12', '2022-06-16 18:36:12'),
(2384, 14, 3, 'Driver', '2022-06-16 18:38:01', '2022-06-16 18:38:01'),
(2385, 14, 3, 'Driver', '2022-06-16 18:38:30', '2022-06-16 18:38:30'),
(2386, 14, 3, 'Driver', '2022-06-16 18:50:27', '2022-06-16 18:50:27'),
(2387, 14, 3, 'Driver', '2022-06-16 18:52:49', '2022-06-16 18:52:49'),
(2388, 14, 3, 'Driver', '2022-06-16 18:57:02', '2022-06-16 18:57:02'),
(2389, 14, 3, 'Driver', '2022-06-16 18:57:44', '2022-06-16 18:57:44'),
(2390, 14, 3, 'Driver', '2022-06-16 18:58:20', '2022-06-16 18:58:20'),
(2391, 14, 3, 'Driver', '2022-06-16 18:58:59', '2022-06-16 18:58:59'),
(2392, 14, 3, 'Driver', '2022-06-16 18:59:20', '2022-06-16 18:59:20'),
(2393, 14, 3, 'Driver', '2022-06-16 19:13:39', '2022-06-16 19:13:39'),
(2394, 14, 3, 'Driver', '2022-06-16 19:13:59', '2022-06-16 19:13:59'),
(2395, 14, 3, 'Driver', '2022-06-16 19:14:03', '2022-06-16 19:14:03'),
(2396, 14, 3, 'Driver', '2022-06-16 19:14:12', '2022-06-16 19:14:12'),
(2397, 14, 3, 'Driver', '2022-06-16 19:16:18', '2022-06-16 19:16:18'),
(2398, 14, 3, 'Driver', '2022-06-16 19:16:20', '2022-06-16 19:16:20'),
(2399, 14, 3, 'Driver', '2022-06-16 19:16:21', '2022-06-16 19:16:21'),
(2400, 14, 3, 'Driver', '2022-06-16 19:18:10', '2022-06-16 19:18:10'),
(2401, 14, 3, 'Driver', '2022-06-16 19:20:59', '2022-06-16 19:20:59'),
(2402, 14, 3, 'Driver', '2022-06-16 19:40:58', '2022-06-16 19:40:58'),
(2403, 14, 3, 'Driver', '2022-06-16 19:44:29', '2022-06-16 19:44:29'),
(2404, 14, 4, 'Consumer', '2022-06-16 19:44:38', '2022-06-16 19:44:38'),
(2405, 20, 3, 'Driver', '2022-06-16 20:02:28', '2022-06-16 20:02:28'),
(2406, 20, 3, 'Driver', '2022-06-16 20:02:54', '2022-06-16 20:02:54'),
(2407, 20, 3, 'Driver', '2022-06-16 20:15:46', '2022-06-16 20:15:46'),
(2408, 20, 3, 'Driver', '2022-06-16 20:15:49', '2022-06-16 20:15:49'),
(2409, 20, 3, 'Driver', '2022-06-16 20:16:57', '2022-06-16 20:16:57'),
(2410, 20, 3, 'Driver', '2022-06-16 20:16:59', '2022-06-16 20:16:59'),
(2411, 20, 3, 'Driver', '2022-06-16 20:39:21', '2022-06-16 20:39:21'),
(2412, 20, 3, 'Driver', '2022-06-16 20:39:25', '2022-06-16 20:39:25'),
(2413, 20, 3, 'Driver', '2022-06-16 20:42:51', '2022-06-16 20:42:51'),
(2414, 20, 3, 'Driver', '2022-06-16 20:42:54', '2022-06-16 20:42:54'),
(2415, 20, 3, 'Driver', '2022-06-16 20:47:00', '2022-06-16 20:47:00'),
(2416, 14, 3, 'Driver', '2022-06-16 21:10:26', '2022-06-16 21:10:26'),
(2417, 14, 3, 'Driver', '2022-06-16 21:51:30', '2022-06-16 21:51:30'),
(2418, 14, 3, 'Driver', '2022-06-16 21:58:16', '2022-06-16 21:58:16'),
(2419, 14, 3, 'Driver', '2022-06-16 22:07:32', '2022-06-16 22:07:32'),
(2420, 14, 3, 'Driver', '2022-06-16 22:07:44', '2022-06-16 22:07:44'),
(2421, 14, 3, 'Driver', '2022-06-16 22:09:57', '2022-06-16 22:09:57'),
(2422, 14, 3, 'Driver', '2022-06-16 22:09:57', '2022-06-16 22:09:57'),
(2423, 14, 3, 'Driver', '2022-06-16 22:11:35', '2022-06-16 22:11:35'),
(2424, 14, 3, 'Driver', '2022-06-16 22:12:30', '2022-06-16 22:12:30'),
(2425, 14, 3, 'Driver', '2022-06-16 22:17:07', '2022-06-16 22:17:07'),
(2426, 14, 4, 'Consumer', '2022-06-16 22:19:29', '2022-06-16 22:19:29'),
(2427, 14, 4, 'Consumer', '2022-06-16 22:22:14', '2022-06-16 22:22:14'),
(2428, 14, 4, 'Consumer', '2022-06-16 22:28:21', '2022-06-16 22:28:21'),
(2429, 14, 4, 'Consumer', '2022-06-16 22:29:31', '2022-06-16 22:29:31'),
(2430, 14, 4, 'Consumer', '2022-06-16 22:30:31', '2022-06-16 22:30:31'),
(2431, 14, 4, 'Consumer', '2022-06-16 22:31:54', '2022-06-16 22:31:54'),
(2432, 14, 4, 'Consumer', '2022-06-16 22:34:14', '2022-06-16 22:34:14'),
(2433, 14, 4, 'Consumer', '2022-06-16 22:37:15', '2022-06-16 22:37:15'),
(2434, 14, 4, 'Consumer', '2022-06-16 22:38:53', '2022-06-16 22:38:53'),
(2435, 14, 4, 'Consumer', '2022-06-16 22:43:36', '2022-06-16 22:43:36'),
(2436, 14, 4, 'Consumer', '2022-06-16 22:46:41', '2022-06-16 22:46:41'),
(2437, 14, 3, 'Driver', '2022-06-17 20:44:25', '2022-06-17 20:44:25'),
(2438, 14, 3, 'Driver', '2022-06-17 20:44:25', '2022-06-17 20:44:25'),
(2439, 14, 3, 'Driver', '2022-06-17 20:48:53', '2022-06-17 20:48:53'),
(2440, 14, 3, 'Driver', '2022-06-17 22:29:19', '2022-06-17 22:29:19'),
(2441, 14, 3, 'Driver', '2022-06-17 22:32:48', '2022-06-17 22:32:48'),
(2442, 14, 3, 'Driver', '2022-06-17 23:45:01', '2022-06-17 23:45:01'),
(2443, 14, 3, 'Driver', '2022-06-17 23:45:50', '2022-06-17 23:45:50'),
(2444, 14, 3, 'Driver', '2022-06-17 23:53:25', '2022-06-17 23:53:25'),
(2445, 14, 3, 'Driver', '2022-06-17 23:56:25', '2022-06-17 23:56:25'),
(2446, 14, 3, 'Driver', '2022-06-17 23:56:30', '2022-06-17 23:56:30'),
(2447, 14, 3, 'Driver', '2022-06-18 00:04:05', '2022-06-18 00:04:05'),
(2448, 14, 3, 'Driver', '2022-07-02 17:54:40', '2022-07-02 17:54:40'),
(2449, 21, 3, 'Driver', '2022-08-23 14:37:09', '2022-08-23 14:37:09'),
(2450, 14, 3, 'Driver', '2022-08-23 14:46:31', '2022-08-23 14:46:31'),
(2451, 14, 3, 'Driver', '2022-08-23 14:46:32', '2022-08-23 14:46:32'),
(2452, 14, 3, 'Driver', '2022-08-23 14:49:56', '2022-08-23 14:49:56'),
(2453, 14, 3, 'Driver', '2022-08-23 14:49:57', '2022-08-23 14:49:57'),
(2454, 14, 3, 'Driver', '2022-08-23 14:53:25', '2022-08-23 14:53:25'),
(2455, 14, 3, 'Driver', '2022-08-23 14:53:26', '2022-08-23 14:53:26'),
(2456, 14, 3, 'Driver', '2022-08-23 14:54:04', '2022-08-23 14:54:04'),
(2457, 14, 3, 'Driver', '2022-08-23 14:55:27', '2022-08-23 14:55:27'),
(2458, 14, 3, 'Driver', '2022-08-23 14:55:27', '2022-08-23 14:55:27'),
(2459, 14, 3, 'Driver', '2022-08-23 14:55:28', '2022-08-23 14:55:28'),
(2460, 14, 3, 'Driver', '2022-08-23 14:55:29', '2022-08-23 14:55:29'),
(2461, 14, 3, 'Driver', '2022-08-23 14:55:29', '2022-08-23 14:55:29'),
(2462, 14, 3, 'Driver', '2022-08-23 14:55:29', '2022-08-23 14:55:29'),
(2463, 14, 4, 'Consumer', '2022-08-23 14:55:33', '2022-08-23 14:55:33'),
(2464, 14, 3, 'Driver', '2022-08-23 14:58:14', '2022-08-23 14:58:14'),
(2465, 14, 3, 'Driver', '2022-08-23 14:58:25', '2022-08-23 14:58:25'),
(2466, 14, 3, 'Driver', '2022-08-23 15:00:23', '2022-08-23 15:00:23'),
(2467, 14, 3, 'Driver', '2022-08-23 15:02:35', '2022-08-23 15:02:35'),
(2468, 14, 4, 'Consumer', '2022-08-23 15:02:50', '2022-08-23 15:02:50'),
(2469, 14, 3, 'Driver', '2022-08-23 15:03:22', '2022-08-23 15:03:22'),
(2470, 14, 4, 'Consumer', '2022-08-23 15:03:31', '2022-08-23 15:03:31'),
(2471, 14, 4, 'Consumer', '2022-08-23 15:08:26', '2022-08-23 15:08:26'),
(2472, 14, 3, 'Driver', '2022-08-23 15:20:44', '2022-08-23 15:20:44'),
(2473, 14, 3, 'Driver', '2022-08-23 15:23:37', '2022-08-23 15:23:37'),
(2474, 14, 4, 'Consumer', '2022-08-23 15:23:48', '2022-08-23 15:23:48'),
(2475, 14, 3, 'Driver', '2022-08-23 15:48:38', '2022-08-23 15:48:38'),
(2476, 14, 4, 'Consumer', '2022-08-23 15:48:40', '2022-08-23 15:48:40'),
(2477, 14, 4, 'Consumer', '2022-08-23 15:51:02', '2022-08-23 15:51:02'),
(2478, 14, 4, 'Consumer', '2022-08-23 15:51:03', '2022-08-23 15:51:03'),
(2479, 14, 4, 'Consumer', '2022-08-23 15:53:55', '2022-08-23 15:53:55'),
(2480, 14, 4, 'Consumer', '2022-08-23 15:55:42', '2022-08-23 15:55:42'),
(2481, 14, 4, 'Consumer', '2022-08-23 15:56:28', '2022-08-23 15:56:28'),
(2482, 14, 4, 'Consumer', '2022-08-23 15:57:37', '2022-08-23 15:57:37'),
(2483, 14, 4, 'Consumer', '2022-08-23 15:57:37', '2022-08-23 15:57:37'),
(2484, 14, 4, 'Consumer', '2022-08-23 15:57:38', '2022-08-23 15:57:38'),
(2485, 14, 4, 'Consumer', '2022-08-23 15:58:46', '2022-08-23 15:58:46'),
(2486, 14, 4, 'Consumer', '2022-08-23 15:58:47', '2022-08-23 15:58:47'),
(2487, 14, 4, 'Consumer', '2022-08-23 15:58:47', '2022-08-23 15:58:47'),
(2488, 14, 3, 'Driver', '2022-08-23 15:59:07', '2022-08-23 15:59:07'),
(2489, 14, 3, 'Driver', '2022-08-23 15:59:11', '2022-08-23 15:59:11'),
(2490, 14, 3, 'Driver', '2022-08-23 15:59:16', '2022-08-23 15:59:16'),
(2491, 14, 3, 'Driver', '2022-08-23 15:59:31', '2022-08-23 15:59:31'),
(2492, 14, 3, 'Driver', '2022-08-23 16:02:51', '2022-08-23 16:02:51'),
(2493, 14, 4, 'Consumer', '2022-08-23 16:02:51', '2022-08-23 16:02:51'),
(2494, 14, 4, 'Consumer', '2022-08-23 16:02:51', '2022-08-23 16:02:51'),
(2495, 14, 4, 'Consumer', '2022-08-23 16:02:52', '2022-08-23 16:02:52'),
(2496, 14, 4, 'Consumer', '2022-08-23 16:02:53', '2022-08-23 16:02:53'),
(2497, 14, 4, 'Consumer', '2022-08-23 16:02:53', '2022-08-23 16:02:53'),
(2498, 14, 4, 'Consumer', '2022-08-23 16:02:53', '2022-08-23 16:02:53'),
(2499, 14, 4, 'Consumer', '2022-08-23 16:02:54', '2022-08-23 16:02:54'),
(2500, 14, 4, 'Consumer', '2022-08-23 16:02:54', '2022-08-23 16:02:54'),
(2501, 14, 3, 'Driver', '2022-08-23 16:02:58', '2022-08-23 16:02:58'),
(2502, 14, 3, 'Driver', '2022-08-23 16:02:59', '2022-08-23 16:02:59'),
(2503, 14, 3, 'Driver', '2022-08-23 16:02:59', '2022-08-23 16:02:59'),
(2504, 14, 3, 'Driver', '2022-08-23 16:03:00', '2022-08-23 16:03:00'),
(2505, 14, 3, 'Driver', '2022-08-23 16:03:00', '2022-08-23 16:03:00'),
(2506, 14, 3, 'Driver', '2022-08-23 16:03:01', '2022-08-23 16:03:01'),
(2507, 14, 3, 'Driver', '2022-08-23 16:03:01', '2022-08-23 16:03:01'),
(2508, 14, 3, 'Driver', '2022-08-23 16:03:01', '2022-08-23 16:03:01'),
(2509, 14, 3, 'Driver', '2022-08-23 16:03:01', '2022-08-23 16:03:01'),
(2510, 14, 3, 'Driver', '2022-08-23 16:03:02', '2022-08-23 16:03:02'),
(2511, 14, 3, 'Driver', '2022-08-23 16:03:03', '2022-08-23 16:03:03'),
(2512, 14, 3, 'Driver', '2022-08-23 16:03:03', '2022-08-23 16:03:03'),
(2513, 14, 3, 'Driver', '2022-08-23 16:03:03', '2022-08-23 16:03:03'),
(2514, 14, 3, 'Driver', '2022-08-23 16:03:04', '2022-08-23 16:03:04'),
(2515, 14, 3, 'Driver', '2022-08-23 16:03:04', '2022-08-23 16:03:04'),
(2516, 14, 3, 'Driver', '2022-08-23 16:03:04', '2022-08-23 16:03:04'),
(2517, 14, 3, 'Driver', '2022-08-23 16:03:05', '2022-08-23 16:03:05'),
(2518, 14, 3, 'Driver', '2022-08-23 16:03:30', '2022-08-23 16:03:30'),
(2519, 14, 3, 'Driver', '2022-08-23 16:04:39', '2022-08-23 16:04:39'),
(2520, 14, 3, 'Driver', '2022-08-23 16:04:44', '2022-08-23 16:04:44'),
(2521, 14, 3, 'Driver', '2022-08-23 16:04:56', '2022-08-23 16:04:56'),
(2522, 14, 3, 'Driver', '2022-08-23 16:20:44', '2022-08-23 16:20:44'),
(2523, 14, 3, 'Driver', '2022-08-23 16:21:13', '2022-08-23 16:21:13'),
(2524, 14, 4, 'Consumer', '2022-08-23 16:21:47', '2022-08-23 16:21:47'),
(2525, 14, 3, 'Driver', '2022-08-23 16:22:13', '2022-08-23 16:22:13'),
(2526, 14, 3, 'Driver', '2022-08-23 16:27:11', '2022-08-23 16:27:11'),
(2527, 14, 3, 'Driver', '2022-08-23 16:28:10', '2022-08-23 16:28:10'),
(2528, 14, 3, 'Driver', '2022-08-23 16:28:29', '2022-08-23 16:28:29'),
(2529, 14, 3, 'Driver', '2022-08-23 16:29:04', '2022-08-23 16:29:04'),
(2530, 14, 3, 'Driver', '2022-08-23 16:29:41', '2022-08-23 16:29:41'),
(2531, 14, 3, 'Driver', '2022-08-23 16:31:16', '2022-08-23 16:31:16'),
(2532, 14, 3, 'Driver', '2022-08-23 16:31:33', '2022-08-23 16:31:33'),
(2533, 14, 3, 'Driver', '2022-08-23 16:32:06', '2022-08-23 16:32:06'),
(2534, 22, 3, 'Driver', '2022-08-23 16:36:19', '2022-08-23 16:36:19'),
(2535, 22, 3, 'Driver', '2022-08-23 16:39:49', '2022-08-23 16:39:49'),
(2536, 22, 3, 'Driver', '2022-08-23 16:40:17', '2022-08-23 16:40:17'),
(2537, 22, 3, 'Driver', '2022-08-23 16:41:00', '2022-08-23 16:41:00'),
(2538, 22, 3, 'Driver', '2022-08-23 16:41:20', '2022-08-23 16:41:20'),
(2539, 22, 3, 'Driver', '2022-08-23 16:41:46', '2022-08-23 16:41:46'),
(2540, 22, 3, 'Driver', '2022-08-23 16:42:19', '2022-08-23 16:42:19'),
(2541, 22, 3, 'Driver', '2022-08-23 16:42:51', '2022-08-23 16:42:51'),
(2542, 22, 3, 'Driver', '2022-08-23 16:45:00', '2022-08-23 16:45:00'),
(2543, 22, 3, 'Driver', '2022-08-23 16:45:16', '2022-08-23 16:45:16'),
(2544, 22, 3, 'Driver', '2022-08-23 16:45:48', '2022-08-23 16:45:48'),
(2545, 22, 3, 'Driver', '2022-08-23 16:46:01', '2022-08-23 16:46:01'),
(2546, 22, 3, 'Driver', '2022-08-23 16:49:11', '2022-08-23 16:49:11'),
(2547, 22, 3, 'Driver', '2022-08-23 16:49:14', '2022-08-23 16:49:14'),
(2548, 22, 3, 'Driver', '2022-08-23 16:53:06', '2022-08-23 16:53:06'),
(2549, 22, 3, 'Driver', '2022-08-23 16:53:09', '2022-08-23 16:53:09'),
(2550, 22, 3, 'Driver', '2022-08-23 16:56:38', '2022-08-23 16:56:38'),
(2551, 22, 3, 'Driver', '2022-08-23 16:56:41', '2022-08-23 16:56:41'),
(2552, 22, 3, 'Driver', '2022-08-23 16:56:59', '2022-08-23 16:56:59'),
(2553, 22, 3, 'Driver', '2022-08-23 16:57:02', '2022-08-23 16:57:02'),
(2554, 22, 3, 'Driver', '2022-08-23 16:59:07', '2022-08-23 16:59:07'),
(2555, 22, 3, 'Driver', '2022-08-23 16:59:10', '2022-08-23 16:59:10'),
(2556, 22, 3, 'Driver', '2022-08-23 17:01:05', '2022-08-23 17:01:05'),
(2557, 22, 3, 'Driver', '2022-08-23 17:01:07', '2022-08-23 17:01:07'),
(2558, 22, 3, 'Driver', '2022-08-23 17:06:23', '2022-08-23 17:06:23'),
(2559, 22, 3, 'Driver', '2022-08-23 17:51:53', '2022-08-23 17:51:53'),
(2560, 22, 3, 'Driver', '2022-08-23 17:54:28', '2022-08-23 17:54:28'),
(2561, 22, 3, 'Driver', '2022-08-23 17:55:50', '2022-08-23 17:55:50'),
(2562, 22, 3, 'Driver', '2022-08-23 17:59:43', '2022-08-23 17:59:43'),
(2563, 22, 3, 'Driver', '2022-08-23 18:01:56', '2022-08-23 18:01:56'),
(2564, 22, 3, 'Driver', '2022-08-23 18:03:41', '2022-08-23 18:03:41'),
(2565, 22, 3, 'Driver', '2022-08-23 18:06:09', '2022-08-23 18:06:09'),
(2566, 22, 3, 'Driver', '2022-08-23 18:06:30', '2022-08-23 18:06:30'),
(2567, 22, 3, 'Driver', '2022-08-23 18:09:53', '2022-08-23 18:09:53'),
(2568, 22, 3, 'Driver', '2022-08-23 18:09:54', '2022-08-23 18:09:54'),
(2569, 22, 3, 'Driver', '2022-08-23 18:09:55', '2022-08-23 18:09:55'),
(2570, 22, 3, 'Driver', '2022-08-23 18:09:55', '2022-08-23 18:09:55'),
(2571, 22, 3, 'Driver', '2022-08-23 18:10:45', '2022-08-23 18:10:45'),
(2572, 22, 3, 'Driver', '2022-08-23 18:11:07', '2022-08-23 18:11:07'),
(2573, 22, 3, 'Driver', '2022-08-23 18:11:58', '2022-08-23 18:11:58'),
(2574, 14, 3, 'Driver', '2022-08-23 18:13:13', '2022-08-23 18:13:13'),
(2575, 14, 3, 'Driver', '2022-08-23 18:13:27', '2022-08-23 18:13:27'),
(2576, 14, 3, 'Driver', '2022-08-23 18:13:39', '2022-08-23 18:13:39'),
(2577, 14, 4, 'Consumer', '2022-08-23 18:14:20', '2022-08-23 18:14:20'),
(2578, 14, 3, 'Driver', '2022-08-23 18:14:58', '2022-08-23 18:14:58'),
(2579, 14, 3, 'Driver', '2022-08-23 18:15:34', '2022-08-23 18:15:34'),
(2580, 14, 3, 'Driver', '2022-08-23 18:21:24', '2022-08-23 18:21:24'),
(2581, 14, 3, 'Driver', '2022-08-23 18:41:47', '2022-08-23 18:41:47'),
(2582, 14, 3, 'Driver', '2022-08-23 18:43:21', '2022-08-23 18:43:21'),
(2583, 14, 4, 'Consumer', '2022-08-23 18:44:13', '2022-08-23 18:44:13'),
(2584, 14, 4, 'Consumer', '2022-08-23 18:49:47', '2022-08-23 18:49:47'),
(2585, 14, 4, 'Consumer', '2022-08-23 18:52:07', '2022-08-23 18:52:07'),
(2586, 14, 4, 'Consumer', '2022-08-23 18:54:53', '2022-08-23 18:54:53'),
(2587, 14, 4, 'Consumer', '2022-08-23 18:54:53', '2022-08-23 18:54:53'),
(2588, 14, 4, 'Consumer', '2022-08-23 18:59:50', '2022-08-23 18:59:50'),
(2589, 14, 4, 'Consumer', '2022-08-23 18:59:50', '2022-08-23 18:59:50'),
(2590, 14, 4, 'Consumer', '2022-08-23 19:00:16', '2022-08-23 19:00:16'),
(2591, 14, 4, 'Consumer', '2022-08-23 19:03:06', '2022-08-23 19:03:06'),
(2592, 14, 4, 'Consumer', '2022-08-23 19:06:10', '2022-08-23 19:06:10'),
(2593, 14, 4, 'Consumer', '2022-08-23 19:06:10', '2022-08-23 19:06:10'),
(2594, 14, 4, 'Consumer', '2022-08-23 19:13:00', '2022-08-23 19:13:00'),
(2595, 14, 4, 'Consumer', '2022-08-23 19:14:39', '2022-08-23 19:14:39'),
(2596, 14, 4, 'Consumer', '2022-08-23 19:17:19', '2022-08-23 19:17:19'),
(2597, 14, 4, 'Consumer', '2022-08-23 19:17:19', '2022-08-23 19:17:19'),
(2598, 14, 4, 'Consumer', '2022-08-23 19:18:27', '2022-08-23 19:18:27'),
(2599, 14, 4, 'Consumer', '2022-08-23 19:19:56', '2022-08-23 19:19:56'),
(2600, 14, 4, 'Consumer', '2022-08-23 19:19:56', '2022-08-23 19:19:56'),
(2601, 14, 4, 'Consumer', '2022-08-23 19:21:55', '2022-08-23 19:21:55'),
(2602, 14, 4, 'Consumer', '2022-08-23 19:21:55', '2022-08-23 19:21:55'),
(2603, 14, 4, 'Consumer', '2022-08-23 19:21:55', '2022-08-23 19:21:55'),
(2604, 14, 4, 'Consumer', '2022-08-23 19:23:05', '2022-08-23 19:23:05'),
(2605, 14, 4, 'Consumer', '2022-08-23 19:23:05', '2022-08-23 19:23:05'),
(2606, 14, 4, 'Consumer', '2022-08-23 19:26:05', '2022-08-23 19:26:05'),
(2607, 14, 4, 'Consumer', '2022-08-23 19:49:34', '2022-08-23 19:49:34'),
(2608, 14, 4, 'Consumer', '2022-08-23 19:49:34', '2022-08-23 19:49:34'),
(2609, 14, 4, 'Consumer', '2022-08-23 19:49:34', '2022-08-23 19:49:34'),
(2610, 14, 4, 'Consumer', '2022-08-23 19:58:04', '2022-08-23 19:58:04'),
(2611, 14, 4, 'Consumer', '2022-08-23 19:59:08', '2022-08-23 19:59:08'),
(2612, 14, 4, 'Consumer', '2022-08-23 20:00:11', '2022-08-23 20:00:11'),
(2613, 14, 4, 'Consumer', '2022-08-23 20:02:17', '2022-08-23 20:02:17'),
(2614, 14, 4, 'Consumer', '2022-08-23 20:07:15', '2022-08-23 20:07:15'),
(2615, 14, 4, 'Consumer', '2022-08-23 20:08:28', '2022-08-23 20:08:28'),
(2616, 14, 4, 'Consumer', '2022-08-23 20:09:35', '2022-08-23 20:09:35'),
(2617, 14, 4, 'Consumer', '2022-08-23 20:12:44', '2022-08-23 20:12:44'),
(2618, 14, 4, 'Consumer', '2022-08-23 20:13:33', '2022-08-23 20:13:33'),
(2619, 14, 4, 'Consumer', '2022-08-23 20:15:48', '2022-08-23 20:15:48'),
(2620, 14, 4, 'Consumer', '2022-08-23 20:16:57', '2022-08-23 20:16:57'),
(2621, 14, 4, 'Consumer', '2022-08-23 20:16:57', '2022-08-23 20:16:57'),
(2622, 14, 4, 'Consumer', '2022-08-23 20:25:54', '2022-08-23 20:25:54'),
(2623, 14, 3, 'Driver', '2022-08-23 20:26:29', '2022-08-23 20:26:29'),
(2624, 14, 4, 'Consumer', '2022-08-23 20:30:37', '2022-08-23 20:30:37'),
(2625, 14, 3, 'Driver', '2022-08-23 20:32:29', '2022-08-23 20:32:29'),
(2626, 14, 4, 'Consumer', '2022-08-23 20:33:46', '2022-08-23 20:33:46'),
(2627, 14, 4, 'Consumer', '2022-08-23 20:33:57', '2022-08-23 20:33:57'),
(2628, 14, 4, 'Consumer', '2022-08-23 20:51:29', '2022-08-23 20:51:29'),
(2629, 14, 4, 'Consumer', '2022-08-23 21:03:07', '2022-08-23 21:03:07');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `details` text COLLATE utf8_unicode_ci,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/December2020/5e03USjW3YUzscSPakCd.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otp_code` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_verified` int(11) NOT NULL DEFAULT '0',
  `email_verified` int(11) NOT NULL DEFAULT '0',
  `email_verified_at_copy1` timestamp NULL DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token_copy1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notify_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notify_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notify_push` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `contact`, `city`, `state`, `street`, `unit`, `zip`, `username`, `api_token`, `fcm_token`, `web_token`, `device_token`, `otp_code`, `contact_verified`, `email_verified`, `email_verified_at_copy1`, `document_id`, `remember_token_copy1`, `profile_pic`, `first_name`, `last_name`, `settings`, `notify_email`, `notify_order`, `notify_push`) VALUES
(1, 1, 'admin', 'superadmin@email.com', NULL, '$2y$12$cURTMOtwqbwyjL4qLY3cduewrNjQXF4Pvuy3Iu5qg6W6oh9JtROte', 'NjrhttYMzUGEPYxyRYwMD6l2szMrD0QyN9HDFN2lzqgX7grvc8AoBfgff5Tq', '2020-12-18 05:59:03', '2020-12-26 01:29:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '{\"locale\":\"en\"}', NULL, NULL, NULL),
(4, 1, 'admin2', 'admin2@email.com', NULL, '$2y$10$uDbKHQujW7r017/zP5.C8.FJ.R21V1YIDkSLDBgzSgpX9630lZePi', NULL, '2020-12-26 01:47:10', '2020-12-26 02:07:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '{\"locale\":\"en\"}', NULL, NULL, NULL),
(5, 2, 'sms', 'shoaibuddin12fx@mailinator.com', NULL, '$2y$10$Nmo2eelMwwaxGwCJxB3tBe84kkB6mODDWv1JhrBTFO8yp7EYfvNx6', 'meHxEFrVrj', '2022-05-09 11:47:19', '2022-05-09 11:47:19', NULL, '+923432322010', 'karachi', 'Sindh', 'raza square', 'N6', '360005', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shoaib', 'uddin', NULL, NULL, NULL, NULL),
(6, 2, 'joe', 'sms@mailinator.com', NULL, '$2y$10$7/J4w.UsZP98TfUoMpKkqeHesDInL.wk95/ngfUYkGoboc7c1DosK', 'M0W3FnMup2', '2022-05-09 13:15:27', '2022-05-09 13:15:27', NULL, '+923432322010', 'karachi', 'Sindh', 'raza square', 'N6', '360005', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shoaib', 'uddin', NULL, NULL, NULL, NULL),
(7, 3, 'joe', 'bs@mailinator.com', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'seUeorVOrY', '2022-05-10 03:12:17', '2022-05-16 15:13:09', NULL, '+923352757725', 'karachi', 'Sindh', 'raza square', 'N6', '360005', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shoaib', 'uddin', NULL, NULL, NULL, NULL),
(8, 4, 'abcd', 'noman@test.com', NULL, '$2y$10$6mKUMGS3gHZM1yrpQ2KPKeWsv9/BJkEx4IhiP/.isflBftnj2KY.e', 'Zrlgu2WNCD', '2022-05-18 21:02:27', '2022-05-25 19:09:35', NULL, '3111964952', 'KHI', 'ASs', '5555', '6546', '112', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'noman', 'ahmed', NULL, NULL, NULL, NULL),
(9, 3, 'joe', 'msmunami63@gmail.com', NULL, '$2y$10$NL0imdrjzNUCAn57SdR.eOGiEaNZh7FF8YdxhhOQFOPHOOTa2DA0W', '0vJLJj5df6', '2022-05-19 17:44:09', '2022-05-19 17:49:28', NULL, '3340289059', 'Karachi', 'Sindh', '22', '18A', '42000', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Virat', 'Kohli', NULL, NULL, NULL, NULL),
(10, 3, 'joe', 'junaidkhan55@gmail.com', NULL, '$2y$10$/t1Me3FcLRtQm19foH.ItuGC.D5SfIwRb13SpFFbBf1OT7h.raVBa', 'JQ0dsOZFa3', '2022-05-19 22:04:58', '2022-05-19 22:05:00', NULL, '3102656545', 'KHI', 'Sindh', '454', '454', '72800', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Ranven', 'Singh', NULL, NULL, NULL, NULL),
(11, 4, 'joe', 'asd@test.com', NULL, '$2y$10$kG25gkucBj1mWUbqWu5w.ukMmxPF5IcaIl6RmFPkhpmhg4Ch79OFu', '95EGddPtEf', '2022-05-21 16:46:28', '2022-05-21 18:40:53', NULL, '3111964952', 'dds', '232', '323', 'sad', 'dsad', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'asd', 'asd', NULL, NULL, NULL, NULL),
(12, 4, 'joe', 'umarhussain55@gmail.com', NULL, '$2y$10$Bz754w6ArBYDHRsvN9Gbf.sjdFJR6Ib3Zw.uvjXr4ds0sFlVu4YRW', 'KIaIhzEzeL', '2022-05-21 19:13:19', '2022-06-15 00:51:53', NULL, '3102667812', '454', '5', '454', '454', '45', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Ms', 'Dhoni', NULL, '1', '1', '1'),
(13, 3, 'joe', 'salman32@test.com', NULL, '$2y$10$5.x0AF2FrDFEkdes/LrcFO8r6x2sr4vuhMGp9QZx.cTJc7a5bQK1S', 'nwyQspNxb3', '2022-05-30 18:40:34', '2022-05-30 18:42:44', NULL, '3125454981', 'karachi', 'sindh', 'bin qasim road', 'C', '78600', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Salman', 'Khan', NULL, NULL, NULL, NULL),
(14, 4, 'joe', 'krantikandagatla59@gmail.com', NULL, '$2y$10$2LuBmyEd35UENg.Fyiu8IOmRgMnqLwqdl8FwLl4zn/Ft8x61qsfPO', 'QxLMETW6DV', '2022-06-04 01:07:59', '2022-08-23 20:33:46', NULL, '3452639781', 'Mumbai,India', 'Maharashtra', '99', 'Block#60', '78500', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Kranthi', 'Kandagatla', NULL, NULL, NULL, NULL),
(19, 3, 'joe', 'majidkhan@gmail.com', NULL, '$2y$10$1vD4ggWP0aj2Wtd5/QbDrOGYQFG33ORWW9M4jGLibjnQisUzAxI8y', 'L754kpxGEC', '2022-06-15 01:00:29', '2022-06-15 01:00:36', NULL, '3102698666', 'Asdsad', 'adSasd', 'Asdsad', 'Asdasd', '78600', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Masjid', 'Khan', NULL, NULL, NULL, NULL),
(22, 3, 'joe', 'shayankhan@gmail.com', NULL, '$2y$10$U4YtU4ounFBJ7vrgTBWw4.nyQq5xURYRwibey6uFWB7LJkvc.N4/u', 'L4jeKxbXf7', '2022-08-23 16:36:12', '2022-08-23 16:36:19', NULL, '3368227469', 'Karachi', 'Scheme 33', '203', 'A-203', '45641', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shayan', 'khan', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `registration_number` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vehicle_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver_licence` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_verified` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `user_id`, `registration_number`, `vehicle_type`, `driver_licence`, `is_verified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 'GJ03PK5532Q', 'suv', 'Y4886041', 1, '2020-11-09 06:18:42', '2020-11-09 06:18:42', NULL),
(2, 4, '12345678qweer', 'truck', 'Y4886041', 1, '2020-12-03 18:38:25', '2020-12-03 18:38:25', NULL),
(3, 7, 'AYH 629', '0', 'C:\\fakepath\\D4BF6A72-AD13-4774-9B38-D7E03BDB808B.jpeg', 1, '2022-05-11 20:12:01', '2022-05-11 20:12:01', NULL),
(4, 7, '4564654564', 'sedan', 'C:\\fakepath\\Screenshot 2022-05-06 at 11.42.57 AM.png', 1, '2022-05-16 14:12:00', '2022-05-16 14:12:00', NULL),
(5, 7, '55523987', 'truck', 'C:\\fakepath\\Screenshot 2022-05-13 at 7.39.37 PM.png', 1, '2022-05-16 14:20:20', '2022-05-16 14:20:20', NULL),
(6, 7, '4545', 'truck', 'C:\\fakepath\\Screenshot 2022-05-06 at 11.43.09 AM.png', 1, '2022-05-16 14:34:53', '2022-05-16 14:34:53', NULL),
(7, 7, '21212123121210', 'suv', 'C:\\fakepath\\Untitled 2.png', 1, '2022-05-16 15:19:18', '2022-05-16 15:19:18', NULL),
(8, 8, '121231313', 'truck', 'C:\\fakepath\\splash.png', 1, '2022-05-19 22:14:26', '2022-05-19 22:14:26', NULL),
(9, 11, 'sadsad', 'sedan', 'C:\\fakepath\\vuechartxexample (1).png', 1, '2022-05-21 16:47:03', '2022-05-21 16:47:03', NULL),
(10, 11, 'sadsad', 'sedan', 'C:\\fakepath\\vuechartxexample (1).png', 1, '2022-05-21 16:47:09', '2022-05-21 16:47:09', NULL),
(11, 12, '45645645641', 'suv', 'C:\\fakepath\\barcode (1).png', 1, '2022-05-21 19:13:44', '2022-05-21 19:13:44', NULL),
(12, 13, '454545621', 'suv', 'C:\\fakepath\\car4.png', 1, '2022-05-30 18:41:11', '2022-05-30 18:41:11', NULL),
(13, 14, '780920000', 'truck', 'C:\\fakepath\\Screenshot_20220603-164242.png', 1, '2022-06-04 01:08:41', '2022-06-04 01:08:41', NULL),
(14, 15, 'Gut', 'suv', 'C:\\fakepath\\IMG_20220603_223634.jpg', 1, '2022-06-04 16:19:21', '2022-06-04 16:19:21', NULL),
(15, 18, '454645644', 'suv', 'C:\\fakepath\\A0432173-A829-4D65-BC5E-E40042E6921F.jpeg', 1, '2022-06-13 20:20:32', '2022-06-13 20:20:32', NULL),
(16, 20, '65456454', 'sedan', 'C:\\fakepath\\7F53C7E6-45BF-4940-B239-9FE778FDC755.jpeg', 1, '2022-06-16 20:44:42', '2022-06-16 20:44:42', NULL),
(17, 22, '64256', 'truck', 'C:\\fakepath\\iTunesArtwork@2x.png', 1, '2022-08-23 17:01:48', '2022-08-23 17:01:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sizes`
--

CREATE TABLE `vehicle_sizes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehicle_sizes`
--

INSERT INTO `vehicle_sizes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'SUV', '2020-09-24 23:09:04', '2020-09-24 23:09:04'),
(2, 'Truck', '2020-09-24 23:09:04', '2020-09-24 23:09:04'),
(3, 'Sudan', '2020-09-24 23:09:04', '2020-09-24 23:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `is_paid` int(1) DEFAULT '0',
  `paid_date` timestamp NULL DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_amount` double DEFAULT '0',
  `driver_amount` double DEFAULT '0',
  `admin_amount` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `user_id`, `job_id`, `amount`, `is_paid`, `paid_date`, `order_id`, `time_taken`, `distance`, `created_at`, `updated_at`, `customer_amount`, `driver_amount`, `admin_amount`) VALUES
(2, 9, 22, 26, 0, NULL, NULL, NULL, NULL, '2020-10-31 10:20:33', '2020-10-31 10:20:33', 0, 0, 0),
(3, 11, 25, 11, 1, '2020-12-03 20:12:58', NULL, NULL, NULL, '2020-10-31 10:49:29', '2020-12-03 20:12:58', 0, 0, 0),
(4, 11, 25, 11, 1, '2020-12-03 20:12:58', NULL, NULL, NULL, '2020-10-31 10:54:11', '2020-12-03 20:12:58', 0, 0, 0),
(5, 11, 26, 11, 1, '2020-12-03 20:12:58', NULL, NULL, NULL, '2020-10-31 10:57:10', '2020-12-03 20:12:58', 0, 0, 0),
(6, 9, 23, 260, 0, NULL, '7NG53908L2794021F', NULL, NULL, '2020-11-01 16:15:21', '2020-11-01 16:15:21', 0, 0, 0),
(7, 9, 30, 11, 0, NULL, '86Y34009H7522402U', NULL, NULL, '2020-11-08 11:08:24', '2020-11-08 11:08:24', 0, 0, 0),
(8, 9, 31, 11, 0, NULL, '2WN76217UG0913841', NULL, NULL, '2020-11-08 11:10:36', '2020-11-08 11:10:36', 0, 0, 0),
(9, 9, 31, 11, 0, NULL, '3XR29549AV242490D', NULL, NULL, '2020-11-08 11:14:10', '2020-11-08 11:14:10', 0, 0, 0),
(10, 9, 31, 11, 0, NULL, '50W713216A209393R', NULL, NULL, '2020-11-08 11:16:08', '2020-11-08 11:16:08', 0, 0, 0),
(11, 9, 32, 11, 0, NULL, '2WK535651N650324R', NULL, NULL, '2020-11-08 11:18:18', '2020-11-08 11:18:18', 0, 0, 0),
(12, 9, 32, 11, 0, NULL, '9WN55105MV393474P', NULL, NULL, '2020-11-08 11:25:09', '2020-11-08 11:25:09', 0, 0, 0),
(13, 9, 32, 11, 0, NULL, '1RL7143626754501K', NULL, NULL, '2020-11-08 11:29:28', '2020-11-08 11:29:28', 0, 0, 0),
(14, 9, 32, 11, 0, NULL, '3UL1799233335963Y', NULL, NULL, '2020-11-08 11:31:42', '2020-11-08 11:31:42', 0, 0, 0),
(15, 9, 32, 11, 0, NULL, '8WM82737RN8252011', NULL, NULL, '2020-11-08 11:34:10', '2020-11-08 11:34:10', 0, 0, 0),
(16, 9, 32, 11, 0, NULL, '18F40743NG587482U', NULL, NULL, '2020-11-08 12:05:53', '2020-11-08 12:05:53', 0, 0, 0),
(17, 9, 32, 11, 0, NULL, '6487209537123714W', NULL, NULL, '2020-11-08 12:14:37', '2020-11-08 12:14:37', 0, 0, 0),
(18, 9, 32, 11, 0, NULL, '21U095421C429050T', NULL, NULL, '2020-11-08 12:21:13', '2020-11-08 12:21:13', 0, 0, 0),
(19, 9, 33, 11, 0, NULL, '1DH12631MH6941741', NULL, NULL, '2020-11-13 16:03:48', '2020-11-13 16:03:48', 0, 0, 0),
(20, 9, 34, 11, 0, NULL, '97563285BF1210907', NULL, NULL, '2020-11-13 16:51:57', '2020-11-13 16:51:57', 0, 0, 0),
(21, 9, 1, 7080, 0, NULL, '7W165611YA207060P', NULL, NULL, '2020-12-03 18:31:59', '2020-12-03 18:31:59', 0, 0, 0),
(22, 9, 1, 6372, 0, NULL, '3ME45987MM392590E', NULL, NULL, '2020-12-03 19:42:53', '2020-12-03 19:42:53', 0, 0, 0),
(28, 12, 5, 3540, 0, NULL, '12', 0, 0, '2022-05-23 20:36:06', '2022-05-23 20:36:06', 0, 0, 0),
(29, 12, 3, 12036, 0, NULL, '12', 0, 0, '2022-05-23 20:40:35', '2022-05-23 20:40:35', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_settings`
--
ALTER TABLE `admin_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashes`
--
ALTER TABLE `cashes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_types`
--
ALTER TABLE `category_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobstatus`
--
ALTER TABLE `jobstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_prices`
--
ALTER TABLE `job_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_urls`
--
ALTER TABLE `job_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `otp_numbers`
--
ALTER TABLE `otp_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `photourls`
--
ALTER TABLE `photourls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runtime_roles`
--
ALTER TABLE `runtime_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_sizes`
--
ALTER TABLE `vehicle_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_settings`
--
ALTER TABLE `admin_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cashes`
--
ALTER TABLE `cashes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `category_types`
--
ALTER TABLE `category_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=575;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `jobstatus`
--
ALTER TABLE `jobstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;

--
-- AUTO_INCREMENT for table `job_prices`
--
ALTER TABLE `job_prices`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_urls`
--
ALTER TABLE `job_urls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `otp_numbers`
--
ALTER TABLE `otp_numbers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `photourls`
--
ALTER TABLE `photourls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `runtime_roles`
--
ALTER TABLE `runtime_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2630;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `vehicle_sizes`
--
ALTER TABLE `vehicle_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
SET FOREIGN_KEY_CHECKS=1;