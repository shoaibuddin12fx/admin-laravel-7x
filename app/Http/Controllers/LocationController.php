<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\User;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = ['Locations'];
        $locations = Location::all();
        return view('admin.locations.index', compact('locations', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = ['Locations', 'Create'];
        $users = User::all();
        return view('locations.create', compact('breadcrumb', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new Location();
        $location->device_token = $request->device_token;
        $location->geohash = $request->geohash;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->role = $request->role;
        $location->document_id = $request->document_id;
        $location->user_id = $request->user;
        $location->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        $breadcrumb = ['Locations', 'Edit'];
        $users = User::all();
        return view('locations.edit', compact('breadcrumb', 'location', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $location = Location::findOrFail($id);

        $location->device_token = $request->device_token;
        $location->geohash = $request->geohash;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->role = $request->role;
        $location->document_id = $request->document_id;
        $location->user_id = $request->user;
        $location->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::destroy($id);
        return back()->with('success','Succesfully');
    }


}
