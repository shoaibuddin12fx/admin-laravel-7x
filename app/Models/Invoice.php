<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table = "invoices";
    protected $fillable = [
        'user_id', 'amount', 'capture_id', 'job_id', 'timestamp', 'document_id', 'order_id'
    ];

    public function job()
    {
        return $this->belongsTo('App\Models\Job', 'job_id', 'id');
    }

    public function getTimestampAttribute($value)
    {
        return $value;
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }
}
