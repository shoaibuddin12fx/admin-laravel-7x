<?php

namespace App\Http\Resources;

use App\Models\Vehicle;
use http\Env\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
//        return parent::toArray($request);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

        $vehicle = Vehicle::where('user_id', $obj->id)->first();
        $isVehicleVerified = false;
        if($vehicle){
            $isVehicleVerified = $vehicle->is_verified == 1;
        }

        return [
            "id" => $obj->id,
            "full_name" => $obj->fullName,
            "email" => $obj->email,
            "contact" => $obj->contact,
            "city" => $obj->city,
            "state" => $obj->state,
            "street" => $obj->street,
            "unit" => $obj->unit,
            "zip" => $obj->zip,
            "device_token" => $obj->device_token,
            "profile_pic" => $obj->profile_pic,
            "role" => $obj->currentRole,
            "is_vehicle_verified" => $isVehicleVerified,
            "rating" => JobResource::getTotalRating($obj->id),
            "notify_email" => $obj->notify_email,
            "notify_order" => $obj->notify_order,
            "notify_push" => $obj->notify_push
            // $obj->roles->pluck('name'),

        ];
    }
}
