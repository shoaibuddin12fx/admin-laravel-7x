<?php

namespace App\Helpers;
use GuzzleHttp\Client;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherHelper{

    public function sendNotification($message, $userId){
        //Remember to change this with your cluster name.
        $options = array(
            'cluster' => env('MIX_PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );

        //Remember to set your credentials below.
        $pusher = new Pusher(
            env('MIX_PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'), $options
        );

        //Send a message to notify channel with an event name of notify-event
        try {
            $pusher->trigger(env('PUSHER_APP_CHANNEL'), 'pusher-event-'.$userId, $message);
            return true;
        } catch (PusherException $e) {
            return false;
        }
    }

}
