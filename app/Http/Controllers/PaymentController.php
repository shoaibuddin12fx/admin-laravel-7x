<?php

namespace App\Http\Controllers;
use App\Http\Resources\PaymentViewCollection;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Resources\Paymentcollection;
class PaymentController extends Controller
{
    //Paymentcollection
     //
     function index(){

        $Invoice = Invoice::whereHas('job')->orderBy('id', 'desc')->get();
       // dd($Invoice);
        $data = new PaymentViewCollection( $Invoice );
        $data = $data->toArray(\request());
        $columns = [];

        if(count($data) > 0){
            $columns = array_keys($data[0]);
        }
        $actions = [
            [
                'label' => 'edit',
                'action' => route('admin.users.show', ':id')
            ],
          ];
          $formaction="admin.payment.edit";
        $breadcrumb = "Payments";
        return view('admin.Payments.index', compact('data','formaction','columns', 'breadcrumb'));
    }

}
