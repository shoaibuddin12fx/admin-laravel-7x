<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Joburls extends Model
{
    //
    protected $table = "job_urls";
    protected $fillable = [
        'owner_id', 'job_id', 'firebase_url'
    ];
}
