<?php

namespace App\Http\Controllers;

use App\Http\Resources\VehicleCollection;
use Illuminate\Http\Request;
use App\Models\Vehicle;
class VehiclesController extends Controller
{
    //
    function index(){

        $vehicles = Vehicle::with('user')->get();
        $data = new VehicleCollection( $vehicles );
        $data = $data->toArray(\request());
        $columns = [];

        if(count($data) > 0) {
            $columns = array_keys($data[0]);
        }
        $actions = [
            [
                'label' => 'edit',
                'action' => route('admin.users.show', ':id')
            ],
        ];
        
        $formaction="admin.vehicles.edit";
        $breadcrumb = "Vehicles";
      
        return view('admin.Vehicles.index', compact('data','formaction','actions', 'columns', 'breadcrumb'));
    }

    function deletevehicle($id){
        Vehicle::find($id)->delete();
        return back()->with('msg','Record has been Deleted ');
    }

    function addvehicle(Request $request){
       // return $request->all();
        $validatedData = $request->validate([
            'Registration' => 'required|max:30',
            'Type' => 'required|max:30',
            'Licence'=>'required|max:50',
            'Status'=>'required|max:30'
        ]);
        $tbl=new Vehicle();
        $tbl->registration_number=$request->Registration;
        $tbl->vehicle_type=$request->Type;
        $tbl->driver_licence=$request->Licence;
        $tbl->is_verified=$request->Status;
        $tbl->user_id="000";
        $tbl->save();
        return back()->with('msg','Vehicle added ');

    }
    function Getvehicle($id){
        return $id;
    }
    function updaterecord(Request $request){
       //unset($data['user_id']);
       $vehicle=Vehicle::findOrFail($request->id);
       if($request->is_verified){
           $is_verified=1;
       }else{
        $is_verified=0;
       }
       $vehicle->registration_number=$request->regisration_number;
       $vehicle->vehicle_type=$request->vehicle_type;
       $vehicle->driver_licence=$request->driver_licence;
       $vehicle->is_verified=$is_verified;
       $vehicle->is_paid=$request->is_paid;
       $vehicle->save();
       return back()->with('msg','record update');
       
       
        
     }

}
