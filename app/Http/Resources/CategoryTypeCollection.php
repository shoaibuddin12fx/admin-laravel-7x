<?php

namespace App\Http\Resources;

use App\Models\CategoryType;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryTypeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (CategoryType $categorytype){
            return new CategoryTypeResource($categorytype);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (CategoryType $categorytype){
            return new CategoryTypeResource($categorytype);
        });

        return $obj;
    }
}
