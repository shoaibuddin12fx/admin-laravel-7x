<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $table = "wallets";
    protected $fillable = [
        'user_id', 'job_id', 'amount', 'customer_amount', 'driver_amount', 'admin_amount', 'is_paid', 'paid_date', 'order_id', 'time_taken', 'distance'
    ];
    function status($id){
      $data=$this->all()->where('job_id','=',$id)->first();
      if($data==null){
        return "";
      }else{
          if( $data->is_paid=1){
              return "Active";
          }else{
            return "Not Active";
          }

      }


    }
}
