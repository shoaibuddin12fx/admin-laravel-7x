@extends('admin.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="box-title">Settings</h3>
                            </div>
                            <div class="card-body">
                                    <div class="form-group">
                                        <label>Admin Commission %:</label>

                                        <div class="input-group">
            {{--                                <span class="input-group-addon"><i class="fa fa-percent btn"></i></span>--}}
                                            <input id="adminCommissionId" value="" type="number" min="0" max="100" class="form-control" name="admin_commission" placeholder="Admin Commission in %" required>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label >$ per mile</label>
                                        <div class="input-group">
            {{--                                <span class="input-group-addon"><i class="fa fa-dollar btn"></i></span>--}}
                                            <input id="perMilePercentId"  value="{{ $settings->dollar_per_mile }}"  type="number" class="form-control" name="dollar_per_mile" placeholder="$/mile" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label >Minimum fare ($)</label>
                                        <div class="input-group">
            {{--                                <span class="input-group-addon"><i class="fa fa-money-bill btn"></i></span>--}}
                                            <input id="minFareId"  value="{{ $settings->min_fare }}" type="number" class="form-control" name="min_fare" placeholder="Min. fare" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Maximum fare ($)</label>

                                        <div class="input-group">
            {{--                                <span class="input-group-addon"><i class="fa fa-money-bill-alt btn"></i></span>--}}
                                            <input  id="maxFareId" value="{{ $settings->max_fare }}" type="number" class="form-control" name="max_fare" placeholder="Max. fare" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Booking cancel time frame (in minutes)</label>

                                        <div class="input-group form-inline">
            {{--                                <span class="input-group-addon"><i class="fa fa-clock-o btn"></i></span>--}}
                                            <input id="bookingId" value="{{ $settings->booking_cancel_time }}" type="number" class="form-control" name="booking_cancel_time" placeholder="Time in minutes" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Admin delivery fee ($)</label>

                                        <div class="input-group">
            {{--                                <span class="input-group-addon"><i class="fa fa-money btn"></i></span>--}}
                                            <input id="deliveryFeeId"  value="{{ $settings->admin_delivery_fee }}"  type="number" class="form-control" name="admin_delivery_fee" placeholder="Admin Delivery Fee" required>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>Minimum fare distance (miles)</label>

                                        <div class="input-group">
            {{--                                <span class="input-group-addon"><i class="fa fa-money btn"></i></span>--}}
                                            <input  id="minFareDistanceId"  value="{{ $settings->min_fare_distance }}" type="number" class="form-control" name="min_fare_distance" placeholder="Min. fare distance" required>
                                        </div>
                                    </div>

                                </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>

                        <!-- /.box-body -->
                        </div>
                    </form>

                    <!-- /.box -->

            <!-- /.box -->

        </div>
        <!-- /.col (left) -->
        <!-- /.col (right) -->
    </div>
        </div>
    </section>
@stop

@section('js')
    <!-- DataTables -->
    <script type="text/javascript">

    function clicked() {

    }
    </script>
@endsection
