<?php

namespace App\Providers;

use App\Helpers\PusherHelper;
use Illuminate\Support\ServiceProvider;

class PusherProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helpers\PusherHelper', function ($app) {
            return new PusherHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
