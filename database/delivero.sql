-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 13, 2022 at 09:19 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `delivero_admin_7x`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_settings`
--

CREATE TABLE `admin_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `admin_commission` int(11) DEFAULT '0',
  `dollar_per_mile` int(11) DEFAULT '0',
  `min_fare` int(11) DEFAULT '0',
  `max_fare` int(11) DEFAULT '0',
  `booking_cancel_time` int(11) DEFAULT '0',
  `admin_delivery_fee` int(11) DEFAULT '0',
  `min_fare_distance` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `user_id`, `admin_commission`, `dollar_per_mile`, `min_fare`, `max_fare`, `booking_cancel_time`, `admin_delivery_fee`, `min_fare_distance`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 10, 10, 20000, 5, 2, 1, '2020-11-20 05:37:15', '2020-12-03 18:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `cashes`
--

CREATE TABLE `cashes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cashout` int(11) NOT NULL,
  `source` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cashes`
--

INSERT INTO `cashes` (`id`, `user_id`, `cashout`, `source`, `created_at`, `updated_at`) VALUES
(1, 12, 300, 'paypal', '2020-10-24 09:52:32', '2020-10-24 09:52:32'),
(2, 11, 33, 'paypal', '2020-12-03 20:12:58', '2020-12-03 20:12:58'),
(3, 11, 12036, 'paypal', '2020-12-03 20:42:02', '2020-12-03 20:42:02'),
(4, 9, 25928, 'paypal', '2022-05-13 11:35:42', '2022-05-13 11:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_type_id` int(11) DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `category_type_id`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 'Toys & Games', NULL, 1, '1CSHyFcMv9DhamII9Kb6', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(2, 'Books, Movies & Music', NULL, 2, '1HKh0pBHJfMREUst9jSy', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(3, 'Women\'s Clothing & Shoes', NULL, 3, '3tbWp5B2zzEbvLhBMxce', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(4, 'Baby Products', NULL, 1, '5MzBEFDNysZFLeee39Uw', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(5, 'Antiques & Collectibles', NULL, 2, '6MDjUXyGnSXesufnwpgJ', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(6, 'Home Decor', NULL, 4, '7RTjF0TY6vgQGQwsEo9l', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(7, 'Major Appliances', NULL, 4, 'G8IwuYLqEaygc8xJEgnD', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(8, 'Men\'s Clothing & Shoes', NULL, 3, 'HBNxhsuO0GkFym2RC8vY', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(9, 'Garden & Outdoor', NULL, 4, 'HVoZ4X3NvJKfXsv1x5Nx', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(10, 'Bath', NULL, 4, 'JPkUZRY4SOWSm29ymg34', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(11, 'Arts & Crafts', NULL, 2, 'Kt6ouh29FGvP7rduthcK', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(12, 'Miscellaneous', NULL, 5, 'NwgK6kbeDTvBXkvjRs8u', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(13, 'Health & Beauty', NULL, 5, 'Oe6zyOJ65SWwJ1QN4pLN', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(14, 'Kid\'s Clothing', NULL, 3, 'Ot7sGj11r2vDBJ0II7w3', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(15, 'Handbags', NULL, 3, 'Pc6tpLjgs8uHYdx1HdwY', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(16, 'Sporting Goods', NULL, 2, 'UBUZBgsY1K1eyhB2uyjC', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(17, 'Office Supplies', NULL, 5, 'VtnWNVvzOo1CwPCRHyCi', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(18, 'Tools & Home Improvement', NULL, 4, 'Wx2kbGyMlWwuujt3dCOX', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(19, 'Furniture', NULL, 4, 'ZrN6irn4Dz4KZre7Dh5P', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(20, 'Home Storage & Organisations', NULL, 4, 'ZyIRPenc8vbWihJDTZRR', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(21, 'Household Supplies & Cleaning', NULL, 4, 'bAzBOWBK0af8XYVNdGyK', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(22, 'Lamps & Lighting', NULL, 4, 'bupFPa0s7IzM5IBpQErR', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(23, 'Kitchen & Dining', NULL, 4, 'goh6Q52m4mvxnfFuEDTI', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(24, 'Kid\'s Clothing', NULL, 1, 'jmaKq2pOr39KnZwngQPr', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(25, 'Video Games & Consoles', NULL, 6, 'lS4EhCiuIdUtVkifvwFJ', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(26, 'Toys & Games', NULL, 2, 'orhQcUpnvrgPVSICK2AS', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(27, 'Musical Instrument', NULL, 2, 'p0DCMOBYAZqwU6ulVIok', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(28, 'Jewellery & Watches', NULL, 3, 'sDZAES7MRz0wvYoKb75e', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(29, 'Auto Parts & Accessories', NULL, 5, 'sSBhItV4tc1vDCalCpoh', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(31, 'Luggage', NULL, 3, 'v4PRx6irEesrrSRomWgu', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(32, 'Computers & Other Electronics', NULL, 6, 'vGlhBdDzGWyjBwVMxR9t', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(33, '', NULL, 3, 'w7FeKadZPkDBquzXxrjh', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(34, 'Bedding', NULL, 4, 'wxwoxKBpgxFzgVv1KtUD', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(35, 'Pet Supplies', NULL, 5, 'yNv3jVebCgLkKaYB1pwB', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(36, 'Cell Phones & Accessories', NULL, 6, 'yPNplEvOQvCnJgWeNpVP', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(37, 'Test Alex', '1599933259.png', 1, NULL, '2020-09-12 12:54:19', '2020-09-12 12:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `category_types`
--

CREATE TABLE `category_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_types`
--

INSERT INTO `category_types` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(2, 'Home', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(3, 'Furniture', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(4, 'Documents', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(5, 'Miscellaneous', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(6, 'Others', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'New Good', '2020-09-24 22:59:57', '2020-09-24 22:59:57'),
(2, 'Old Good', '2020-09-24 22:59:57', '2020-09-24 22:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 0, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'cashout', 'text', 'Cashout', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'source', 'text', 'Source', 1, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(30, 5, 'image', 'text', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(31, 5, 'category_type_id', 'text', 'Category Type Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(32, 5, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(33, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(34, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(37, 6, 'image', 'text', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(38, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(39, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(40, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(41, 7, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(42, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(43, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(44, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(45, 8, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(46, 8, 'product_id', 'text', 'Product Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(47, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(48, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(49, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(50, 9, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(51, 9, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, '{}', 3),
(52, 9, 'capture_id', 'text', 'Capture Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(53, 9, 'job_id', 'text', 'Job Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(54, 9, 'order_id', 'text', 'Order Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(55, 9, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(56, 9, 'timestamp', 'text', 'Timestamp', 0, 1, 1, 1, 1, 1, '{}', 8),
(57, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(58, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(59, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(60, 10, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(61, 10, 'customer_amount', 'text', 'Customer Amount', 1, 1, 1, 1, 1, 1, '{}', 3),
(62, 10, 'driver_amount', 'text', 'Driver Amount', 1, 1, 1, 1, 1, 1, '{}', 4),
(63, 10, 'admin_amount', 'text', 'Admin Amount', 1, 1, 1, 1, 1, 1, '{}', 5),
(64, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(65, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(66, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(67, 12, 'delivery_address', 'text', 'Delivery Address', 0, 1, 1, 1, 1, 1, '{}', 2),
(68, 12, 'delivery_latitude', 'text', 'Delivery Latitude', 0, 1, 1, 1, 1, 1, '{}', 3),
(69, 12, 'delivery_longitude', 'text', 'Delivery Longitude', 0, 1, 1, 1, 1, 1, '{}', 4),
(70, 12, 'distance', 'text', 'Distance', 0, 1, 1, 1, 1, 1, '{}', 5),
(71, 12, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(72, 12, 'expected_delivery_time', 'timestamp', 'Expected Delivery Time', 0, 1, 1, 1, 1, 1, '{}', 7),
(73, 12, 'geohash', 'text', 'Geohash', 0, 1, 1, 1, 1, 1, '{}', 8),
(74, 12, 'item_category', 'text', 'Item Category', 0, 1, 1, 1, 1, 1, '{}', 9),
(75, 12, 'job_address', 'text', 'Job Address', 0, 1, 1, 1, 1, 1, '{}', 10),
(76, 12, 'job_latitude', 'text', 'Job Latitude', 0, 1, 1, 1, 1, 1, '{}', 11),
(77, 12, 'job_longitude', 'text', 'Job Longitude', 0, 1, 1, 1, 1, 1, '{}', 12),
(78, 12, 'job_price', 'text', 'Job Price', 1, 1, 1, 1, 1, 1, '{}', 13),
(79, 12, 'package_size', 'text', 'Package Size', 0, 1, 1, 1, 1, 1, '{}', 14),
(80, 12, 'posted_at', 'text', 'Posted At', 0, 1, 1, 1, 1, 1, '{}', 15),
(81, 12, 'receiver_id', 'text', 'Receiver Id', 0, 1, 1, 1, 1, 1, '{}', 16),
(82, 12, 'priority', 'text', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 17),
(83, 12, 'receiver_name', 'text', 'Receiver Name', 0, 1, 1, 1, 1, 1, '{}', 18),
(84, 12, 'receiver_contact', 'text', 'Receiver Contact', 0, 1, 1, 1, 1, 1, '{}', 19),
(85, 12, 'receiver_instructions', 'text', 'Receiver Instructions', 0, 1, 1, 1, 1, 1, '{}', 20),
(86, 12, 'security_code', 'text', 'Security Code', 0, 1, 1, 1, 1, 1, '{}', 21),
(87, 12, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 22),
(88, 12, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 23),
(89, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 24),
(90, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 25),
(91, 12, 'sender_id', 'text', 'Sender Id', 0, 1, 1, 1, 1, 1, '{}', 26),
(92, 12, 'source_address_appartment', 'text', 'Source Address Appartment', 0, 1, 1, 1, 1, 1, '{}', 27),
(93, 12, 'delivery_address_appartment', 'text', 'Delivery Address Appartment', 0, 1, 1, 1, 1, 1, '{}', 28),
(94, 12, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 29),
(95, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(96, 15, 'device_token', 'text', 'Device Token', 0, 1, 1, 1, 1, 1, '{}', 2),
(97, 15, 'geohash', 'text', 'Geohash', 0, 1, 1, 1, 1, 1, '{}', 3),
(98, 15, 'latitude', 'text', 'Latitude', 0, 1, 1, 1, 1, 1, '{}', 4),
(99, 15, 'longitude', 'text', 'Longitude', 0, 1, 1, 1, 1, 1, '{}', 5),
(100, 15, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 6),
(101, 15, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(102, 15, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 8),
(103, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(104, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(105, 15, 'distance', 'text', 'Distance', 0, 1, 1, 1, 1, 1, '{}', 11),
(106, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(107, 16, 'phone_number', 'text', 'Phone Number', 0, 1, 1, 1, 1, 1, '{}', 2),
(108, 16, 'otp_code', 'text', 'Otp Code', 0, 1, 1, 1, 1, 1, '{}', 3),
(109, 16, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(110, 16, 'is_verified', 'text', 'Is Verified', 0, 1, 1, 1, 1, 1, '{}', 5),
(111, 16, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(112, 16, 'expires', 'timestamp', 'Expires', 0, 1, 1, 1, 1, 1, '{}', 7),
(113, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(114, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(115, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(116, 17, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(117, 17, 'message', 'text', 'Message', 1, 1, 1, 1, 1, 1, '{}', 3),
(118, 17, 'room_id', 'text', 'Room Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(119, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(120, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(121, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(122, 19, 'added_by', 'text', 'Added By', 1, 1, 1, 1, 1, 1, '{}', 2),
(123, 19, 'category_id', 'text', 'Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(124, 19, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 4),
(125, 19, 'extra_labels', 'text', 'Extra Labels', 0, 1, 1, 1, 1, 1, '{}', 5),
(126, 19, 'geohash', 'text', 'Geohash', 0, 1, 1, 1, 1, 1, '{}', 6),
(127, 19, 'insured', 'text', 'Insured', 0, 1, 1, 1, 1, 1, '{}', 7),
(128, 19, 'latitude', 'text', 'Latitude', 1, 1, 1, 1, 1, 1, '{}', 8),
(129, 19, 'longitude', 'text', 'Longitude', 1, 1, 1, 1, 1, 1, '{}', 9),
(130, 19, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 10),
(131, 19, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 11),
(132, 19, 'quantities_available', 'text', 'Quantities Available', 1, 1, 1, 1, 1, 1, '{}', 12),
(133, 19, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 13),
(134, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(135, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(136, 19, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 16),
(137, 19, 'condition_id', 'text', 'Condition Id', 0, 1, 1, 1, 1, 1, '{}', 17),
(138, 19, 'brand', 'text', 'Brand', 0, 1, 1, 1, 1, 1, '{}', 18),
(139, 19, 'vehicle_id', 'text', 'Vehicle Id', 0, 1, 1, 1, 1, 1, '{}', 19),
(140, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(141, 20, 'messeges_count', 'text', 'Messeges Count', 1, 1, 1, 1, 1, 1, '{}', 2),
(142, 20, 'initiator_id', 'text', 'Initiator Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(143, 20, 'recipient_id', 'text', 'Recipient Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(144, 20, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 5),
(145, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(146, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(147, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(148, 22, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(149, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(150, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(151, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(152, 23, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(153, 23, 'registration_number', 'text', 'Registration Number', 0, 1, 1, 1, 1, 1, '{}', 3),
(154, 23, 'vehicle_type', 'text', 'Vehicle Type', 0, 1, 1, 1, 1, 1, '{}', 4),
(155, 23, 'driver_licence', 'text', 'Driver Licence', 0, 1, 1, 1, 1, 1, '{}', 5),
(156, 23, 'is_verified', 'text', 'Is Verified', 0, 1, 1, 1, 1, 1, '{}', 6),
(157, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(158, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(159, 23, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 9),
(160, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 0),
(161, 24, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(162, 24, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(163, 24, 'amount', 'text', 'Amount', 1, 1, 1, 1, 1, 1, '{}', 4),
(164, 24, 'is_paid', 'text', 'Is Paid', 0, 1, 1, 1, 1, 1, '{}', 5),
(165, 24, 'paid_date', 'timestamp', 'Paid Date', 0, 1, 1, 1, 1, 1, '{}', 6),
(166, 24, 'order_id', 'text', 'Order Id', 0, 1, 1, 1, 1, 1, '{}', 7),
(167, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(168, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(169, 24, 'customer_amount', 'text', 'Customer Amount', 0, 1, 1, 1, 1, 1, '{}', 10),
(170, 24, 'driver_amount', 'text', 'Driver Amount', 0, 1, 1, 1, 1, 1, '{}', 11),
(171, 24, 'admin_amount', 'text', 'Admin Amount', 0, 1, 1, 1, 1, 1, '{}', 12),
(182, 33, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(183, 33, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(184, 33, 'role_id', 'text', 'Role Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(185, 33, 'role_name', 'text', 'Role Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(186, 33, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(187, 33, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(198, 38, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(199, 38, 'owner_id', 'text', 'Owner Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(200, 38, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(201, 38, 'firebase_url', 'text', 'Firebase Url', 1, 1, 1, 1, 1, 1, '{}', 4),
(202, 38, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(203, 38, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(204, 39, 'id', 'text', 'Id', 1, 1, 1, 1, 1, 1, '{}', 1),
(205, 39, 'job_id', 'text', 'Job Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(206, 39, 'sender_id', 'text', 'Sender Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(207, 39, 'reviewer_id', 'text', 'Reviewer Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(208, 39, 'rating', 'text', 'Rating', 0, 1, 1, 1, 1, 1, '{}', 5),
(209, 39, 'review', 'text', 'Review', 0, 1, 1, 1, 1, 1, '{}', 6),
(210, 39, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(211, 39, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(212, 39, 'client_reviewed', 'text', 'Client Reviewed', 0, 1, 1, 1, 1, 1, '{}', 9),
(213, 39, 'driver_reviewed', 'text', 'Driver Reviewed', 0, 1, 1, 1, 1, 1, '{}', 10),
(214, 40, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(215, 40, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(216, 40, 'admin_commission', 'text', 'Admin Commission', 0, 1, 1, 1, 1, 1, '{}', 3),
(217, 40, 'dollar_per_mile', 'text', 'Dollar Per Mile', 0, 1, 1, 1, 1, 1, '{}', 4),
(218, 40, 'min_fare', 'text', 'Min Fare', 0, 1, 1, 1, 1, 1, '{}', 5),
(219, 40, 'max_fare', 'text', 'Max Fare', 0, 1, 1, 1, 1, 1, '{}', 6),
(220, 40, 'booking_cancel_time', 'text', 'Booking Cancel Time', 0, 1, 1, 1, 1, 1, '{}', 7),
(221, 40, 'admin_delivery_fee', 'text', 'Admin Delivery Fee', 0, 1, 1, 1, 1, 1, '{}', 8),
(222, 40, 'min_fare_distance', 'text', 'Min Fare Distance', 0, 1, 1, 1, 1, 1, '{}', 9),
(223, 40, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(224, 40, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(225, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 0, '{}', 5),
(226, 1, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 10),
(227, 1, 'contact', 'text', 'Contact', 0, 1, 1, 1, 1, 1, '{}', 11),
(228, 1, 'city', 'text', 'City', 0, 1, 1, 1, 1, 1, '{}', 12),
(229, 1, 'state', 'text', 'State', 0, 1, 1, 1, 1, 1, '{}', 13),
(230, 1, 'street', 'text', 'Street', 0, 1, 1, 1, 1, 1, '{}', 14),
(231, 1, 'unit', 'text', 'Unit', 0, 1, 1, 1, 1, 1, '{}', 15),
(232, 1, 'zip', 'text', 'Zip', 0, 1, 1, 1, 1, 1, '{}', 16),
(233, 1, 'username', 'text', 'Username', 0, 1, 1, 1, 1, 1, '{}', 17),
(234, 1, 'api_token', 'text', 'Api Token', 0, 0, 0, 0, 0, 0, '{}', 18),
(235, 1, 'fcm_token', 'text', 'Fcm Token', 0, 0, 0, 0, 0, 0, '{}', 19),
(236, 1, 'web_token', 'text', 'Web Token', 0, 0, 0, 0, 0, 0, '{}', 20),
(237, 1, 'device_token', 'text', 'Device Token', 0, 0, 0, 0, 0, 0, '{}', 21),
(238, 1, 'otp_code', 'text', 'Otp Code', 0, 0, 0, 0, 0, 0, '{}', 22),
(239, 1, 'contact_verified', 'text', 'Contact Verified', 1, 0, 0, 0, 0, 0, '{}', 23),
(240, 1, 'email_verified', 'text', 'Email Verified', 1, 0, 0, 0, 0, 0, '{}', 24),
(241, 1, 'email_verified_at_copy1', 'timestamp', 'Email Verified At Copy1', 0, 0, 0, 0, 0, 0, '{}', 25),
(242, 1, 'document_id', 'text', 'Document Id', 0, 0, 0, 0, 0, 0, '{}', 26),
(243, 1, 'remember_token_copy1', 'text', 'Remember Token Copy1', 0, 0, 0, 0, 0, 0, '{}', 27),
(244, 1, 'profile_pic', 'text', 'Profile Pic', 0, 0, 0, 0, 0, 0, '{}', 28),
(245, 1, 'first_name', 'text', 'First Name', 0, 1, 1, 1, 1, 1, '{}', 29),
(246, 1, 'last_name', 'text', 'Last Name', 0, 1, 1, 1, 1, 1, '{}', 30),
(247, 1, 'user_belongsto_role_relationship', 'relationship', 'roles', 0, 1, 1, 1, 1, 1, '{\"scope\":\"driver\",\"model\":\"\\\\App\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"admin_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 31),
(248, 1, 'settings', 'text', 'Settings', 0, 0, 1, 1, 1, 1, '{}', 31),
(249, 1, 'user_hasone_vehicle_relationship', 'relationship', 'vehicles', 0, 1, 1, 1, 1, 1, '{\"model\":\"\\\\App\\\\Models\\\\Vehicle\",\"table\":\"vehicles\",\"type\":\"hasOne\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"registration_number\",\"pivot_table\":\"admin_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 32);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-12-18 05:55:42', '2020-12-26 02:21:42'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(4, 'cashes', 'cashes', 'Cash', 'Cashes', NULL, 'App\\Models\\Cash', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-18 09:08:59', '2020-12-18 09:09:19'),
(5, 'categories', 'categories', 'Category', 'Categories', NULL, 'App\\Models\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(6, 'category_types', 'category-types', 'Category Type', 'Category Types', NULL, 'App\\Models\\CategoryTypes', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(7, 'conditions', 'conditions', 'Condition', 'Conditions', NULL, 'App\\Models\\Conditions', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(8, 'favorites', 'favorites', 'Favorite', 'Favorites', NULL, 'App\\Models\\Favorites', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(9, 'invoices', 'invoices', 'Invoice', 'Invoices', NULL, 'App\\Models\\Invoices', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(10, 'job_prices', 'job-prices', 'Job Price', 'Job Prices', NULL, 'App\\Models\\JobPrices', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(11, 'jobreviews', 'jobreviews', 'Jobreview', 'Jobreviews', NULL, 'App\\Models\\JobReviews', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(12, 'jobs', 'jobs', 'Job', 'Jobs', NULL, 'App\\Models\\Jobs', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(15, 'locations', 'locations', 'Location', 'Locations', NULL, 'App\\Models\\Location', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(16, 'otp_numbers', 'otp-numbers', 'Otp Number', 'Otp Numbers', NULL, 'App\\Models\\OtpNumber', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(17, 'messages', 'messages', 'Message', 'Messages', NULL, 'App\\Models\\Message', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(19, 'products', 'products', 'Product', 'Products', NULL, 'App\\Models\\Products', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(20, 'rooms', 'rooms', 'Room', 'Rooms', NULL, 'App\\Models\\Room', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(22, 'vehicle_sizes', 'vehicle-sizes', 'Vehicle Size', 'Vehicle Sizes', NULL, 'App\\Models\\VehicleSize', NULL, 'VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-18 09:20:14', '2020-12-25 14:46:14'),
(23, 'vehicles', 'vehicles', 'Vehicle', 'Vehicles', NULL, 'App\\Models\\Vehicles', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(24, 'wallets', 'wallets', 'Wallet', 'Wallets', NULL, 'App\\Models\\Wallet', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(33, 'runtime_roles', 'runtime-roles', 'Runtime Role', 'Runtime Roles', NULL, 'App\\Models\\RuntimeRoles', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(38, 'job_urls', 'job-urls', 'Job Url', 'Job Urls', NULL, 'App\\Models\\JobUrl', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(39, 'job_reviews', 'job-reviews', 'Job Review', 'Job Reviews', NULL, 'App\\Models\\JobReview', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(40, 'admin_settings', 'admin-settings', 'Admin Setting', 'Admin Settings', NULL, 'App\\Models\\AdminSetting', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-23 09:36:30', '2020-12-23 09:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(11) NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 27, 21, '2020-09-05 00:16:45', '2020-09-05 00:16:45'),
(2, 9, 1, '2020-11-25 18:33:47', '2020-11-25 18:33:47'),
(3, 9, 1, '2020-11-25 18:33:47', '2020-11-25 18:33:47');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `capture_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timestamp` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `user_id`, `amount`, `capture_id`, `job_id`, `order_id`, `document_id`, `timestamp`, `created_at`, `updated_at`) VALUES
(5, NULL, 11, '0E335874U8970894S', '30', NULL, '0DtqQPm2DfFwLMujmBUH', '1595832626296', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(11, NULL, 11, '2A7524374D4396218', '134', NULL, '1IemgjtHJo3oiBeXVd20', '1592415720860', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(12, NULL, 11, '2ED936645C2606801', '63', NULL, '1Iv3tXTaRwNAGD8XweS7', '1596303814587', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(16, NULL, 11, '0E335874U8970894S', '30', NULL, '1ijscTMPBpKRmKbMN5nc', '1595907105387', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(17, NULL, 11, '2ED936645C2606801', '63', NULL, '1kAYRL5eI026nfZRrCHL', '1596296571217', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(19, NULL, 11, '2ED936645C2606801', '63', NULL, '1uSwMAUcGiwiHE60cT0e', '1596267543888', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(21, NULL, 11, '0E335874U8970894S', '30', NULL, '25M3XjMvmKsmAIQyKOaM', '1595883541480', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(25, NULL, 11, '8U4557355R056771V', '66', NULL, '2LGZd3q4M7LtkdAIS4qd', '1594733647978', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(30, NULL, 11, '0E335874U8970894S', '30', NULL, '2nddM8KC0VDgQyhul77i', '1595959047594', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(31, NULL, 11, '2ED936645C2606801', '63', NULL, '2tr8cSg6L5HSW4QAo2j9', '1596282040681', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(34, NULL, 10, '9JK76687C02870028', '136', NULL, '2ut2KkGbRYXGAxe0YPXv', '1593275545407', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(35, NULL, 11, '8U4557355R056771V', '66', NULL, '30l24SfGLVfJglEFP7HQ', '1594596805027', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(47, NULL, 11, '2A7524374D4396218', '134', NULL, '4vrTFIjTS5uR88MkTr5D', '1592283575146', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(51, NULL, 10, '9JK76687C02870028', '136', NULL, '58mS12HnQhWVNzdmQdiH', '1592731985216', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(53, NULL, 11, '2A7524374D4396218', '134', NULL, '5Ya1NYCImaoGAIbIdRTo', '1592675089810', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(54, NULL, 11, '8U4557355R056771V', '66', NULL, '5gfci0fOZW4uv12XFMHa', '1594560304283', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(61, NULL, 11, '0E335874U8970894S', '30', NULL, '6blFVAMZVm8p1rs8RA2w', '1595868990883', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(66, NULL, 11, '95E71356UF668590D', '38', NULL, '7akBT64LAWAXXzUEkv0X', '1592371558472', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(68, NULL, 11, '2AC22342N8007443X', '78', NULL, '7htjABZ7YwemY7xOJdZu', '1592325189259', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(71, NULL, 11, '2ED936645C2606801', '63', NULL, '80ANlR9lAeF2sAk5vZ5T', '1596376317207', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(81, NULL, 11, '2ED936645C2606801', '63', NULL, '90A2oNaSG0V9edXMVfPO', '1596347331675', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(84, NULL, 11, '2AC22342N8007443X', '78', NULL, '9bWheKcNINYeFoXUQepv', '1592369806760', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(86, NULL, 11, '0E335874U8970894S', '30', NULL, '9r0q9Ob6i2tMDymZWDqd', '1595966302289', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(87, NULL, 11, '2AC22342N8007443X', '78', NULL, '9urd9xXdhtZsUDb1biee', '1592559352661', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(89, NULL, 10, '9JK76687C02870028', '136', NULL, 'AFXW8ADUS9yNy0A7UKh1', '1593524333104', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(93, NULL, 11, '95E71356UF668590D', '38', NULL, 'Ajb0kBU5bZ0m98ALWtkN', '1592510161550', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(94, NULL, 11, '8U4557355R056771V', '66', NULL, 'AlBxXNyxmPM9K7AszRjx', '1594643003768', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(95, NULL, 11, '8U4557355R056771V', '66', NULL, 'AoKGLCgSv3QhxHhO5eLS', '1594580677121', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(96, NULL, 11, '2AC22342N8007443X', '78', NULL, 'AqYKwfNsUBgfdCcmEmHC', '1592460373903', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(102, NULL, 11, '8U4557355R056771V', '66', NULL, 'BYgiRzj1jH21Coae3n4J', '1594696258594', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(110, NULL, 10, '9JK76687C02870028', '136', NULL, 'CO9Zci5G2EcytG3iLQXs', '1592995092736', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(111, NULL, 11, '2A7524374D4396218', '134', NULL, 'CQqZz6i1Vp2ACCqEGat5', '1592098550202', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(117, NULL, 11, '8U4557355R056771V', '66', NULL, 'DGMO4SRdqUY32QxoNLyo', '1594686794985', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(118, NULL, 11, '2ED936645C2606801', '63', NULL, 'DY107tO5rdl1Tb5Zscsw', '1596398082942', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(120, NULL, 10, '9JK76687C02870028', '136', NULL, 'DjiGRvmqk0xVPxHy6ZfQ', '1593556488088', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(127, NULL, 10, '9JK76687C02870028', '136', NULL, 'EWejQywwVX8E7xMDxg1X', '1593348672250', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(128, NULL, 10, '9JK76687C02870028', '136', NULL, 'Ec4utciIQEYUcx7H30hf', '1593238196177', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(129, NULL, 11, '8U4557355R056771V', '66', NULL, 'EepbIIiBbcFBXb7BjcS0', '1594665343834', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(130, NULL, 11, '0E335874U8970894S', '30', NULL, 'Ele7MqFVIF009GiWctoo', '1595898125514', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(136, NULL, 11, '8U4557355R056771V', '66', NULL, 'FbkFQDK7bAuFEQuGNg5m', '1594546517183', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(141, NULL, 11, '8U4557355R056771V', '66', NULL, 'GSMcxl9rqGL1Hopl1Yni', '1594574291888', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(145, NULL, 11, '2AC22342N8007443X', '78', NULL, 'Gul5WsMJDFdZDHG0f8aX', '1592828888528', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(146, NULL, 10, '9JK76687C02870028', '136', NULL, 'GvUuJHS3XuCNj5CeROo7', '1592580244566', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(149, NULL, 11, '2ED936645C2606801', '63', NULL, 'HcW44j1ZEgbxUyvya0QO', '1596311054241', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(152, NULL, 11, '8U4557355R056771V', '66', NULL, 'HmbcYoluuUowYXs0le36', '1594608843103', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(156, NULL, 11, '8U4557355R056771V', '66', NULL, 'IAGMBv1vxP2pQ28ZMwhi', '1594587786212', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(158, NULL, 11, '95E71356UF668590D', '38', NULL, 'IHdQb0hZpwVgJOJu4qKG', '1591987957245', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(161, NULL, 11, '2ED936645C2606801', '63', NULL, 'IoEmA1GeKgahQYjMUDTX', '1596261667447', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(164, NULL, 11, '0E335874U8970894S', '30', NULL, 'JZFVrpEOSHhEla7zsjy3', '1595825604972', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(165, NULL, 11, '2A7524374D4396218', '134', NULL, 'JaztTZD1MMypTftFNMQ2', '1591972474338', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(170, NULL, 11, '2AC22342N8007443X', '78', NULL, 'K50m7mFtZDvb5HtTGV1T', '1591986110155', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(173, NULL, 10, '9JK76687C02870028', '136', NULL, 'KRiukLlJ2RU3ml2iSx5M', '1592773935561', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(176, NULL, 11, '95E71356UF668590D', '38', NULL, 'Kss5OCeg4QNNmxpWGjwu', '1592160373271', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(178, NULL, 11, '0E335874U8970894S', '30', NULL, 'LDQ0dWA9FJjlq0FSm4gk', '1595861743448', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(179, NULL, 11, '2ED936645C2606801', '63', NULL, 'LGCMveXFz9gEoUjBbxht', '1596259574278', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(181, NULL, 11, '8U4557355R056771V', '66', NULL, 'LYAxOUuHz1Gi58jryh5B', '1594553677922', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(184, NULL, 11, '2ED936645C2606801', '63', NULL, 'LetVIhisFc4Y1pupmFx2', '1596259637109', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(185, NULL, 11, '0E335874U8970894S', '30', NULL, 'Lg9UKKsqtM9TvVecqO73', '1595824731502', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(186, NULL, 11, '8U4557355R056771V', '66', NULL, 'LhRWZ1ludHNXmM60S85B', '1594726369504', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(187, NULL, 10, '9JK76687C02870028', '136', NULL, 'Ljq5zjUP7tw4xDsl20Ed', '1593163150286', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(188, NULL, 11, '95E71356UF668590D', '38', NULL, 'M1AxzYSvnlEt1arYJO3S', '1593012472665', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(189, NULL, 11, '8U4557355R056771V', '66', NULL, 'M5SOWKfkd2R67TbnjQTN', '1594719061779', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(191, NULL, 11, '2ED936645C2606801', '63', NULL, 'MQH5D5JLzpL3GDdHiDMp', '1596260662244', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(192, NULL, 11, '8U4557355R056771V', '66', NULL, 'MQh2P7gSlE526NQwM5nI', '1594567742028', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(194, NULL, 11, '8U4557355R056771V', '66', NULL, 'MWe18ejTrD7r8gPj8uvb', '1594748145503', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(196, NULL, 11, '2A7524374D4396218', '134', NULL, 'MbOFO9Hn5JWcFg6w4mQy', '1592461532105', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(200, NULL, 11, '95E71356UF668590D', '38', NULL, 'MnRnBqHIKzH81RAAtlmb', '1592924933360', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(204, NULL, 11, '2ED936645C2606801', '63', NULL, 'NBTZjBuOadBP9AlLIInM', '1596318310170', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(205, NULL, 11, '0E335874U8970894S', '30', NULL, 'NGaXy2AduW2z9y6p24Ys', '1595847216772', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(206, NULL, 10, '9JK76687C02870028', '136', NULL, 'NIpsaF5idTobkoryZcvy', '1592628489442', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(207, NULL, 11, '95E71356UF668590D', '38', NULL, 'NM9kGHg2lnzNi8d58RWr', '1591924904705', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(209, NULL, 11, '2AC22342N8007443X', '78', NULL, 'NRkrmuWZJFV49MBHvyoc', '1593012926373', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(210, NULL, 10, '9JK76687C02870028', '136', NULL, 'NbjUVHzlZswSkiVcjG2F', '1593597721494', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(215, NULL, 10, '9JK76687C02870028', '136', NULL, 'OY6MxLTgk8s6SlojVYtF', '1593383784921', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(216, NULL, 11, '2ED936645C2606801', '63', NULL, 'Obqt53AoxBL7R5NXn4W1', '1596369088369', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(217, NULL, 11, '2AC22342N8007443X', '78', NULL, 'Oh2CnJYqG0zPTD5NaCC6', '1592612996734', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(220, NULL, 11, '95E71356UF668590D', '38', NULL, 'Ol8UDD5qXlsJ9xps2yGP', '1591781806806', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(221, NULL, 11, '0E335874U8970894S', '30', NULL, 'OlXSJ2mDZapoBnFyIYWd', '1595915503195', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(232, NULL, 11, '95E71356UF668590D', '38', NULL, 'QhahlxTxj1UITLUTSNpp', '1592732607542', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(234, NULL, 11, '8U4557355R056771V', '66', NULL, 'QxNzmQo7H9gQCuH7JxQv', '1594654217457', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(240, NULL, 11, '2AC22342N8007443X', '78', NULL, 'RUIbcR3sD6ETMlxA9iLw', '1592100318017', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(246, NULL, 10, '9JK76687C02870028', '136', NULL, 'S6lceZRKR0iMeZ50Ahlx', '1592951477991', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(248, NULL, 10, '9JK76687C02870028', '136', NULL, 'SBQdBX3bTiXXUqIQ6ONZ', '1593311904244', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(251, NULL, 11, '2ED936645C2606801', '63', NULL, 'SNH0IHv3uklNg24MeKI4', '1596354583196', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(254, NULL, 11, '0E335874U8970894S', '30', NULL, 'StrSVFTSu6H8jNVbZEhV', '1595825022668', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(256, NULL, 11, '2A7524374D4396218', '134', NULL, 'TBYnPEHoFSdPclQGfSDm', '1592159981573', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(262, NULL, 11, '2AC22342N8007443X', '78', NULL, 'Tq3GSNBq47s4ixjG1B5r', '1592969462275', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(263, NULL, 11, '0E335874U8970894S', '30', NULL, 'TqvjJYnPBV9RwNZcjbeo', '1595930026919', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(266, NULL, 11, '2AC22342N8007443X', '78', NULL, 'UBxHGuRDVAJDfpMk83yw', '1592508438416', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(268, NULL, 11, '2ED936645C2606801', '63', NULL, 'UUwm0XU3j2lA5hnxaE04', '1596390830276', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(272, NULL, 11, '8U4557355R056771V', '66', NULL, 'Usm7YhCffqTI3bQTzLwz', '1594676133116', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(276, NULL, 11, '2AC22342N8007443X', '78', NULL, 'VFI34KTDcqEl58nGPLoU', '1591923206362', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(277, NULL, 10, '9JK76687C02870028', '136', NULL, 'VMynHzrge7yV9pDKCHN3', '1593082418465', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(278, NULL, 11, '2A7524374D4396218', '134', NULL, 'VSNbGeWr7NCDVBW6Frhd', '1592509326562', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(280, NULL, 11, '95E71356UF668590D', '38', NULL, 'VVArXMgeckmzbL8I1I9c', '1592327108770', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(283, NULL, 11, '95E71356UF668590D', '38', NULL, 'VmZgWCBrxjJSyampG7DN', '1593055060894', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(290, NULL, 10, '9JK76687C02870028', '136', NULL, 'WhO2ZBRgcK5sE8qJ7tzJ', '1593122964837', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(296, NULL, 10, '9JK76687C02870028', '136', NULL, 'XT1mHceCkP7HU9JxFbUm', '1593453611389', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(297, NULL, 11, '2A7524374D4396218', '134', NULL, 'XU3GhHTnb6IjJAHFhcnx', '1591718890183', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(298, NULL, 11, '0E335874U8970894S', '30', NULL, 'XiPgn2xaV4qAQNsE0fzn', '1595944555299', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(305, NULL, 11, '2A7524374D4396218', '134', NULL, 'YbwZapWCjLpDxqtapw9o', '1592779899043', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(314, NULL, 11, '2AC22342N8007443X', '78', NULL, 'ZeCLhssomaclm5DqLMhe', '1592233908396', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(315, NULL, 11, '0E335874U8970894S', '30', NULL, 'ZhYz2XmTtwGfvbQ53pdj', '1595890838322', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(316, NULL, 11, '2ED936645C2606801', '63', NULL, 'ZoAAoQToud8gDImIwSJg', '1596259788772', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(322, NULL, 11, '95E71356UF668590D', '38', NULL, 'aUljL4mCYubroOiUeCJ7', '1591826565152', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(323, NULL, 11, '0E335874U8970894S', '30', NULL, 'abtimxTi35FeNJ1Srf80', '1595876254293', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(324, NULL, 11, '2A7524374D4396218', '134', NULL, 'aiYZMVawECW154at9F5g', '1592371137361', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(325, NULL, 11, '2AC22342N8007443X', '78', NULL, 'ajZCB6PG5qWN81FgKnF9', '1592731533741', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(326, NULL, 11, '2A7524374D4396218', '134', NULL, 'amHXmMmRF387y2rUqqgQ', '1591852262812', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(331, NULL, 10, '9JK76687C02870028', '136', NULL, 'bJQdUbiBjzJPEe664DDU', '1593039477893', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(332, NULL, 11, '8U4557355R056771V', '66', NULL, 'bMmayR5h8t9SfctkcPqe', '1594704569916', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(333, NULL, 11, '0E335874U8970894S', '30', NULL, 'bkzv2V2LRx2DhFqFYsG4', '1595824336084', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(343, NULL, 11, '95E71356UF668590D', '38', NULL, 'cj4QMKc2qi60lNc8Fjza', '1592462326932', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(348, NULL, 11, '8U4557355R056771V', '66', NULL, 'dQz6gyvTLoxpQQG9UZVn', '1594620557489', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(352, NULL, 11, '2A7524374D4396218', '134', NULL, 'dlc3i9bpv8kxBfTKpiVZ', '1591644325824', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(354, NULL, 11, '0E335874U8970894S', '30', NULL, 'eLzsiTyn5dF0sZL6GDMd', '1595839975377', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(359, NULL, 11, '2A7524374D4396218', '134', NULL, 'emVAGKvmx2F1omhoygq6', '1591804020127', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(361, NULL, 11, '2ED936645C2606801', '63', NULL, 'ewekfxMFzOSlafEwIzwG', '1596263679262', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(362, NULL, 11, '2ED936645C2606801', '63', NULL, 'eyDqcYlQCzAeSQB6d8tU', '1596289294376', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(365, NULL, 11, '8U4557355R056771V', '66', NULL, 'fcBd2YvYhIi8Giv2ZzbI', '1594768608305', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(369, NULL, 11, '95E71356UF668590D', '38', NULL, 'g3EHGYLB3S9mOt8VLF6O', '1592829218067', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(374, NULL, 10, '9JK76687C02870028', '136', NULL, 'gsvCNYqspFPD4ukKbzQP', '1593419020667', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(381, NULL, 11, '8U4557355R056771V', '66', NULL, 'hRbgp7R9BJiZCEO30HjD', '1594631874165', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(393, NULL, 10, '9JK76687C02870028', '136', NULL, 'iHfROZSowwQaN7lEWUB9', '1593200330851', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(396, NULL, 11, '95E71356UF668590D', '38', NULL, 'iZk2xzEGk5tvQod4b4d0', '1592969093778', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(397, NULL, 11, '0E335874U8970894S', '30', NULL, 'icQVloZdfSdScxTy4jfb', '1595951807512', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(400, NULL, 11, '2AC22342N8007443X', '78', NULL, 'iiWwphjMrVE3M6W5G6O3', '1592674744139', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(408, NULL, 11, '8U4557355R056771V', '66', NULL, 'kHlftyFWDvqR3h5Dp0TK', '1594755677206', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(410, NULL, 11, '95E71356UF668590D', '38', NULL, 'kLR8hL1KMN382T3Malaf', '1592877730663', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(414, NULL, 11, '95E71356UF668590D', '38', NULL, 'kl9kxAI31bWXY1WbJsxt', '1592676186509', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(425, NULL, 11, '95E71356UF668590D', '38', NULL, 'ls93eWj0tYb2ITI3c5Wj', '1591874764154', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(429, NULL, 11, '2A7524374D4396218', '134', NULL, 'mzPcSravJzVYXqQTgJrC', '1592560481389', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(438, NULL, 11, '2ED936645C2606801', '63', NULL, 'o3m8IgDYOXUgaE4GrJL2', '1596361850071', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(442, NULL, 10, '9JK76687C02870028', '136', NULL, 'oKnduITM3E0XBSkErqAT', '1592682824378', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(443, NULL, 11, '2ED936645C2606801', '63', NULL, 'oXdIxyHfGDKbLoMU8ZTm', '1596325570294', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(444, NULL, 11, '2A7524374D4396218', '134', NULL, 'oeNILvIq87DbvjFmggdz', '1592878296692', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(447, NULL, 11, '2AC22342N8007443X', '78', NULL, 'okIlV1hnn4ZgBXn7SSiX', '1592878496369', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(448, NULL, 11, '2A7524374D4396218', '134', NULL, 'onqO45LsE9jqHyRwpH3T', '1591905315027', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(449, NULL, 11, '95E71356UF668590D', '38', NULL, 'oozkEeh2zCLtw4UIQ4Py', '1592236009577', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(450, NULL, 11, '95E71356UF668590D', '38', NULL, 'orSyKgLuwStzA1zii6dr', '1592615422551', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(461, NULL, 11, '95E71356UF668590D', '38', NULL, 'pnsWFnIGc72zpC9S7Ft8', '1592047996663', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(462, NULL, 11, '2ED936645C2606801', '63', NULL, 'pp0HGV29TJa6oDUnqMwU', '1596274791757', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(463, NULL, 11, '2A7524374D4396218', '134', NULL, 'pp2fJhNAuTq4YCfDShOh', '1592614282394', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(464, NULL, 11, '2AC22342N8007443X', '78', NULL, 'q9k50mN2LaJKYctDhheH', '1593055321860', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(466, NULL, 11, '95E71356UF668590D', '38', NULL, 'qG1idcHDplLm7yH7zdq3', '1592561224218', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(470, NULL, 11, '2AC22342N8007443X', '78', NULL, 'qh7DdTcQXsP9vhkG1MAX', '1591873587048', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(471, NULL, 11, '2AC22342N8007443X', '78', NULL, 'qjaTMe8yV5Kt6dJFPuPV', '1592282294849', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(473, NULL, 11, '2A7524374D4396218', '134', NULL, 'qm6MjHx45KYx79kKb1WP', '1592038661164', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(474, NULL, 11, '0E335874U8970894S', '30', NULL, 'qzwRqYvVSnOmBWwz6Px8', '1595824497103', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(477, NULL, 11, '2AC22342N8007443X', '78', NULL, 'rXynuy2ayOOkvbtJXki8', '1592158281877', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(479, NULL, 11, '2ED936645C2606801', '63', NULL, 'rmkQTVWJNdRNk7yo6z7l', '1596340082954', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(480, NULL, 11, '0E335874U8970894S', '30', NULL, 'roBBkaEsrqGvdEB6m4Wx', '1595937307498', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(481, NULL, 11, '2AC22342N8007443X', '78', NULL, 's8mmKBa1s3Kld2Rh1Soy', '1591825463437', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(484, NULL, 10, '9JK76687C02870028', '136', NULL, 'sOHydlSYfbHuATCzxXez', '1593488733409', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(490, NULL, 11, '0E335874U8970894S', '30', NULL, 't1OrESQh85pd7wdiq0SQ', '1595854467193', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(492, NULL, 11, '0E335874U8970894S', '30', NULL, 'tNlatK98Q0MJF6Nh0eEJ', '1595828676965', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(509, NULL, 11, '2AC22342N8007443X', '78', NULL, 'ugno8Vr0kQh1GMJjC1KV', '1592414315541', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(511, NULL, 11, '95E71356UF668590D', '38', NULL, 'ukx1EHjjnGAvSfXaarDd', '1592102150030', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(515, NULL, 11, '8U4557355R056771V', '66', NULL, 'vKy3gPTl8OPfRHUK5ABh', '1594740892671', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(519, NULL, 11, '2A7524374D4396218', '134', NULL, 'w3Jw3FQzOo79EIvapgZc', '1592235878048', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(520, NULL, 11, '2ED936645C2606801', '63', NULL, 'wABQVJTU4fz86mhs7N3S', '1596332821309', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(521, NULL, 11, '2ED936645C2606801', '63', NULL, 'wFOk395dduFQUnzLOreV', '1596260120830', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(526, NULL, 10, '9JK76687C02870028', '136', NULL, 'wKg9xFLfYrUu6BsDzIBR', '1592816384507', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(531, NULL, 11, '2A7524374D4396218', '134', NULL, 'wrUel3fcQxQFvLJiimgU', '1592731578757', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(532, NULL, 11, '2A7524374D4396218', '134', NULL, 'xC62jvJ7gQIrR1JB8Mpf', '1591680275890', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(533, NULL, 11, '2AC22342N8007443X', '78', NULL, 'xGP6TL3HtwXOO5hh1cJt', '1591780633922', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(535, NULL, 10, '9JK76687C02870028', '136', NULL, 'xI6CiQ6O4XGphmvbn8Lb', '1592860738796', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(538, NULL, 11, '95E71356UF668590D', '38', NULL, 'xVWMSH7CeQ0psIYxvHFh', '1592416138922', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(542, NULL, 11, '0E335874U8970894S', '30', NULL, 'y4lzJtRda26mLGEXpTbS', '1595826685690', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(544, NULL, 11, '8U4557355R056771V', '66', NULL, 'yIpEakyi5ey7iY65JdvJ', '1594711811474', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(546, NULL, 11, '2AC22342N8007443X', '78', NULL, 'yhbwwHGHPxVIZGTdFG7a', '1592045870676', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(548, NULL, 11, '2A7524374D4396218', '134', NULL, 'yktbT3617oxIF3JynMLV', '1592327206460', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(556, NULL, NULL, NULL, NULL, NULL, '3zkkGid4rZtaTt0aMQTV', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(557, NULL, NULL, NULL, NULL, NULL, 'DI8Q5xwiKpYnX2J58M1O', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(558, NULL, NULL, NULL, NULL, NULL, 'FXerJyvpp5HmuXKjUMwo', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(559, NULL, NULL, NULL, NULL, NULL, 'bbBemZorT3o3C0RM0ILo', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(560, NULL, NULL, NULL, NULL, NULL, 'cEo042HGUu3GqduHsSKa', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(561, NULL, NULL, NULL, NULL, NULL, 'g1jXf1lkaHLVoLBBTR0x', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(562, NULL, NULL, NULL, NULL, NULL, 'jg42ZEDBTMWaV3tRMVAZ', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(563, NULL, NULL, NULL, NULL, NULL, 'lxqfT8ABjswc1kWTuypt', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(564, 9, 11, '1RL7143626754501K', '32', '1RL7143626754501K', NULL, NULL, '2020-11-08 11:29:46', '2020-11-08 11:29:46'),
(565, 9, 11, '3UL1799233335963Y', '32', '3UL1799233335963Y', NULL, NULL, '2020-11-08 11:32:14', '2020-11-08 11:32:14'),
(566, 9, 11, '-1', '32', '8WM82737RN8252011', NULL, NULL, '2020-11-08 11:34:35', '2020-11-08 11:34:35'),
(567, 9, 11, '3WH73450V8647543X', '32', '18F40743NG587482U', NULL, NULL, '2020-11-08 12:06:46', '2020-11-08 12:06:46'),
(568, 9, 11, '67R74195L7514794D', '32', '6487209537123714W', NULL, NULL, '2020-11-08 12:16:05', '2020-11-08 12:16:05'),
(569, 9, 11, '1G135603TR0449643', '32', '21U095421C429050T', NULL, NULL, '2020-11-08 12:21:40', '2020-11-08 12:21:40'),
(570, 9, 11, '3VE93794S5054010K', '33', '1DH12631MH6941741', NULL, NULL, '2020-11-13 16:04:11', '2020-11-13 16:04:11'),
(571, 9, 11, '8R541627N99980909', '34', '97563285BF1210907', NULL, NULL, '2020-11-13 16:52:20', '2020-11-13 16:52:20'),
(572, 9, 7080, '0D481122AR241015B', '1', '7W165611YA207060P', NULL, NULL, '2020-12-03 18:32:31', '2020-12-03 18:32:31'),
(573, 9, 6372, '42788812W0746711N', '1', '3ME45987MM392590E', NULL, NULL, '2020-12-03 19:43:29', '2020-12-03 19:43:29'),
(574, 9, 12036, '7RS52191DM777914D', '4', '9UF40488VB728745W', NULL, NULL, '2020-12-03 20:21:47', '2020-12-03 20:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `delivery_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_latitude` double DEFAULT NULL,
  `delivery_longitude` double DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_delivery_time` timestamp NULL DEFAULT NULL,
  `geohash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_latitude` double DEFAULT NULL,
  `job_longitude` double DEFAULT NULL,
  `job_price` double NOT NULL,
  `package_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_contact` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_instructions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `security_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `source_address_appartment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_address_appartment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `delivery_address`, `delivery_latitude`, `delivery_longitude`, `distance`, `description`, `expected_delivery_time`, `geohash`, `item_category`, `job_address`, `job_latitude`, `job_longitude`, `job_price`, `package_size`, `posted_at`, `receiver_id`, `priority`, `receiver_name`, `receiver_contact`, `receiver_instructions`, `security_code`, `status`, `document_id`, `created_at`, `updated_at`, `sender_id`, `source_address_appartment`, `delivery_address_appartment`, `deleted_at`) VALUES
(1, 'Mavdi Chowkdi, Rajkot, Gujarat 360004, India', 22.2629975, 70.7862588, 1.8, NULL, '2020-12-17 00:00:00', NULL, 'Electronic', 'nr. Shree Mahalaxmi, Suryoday Society, Nalanda Society, Rajkot, Gujarat 360001, India', 22.2862485, 70.7725263, 6372, 'Sedan', NULL, 20, 'Flexible', 'Test Receiver', '7041439950', NULL, '883651', 'Received', NULL, '2020-12-03 19:42:46', '2022-07-02 13:17:22', 11, 'Honda Show room', 'Hotel RK', NULL),
(2, 'Madhapar, Rajkot, Gujarat 360006, India', 24.1545645, 67.5552225, 3.4, NULL, NULL, NULL, 'Furniture', 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 12036, 'Sedan', NULL, 13, 'Immediate', 'Test Receiver', '7041439950', 'i want to say you jan to say', '703610', 'Pending', NULL, '2020-12-03 20:19:38', '2022-07-02 12:37:22', 11, 'Hotel Indira', 'Flat Krishna', NULL),
(3, 'Madhapar, Rajkot, Gujarat 360006, India', 22.3315435, 70.7660939, 3.4, NULL, NULL, NULL, 'Furniture', 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 12036, 'Sedan', NULL, 13, 'Immediate', 'Test Receiver', '7041439950', 'i want to say you jan to say', '904439', 'Pending', NULL, '2020-12-03 20:20:26', '2022-07-02 12:48:22', 11, 'Hotel Indira', 'Flat Krishna', NULL),
(4, 'Madhapar, Rajkot, Gujarat 360006, India', 22.3315435, 70.7660939, 3.4, NULL, NULL, NULL, 'Furniture', 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 12036, 'Sedan', NULL, 13, 'Immediate', 'Test Receiver', '7041439950', 'i want to say you jan to say', '368339', 'Pending', NULL, '2020-12-03 20:21:14', '2022-07-02 12:53:55', 11, 'Hotel Indira', 'Flat Krishna', NULL),
(5, 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 24.9180271, 67.0970916, 1, NULL, NULL, NULL, 'Miscellaneous', 'nr. Shree Mahalaxmi, Suryoday Society, Nalanda Society, Rajkot, Gujarat 360001, India', 27.2046, 77.4977, 3540, 'Sedan', NULL, 13, 'Immediate', NULL, NULL, '123546', '292338', 'Pending', NULL, '2020-12-17 12:34:01', '2022-05-16 12:47:41', 11, '12', '123', NULL),
(6, 'kolkatta', 22.5726, 88.3639, 5, NULL, '2022-06-24 19:00:00', NULL, 'Furnitutre', 'kolkatta', 27.2046, 70.7709278, 7897897, 'Truck', NULL, 10, 'Flexible', 'Test Reciever', '0312121654', '045645', '786000', 'Pending', NULL, '2022-05-17 12:35:02', '2022-05-17 10:35:02', 11, 'pta nh', NULL, NULL),
(7, 'Sharjah - United Arab Emirates', 25.3461555, 55.4210932, 2, NULL, NULL, NULL, 'Electronic', 'W39W+4V3, Block 10 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.917755, 67.0971768, 11, 'Sedan', NULL, 13, 'Immediate', 'sdfsfd', 'sdfsdf', NULL, '979607', 'Pending', NULL, '2022-05-19 07:21:58', '2022-05-19 07:21:58', 11, 'dsfsdf', 'sdf', NULL),
(8, 'Sharjah - United Arab Emirates', 25.3461555, 55.4210932, 2, NULL, NULL, NULL, 'Electronic', 'W39W+4V3, Block 10 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.917755, 67.0971768, 11, 'Sedan', NULL, 13, 'Immediate', 'sdfsfd', 'sdfsdf', NULL, '250398', 'Pending', NULL, '2022-05-19 07:22:13', '2022-05-19 07:22:13', 11, 'dsfsdf', 'sdf', NULL),
(9, 'LA-2/B, Block 21، Main Rashid Minhas Rd, opp. UBL Sports Complex، FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 24.9323526, 67.0872638, NULL, '', '2022-05-19 07:43:25', NULL, '6', '', 24.9180271, 67.0970916, 56, 'Sedan', NULL, 13, 'Immediate', NULL, NULL, '', NULL, 'Pending', NULL, '2022-05-19 07:43:25', '2022-05-19 07:43:25', 11, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobstatus`
--

CREATE TABLE `jobstatus` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobstatus`
--

INSERT INTO `jobstatus` (`id`, `status`, `job_id`, `receiver_id`, `latitude`, `longitude`, `updated_at`, `created_at`) VALUES
(1, 'Accepted', 2, 9, NULL, NULL, '2022-05-17 16:52:03', '2022-05-17 16:52:03'),
(2, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-17 16:52:10', '2022-05-17 16:52:10'),
(3, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-17 16:52:34', '2022-05-17 16:52:34'),
(4, 'Accepted', 2, 9, NULL, NULL, '2022-05-18 07:52:00', '2022-05-18 07:52:00'),
(5, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-18 07:52:06', '2022-05-18 07:52:06'),
(6, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 07:59:30', '2022-05-18 07:59:30'),
(7, 'Accepted', 2, 9, NULL, NULL, '2022-05-18 08:27:01', '2022-05-18 08:27:01'),
(8, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-18 08:53:36', '2022-05-18 08:53:36'),
(9, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 09:16:09', '2022-05-18 09:16:09'),
(10, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 09:21:29', '2022-05-18 09:21:29'),
(11, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 09:22:49', '2022-05-18 09:22:49'),
(12, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 10:41:38', '2022-05-18 10:41:38'),
(13, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 10:48:07', '2022-05-18 10:48:07'),
(14, 'Accepted', 1, 9, NULL, NULL, '2022-05-18 10:49:31', '2022-05-18 10:49:31'),
(15, 'Received', 1, 9, '24.9180271', '67.0970916', '2022-05-18 10:49:37', '2022-05-18 10:49:37'),
(16, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 10:50:04', '2022-05-18 10:50:04'),
(17, 'Accepted', 1, 9, NULL, NULL, '2022-05-18 16:22:50', '2022-05-18 16:22:50'),
(18, 'Received', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:23:21', '2022-05-18 16:23:21'),
(19, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:23:47', '2022-05-18 16:23:47'),
(20, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:24:20', '2022-05-18 16:24:20'),
(21, 'Delivered', 1, 9, '24.9180271', '67.0970916', '2022-05-18 16:34:26', '2022-05-18 16:34:26'),
(22, 'Accepted', 2, 9, NULL, NULL, '2022-05-18 16:38:06', '2022-05-18 16:38:06'),
(23, 'Received', 2, 9, '24.9180271', '67.0970916', '2022-05-18 16:38:11', '2022-05-18 16:38:11'),
(24, 'Delivered', 2, 9, '24.9180271', '67.0970916', '2022-05-18 16:38:24', '2022-05-18 16:38:24'),
(25, 'Accepted', 3, 9, NULL, NULL, '2022-05-18 16:39:39', '2022-05-18 16:39:39'),
(26, 'Received', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:39:53', '2022-05-18 16:39:53'),
(27, 'Delivered', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:39:58', '2022-05-18 16:39:58'),
(28, 'Accepted', 3, 9, NULL, NULL, '2022-05-18 16:40:42', '2022-05-18 16:40:42'),
(29, 'Received', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:40:45', '2022-05-18 16:40:45'),
(30, 'Delivered', 3, 9, '24.9180271', '67.0970916', '2022-05-18 16:40:57', '2022-05-18 16:40:57'),
(31, 'Accepted', 4, 10, NULL, NULL, '2022-05-18 17:45:53', '2022-05-18 17:45:53'),
(32, 'Accepted', 4, 10, NULL, NULL, '2022-05-18 17:48:47', '2022-05-18 17:48:47'),
(33, 'Received', 4, 10, '24.9180271', '67.0970916', '2022-05-18 17:48:58', '2022-05-18 17:48:58'),
(34, 'Delivered', 4, 10, '24.9180271', '67.0970916', '2022-05-18 17:49:24', '2022-05-18 17:49:24'),
(35, 'Accepted', 3, 10, NULL, NULL, '2022-05-18 17:51:41', '2022-05-18 17:51:41'),
(36, 'Received', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:51:44', '2022-05-18 17:51:44'),
(37, 'Delivered', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:51:56', '2022-05-18 17:51:56'),
(38, 'Accepted', 3, 10, NULL, NULL, '2022-05-18 17:52:06', '2022-05-18 17:52:06'),
(39, 'Received', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:52:11', '2022-05-18 17:52:11'),
(40, 'Delivered', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:52:17', '2022-05-18 17:52:17'),
(41, 'Delivered', 3, 10, '24.9180271', '67.0970916', '2022-05-18 17:52:55', '2022-05-18 17:52:55'),
(42, 'Accepted', 1, 10, NULL, NULL, '2022-05-18 17:54:47', '2022-05-18 17:54:47'),
(43, 'Received', 1, 10, '24.9180271', '67.0970916', '2022-05-18 17:55:46', '2022-05-18 17:55:46'),
(44, 'Delivered', 1, 10, '24.9180271', '67.0970916', '2022-05-18 17:55:57', '2022-05-18 17:55:57'),
(45, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 09:26:20', '2022-05-19 09:26:20'),
(46, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-19 09:27:04', '2022-05-19 09:27:04'),
(47, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-19 09:27:17', '2022-05-19 09:27:17'),
(48, 'Accepted', 2, 13, NULL, NULL, '2022-05-19 09:33:46', '2022-05-19 09:33:46'),
(49, 'Received', 2, 13, '24.9180271', '67.0970916', '2022-05-19 09:34:06', '2022-05-19 09:34:06'),
(50, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-19 09:35:15', '2022-05-19 09:35:15'),
(51, 'Pending', 7, NULL, NULL, NULL, '2022-05-19 12:21:58', '2022-05-19 12:21:58'),
(52, 'Pending', 8, NULL, NULL, NULL, '2022-05-19 12:22:13', '2022-05-19 12:22:13'),
(53, 'Pending', 11, NULL, NULL, NULL, '2022-05-19 15:13:32', '2022-05-19 15:13:32'),
(54, 'Accepted', 3, 13, NULL, NULL, '2022-05-19 15:44:31', '2022-05-19 15:44:31'),
(55, 'Received', 3, 13, '24.9180271', '67.0970916', '2022-05-19 15:44:38', '2022-05-19 15:44:38'),
(56, 'Delivered', 3, 13, '24.9180271', '67.0970916', '2022-05-19 15:45:05', '2022-05-19 15:45:05'),
(57, 'Accepted', 4, 13, NULL, NULL, '2022-05-19 15:45:19', '2022-05-19 15:45:19'),
(58, 'Received', 4, 13, '24.9180271', '67.0970916', '2022-05-19 15:45:23', '2022-05-19 15:45:23'),
(59, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:31', '2022-05-19 17:23:31'),
(60, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:37', '2022-05-19 17:23:37'),
(61, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:54', '2022-05-19 17:23:54'),
(62, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:23:55', '2022-05-19 17:23:55'),
(63, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:24:34', '2022-05-19 17:24:34'),
(64, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:25:29', '2022-05-19 17:25:29'),
(65, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:26:33', '2022-05-19 17:26:33'),
(66, 'Accepted', 1, 13, NULL, NULL, '2022-05-19 17:26:40', '2022-05-19 17:26:40'),
(67, NULL, 1, 13, '22.0212', '70.7709278', '2022-05-19 17:58:22', '2022-05-19 17:58:22'),
(68, NULL, 1, 13, '22.0212', '70.7709278', '2022-05-19 17:59:55', '2022-05-19 17:59:55'),
(69, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 07:53:18', '2022-05-20 07:53:18'),
(70, 'Received', 2, 13, '24.9180271', '67.0970916', '2022-05-20 07:53:37', '2022-05-20 07:53:37'),
(71, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-20 07:54:03', '2022-05-20 07:54:03'),
(72, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-20 07:54:49', '2022-05-20 07:54:49'),
(73, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 08:16:28', '2022-05-20 08:16:28'),
(74, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 08:20:22', '2022-05-20 08:20:22'),
(75, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 08:23:37', '2022-05-20 08:23:37'),
(76, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-20 08:23:52', '2022-05-20 08:23:52'),
(77, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 08:24:13', '2022-05-20 08:24:13'),
(78, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 08:26:57', '2022-05-20 08:26:57'),
(79, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 10:20:56', '2022-05-20 10:20:56'),
(80, NULL, 1, 13, '22.0212', '70.7709278', '2022-05-20 10:28:33', '2022-05-20 10:28:33'),
(81, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 10:30:19', '2022-05-20 10:30:19'),
(82, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 10:34:03', '2022-05-20 10:34:03'),
(83, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 11:41:09', '2022-05-20 11:41:09'),
(84, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:15:00', '2022-05-20 15:15:00'),
(85, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:18:18', '2022-05-20 15:18:18'),
(86, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:18:30', '2022-05-20 15:18:30'),
(87, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 15:22:10', '2022-05-20 15:22:10'),
(88, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 15:24:46', '2022-05-20 15:24:46'),
(89, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:28:52', '2022-05-20 15:28:52'),
(90, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:31:08', '2022-05-20 15:31:08'),
(91, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 15:32:36', '2022-05-20 15:32:36'),
(92, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:33:07', '2022-05-20 15:33:07'),
(93, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:40:07', '2022-05-20 15:40:07'),
(94, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 15:43:00', '2022-05-20 15:43:00'),
(95, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:43:24', '2022-05-20 15:43:24'),
(96, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 15:44:26', '2022-05-20 15:44:26'),
(97, 'Accepted', 4, 13, NULL, NULL, '2022-05-20 15:45:12', '2022-05-20 15:45:12'),
(98, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-20 15:45:30', '2022-05-20 15:45:30'),
(99, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 15:45:43', '2022-05-20 15:45:43'),
(100, 'Accepted', 3, 13, NULL, NULL, '2022-05-20 15:50:35', '2022-05-20 15:50:35'),
(101, 'Received', 3, 13, '24.9180271', '67.0970916', '2022-05-20 15:50:39', '2022-05-20 15:50:39'),
(102, 'Delivered', 3, 13, '24.9180271', '67.0970916', '2022-05-20 15:50:53', '2022-05-20 15:50:53'),
(103, 'Accepted', 1, 13, NULL, NULL, '2022-05-20 16:50:29', '2022-05-20 16:50:29'),
(104, 'Received', 1, 13, '24.9180271', '67.0970916', '2022-05-20 16:51:59', '2022-05-20 16:51:59'),
(105, 'Delivered', 1, 13, '24.9180271', '67.0970916', '2022-05-20 16:52:39', '2022-05-20 16:52:39'),
(106, 'Pending', 13, NULL, NULL, NULL, '2022-05-20 17:15:04', '2022-05-20 17:15:04'),
(107, 'Pending', 14, NULL, NULL, NULL, '2022-05-20 17:15:16', '2022-05-20 17:15:16'),
(108, 'Pending', 15, NULL, NULL, NULL, '2022-05-20 17:19:17', '2022-05-20 17:19:17'),
(109, 'Pending', 16, NULL, NULL, NULL, '2022-05-20 17:19:31', '2022-05-20 17:19:31'),
(110, 'Pending', 17, NULL, NULL, NULL, '2022-05-20 17:23:29', '2022-05-20 17:23:29'),
(111, 'Pending', 18, NULL, NULL, NULL, '2022-05-20 17:23:31', '2022-05-20 17:23:31'),
(112, 'Pending', 19, NULL, NULL, NULL, '2022-05-20 17:23:32', '2022-05-20 17:23:32'),
(113, 'Pending', 20, NULL, NULL, NULL, '2022-05-20 17:23:33', '2022-05-20 17:23:33'),
(114, 'Pending', 21, NULL, NULL, NULL, '2022-05-20 17:23:33', '2022-05-20 17:23:33'),
(115, 'Pending', 22, NULL, NULL, NULL, '2022-05-20 17:23:34', '2022-05-20 17:23:34'),
(116, 'Pending', 23, NULL, NULL, NULL, '2022-05-20 17:23:34', '2022-05-20 17:23:34'),
(117, 'Pending', 24, NULL, NULL, NULL, '2022-05-20 17:23:34', '2022-05-20 17:23:34'),
(118, 'Pending', 25, NULL, NULL, NULL, '2022-05-20 17:23:35', '2022-05-20 17:23:35'),
(119, 'Pending', 26, NULL, NULL, NULL, '2022-05-20 17:23:35', '2022-05-20 17:23:35'),
(120, 'Pending', 27, NULL, NULL, NULL, '2022-05-20 17:25:35', '2022-05-20 17:25:35'),
(121, 'Pending', 28, NULL, NULL, NULL, '2022-05-20 17:52:13', '2022-05-20 17:52:13'),
(122, 'Pending', 29, NULL, NULL, NULL, '2022-05-20 17:52:58', '2022-05-20 17:52:58'),
(123, 'Pending', 30, NULL, NULL, NULL, '2022-05-20 17:53:06', '2022-05-20 17:53:06'),
(124, 'Pending', 31, NULL, NULL, NULL, '2022-05-20 17:53:10', '2022-05-20 17:53:10'),
(125, 'Pending', 32, NULL, NULL, NULL, '2022-05-20 17:53:11', '2022-05-20 17:53:11'),
(126, 'Pending', 33, NULL, NULL, NULL, '2022-05-20 17:53:11', '2022-05-20 17:53:11'),
(127, 'Pending', 34, NULL, NULL, NULL, '2022-05-20 17:53:12', '2022-05-20 17:53:12'),
(128, 'Pending', 35, NULL, NULL, NULL, '2022-05-20 17:53:12', '2022-05-20 17:53:12'),
(129, 'Accepted', 2, 13, NULL, NULL, '2022-05-20 18:28:31', '2022-05-20 18:28:31'),
(130, 'Received', 2, 13, '24.9180271', '67.0970916', '2022-05-20 18:28:57', '2022-05-20 18:28:57'),
(131, 'Delivered', 2, 13, '24.9180271', '67.0970916', '2022-05-20 18:29:27', '2022-05-20 18:29:27'),
(132, 'Pending', 36, NULL, NULL, NULL, '2022-05-21 13:16:13', '2022-05-21 13:16:13'),
(133, 'Accepted', 4, 18, NULL, NULL, '2022-07-02 16:05:47', '2022-07-02 16:05:47'),
(134, 'Received', 4, 18, '24.9180271', '67.0970916', '2022-07-02 16:05:53', '2022-07-02 16:05:53'),
(135, 'Accepted', 4, 18, NULL, NULL, '2022-07-02 16:13:26', '2022-07-02 16:13:26'),
(136, 'Accepted', 4, 18, NULL, NULL, '2022-07-02 16:13:26', '2022-07-02 16:13:26'),
(137, 'Accepted', 2, 18, NULL, NULL, '2022-07-02 16:44:42', '2022-07-02 16:44:42'),
(138, 'Received', 2, 18, '24.9180271', '67.0970916', '2022-07-02 16:44:47', '2022-07-02 16:44:47'),
(139, 'Accepted', 3, 18, NULL, NULL, '2022-07-02 16:46:49', '2022-07-02 16:46:49'),
(140, 'Received', 3, 18, '24.9180271', '67.0970916', '2022-07-02 16:46:54', '2022-07-02 16:46:54'),
(141, 'Accepted', 4, 18, NULL, NULL, '2022-07-02 16:48:11', '2022-07-02 16:48:11'),
(142, 'Received', 4, 18, '24.9180271', '67.0970916', '2022-07-02 16:48:21', '2022-07-02 16:48:21'),
(143, 'Accepted', 1, 18, NULL, NULL, '2022-07-02 16:53:40', '2022-07-02 16:53:40'),
(144, 'Accepted', 2, 13, NULL, NULL, '2022-07-02 17:37:22', '2022-07-02 17:37:22'),
(145, 'Accepted', 3, 13, NULL, NULL, '2022-07-02 17:48:19', '2022-07-02 17:48:19'),
(146, 'Received', 3, 13, '24.9180271', '67.0970916', '2022-07-02 17:48:22', '2022-07-02 17:48:22'),
(147, 'Accepted', 4, 13, NULL, NULL, '2022-07-02 17:53:49', '2022-07-02 17:53:49'),
(148, 'Received', 4, 13, '24.9180271', '67.0970916', '2022-07-02 17:53:55', '2022-07-02 17:53:55'),
(149, 'Accepted', 1, 20, NULL, NULL, '2022-07-02 18:17:17', '2022-07-02 18:17:17'),
(150, 'Received', 1, 20, '24.9180271', '67.0970916', '2022-07-02 18:17:22', '2022-07-02 18:17:22');

-- --------------------------------------------------------

--
-- Table structure for table `job_prices`
--

CREATE TABLE `job_prices` (
  `id` int(11) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `customer_amount` int(11) NOT NULL,
  `driver_amount` int(11) NOT NULL,
  `admin_amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_reviews`
--

CREATE TABLE `job_reviews` (
  `id` bigint(20) NOT NULL,
  `job_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `client_reviewed` int(1) DEFAULT '0',
  `driver_reviewed` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_reviews`
--

INSERT INTO `job_reviews` (`id`, `job_id`, `sender_id`, `reviewer_id`, `rating`, `review`, `created_at`, `updated_at`, `client_reviewed`, `driver_reviewed`) VALUES
(1, 1, 10, 9, 0, NULL, '2022-05-18 05:50:08', '2022-05-18 05:50:08', 0, 1),
(2, 2, 10, 9, 0, NULL, '2022-05-18 11:40:13', '2022-05-18 11:40:13', 0, 1),
(3, 4, 7, 10, 0, NULL, '2022-05-18 12:49:42', '2022-05-18 12:49:42', 0, 1),
(4, 3, 8, 10, 0, NULL, '2022-05-18 12:51:59', '2022-05-18 12:51:59', 0, 1),
(5, 1, 7, 13, 1, NULL, '2022-05-19 13:03:54', '2022-05-19 13:03:54', 0, 0),
(6, 1, 7, 13, 1, NULL, '2022-05-19 13:06:17', '2022-05-19 13:06:17', 0, 0),
(7, 1, 7, 13, 1, NULL, '2022-05-19 13:09:49', '2022-05-19 13:09:49', 0, 0),
(8, 1, 7, 13, 1, NULL, '2022-05-20 05:35:55', '2022-05-20 05:35:55', 0, 0),
(9, 1, 7, 13, 5, 'great application', '2022-05-20 05:36:23', '2022-05-20 05:36:23', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_urls`
--

CREATE TABLE `job_urls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `owner_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `firebase_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_urls`
--

INSERT INTO `job_urls` (`id`, `owner_id`, `job_id`, `firebase_url`, `created_at`, `updated_at`) VALUES
(1, 9, 1, 'https://deliveryist.com/admin-laravel-6x/storage/app/images/products/03eea3d1c89302b5c5d5b52399d244ef.png', '2020-12-03 18:31:46', '2020-12-03 18:31:46'),
(2, 9, 1, 'https://deliveryist.com/admin-laravel-6x/storage/app/images/products/210f6a994043e19708df07090567f01e.png', '2020-12-03 18:31:46', '2020-12-03 18:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geohash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `distance` double DEFAULT '25'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `device_token`, `geohash`, `latitude`, `longitude`, `role_id`, `role`, `user_id`, `document_id`, `created_at`, `updated_at`, `distance`) VALUES
(1, 'f861911e5fb7317383978d', 'tkrtktu8r8u', 24.9728707, 67.0643315, NULL, 'Driver', 2, '3zkkGid4rZtaTt0aMQTV', '2020-08-26 18:37:41', '2020-09-15 16:51:29', 25),
(2, 'e2833d876557a98bb2ce09', 'tefqdryb6ez', 22.2789632, 70.7723264, NULL, 'Consumer', 32, 'DI8Q5xwiKpYnX2J58M1O', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(3, '4d5914a5c91b62944e2547', 'tg0pn1k0e5h', 18.1124372, 79.0192997, NULL, 'Driver', 22, 'FXerJyvpp5HmuXKjUMwo', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(4, '37d241cc9914d3cc34c52d', '9vg512zu3vw', 32.8759731, -96.9655921, NULL, 'Consumer', 24, 'bbBemZorT3o3C0RM0ILo', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(5, '391c8fa525e75d2475b02f', 'tefqdryb6ez', 22.2789632, 70.7723264, NULL, 'Consumer', 20, 'cEo042HGUu3GqduHsSKa', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(6, '4d5914a5c91b62944e2547', 'tg0pn1k0e5h', 18.1124372, 79.0192997, NULL, 'Consumer', 22, 'g1jXf1lkaHLVoLBBTR0x', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(7, '8e77089e8ab0e266c7d1a3', 'tefqdq5571v', 22.2698551, 70.7671948, NULL, 'Consumer', 30, 'jg42ZEDBTMWaV3tRMVAZ', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(8, 'e601e712dbcea16efaa8a1', 'wh0r0et9dk0', 23.7499741, 90.381185, NULL, 'Consumer', 19, 'lxqfT8ABjswc1kWTuypt', '2020-08-26 18:37:41', '2020-08-26 18:37:41', 25),
(9, NULL, NULL, 22.2789632, 70.7854336, NULL, NULL, 41, NULL, '2020-09-16 09:23:55', '2020-09-16 09:23:55', 25);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-12-18 05:55:42', '2020-12-18 05:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-12-18 05:55:42', '2020-12-24 21:55:10', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 4, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 3, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2020-12-18 05:55:42', '2020-12-24 21:56:24', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 7, '2020-12-18 05:55:42', '2020-12-24 21:56:24', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-12-18 05:55:42', '2020-12-24 21:55:08', 'voyager.hooks', NULL),
(12, 1, 'Cashes', '', '_self', NULL, NULL, NULL, 8, '2020-12-18 09:08:59', '2020-12-24 21:56:18', 'voyager.cashes.index', NULL),
(13, 1, 'Categories', '', '_self', NULL, NULL, NULL, 9, '2020-12-18 09:09:58', '2020-12-24 21:56:18', 'voyager.categories.index', NULL),
(14, 1, 'Category Types', '', '_self', NULL, NULL, NULL, 10, '2020-12-18 09:10:15', '2020-12-24 21:56:18', 'voyager.category-types.index', NULL),
(15, 1, 'Conditions', '', '_self', NULL, NULL, NULL, 11, '2020-12-18 09:10:55', '2020-12-24 21:56:18', 'voyager.conditions.index', NULL),
(16, 1, 'Favorites', '', '_self', NULL, NULL, NULL, 12, '2020-12-18 09:11:53', '2020-12-24 21:56:18', 'voyager.favorites.index', NULL),
(17, 1, 'Invoices', '', '_self', NULL, NULL, NULL, 13, '2020-12-18 09:12:10', '2020-12-24 21:56:18', 'voyager.invoices.index', NULL),
(18, 1, 'Job Prices', '', '_self', NULL, NULL, NULL, 14, '2020-12-18 09:12:28', '2020-12-24 21:56:13', 'voyager.job-prices.index', NULL),
(20, 1, 'Jobs', '', '_self', NULL, NULL, NULL, 15, '2020-12-18 09:13:02', '2020-12-24 21:56:13', 'voyager.jobs.index', NULL),
(23, 1, 'Locations', '', '_self', NULL, NULL, NULL, 16, '2020-12-18 09:15:31', '2020-12-24 21:56:13', 'voyager.locations.index', NULL),
(24, 1, 'Otp Numbers', '', '_self', NULL, NULL, NULL, 17, '2020-12-18 09:16:06', '2020-12-24 21:56:13', 'voyager.otp-numbers.index', NULL),
(25, 1, 'Messages', '', '_self', NULL, NULL, NULL, 18, '2020-12-18 09:16:25', '2020-12-24 21:56:13', 'voyager.messages.index', NULL),
(27, 1, 'Products', '', '_self', NULL, NULL, NULL, 19, '2020-12-18 09:17:11', '2020-12-24 21:56:13', 'voyager.products.index', NULL),
(28, 1, 'Rooms', '', '_self', NULL, NULL, NULL, 20, '2020-12-18 09:17:34', '2020-12-24 21:56:13', 'voyager.rooms.index', NULL),
(30, 1, 'Vehicle Sizes', '', '_self', NULL, NULL, NULL, 21, '2020-12-18 09:20:14', '2020-12-24 21:56:13', 'voyager.vehicle-sizes.index', NULL),
(31, 1, 'Vehicles', '', '_self', NULL, NULL, NULL, 22, '2020-12-18 09:20:55', '2020-12-24 21:56:13', 'voyager.vehicles.index', NULL),
(32, 1, 'Wallets', '', '_self', NULL, NULL, NULL, 23, '2020-12-18 09:22:09', '2020-12-24 21:56:13', 'voyager.wallets.index', NULL),
(37, 1, 'Runtime Roles', '', '_self', NULL, NULL, NULL, 24, '2020-12-23 08:16:17', '2020-12-24 21:56:13', 'voyager.runtime-roles.index', NULL),
(42, 1, 'Job Urls', '', '_self', NULL, NULL, NULL, 25, '2020-12-23 08:42:31', '2020-12-24 21:56:13', 'voyager.job-urls.index', NULL),
(43, 1, 'Job Reviews', '', '_self', NULL, NULL, NULL, 26, '2020-12-23 08:52:25', '2020-12-24 21:56:13', 'voyager.job-reviews.index', NULL),
(44, 1, 'Admin Settings', '', '_self', NULL, NULL, NULL, 27, '2020-12-23 09:36:30', '2020-12-24 21:56:13', 'voyager.admin-settings.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `message`, `room_id`, `created_at`, `updated_at`) VALUES
(1, 2, 'bypass', 4, '2020-09-26 09:07:50', '2020-09-26 09:07:50'),
(2, 2, 'pure', 4, '2020-09-26 09:08:36', '2020-09-26 09:08:36'),
(3, 2, 'mainly', 4, '2020-09-26 09:08:43', '2020-09-26 09:08:43'),
(4, 9, 'ping', 1, '2020-11-28 12:14:34', '2020-11-28 12:14:34'),
(5, 9, 'hello ?', 1, '2020-11-28 14:08:04', '2020-11-28 14:08:04'),
(6, 9, 'What is Wrong?', 1, '2020-11-28 14:15:01', '2020-11-28 14:15:01'),
(7, 9, 'ytr', 1, '2020-11-28 14:26:40', '2020-11-28 14:26:40'),
(8, 9, 'scroll check', 1, '2020-11-28 14:28:21', '2020-11-28 14:28:21'),
(9, 9, 'hi', 1, '2020-12-03 22:36:39', '2020-12-03 22:36:39'),
(10, 9, 'hi', 1, '2020-12-17 11:47:05', '2020-12-17 11:47:05'),
(11, 13, 'Hi, I am interested', 2, '2022-05-19 07:36:29', '2022-05-19 07:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(9, '2016_06_01_000004_create_oauth_clients_table', 1),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(11, '2016_10_21_190000_create_roles_table', 1),
(12, '2016_10_21_190000_create_settings_table', 1),
(13, '2016_11_30_135954_create_permission_table', 1),
(14, '2016_11_30_141208_create_permission_role_table', 1),
(15, '2016_12_26_201236_data_types__add__server_side', 1),
(16, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(17, '2017_01_14_005015_create_translations_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(21, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(22, '2017_08_05_000000_add_group_to_settings_table', 1),
(23, '2017_11_26_013050_add_user_role_relationship', 1),
(24, '2017_11_26_015000_create_user_roles_table', 1),
(25, '2018_03_11_000000_add_user_settings', 1),
(26, '2018_03_14_000000_add_details_to_data_types_table', 1),
(27, '2018_03_16_000000_make_settings_value_nullable', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\SuperAdmin', 1),
(2, 'App\\SuperAdmin', 1),
(7, 'App\\User', 19),
(7, 'App\\User', 20),
(7, 'App\\User', 21),
(7, 'App\\User', 22),
(7, 'App\\User', 23),
(7, 'App\\User', 24),
(7, 'App\\User', 25),
(7, 'App\\User', 26),
(7, 'App\\User', 27),
(7, 'App\\User', 28),
(7, 'App\\User', 29),
(7, 'App\\User', 30),
(7, 'App\\User', 31),
(7, 'App\\User', 32);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('063ea08234e5b56fc01957d7b3677d577999a2839a7b423e8f0005dcabf014953417ef0c430f114b', 19, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 13:02:29', '2022-07-02 13:02:29', '2023-07-02 18:02:29'),
('0889274d4592bb3799cafef2b0392f680393110908f2d3eb37820f2e52738a87a26a2c171e1fa660', 15, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 10:00:52', '2022-07-02 10:00:52', '2023-07-02 15:00:52'),
('0e61bc9ecef2be5f0dec0e67c339f14bffcd66294bc7026309ccf8c2a9b112a6f99805d05dfa60e8', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:09:59', '2022-05-16 07:09:59', '2023-05-16 12:09:59'),
('13e24517b96eefb725076bd08e9ec8671ad79ffc35232096b3fd5b0c4d5afe9676c83f74b3b238f5', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:16:45', '2022-05-16 07:16:45', '2023-05-16 12:16:45'),
('178583bfad121f957a52ae6bd1dcd482778fedcebdd15b7249ac8bb5db2430f0f38b3f243019f1d8', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:13:11', '2022-05-16 07:13:11', '2023-05-16 12:13:11'),
('1d1223a9b728219643367ab9a88eedea9663ccf736ce34370a0dbe490da8fcbd80f7b946516cd3c5', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 05:52:14', '2022-05-16 05:52:14', '2023-05-16 10:52:14'),
('24fabec83529d3fb6419a7ec8f4b0647b222a23f61b24076d782dbce5167e54fc4d3edd0cb2651fd', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-09 11:51:41', '2022-05-09 11:51:41', '2023-05-09 16:51:41'),
('28688a76c4b1bd7fd9eddbd0d3d25bbd410838a4f58af0141cd0a2be117c3cea7f38565a1e67ae5b', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-18 02:45:08', '2022-05-18 02:45:08', '2023-05-18 07:45:08'),
('30964d0592247436d501995b9b9b4d07f55e61adf9c2c2e2574f6db6d59baf9e104e2335e25c3533', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:22:19', '2022-05-16 07:22:19', '2023-05-16 12:22:19'),
('309a358a0fd190905e63c8ed7a4a24ddc40b81a80140efdcaab8b2a05397461872536aa29f2377ea', 20, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 13:16:41', '2022-07-02 13:16:41', '2023-07-02 18:16:41'),
('316eef7155a05d9e54d37943d4cf602b8196ada53000b09f7338ef61021bb274e402837b5c80e03a', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 08:50:20', '2022-05-19 08:50:20', '2023-05-19 13:50:20'),
('3287ab4b778ff3f64e3a266592080f5d633e46994e68bde770ed9ae2fc2e88c90faa11c3116b5cdb', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 06:22:40', '2022-05-16 06:22:40', '2023-05-16 11:22:40'),
('32f5a73b05d03bef63e2edc9d423f6ecee59c321c32b685f0a5bb03c6d065f71696ad526286b6578', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:10:03', '2022-05-16 07:10:03', '2023-05-16 12:10:03'),
('3747a10d4823d77c2f68fb2498808454dc0540409e8bd8c66ef612960be5a58cbae51ef124808504', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:10:16', '2022-05-16 07:10:16', '2023-05-16 12:10:16'),
('3a145925dc80c2a9bcb670891154ab4faa64b3887dc842c22fc09a026d5cec7f884cfb0e26b219cb', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-13 10:50:24', '2022-05-13 10:50:24', '2023-05-13 15:50:24'),
('3ce3821529dc92f0cb20ce02bfb812e80ad48cbfaea3d509eca37c2ee3efb59688174c1f374a45cb', 11, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-17 09:49:25', '2022-05-17 09:49:25', '2023-05-17 14:49:25'),
('3dce689a54c10e49bb5ff11a6412075de863d2e19412248e9477af45c38ea7e8cf789deb8bcfc89b', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:13:44', '2022-05-16 07:13:44', '2023-05-16 12:13:44'),
('3fbaa84d917aeee82112b17af58ee22ee481382f23f2989d3536753fa2ec1de63cc0053567011532', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 06:20:41', '2022-05-16 06:20:41', '2023-05-16 11:20:41'),
('4327f9b096e505b10f1dc37d6a43fc2a3839d61fe3973bc9245fba24ae1c0e81f9e004d4b11304b4', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:24:00', '2022-05-16 07:24:00', '2023-05-16 12:24:00'),
('45fb8e642e9d81cad2a0b32ead1cbe8aa1ced4d6b0dd162f733115d7fd1518639482fd0bab69471f', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-21 06:21:14', '2022-05-21 06:21:14', '2023-05-21 11:21:14'),
('487477cdc915edfbd646c0f83b51aba0c995eb88cf59828d7c9f653342a785b7a5985f5a5cbdc129', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:15:13', '2022-05-16 07:15:13', '2023-05-16 12:15:13'),
('48d0b5fe5432f53f998864fb928adf992f59742524dbab13a97c1cf007ce543aa55091692dec0ba8', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 10:05:51', '2022-05-19 10:05:51', '2023-05-19 15:05:51'),
('490d0159089840802a4bfb9468fee772d64412729f595bd15f02a10ff2e6828db7e8fe3cc05c6699', 16, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 10:00:57', '2022-07-02 10:00:57', '2023-07-02 15:00:57'),
('49197b6e1f9fb918224f6eec4d5ea54433fd71be1416714e3fd135d3544296fc2f0230d2a03ccf3a', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 12:37:08', '2022-07-02 12:37:08', '2023-07-02 17:37:08'),
('4995dc82085f0fcb2e5a7f90ec7d445ea3d24d92f4c968f3748e4fe61c72a9afafb699e189f7727b', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 03:56:03', '2022-05-19 03:56:03', '2023-05-19 08:56:03'),
('4b0cf2e56bab2e74b65cc70a603c1a2f5923b1f022ff9f14708caa7812a4f6877fd12deb82fffc1a', 4, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-17 11:43:23', '2022-05-17 11:43:23', '2023-05-17 16:43:23'),
('4d73e8bf1d90ef87f80fff8684d585a68b065ca18bba4d2705bf64fa65e97a3e7a984cf7ee078cba', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-17 11:44:07', '2022-05-17 11:44:07', '2023-05-17 16:44:07'),
('4f9919630f0969a895484e6febc62449f6f32148e1a103d9ac19b3a271ea0ea0c60f85af3f6ff763', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 13:21:19', '2022-05-16 13:21:19', '2023-05-16 18:21:19'),
('503afaaeb417f09b5e88c732b55aa165d6ad577ef23311a6d2af9855994f643d55130446e95e548e', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 06:29:33', '2022-05-16 06:29:33', '2023-05-16 11:29:33'),
('5791999aace21eb95682aa81e584d212197fa31ae57065810e8fc3e8ad7c86594f8dda88eb22aa3b', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 08:11:34', '2022-05-19 08:11:34', '2023-05-19 13:11:34'),
('5d5a4f9c44132dc1b5e9727f3d5d0a6e1a996fdd1b7c073d5aa3d6c32cfd58f661f331c5811da0f7', 12, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-18 08:28:22', '2022-05-18 08:28:22', '2023-05-18 13:28:22'),
('6025b796acde562392829586f042a9a91e13d3ead5fd5cbe021eacaed9d8df89382874cd5cebfe75', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-20 10:44:22', '2022-05-20 10:44:22', '2023-05-20 15:44:22'),
('6759056347d5d3ae1137a0d709ce5c1478ff6b4fad555051ca155a991cd1e40c8568e88c8e97489e', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:20:49', '2022-05-16 07:20:49', '2023-05-16 12:20:49'),
('768c05a29cff0ba26468e3a07da582dadba85fba7e64b2e00ee63f25d727cfd8d49a389cff49c23a', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 06:15:19', '2022-05-19 06:15:19', '2023-05-19 11:15:19'),
('7934bf6704aae2ac1718f2f166b945763b2f7e6e8218d2ab2c650f2f6cadc0b1ad55a6e1290c87f2', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:11:40', '2022-05-10 03:11:40', '2023-05-10 08:11:40'),
('7e53c9a58d686c6211829e3f6c9846dc1844b7a6782e8309e9b7397fdd143c996186afeaa36bad0d', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-20 08:40:36', '2022-05-20 08:40:36', '2023-05-20 13:40:36'),
('8288b28f9e19c31e335cc826921a4f830818ac4095a8804b908762de738a9c53f88cefaa42c4a0c2', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 05:24:19', '2022-05-16 05:24:19', '2023-05-16 10:24:19'),
('84cb1bf6beae624db4195b4952f11025cfee1fad8034bfa91e4618b177c93e5a5d989bff55b60514', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-14 12:40:41', '2022-05-14 12:40:41', '2023-05-14 17:40:41'),
('85673d8ea7339481f3ed74f3564edd9c9951b83dff75daffb08d1fe6a2ed79c6c1d3b855ed2da4a5', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-20 08:40:14', '2022-05-20 08:40:14', '2023-05-20 13:40:14'),
('873259295960ce8bf63e255864a6c8e8f1f5ae6ff3c0f490e0098b266252f1ed4408e5304a949502', 18, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 11:04:07', '2022-07-02 11:04:07', '2023-07-02 16:04:07'),
('8c02377bf9b75115f118807ded2bc226c84568dc59df5bcf25c041da617a4564d70e7ff7b05e7f85', 7, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:12:18', '2022-05-10 03:12:18', '2023-05-10 08:12:18'),
('8cb8b196e0622d1922d8251b9f80a0e646dd6386bbdc89a4284eaca11c4b8b47ffd08c7c13619d77', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:22:28', '2022-05-10 03:22:28', '2023-05-10 08:22:28'),
('93f521f6ca00fbbe4ff702a0a22fa09dce92d3b9e71e25593b4eff29e6c6354f79f7862c2714351d', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-09 11:47:19', '2022-05-09 11:47:19', '2023-05-09 16:47:19'),
('94edcd093778a63c4ad5ecf6aafee73470eff903c173491fc80308c22ba72c1484d29d07fadbbcc4', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:20:27', '2022-05-16 07:20:27', '2023-05-16 12:20:27'),
('a302b152cd06b344320a6fd277da96181c89efc9367209b81c0d0676d0af5c8588ba9ae1ff5e6123', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-10 03:22:37', '2022-05-10 03:22:37', '2023-05-10 08:22:37'),
('a5c4f5e2a4b8f7e088097d76b61fa7cc797849406988b8753ee95493020f1f0436e9182a7aa6b9d5', 17, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 10:00:59', '2022-07-02 10:00:59', '2023-07-02 15:00:59'),
('a879c6fecf7c16811c53e95c1374119d0c72665b4847068bf70eecec64ee92767962f9b57420b5b6', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 03:57:33', '2022-05-19 03:57:33', '2023-05-19 08:57:33'),
('af4c8aee9e282f9917be9caca94774c36aba2aba1c97c4ef2f74d3995140b49557f0e5ec5699a1a8', 14, 1, 'Laravel Password Grant Client', '[]', 0, '2022-07-02 09:57:18', '2022-07-02 09:57:18', '2023-07-02 14:57:18'),
('b0ea4304eab007294af5d35f29aa452c07303f1c13fdfa4477d41fab73ef95f5b4126e2ac6e3bf2e', 6, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-09 13:15:27', '2022-05-09 13:15:27', '2023-05-09 18:15:27'),
('b294e920f48a04956ac050d6f506c6647005f9c9036cb9c904f6cdcdb79e189a215607f43faef3b4', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 13:24:32', '2022-05-16 13:24:32', '2023-05-16 18:24:32'),
('bc1e9b0e66b6b31de5d317d2eb67d07690e12bdb9cd8431356b11e34ea0199b46394e96e11681b53', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 05:53:20', '2022-05-19 05:53:20', '2023-05-19 10:53:20'),
('c0b19ca7e97b7e04055d85a2e754bcb34ad7a7e1c6ed87e9bbf871781949ca9c210e8502036b5155', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:20:12', '2022-05-16 07:20:12', '2023-05-16 12:20:12'),
('c114520405d0c2a0697928221a3495f502f48fa7950d55dae5ae449ca5c380bb715fd885e7e90083', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-18 11:22:07', '2022-05-18 11:22:07', '2023-05-18 16:22:07'),
('c2b209212d40d26b177a9a00c402ab535b6a5eea4f012fa1d55855af64c726df572a12fe5fdc5632', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 06:41:17', '2022-05-16 06:41:17', '2023-05-16 11:41:17'),
('c33036b30694f1fb6b85700a5dfbb3449962eff1fd427cba0a282a58256e7f8b948627877de35190', 8, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-13 10:33:34', '2022-05-13 10:33:34', '2023-05-13 15:33:34'),
('c688573691d7bdbcf56e071ae9a89d8ad29b057ab6dc1980c7f508560b5881332d9abf3b19f7d651', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 13:31:03', '2022-05-16 13:31:03', '2023-05-16 18:31:03'),
('ce2ebb3ffbde2b08ccbae5c77b4f995102e4fc9694edd725da7e8572aeee1c0f576bb33bde5b759d', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:11:51', '2022-05-16 07:11:51', '2023-05-16 12:11:51'),
('d86ce3d74b7695f87285c027f24fb5d3b339a1fc4a17f6a01b812a285ea0fb90dca949c8f101d495', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-17 11:59:14', '2022-05-17 11:59:14', '2023-05-17 16:59:14'),
('da0090e5c18778b6db13671e7fea5555661235646990d6d9b650c0a8c677147e12d0bce83c2a4191', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:15:21', '2022-05-16 07:15:21', '2023-05-16 12:15:21'),
('e3c3ba02dcb9fcb8eae9d39150cef6ff5c0381b503963de90fc5888076d31b2c9d2aebd562d11fa5', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 07:21:19', '2022-05-16 07:21:19', '2023-05-16 12:21:19'),
('e511679d4669a74382175a0c7deed6712df77a8df86ca484d955f8db39eac99648a62c49336d07fa', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-19 07:09:46', '2022-05-19 07:09:46', '2023-05-19 12:09:46'),
('e6e1a12d35d35416bf0ddf889ad6087e125afc15802cf5c50270f6c98163703f94782c361a23564e', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-20 05:14:15', '2022-05-20 05:14:15', '2023-05-20 10:14:15'),
('e717fa835ef68f84c4efb7946bf232dc37430f281b2a11f5353f136493f80b5bad7983d058a7b493', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-18 12:45:47', '2022-05-18 12:45:47', '2023-05-18 17:45:47'),
('e76dd9c2728a16ea9419220edc065da1397554d8a3408446329dbe267ae59c5a310818ca89badf09', 10, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 06:14:03', '2022-05-16 06:14:03', '2023-05-16 11:14:03'),
('eeca106d40a135b1d4f8198415a60eb486c18824012f2adcef7ef78ce78fbc7a6fa99ca034f1e7ec', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 06:07:11', '2022-05-16 06:07:11', '2023-05-16 11:07:11'),
('efa5e6ee0637119fb26c0529e3ba24b93ca4ac06bca8fc4dc8907efa4c1061c54682f6005c02f4b8', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-16 05:48:00', '2022-05-16 05:48:00', '2023-05-16 10:48:00'),
('f99ad8e13b577873a8c4e75d3f7e3726c63919ff20778f1aad04a5af2ae0a1738117d95c050613c7', 9, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-17 11:41:36', '2022-05-17 11:41:36', '2023-05-17 16:41:36'),
('f9c9015ba62586739f17629c41e74a50c5f2a672c80a4de67ab56a9c89553f77f677dd4b22a9cdee', 5, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-15 12:11:50', '2022-05-15 12:11:50', '2023-05-15 17:11:50'),
('fb50243fcd112543a748cb682251589c6183f8dd828c6d5a63aef0c5a82c55471da04e6f9918727f', 13, 1, 'Laravel Password Grant Client', '[]', 0, '2022-05-20 02:53:02', '2022-05-20 02:53:02', '2023-05-20 07:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '0SCNZrVA6MxOav6GCEQIfphpzTaoVz0BOJp96GYT', NULL, 'http://localhost', 1, 0, 0, '2020-12-18 05:42:14', '2020-12-18 05:42:14'),
(2, NULL, 'Laravel Password Grant Client', 'xSoG1YOrG03D7VFgvD1lgmTAOGli10V1JP5LUx7P', 'users', 'http://localhost', 0, 1, 0, '2020-12-18 05:42:14', '2020-12-18 05:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-18 05:42:14', '2020-12-18 05:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otp_numbers`
--

CREATE TABLE `otp_numbers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otp_numbers`
--

INSERT INTO `otp_numbers` (`id`, `phone_number`, `otp_code`, `email`, `is_verified`, `user_id`, `expires`, `created_at`, `updated_at`) VALUES
(28, '++16287779619', NULL, NULL, 1, 21, NULL, '2020-11-05 16:04:33', '2020-11-05 16:08:53'),
(29, '++918866765733', '7803', NULL, 0, NULL, '2020-11-11 10:48:48', '2020-11-10 18:12:34', '2020-11-11 10:43:48'),
(30, '+918866765733', NULL, NULL, 1, NULL, NULL, '2020-11-10 18:52:30', '2020-11-11 08:32:05'),
(31, '+91(886) 676-5733', '2265', NULL, 0, NULL, '2020-11-11 13:58:28', '2020-11-11 12:51:52', '2020-11-11 13:53:28'),
(32, '+923452433770', '6732', NULL, 0, NULL, '2022-05-07 04:41:15', '2022-05-07 04:36:15', '2022-05-07 04:36:15'),
(33, '+923310257603', '7530', NULL, 0, NULL, '2022-05-07 04:45:31', '2022-05-07 04:38:34', '2022-05-07 04:40:31'),
(34, '+923432322010', NULL, NULL, NULL, 6, NULL, '2022-05-09 11:47:19', '2022-05-09 13:15:27'),
(35, '+923352757725', NULL, NULL, NULL, 7, NULL, '2022-05-10 03:12:17', '2022-05-10 03:12:17'),
(36, '+923423698745', NULL, NULL, 1, NULL, NULL, '2022-05-13 08:24:56', '2022-05-13 08:26:23'),
(37, '+923215584265', '9195', NULL, 0, NULL, '2022-05-13 09:12:33', '2022-05-13 09:07:33', '2022-05-13 09:07:33'),
(38, '+923452121457', '3316', NULL, 0, NULL, '2022-05-13 09:13:38', '2022-05-13 09:08:38', '2022-05-13 09:08:38'),
(39, '+923456565232', '3018', NULL, 0, NULL, '2022-05-13 09:14:13', '2022-05-13 09:09:13', '2022-05-13 09:09:13'),
(40, '+923456123457', '6350', NULL, 0, NULL, '2022-05-13 09:14:47', '2022-05-13 09:09:47', '2022-05-13 09:09:47'),
(41, '+923456321457', NULL, NULL, 1, NULL, NULL, '2022-05-13 09:10:35', '2022-05-13 09:10:42'),
(42, '+923421094825', NULL, NULL, 1, NULL, NULL, '2022-05-13 09:50:02', '2022-05-13 09:50:08'),
(43, '+923412548000', NULL, NULL, 1, NULL, NULL, '2022-05-13 09:53:28', '2022-05-13 09:53:50'),
(44, '+923211025456', NULL, NULL, 1, NULL, NULL, '2022-05-13 09:57:25', '2022-05-13 09:57:32'),
(45, '+923308529824', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:05:39', '2022-05-13 10:05:51'),
(46, '+923456669119', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:12:13', '2022-05-13 10:12:25'),
(47, '+923424467532', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:15:39', '2022-05-13 10:15:47'),
(48, '+923408766719', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:20:07', '2022-05-13 10:20:18'),
(49, '+923421054544', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:25:01', '2022-05-13 10:25:07'),
(50, '+923457896314', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:30:55', '2022-05-13 10:31:01'),
(51, '+923456789102', NULL, NULL, 1, NULL, NULL, '2022-05-13 10:32:54', '2022-05-13 10:32:59'),
(52, '+923452669871', NULL, NULL, 1, 8, NULL, '2022-05-13 10:33:34', '2022-05-13 10:49:42'),
(54, '+923131294717', NULL, NULL, 1, 10, NULL, '2022-05-16 06:14:03', '2022-05-19 03:56:44'),
(55, '+923102675961', NULL, NULL, 1, NULL, NULL, '2022-05-17 09:47:54', '2022-05-17 09:48:01'),
(56, '3102675961', NULL, NULL, NULL, 11, NULL, '2022-05-17 09:49:25', '2022-05-17 09:49:25'),
(57, '3102471803', NULL, NULL, NULL, 12, NULL, '2022-05-18 08:28:22', '2022-05-18 08:28:22'),
(58, '+923104897845', '2872', NULL, 0, 13, '2022-05-19 06:19:26', '2022-05-19 03:57:33', '2022-05-19 06:14:26'),
(59, 'sdfsdf', '6342', NULL, 0, NULL, '2022-05-19 07:26:58', '2022-05-19 07:21:58', '2022-05-19 07:21:58'),
(60, '0321459721', '3678', NULL, 0, NULL, '2022-05-19 10:18:32', '2022-05-19 10:13:32', '2022-05-19 10:13:32'),
(73, '+923131094717', NULL, NULL, 1, 20, NULL, '2022-07-02 13:15:47', '2022-07-02 13:16:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(2, 'browse_bread', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(3, 'browse_database', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(4, 'browse_media', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(5, 'browse_compass', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(6, 'browse_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(7, 'read_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(8, 'edit_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(9, 'add_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(10, 'delete_menus', 'menus', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(11, 'browse_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(12, 'read_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(13, 'edit_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(14, 'add_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(15, 'delete_roles', 'roles', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(16, 'browse_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(17, 'read_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(18, 'edit_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(19, 'add_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(20, 'delete_users', 'users', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(21, 'browse_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(22, 'read_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(23, 'edit_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(24, 'add_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(25, 'delete_settings', 'settings', '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(26, 'browse_hooks', NULL, '2020-12-18 05:55:42', '2020-12-18 05:55:42'),
(27, 'browse_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(28, 'read_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(29, 'edit_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(30, 'add_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(31, 'delete_cashes', 'cashes', '2020-12-18 09:08:59', '2020-12-18 09:08:59'),
(32, 'browse_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(33, 'read_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(34, 'edit_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(35, 'add_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(36, 'delete_categories', 'categories', '2020-12-18 09:09:58', '2020-12-18 09:09:58'),
(37, 'browse_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(38, 'read_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(39, 'edit_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(40, 'add_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(41, 'delete_category_types', 'category_types', '2020-12-18 09:10:15', '2020-12-18 09:10:15'),
(42, 'browse_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(43, 'read_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(44, 'edit_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(45, 'add_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(46, 'delete_conditions', 'conditions', '2020-12-18 09:10:55', '2020-12-18 09:10:55'),
(47, 'browse_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(48, 'read_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(49, 'edit_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(50, 'add_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(51, 'delete_favorites', 'favorites', '2020-12-18 09:11:53', '2020-12-18 09:11:53'),
(52, 'browse_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(53, 'read_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(54, 'edit_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(55, 'add_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(56, 'delete_invoices', 'invoices', '2020-12-18 09:12:10', '2020-12-18 09:12:10'),
(57, 'browse_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(58, 'read_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(59, 'edit_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(60, 'add_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(61, 'delete_job_prices', 'job_prices', '2020-12-18 09:12:27', '2020-12-18 09:12:27'),
(62, 'browse_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(63, 'read_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(64, 'edit_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(65, 'add_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(66, 'delete_jobreviews', 'jobreviews', '2020-12-18 09:12:48', '2020-12-18 09:12:48'),
(67, 'browse_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(68, 'read_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(69, 'edit_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(70, 'add_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(71, 'delete_jobs', 'jobs', '2020-12-18 09:13:02', '2020-12-18 09:13:02'),
(82, 'browse_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(83, 'read_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(84, 'edit_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(85, 'add_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(86, 'delete_locations', 'locations', '2020-12-18 09:15:31', '2020-12-18 09:15:31'),
(87, 'browse_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(88, 'read_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(89, 'edit_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(90, 'add_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(91, 'delete_otp_numbers', 'otp_numbers', '2020-12-18 09:16:06', '2020-12-18 09:16:06'),
(92, 'browse_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(93, 'read_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(94, 'edit_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(95, 'add_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(96, 'delete_messages', 'messages', '2020-12-18 09:16:25', '2020-12-18 09:16:25'),
(102, 'browse_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(103, 'read_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(104, 'edit_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(105, 'add_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(106, 'delete_products', 'products', '2020-12-18 09:17:11', '2020-12-18 09:17:11'),
(107, 'browse_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(108, 'read_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(109, 'edit_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(110, 'add_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(111, 'delete_rooms', 'rooms', '2020-12-18 09:17:34', '2020-12-18 09:17:34'),
(117, 'browse_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(118, 'read_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(119, 'edit_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(120, 'add_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(121, 'delete_vehicle_sizes', 'vehicle_sizes', '2020-12-18 09:20:14', '2020-12-18 09:20:14'),
(122, 'browse_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(123, 'read_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(124, 'edit_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(125, 'add_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(126, 'delete_vehicles', 'vehicles', '2020-12-18 09:20:55', '2020-12-18 09:20:55'),
(127, 'browse_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(128, 'read_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(129, 'edit_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(130, 'add_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(131, 'delete_wallets', 'wallets', '2020-12-18 09:22:09', '2020-12-18 09:22:09'),
(152, 'browse_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(153, 'read_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(154, 'edit_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(155, 'add_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(156, 'delete_runtime_roles', 'runtime_roles', '2020-12-23 08:16:17', '2020-12-23 08:16:17'),
(177, 'browse_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(178, 'read_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(179, 'edit_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(180, 'add_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(181, 'delete_job_urls', 'job_urls', '2020-12-23 08:42:31', '2020-12-23 08:42:31'),
(182, 'browse_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(183, 'read_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(184, 'edit_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(185, 'add_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(186, 'delete_job_reviews', 'job_reviews', '2020-12-23 08:52:25', '2020-12-23 08:52:25'),
(187, 'browse_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(188, 'read_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(189, 'edit_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(190, 'add_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30'),
(191, 'delete_admin_settings', 'admin_settings', '2020-12-23 09:36:30', '2020-12-23 09:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1);

-- --------------------------------------------------------

--
-- Table structure for table `photourls`
--

CREATE TABLE `photourls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `firebase_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photourls`
--

INSERT INTO `photourls` (`id`, `name`, `type`, `owner_id`, `product_id`, `firebase_url`, `created_at`, `updated_at`) VALUES
(1, '1592682500000-img5.jpg', 'product', 27, 1, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1592682500000-img5.jpg?alt=media&token=c7ef9eeb-505a-4f9f-a33d-b2accb630532', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(2, '1594569524000-img7.jpg', 'product', 21, 2, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594569524000-img7.jpg?alt=media&token=bda55769-4cb9-4b22-8fc7-6c525013169d', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(3, '1594568287000-img4.jpg', 'product', 21, 3, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594568287000-img4.jpg?alt=media&token=de9352e6-e3c6-4c05-bbf7-281c8f2f2cd4', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(4, '1591299931000-6f3b215b6530bbd88dfc7f3d3b390aa1.jpg', 'product', 27, 4, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591299931000-6f3b215b6530bbd88dfc7f3d3b390aa1.jpg?alt=media&token=943e91ab-1057-45d7-b11d-e6f522016876', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(5, '1591526431000-0ef2d09faab8f4aa03a6a898472a54e1.jpg', 'product', 27, 5, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591526431000-0ef2d09faab8f4aa03a6a898472a54e1.jpg?alt=media&token=9e515153-1660-4a33-b286-1af04489456e', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(6, '1591268045000-1ec2838d5cd0cd53eaface83a1bfa921.jpg', 'product', 27, 6, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268045000-1ec2838d5cd0cd53eaface83a1bfa921.jpg?alt=media&token=0a9eb420-4204-4ed3-9f23-63a946b7c4f5', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(7, '1591278598000-5a24033a48ce0dfe82ca9ed68dff0d46.jpg', 'product', 27, 7, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591278598000-5a24033a48ce0dfe82ca9ed68dff0d46.jpg?alt=media&token=fed9533d-e763-4230-8933-2c3f924852ce', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(8, '1592682419000-img7.jpg', 'product', 27, 8, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1592682419000-img7.jpg?alt=media&token=4cbc3d8e-326b-4b8b-bfea-5e2f5a9c840c', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(9, '1591267801000-0bfcbc2ef78ad1ec040541ab72000bfe.jpg', 'product', 27, 9, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591267801000-0bfcbc2ef78ad1ec040541ab72000bfe.jpg?alt=media&token=27fe59d5-3cff-45b9-9aa9-d9dfc99d5fb8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(10, '1591698232000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 10, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591698232000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=991f9cd1-002b-40c0-a313-c8066df6a2ad', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(11, '1591278655000-5120 x 2880.jpg', 'product', 27, 11, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591278655000-5120%20x%202880.jpg?alt=media&token=2ae9d903-5cf8-4580-9620-ad87f7507019', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(12, '1591526165000-1 JAVA SCRIPT 2.png', 'product', 27, 12, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591526165000-1%20JAVA%20SCRIPT%202.png?alt=media&token=97ec6348-aac6-4c1d-8d0b-4c810c7ceba7', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(13, '1594569784000-img5.jpg', 'product', 21, 13, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594569784000-img5.jpg?alt=media&token=83c46e59-f287-49bb-8852-b971534f8908', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(14, '1591268493000-1c6249eced8071ae829575b0b59a9712.jpg', 'product', 27, 14, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268493000-1c6249eced8071ae829575b0b59a9712.jpg?alt=media&token=0de2601e-67c5-4ea3-a78c-11fcab3b64b9', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(15, '1591268423000-1c46e74a667b88260e699408d659ce38.jpg', 'product', 27, 15, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268423000-1c46e74a667b88260e699408d659ce38.jpg?alt=media&token=a8941c94-bd35-4936-9b71-3d0a9c79af54', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(16, '1591268341000-1b4a55d9d5fa19756be4c73500007934.jpg', 'product', 27, 16, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268341000-1b4a55d9d5fa19756be4c73500007934.jpg?alt=media&token=7904fb13-e194-43fd-a145-857f647c07e8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(17, '1591279007000-Wall92.jpg', 'product', 27, 17, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591279007000-Wall92.jpg?alt=media&token=8d3de557-9e62-4ed6-a5c6-2d431e51cf98', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(18, '1591268446000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 18, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268446000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=ded2709a-b822-46e3-97f7-be183348c711', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(19, '1591268182000-1d2430b7f93bd1867186c2f15b076f74.jpg', 'product', 27, 19, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268182000-1d2430b7f93bd1867186c2f15b076f74.jpg?alt=media&token=dcfda565-99e5-470c-8e2c-c3478400674b', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(20, '1591268186000-2a879d3c957727694634d9e5234bd94c.jpg', 'product', 27, 19, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268186000-2a879d3c957727694634d9e5234bd94c.jpg?alt=media&token=115d410f-8dd9-4b7c-9a79-a54b1a70bdd8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(21, '1591387090000-052ce910cc5b24a696b788d603407adb.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387090000-052ce910cc5b24a696b788d603407adb.jpg?alt=media&token=e783c8fc-c9d2-460e-8d45-3cae920495d8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(22, '1591387093000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387093000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=f91684b7-efa5-4050-895d-3fa4de6727f8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(23, '1591387096000-9aadf953cee378f14496675c7f23095a.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387096000-9aadf953cee378f14496675c7f23095a.jpg?alt=media&token=44aceea6-5098-43fb-9742-0316a2821bb8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(24, '1591379023000-2f409645a0f2d134c77cac7f1ea50cb9.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379023000-2f409645a0f2d134c77cac7f1ea50cb9.jpg?alt=media&token=00ed7ab9-2dbd-4fb3-bd1b-5abd095a6d64', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(25, '1591379024000-3aa0dd5eaffc4dddfd637990f2cead48.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379024000-3aa0dd5eaffc4dddfd637990f2cead48.jpg?alt=media&token=0fabe708-71de-4452-a0ca-f9471f63a7e2', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(26, '1591379026000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379026000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=f7805d43-2e51-4c32-97d9-b8d4a41fec06', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(27, '1591379028000-3edfbb5f58144180b9933e9df562e273.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379028000-3edfbb5f58144180b9933e9df562e273.jpg?alt=media&token=0c151a53-edf8-42f2-a2d7-2e771c754bd4', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(28, '1591268327000-3f44c357d1648815285bd5daaa629277.jpg', 'product', 27, 24, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268327000-3f44c357d1648815285bd5daaa629277.jpg?alt=media&token=a4748afa-3628-4b14-aa06-36f5856b74fd', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(29, '4cb04c1348c82b46457d02e218a0971e.png', 'product', 13, 29, 'http://localhost:8000/storage/app/images/products/4cb04c1348c82b46457d02e218a0971e.png', '2022-05-19 07:36:01', '2022-05-19 07:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_labels` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `geohash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insured` int(1) DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `quantities_available` double NOT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `added_by`, `category_id`, `description`, `extra_labels`, `geohash`, `insured`, `latitude`, `longitude`, `name`, `price`, `quantities_available`, `document_id`, `created_at`, `updated_at`, `deleted_at`, `condition_id`, `brand`, `vehicle_id`) VALUES
(1, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrtvgd97ff', 1, 24.936448, 67.0531584, 'Microsoft Os', 1500, 20, '3dyvREeXZgKq9jeB0Qp6', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(2, '21', '31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttfwqteh', 0, 24.887913, 67.0583857, 'Twilight', 855, 2, '3phqgpgMrIIYLqM93pPs', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(3, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrtktu8r8u', 1, 24.8607343, 67.0011364, 'Product 1', 44, 100, '7LhSKGikI792VLtRQww7', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(4, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 100058', 5000, 100, '7dZstmkdkRiJGHq7NiVn', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(5, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttwjve1k', 0, 24.9069568, 67.0466048, 'Product 1', 44, 100, 'BHjpKH27pHoAmjhYhwIo', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(6, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"fooo\", \"freeee\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'table', 5000, 110, 'G6uF27QOm0LaQQIsmnGs', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(7, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Ubuntu Dis', 4455, 1000, 'HCTrjwMd7Jrxw2eIsHFz', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(8, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"10%\"]', 'wy6u72v1c54', 1, 35.907757, 127.766922, 'Product Testing', 500, 50, 'OWQBdnyJSxv3pRb9z1Ho', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(9, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"boo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 1852', 500, 100, 'OovTWSiehRVSuDTAxzpb', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(10, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'ThyDIKrHsmSF1ZaA1wcl', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(11, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"limited\", \"75%\"]', 'tkrtwpywc9g', 1, 24.9167872, 67.0695424, 'Product 12345', 555, 100, 'UH9oVxe0QENFUKxjn6kj', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(12, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 500, 100, 'XL5dsPTLyOBSuvUwr6RQ', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(13, '21', '10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttfwqteh', 0, 24.887913, 67.0583857, 'just this', 4455, 10, 'XQC9CbK2iZGrAL4Qkczi', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(14, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"free\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'apsBwLrzjpwAE0HxH1lt', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(15, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 5005', 44, 100, 'eci1mVkIROjpijp4NInE', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(16, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'ehZNwDqI4zo9tjLAI0UR', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(17, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"freeee\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Testing 150', 44, 100, 'gOeXbBEwIx1hOEC86gNF', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(18, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'gb5vHGcjwmBliYMJAYBI', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(19, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"foo\"]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Computer Speakers', 50, 100, 'grOVNZPfozu6neSanTmA', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(20, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrwnnhj14y', 0, 24.994770260615, 67.066092302655, 'Product 1', 44, 100, 'juI5RrvyAnED5bEogv3I', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(21, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"ditro\", \"dsd\", \"asdas\"]', 'tkrtwpywc9g', 1, 24.9167872, 67.0695424, 'Linux DX', 44, 100, 'kmeQPn0BlPTGmkWVXcYC', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(22, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"fere\"]', 'tkrtvgd97ff', 0, 24.936448, 67.0531584, 'TESTING 128', 44, 100, 'lcAKxFTSAAkeyidpebq3', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(23, '21', '2', 'Rarely used, inflatable, good quality mattress', '[\"Furniture\", \"Mattress\"]', 'tusxf00d638', 0, 26.6752, 85.1668, 'Ezcomf Single Air Mattress', 45, 1, 'orTxKl0ERFmFWwTwtU0J', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(24, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"free\"]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Bed Sheet', 44, 100, 'xK4YvEcW3xs9ALzNvhod', '2020-08-26 17:18:43', '2020-08-26 17:18:43', NULL, NULL, NULL, NULL),
(25, '9', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"red\",\"fruits\"]', 'tkrtyb4w58ctr', 0, 24.9180271, 67.0970916, 'fruits', 500, 100, NULL, '2020-11-28 18:42:35', '2020-11-28 18:42:35', NULL, 1, 'fruits', NULL),
(26, '9', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"rat\"]', 'tkrtyb4w58ctr', 0, 24.9180271, 67.0970916, 'fruits2', 499, 100, NULL, '2020-11-28 19:21:04', '2020-11-28 19:21:04', NULL, 2, 'Box', 2),
(27, '9', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"test\",\"bulb\"]', 'tefqdq555wt3r', 0, 22.2698372, 70.7672169, 'test bulb', 12, 100, NULL, '2020-12-03 20:47:40', '2020-12-03 20:47:40', NULL, 1, 'Sony', 3),
(28, '9', '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tefqdq555wt3r', 0, 22.2698372, 70.7672169, 'AI things', 123, 100, NULL, '2020-12-03 20:52:30', '2020-12-03 20:52:30', NULL, 2, 'SpaceX', 2),
(29, '13', '6', 'mera jism meri marzi.....', '[]', 'tkrtyb4w58ctr', 0, 24.9180271, 67.0970916, 'Giggly', 412, 100, NULL, '2022-05-19 07:36:01', '2022-05-19 07:36:01', NULL, 1, 'assd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrator', NULL, '2020-12-18 05:55:42', '2020-12-26 02:37:24'),
(2, 'User', 'User', NULL, '2020-12-18 05:55:42', '2020-12-26 02:37:41'),
(3, 'Driver', 'Driver', NULL, '2020-12-26 00:58:36', '2020-12-26 00:58:36'),
(4, 'Consumer', 'Consumer', NULL, '2020-12-26 02:36:41', '2020-12-26 02:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-10-13 20:12:32', '2020-10-13 20:12:32'),
(2, 4, 1, NULL, NULL),
(3, 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `messeges_count` int(11) NOT NULL,
  `initiator_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `messeges_count`, `initiator_id`, `recipient_id`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 1, '9', '21', NULL, '2020-11-28 12:14:33', '2020-11-28 12:14:34'),
(2, 1, '13', '13', NULL, '2022-05-19 07:36:29', '2022-05-19 07:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `runtime_roles`
--

CREATE TABLE `runtime_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `runtime_roles`
--

INSERT INTO `runtime_roles` (`id`, `user_id`, `role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 27, 0, 'Driver', '2020-09-11 03:08:37', '2020-09-11 03:08:37'),
(2, 27, 0, 'Driver', '2020-09-11 15:37:23', '2020-09-11 15:37:23'),
(3, 27, 0, 'Driver', '2020-09-11 16:39:03', '2020-09-11 16:39:03'),
(4, 27, 0, 'Driver', '2020-09-11 16:42:16', '2020-09-11 16:42:16'),
(5, 27, 0, 'Driver', '2020-09-11 16:48:06', '2020-09-11 16:48:06'),
(6, 27, 0, 'Driver', '2020-09-11 16:48:35', '2020-09-11 16:48:35'),
(7, 27, 0, 'Driver', '2020-09-11 16:54:39', '2020-09-11 16:54:39'),
(8, 27, 0, 'Driver', '2020-09-11 17:02:53', '2020-09-11 17:02:53'),
(9, 27, 0, 'Driver', '2020-09-11 17:04:21', '2020-09-11 17:04:21'),
(10, 27, 0, 'Driver', '2020-09-11 17:10:23', '2020-09-11 17:10:23'),
(11, 27, 0, 'Driver', '2020-09-11 17:13:12', '2020-09-11 17:13:12'),
(12, 27, 0, 'Driver', '2020-09-11 17:13:47', '2020-09-11 17:13:47'),
(13, 27, 0, 'Driver', '2020-09-11 17:18:08', '2020-09-11 17:18:08'),
(14, 27, 0, 'Driver', '2020-09-11 17:18:18', '2020-09-11 17:18:18'),
(15, 27, 0, 'Consumer', '2020-09-11 17:40:56', '2020-09-11 17:40:56'),
(16, 27, 0, 'Consumer', '2020-09-11 17:41:06', '2020-09-11 17:41:06'),
(17, 27, 0, 'Driver', '2020-09-12 12:56:58', '2020-09-12 12:56:58'),
(18, 27, 0, 'Consumer', '2020-09-12 12:59:08', '2020-09-12 12:59:08'),
(19, 27, 0, 'Driver', '2020-09-12 12:59:20', '2020-09-12 12:59:20'),
(20, 27, 0, 'Consumer', '2020-09-12 13:07:20', '2020-09-12 13:07:20'),
(21, 27, 0, 'Driver', '2020-09-12 13:34:13', '2020-09-12 13:34:13'),
(22, 27, 0, 'Driver', '2020-09-12 13:35:49', '2020-09-12 13:35:49'),
(23, 27, 0, 'Consumer', '2020-09-12 13:36:32', '2020-09-12 13:36:32'),
(24, 27, 0, 'Driver', '2020-09-15 16:37:42', '2020-09-15 16:37:42'),
(25, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(26, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(27, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(28, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(29, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(30, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(31, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(32, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(33, 41, 0, 'Driver', '2020-09-16 09:23:54', '2020-09-16 09:23:54'),
(34, 41, 0, 'Driver', '2020-09-16 09:24:34', '2020-09-16 09:24:34'),
(35, 41, 0, 'Consumer', '2020-09-16 09:26:36', '2020-09-16 09:26:36'),
(36, 41, 0, 'Driver', '2020-09-19 05:08:44', '2020-09-19 05:08:44'),
(37, 27, 0, 'Driver', '2020-09-20 09:50:16', '2020-09-20 09:50:16'),
(38, 41, 0, 'Driver', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(39, 41, 0, 'Consumer', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(40, 41, 0, 'Consumer', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(41, 41, 0, 'Driver', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(42, 41, 0, 'Consumer', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(43, 41, 0, 'Driver', '2020-09-21 03:56:33', '2020-09-21 03:56:33'),
(44, 41, 0, 'Driver', '2020-09-21 03:56:34', '2020-09-21 03:56:34'),
(45, 41, 0, 'Consumer', '2020-09-21 03:59:28', '2020-09-21 03:59:28'),
(46, 41, 0, 'Consumer', '2020-09-21 04:11:55', '2020-09-21 04:11:55'),
(47, 41, 0, 'Consumer', '2020-09-21 04:11:57', '2020-09-21 04:11:57'),
(48, 41, 0, 'Consumer', '2020-09-21 04:14:53', '2020-09-21 04:14:53'),
(49, 41, 0, 'Consumer', '2020-09-21 04:18:45', '2020-09-21 04:18:45'),
(50, 41, 0, 'Consumer', '2020-09-21 04:22:41', '2020-09-21 04:22:41'),
(51, 41, 0, 'Consumer', '2020-09-21 04:24:46', '2020-09-21 04:24:46'),
(52, 41, 0, 'Consumer', '2020-09-21 04:30:05', '2020-09-21 04:30:05'),
(53, 41, 0, 'Consumer', '2020-09-21 04:32:19', '2020-09-21 04:32:19'),
(54, 41, 0, 'Consumer', '2020-09-21 04:36:49', '2020-09-21 04:36:49'),
(55, 41, 0, 'Consumer', '2020-09-21 04:39:16', '2020-09-21 04:39:16'),
(56, 41, 0, 'Consumer', '2020-09-21 04:40:50', '2020-09-21 04:40:50'),
(57, 41, 0, 'Consumer', '2020-09-21 04:45:55', '2020-09-21 04:45:55'),
(58, 41, 0, 'Consumer', '2020-09-21 04:57:55', '2020-09-21 04:57:55'),
(59, 41, 0, 'Consumer', '2020-09-21 04:57:55', '2020-09-21 04:57:55'),
(60, 41, 0, 'Consumer', '2020-09-21 04:59:14', '2020-09-21 04:59:14'),
(61, 41, 0, 'Driver', '2020-09-21 05:00:20', '2020-09-21 05:00:20'),
(62, 41, 0, 'Consumer', '2020-09-21 05:00:24', '2020-09-21 05:00:24'),
(63, 41, 0, 'Consumer', '2020-09-21 05:09:01', '2020-09-21 05:09:01'),
(64, 41, 0, 'Consumer', '2020-09-21 05:10:01', '2020-09-21 05:10:01'),
(65, 41, 0, 'Consumer', '2020-09-21 05:10:50', '2020-09-21 05:10:50'),
(66, 41, 0, 'Consumer', '2020-09-21 05:13:15', '2020-09-21 05:13:15'),
(67, 41, 0, 'Consumer', '2020-09-21 05:15:21', '2020-09-21 05:15:21'),
(68, 41, 0, 'Consumer', '2020-09-21 05:18:26', '2020-09-21 05:18:26'),
(69, 41, 0, 'Consumer', '2020-09-21 05:20:53', '2020-09-21 05:20:53'),
(70, 41, 0, 'Consumer', '2020-09-21 05:21:38', '2020-09-21 05:21:38'),
(71, 41, 0, 'Consumer', '2020-09-21 05:23:52', '2020-09-21 05:23:52'),
(72, 41, 0, 'Consumer', '2020-09-21 05:25:30', '2020-09-21 05:25:30'),
(73, 41, 0, 'Consumer', '2020-09-21 05:27:16', '2020-09-21 05:27:16'),
(74, 41, 0, 'Consumer', '2020-09-21 05:28:15', '2020-09-21 05:28:15'),
(75, 41, 0, 'Consumer', '2020-09-21 05:30:27', '2020-09-21 05:30:27'),
(76, 41, 0, 'Driver', '2020-09-21 05:38:07', '2020-09-21 05:38:07'),
(77, 41, 0, 'Consumer', '2020-09-21 06:04:50', '2020-09-21 06:04:50'),
(78, 41, 0, 'Consumer', '2020-09-21 06:37:41', '2020-09-21 06:37:41'),
(79, 41, 0, 'Driver', '2020-09-21 06:37:48', '2020-09-21 06:37:48'),
(80, 41, 0, 'Consumer', '2020-09-21 06:38:03', '2020-09-21 06:38:03'),
(81, 41, 0, 'Consumer', '2020-09-21 06:59:12', '2020-09-21 06:59:12'),
(82, 41, 0, 'Consumer', '2020-09-21 07:18:03', '2020-09-21 07:18:03'),
(83, 41, 0, 'Driver', '2020-09-21 07:19:59', '2020-09-21 07:19:59'),
(84, 27, 0, 'Driver', '2020-09-21 18:59:09', '2020-09-21 18:59:09'),
(85, 41, 0, 'Consumer', '2020-09-23 10:35:14', '2020-09-23 10:35:14'),
(86, 41, 0, 'Consumer', '2020-09-23 11:15:17', '2020-09-23 11:15:17'),
(87, 2, 0, 'Consumer', '2020-09-25 15:02:16', '2020-09-25 15:02:16'),
(88, 2, 0, 'Driver', '2020-09-25 16:18:57', '2020-09-25 16:18:57'),
(89, 2, 0, 'Driver', '2020-09-25 16:23:06', '2020-09-25 16:23:06'),
(90, 2, 0, 'Driver', '2020-09-25 16:27:34', '2020-09-25 16:27:34'),
(91, 2, 0, 'Consumer', '2020-09-26 07:13:38', '2020-09-26 07:13:38'),
(92, 2, 0, 'Driver', '2020-09-26 07:17:53', '2020-09-26 07:17:53'),
(93, 2, 0, 'Driver', '2020-09-26 07:20:41', '2020-09-26 07:20:41'),
(94, 2, 0, 'Driver', '2020-09-26 07:23:54', '2020-09-26 07:23:54'),
(95, 2, 0, 'Driver', '2020-09-26 07:40:31', '2020-09-26 07:40:31'),
(96, 2, 0, 'Driver', '2020-09-26 07:51:57', '2020-09-26 07:51:57'),
(97, 2, 0, 'Driver', '2020-09-26 07:56:51', '2020-09-26 07:56:51'),
(98, 2, 0, 'Driver', '2020-09-26 08:16:22', '2020-09-26 08:16:22'),
(99, 2, 0, 'Driver', '2020-09-26 08:16:22', '2020-09-26 08:16:22'),
(100, 2, 0, 'Driver', '2020-09-26 08:31:37', '2020-09-26 08:31:37'),
(101, 2, 0, 'Driver', '2020-09-26 09:11:37', '2020-09-26 09:11:37'),
(102, 2, 0, 'Consumer', '2020-09-26 09:31:14', '2020-09-26 09:31:14'),
(103, 2, 0, 'Consumer', '2020-09-26 09:36:48', '2020-09-26 09:36:48'),
(104, 2, 0, 'Driver', '2020-09-26 09:37:08', '2020-09-26 09:37:08'),
(105, 2, 0, 'Consumer', '2020-09-26 09:38:17', '2020-09-26 09:38:17'),
(106, 2, 0, 'Driver', '2020-09-26 09:40:58', '2020-09-26 09:40:58'),
(107, 2, 0, 'Consumer', '2020-09-26 09:42:52', '2020-09-26 09:42:52'),
(108, 2, 0, 'Driver', '2020-09-26 09:51:58', '2020-09-26 09:51:58'),
(109, 2, 0, 'Driver', '2020-09-26 10:17:30', '2020-09-26 10:17:30'),
(110, 2, 0, 'Driver', '2020-09-26 10:29:47', '2020-09-26 10:29:47'),
(111, 2, 0, 'Driver', '2020-09-26 13:56:00', '2020-09-26 13:56:00'),
(112, 2, 0, 'Driver', '2020-09-26 14:20:50', '2020-09-26 14:20:50'),
(113, 2, 0, 'Driver', '2020-09-26 14:22:53', '2020-09-26 14:22:53'),
(114, 2, 0, 'Driver', '2020-09-26 14:28:06', '2020-09-26 14:28:06'),
(115, 2, 0, 'Driver', '2020-09-26 14:46:22', '2020-09-26 14:46:22'),
(116, 2, 0, 'Driver', '2020-09-26 14:53:30', '2020-09-26 14:53:30'),
(117, 2, 0, 'Consumer', '2020-09-26 15:30:55', '2020-09-26 15:30:55'),
(118, 2, 0, 'Driver', '2020-09-26 16:01:19', '2020-09-26 16:01:19'),
(119, 2, 0, 'Driver', '2020-09-26 16:17:38', '2020-09-26 16:17:38'),
(120, 2, 0, 'Driver', '2020-09-26 16:19:28', '2020-09-26 16:19:28'),
(121, 2, 0, 'Driver', '2020-09-28 16:22:13', '2020-09-28 16:22:13'),
(122, 2, 0, 'Driver', '2020-09-28 16:29:47', '2020-09-28 16:29:47'),
(123, 2, 0, 'Driver', '2020-09-28 16:35:23', '2020-09-28 16:35:23'),
(124, 2, 0, 'Driver', '2020-09-28 16:48:27', '2020-09-28 16:48:27'),
(125, 2, 0, 'Driver', '2020-09-28 16:57:07', '2020-09-28 16:57:07'),
(126, 2, 0, 'Driver', '2020-09-28 17:02:39', '2020-09-28 17:02:39'),
(127, 2, 0, 'Driver', '2020-09-28 17:02:39', '2020-09-28 17:02:39'),
(128, 2, 0, 'Driver', '2020-09-28 17:06:29', '2020-09-28 17:06:29'),
(129, 2, 0, 'Driver', '2020-09-28 17:08:33', '2020-09-28 17:08:33'),
(130, 2, 0, 'Driver', '2020-09-28 17:11:27', '2020-09-28 17:11:27'),
(131, 2, 0, 'Driver', '2020-09-28 17:22:19', '2020-09-28 17:22:19'),
(132, 2, 0, 'Driver', '2020-09-29 09:00:47', '2020-09-29 09:00:47'),
(133, 2, 0, 'Consumer', '2020-09-29 09:00:53', '2020-09-29 09:00:53'),
(134, 2, 0, 'Driver', '2020-09-29 15:37:47', '2020-09-29 15:37:47'),
(135, 2, 0, 'Driver', '2020-09-29 15:57:38', '2020-09-29 15:57:38'),
(136, 2, 0, 'Driver', '2020-09-29 16:02:01', '2020-09-29 16:02:01'),
(137, 2, 0, 'Driver', '2020-09-29 16:12:56', '2020-09-29 16:12:56'),
(138, 2, 0, 'Driver', '2020-09-29 16:25:21', '2020-09-29 16:25:21'),
(139, 2, 0, 'Driver', '2020-09-29 16:33:52', '2020-09-29 16:33:52'),
(140, 2, 0, 'Driver', '2020-09-29 16:42:14', '2020-09-29 16:42:14'),
(141, 2, 0, 'Driver', '2020-09-29 16:43:44', '2020-09-29 16:43:44'),
(142, 2, 0, 'Driver', '2020-09-29 17:08:09', '2020-09-29 17:08:09'),
(143, 2, 0, 'Driver', '2020-09-29 17:09:57', '2020-09-29 17:09:57'),
(144, 2, 0, 'Driver', '2020-09-30 16:44:12', '2020-09-30 16:44:12'),
(145, 2, 0, 'Consumer', '2020-10-01 06:34:41', '2020-10-01 06:34:41'),
(146, 2, 0, 'Consumer', '2020-10-02 15:34:49', '2020-10-02 15:34:49'),
(147, 2, 0, 'Consumer', '2020-10-02 15:34:49', '2020-10-02 15:34:49'),
(148, 2, 0, 'Consumer', '2020-10-02 16:23:01', '2020-10-02 16:23:01'),
(149, 2, 0, 'Driver', '2020-10-02 16:23:39', '2020-10-02 16:23:39'),
(150, 2, 0, 'Driver', '2020-10-02 16:23:58', '2020-10-02 16:23:58'),
(151, 2, 0, 'Driver', '2020-10-02 16:24:08', '2020-10-02 16:24:08'),
(152, 2, 0, 'Consumer', '2020-10-02 16:24:56', '2020-10-02 16:24:56'),
(153, 2, 0, 'Driver', '2020-10-02 16:25:21', '2020-10-02 16:25:21'),
(154, 2, 0, 'Consumer', '2020-10-02 16:25:37', '2020-10-02 16:25:37'),
(155, 2, 0, 'Consumer', '2020-10-02 18:12:02', '2020-10-02 18:12:02'),
(156, 2, 0, 'Consumer', '2020-10-02 18:14:48', '2020-10-02 18:14:48'),
(157, 2, 0, 'Consumer', '2020-10-02 18:20:37', '2020-10-02 18:20:37'),
(158, 2, 0, 'Consumer', '2020-10-02 18:25:16', '2020-10-02 18:25:16'),
(159, 2, 0, 'Driver', '2020-10-02 18:35:36', '2020-10-02 18:35:36'),
(160, 2, 0, 'Consumer', '2020-10-02 18:35:46', '2020-10-02 18:35:46'),
(161, 2, 0, 'Consumer', '2020-10-02 18:40:49', '2020-10-02 18:40:49'),
(162, 2, 0, 'Consumer', '2020-10-02 18:43:38', '2020-10-02 18:43:38'),
(163, 2, 0, 'Consumer', '2020-10-02 18:47:33', '2020-10-02 18:47:33'),
(164, 2, 0, 'Consumer', '2020-10-02 18:57:56', '2020-10-02 18:57:56'),
(165, 2, 0, 'Consumer', '2020-10-02 19:00:36', '2020-10-02 19:00:36'),
(166, 2, 0, 'Consumer', '2020-10-02 19:05:50', '2020-10-02 19:05:50'),
(167, 2, 0, 'Consumer', '2020-10-02 19:17:28', '2020-10-02 19:17:28'),
(168, 2, 0, 'Consumer', '2020-10-02 19:22:52', '2020-10-02 19:22:52'),
(169, 2, 0, 'Consumer', '2020-10-02 19:26:22', '2020-10-02 19:26:22'),
(170, 2, 0, 'Driver', '2020-10-02 19:29:02', '2020-10-02 19:29:02'),
(171, 2, 0, 'Consumer', '2020-10-02 19:29:07', '2020-10-02 19:29:07'),
(172, 2, 0, 'Consumer', '2020-10-02 19:34:05', '2020-10-02 19:34:05'),
(173, 2, 0, 'Consumer', '2020-10-02 19:38:31', '2020-10-02 19:38:31'),
(174, 2, 0, 'Consumer', '2020-10-02 19:39:51', '2020-10-02 19:39:51'),
(175, 2, 0, 'Consumer', '2020-10-02 19:41:34', '2020-10-02 19:41:34'),
(176, 2, 0, 'Consumer', '2020-10-02 19:44:58', '2020-10-02 19:44:58'),
(177, 2, 0, 'Consumer', '2020-10-02 19:48:57', '2020-10-02 19:48:57'),
(178, 2, 0, 'Consumer', '2020-10-03 13:01:13', '2020-10-03 13:01:13'),
(179, 2, 0, 'Driver', '2020-10-03 13:02:39', '2020-10-03 13:02:39'),
(180, 2, 0, 'Driver', '2020-10-03 13:03:40', '2020-10-03 13:03:40'),
(181, 2, 0, 'Consumer', '2020-10-03 13:09:18', '2020-10-03 13:09:18'),
(182, 2, 0, 'Driver', '2020-10-03 13:09:31', '2020-10-03 13:09:31'),
(183, 2, 0, 'Consumer', '2020-10-04 10:52:48', '2020-10-04 10:52:48'),
(184, 2, 0, 'Driver', '2020-10-05 15:49:28', '2020-10-05 15:49:28'),
(185, 2, 0, 'Driver', '2020-10-05 15:50:36', '2020-10-05 15:50:36'),
(186, 2, 0, 'Consumer', '2020-10-05 15:50:41', '2020-10-05 15:50:41'),
(187, 2, 0, 'Driver', '2020-10-05 16:03:56', '2020-10-05 16:03:56'),
(188, 2, 0, 'Driver', '2020-10-05 16:03:56', '2020-10-05 16:03:56'),
(189, 2, 0, 'Consumer', '2020-10-05 16:03:56', '2020-10-05 16:03:56'),
(190, 2, 0, 'Driver', '2020-10-05 16:04:09', '2020-10-05 16:04:09'),
(191, 2, 0, 'Consumer', '2020-10-05 16:05:00', '2020-10-05 16:05:00'),
(192, 2, 0, 'Driver', '2020-10-05 16:05:12', '2020-10-05 16:05:12'),
(193, 2, 0, 'Consumer', '2020-10-05 16:17:34', '2020-10-05 16:17:34'),
(194, 2, 0, 'Consumer', '2020-10-05 16:17:44', '2020-10-05 16:17:44'),
(195, 2, 0, 'Consumer', '2020-10-05 16:40:17', '2020-10-05 16:40:17'),
(196, 2, 0, 'Consumer', '2020-10-05 16:44:42', '2020-10-05 16:44:42'),
(197, 2, 0, 'Consumer', '2020-10-05 16:47:24', '2020-10-05 16:47:24'),
(198, 2, 0, 'Consumer', '2020-10-05 16:48:47', '2020-10-05 16:48:47'),
(199, 2, 0, 'Consumer', '2020-10-05 16:50:36', '2020-10-05 16:50:36'),
(200, 2, 0, 'Consumer', '2020-10-05 16:57:41', '2020-10-05 16:57:41'),
(201, 2, 0, 'Consumer', '2020-10-05 16:59:57', '2020-10-05 16:59:57'),
(202, 2, 0, 'Consumer', '2020-10-05 17:03:49', '2020-10-05 17:03:49'),
(203, 2, 0, 'Consumer', '2020-10-05 17:05:30', '2020-10-05 17:05:30'),
(204, 2, 0, 'Consumer', '2020-10-05 17:10:49', '2020-10-05 17:10:49'),
(205, 2, 0, 'Consumer', '2020-10-05 17:19:28', '2020-10-05 17:19:28'),
(206, 2, 0, 'Consumer', '2020-10-05 17:27:36', '2020-10-05 17:27:36'),
(207, 2, 0, 'Consumer', '2020-10-05 17:28:58', '2020-10-05 17:28:58'),
(208, 2, 0, 'Consumer', '2020-10-05 17:32:17', '2020-10-05 17:32:17'),
(209, 2, 0, 'Consumer', '2020-10-05 17:34:01', '2020-10-05 17:34:01'),
(210, 2, 0, 'Consumer', '2020-10-05 17:34:03', '2020-10-05 17:34:03'),
(211, 2, 0, 'Consumer', '2020-10-05 17:36:20', '2020-10-05 17:36:20'),
(212, 2, 0, 'Consumer', '2020-10-05 17:38:50', '2020-10-05 17:38:50'),
(213, 2, 0, 'Consumer', '2020-10-05 17:42:53', '2020-10-05 17:42:53'),
(214, 2, 0, 'Consumer', '2020-10-05 17:47:25', '2020-10-05 17:47:25'),
(215, 2, 0, 'Consumer', '2020-10-05 18:08:41', '2020-10-05 18:08:41'),
(216, 2, 0, 'Consumer', '2020-10-05 18:12:02', '2020-10-05 18:12:02'),
(217, 2, 0, 'Consumer', '2020-10-05 18:16:10', '2020-10-05 18:16:10'),
(218, 2, 0, 'Consumer', '2020-10-05 18:16:18', '2020-10-05 18:16:18'),
(219, 2, 0, 'Consumer', '2020-10-05 18:17:23', '2020-10-05 18:17:23'),
(220, 2, 0, 'Consumer', '2020-10-05 18:26:24', '2020-10-05 18:26:24'),
(221, 2, 0, 'Consumer', '2020-10-05 18:27:34', '2020-10-05 18:27:34'),
(222, 2, 0, 'Consumer', '2020-10-05 18:30:23', '2020-10-05 18:30:23'),
(223, 2, 0, 'Consumer', '2020-10-05 18:32:51', '2020-10-05 18:32:51'),
(224, 2, 0, 'Consumer', '2020-10-05 18:34:07', '2020-10-05 18:34:07'),
(225, 2, 0, 'Consumer', '2020-10-05 18:35:14', '2020-10-05 18:35:14'),
(226, 2, 0, 'Consumer', '2020-10-05 18:36:51', '2020-10-05 18:36:51'),
(227, 2, 0, 'Consumer', '2020-10-05 18:37:55', '2020-10-05 18:37:55'),
(228, 2, 0, 'Consumer', '2020-10-05 18:40:02', '2020-10-05 18:40:02'),
(229, 2, 0, 'Consumer', '2020-10-05 18:40:38', '2020-10-05 18:40:38'),
(230, 2, 0, 'Consumer', '2020-10-05 18:41:11', '2020-10-05 18:41:11'),
(231, 2, 0, 'Consumer', '2020-10-05 18:43:22', '2020-10-05 18:43:22'),
(232, 2, 0, 'Consumer', '2020-10-06 16:30:59', '2020-10-06 16:30:59'),
(233, 2, 0, 'Consumer', '2020-10-06 16:38:22', '2020-10-06 16:38:22'),
(234, 2, 0, 'Driver', '2020-10-06 16:40:23', '2020-10-06 16:40:23'),
(235, 2, 0, 'Driver', '2020-10-06 17:19:24', '2020-10-06 17:19:24'),
(236, 2, 0, 'Driver', '2020-10-06 17:29:25', '2020-10-06 17:29:25'),
(237, 2, 0, 'Driver', '2020-10-06 17:34:18', '2020-10-06 17:34:18'),
(238, 2, 0, 'Driver', '2020-10-06 17:34:19', '2020-10-06 17:34:19'),
(239, 2, 0, 'Consumer', '2020-10-06 18:10:06', '2020-10-06 18:10:06'),
(240, 2, 0, 'Driver', '2020-10-06 18:14:52', '2020-10-06 18:14:52'),
(241, 2, 0, 'Driver', '2020-10-06 18:16:30', '2020-10-06 18:16:30'),
(242, 2, 0, 'Consumer', '2020-10-06 18:16:40', '2020-10-06 18:16:40'),
(243, 7, 0, 'Consumer', '2020-10-07 06:58:19', '2020-10-07 06:58:19'),
(244, 7, 0, 'Consumer', '2020-10-07 07:00:51', '2020-10-07 07:00:51'),
(245, 7, 0, 'Driver', '2020-10-07 07:12:46', '2020-10-07 07:12:46'),
(246, 7, 0, 'Consumer', '2020-10-07 07:42:32', '2020-10-07 07:42:32'),
(247, 7, 0, 'Consumer', '2020-10-07 07:44:22', '2020-10-07 07:44:22'),
(248, 7, 0, 'Consumer', '2020-10-07 07:49:13', '2020-10-07 07:49:13'),
(249, 7, 0, 'Consumer', '2020-10-07 08:28:50', '2020-10-07 08:28:50'),
(250, 9, 0, 'Consumer', '2020-10-08 05:30:27', '2020-10-08 05:30:27'),
(251, 9, 0, 'Consumer', '2020-10-08 05:30:55', '2020-10-08 05:30:55'),
(252, 9, 0, 'Consumer', '2020-10-08 05:33:14', '2020-10-08 05:33:14'),
(253, 9, 0, 'Consumer', '2020-10-08 17:58:51', '2020-10-08 17:58:51'),
(254, 9, 0, 'Consumer', '2020-10-08 18:13:53', '2020-10-08 18:13:53'),
(255, 9, 0, 'Consumer', '2020-10-08 18:13:53', '2020-10-08 18:13:53'),
(256, 9, 0, 'Consumer', '2020-10-08 18:17:43', '2020-10-08 18:17:43'),
(257, 9, 0, 'Consumer', '2020-10-08 18:23:14', '2020-10-08 18:23:14'),
(258, 9, 0, 'Consumer', '2020-10-08 18:25:57', '2020-10-08 18:25:57'),
(259, 9, 0, 'Consumer', '2020-10-08 18:37:10', '2020-10-08 18:37:10'),
(260, 9, 0, 'Consumer', '2020-10-08 18:42:09', '2020-10-08 18:42:09'),
(261, 1, 0, 'Consumer', '2020-10-10 12:08:50', '2020-10-10 12:08:50'),
(262, 2, 0, 'Consumer', '2020-10-10 12:14:52', '2020-10-10 12:14:52'),
(263, 2, 0, 'Consumer', '2020-10-10 12:16:28', '2020-10-10 12:16:28'),
(264, 2, 0, 'Consumer', '2020-10-10 12:21:23', '2020-10-10 12:21:23'),
(265, 2, 0, 'Consumer', '2020-10-10 12:23:18', '2020-10-10 12:23:18'),
(266, 3, 0, 'Consumer', '2020-10-14 16:30:36', '2020-10-14 16:30:36'),
(267, 6, 0, 'Consumer', '2020-10-14 19:43:07', '2020-10-14 19:43:07'),
(268, 6, 0, 'Consumer', '2020-10-14 19:43:50', '2020-10-14 19:43:50'),
(269, 3, 0, 'Consumer', '2020-10-14 19:49:26', '2020-10-14 19:49:26'),
(270, 3, 0, 'Consumer', '2020-10-14 19:51:06', '2020-10-14 19:51:06'),
(271, 7, 0, 'Consumer', '2020-10-17 12:28:22', '2020-10-17 12:28:22'),
(272, 7, 0, 'Consumer', '2020-10-17 12:28:58', '2020-10-17 12:28:58'),
(273, 7, 0, 'Consumer', '2020-10-17 13:39:39', '2020-10-17 13:39:39'),
(274, 7, 0, 'Consumer', '2020-10-17 13:39:39', '2020-10-17 13:39:39'),
(275, 7, 0, 'Consumer', '2020-10-17 13:40:27', '2020-10-17 13:40:27'),
(276, 8, 0, 'Consumer', '2020-10-17 13:42:06', '2020-10-17 13:42:06'),
(277, 8, 0, 'Consumer', '2020-10-17 13:42:25', '2020-10-17 13:42:25'),
(278, 9, 0, 'Consumer', '2020-10-21 14:43:05', '2020-10-21 14:43:05'),
(279, 9, 0, 'Consumer', '2020-10-21 14:43:46', '2020-10-21 14:43:46'),
(280, 10, 0, 'Consumer', '2020-10-21 14:53:33', '2020-10-21 14:53:33'),
(281, 10, 0, 'Consumer', '2020-10-21 14:54:15', '2020-10-21 14:54:15'),
(282, 9, 0, 'Consumer', '2020-10-21 14:57:33', '2020-10-21 14:57:33'),
(283, 9, 0, 'Consumer', '2020-10-21 16:55:47', '2020-10-21 16:55:47'),
(284, 9, 0, 'Consumer', '2020-10-21 16:55:47', '2020-10-21 16:55:47'),
(285, 9, 0, 'Consumer', '2020-10-26 12:46:52', '2020-10-26 12:46:52'),
(286, 9, 0, 'Consumer', '2020-10-26 12:54:34', '2020-10-26 12:54:34'),
(287, 9, 0, 'Consumer', '2020-10-26 13:08:16', '2020-10-26 13:08:16'),
(288, 9, 0, 'Consumer', '2020-10-26 13:12:28', '2020-10-26 13:12:28'),
(289, 9, 0, 'Consumer', '2020-10-26 13:19:27', '2020-10-26 13:19:27'),
(290, 13, 0, 'Consumer', '2020-10-27 17:05:21', '2020-10-27 17:05:21'),
(291, 14, 0, 'Consumer', '2020-10-27 17:16:37', '2020-10-27 17:16:37'),
(292, 14, 0, 'Driver', '2020-10-27 17:18:06', '2020-10-27 17:18:06'),
(293, 14, 0, 'Consumer', '2020-10-27 17:18:26', '2020-10-27 17:18:26'),
(294, 14, 0, 'Consumer', '2020-10-27 17:18:26', '2020-10-27 17:18:26'),
(295, 13, 0, 'Consumer', '2020-10-27 17:28:26', '2020-10-27 17:28:26'),
(296, 13, 0, 'Driver', '2020-10-27 17:28:31', '2020-10-27 17:28:31'),
(297, 13, 0, 'Driver', '2020-10-27 20:29:50', '2020-10-27 20:29:50'),
(298, 13, 0, 'Driver', '2020-10-27 20:29:50', '2020-10-27 20:29:50'),
(299, 13, 0, 'Consumer', '2020-10-27 20:29:50', '2020-10-27 20:29:50'),
(300, 13, 0, 'Consumer', '2020-10-27 20:29:52', '2020-10-27 20:29:52'),
(301, 1, 0, 'Consumer', '2020-10-27 20:45:58', '2020-10-27 20:45:58'),
(302, 1, 0, 'Driver', '2020-10-27 20:46:23', '2020-10-27 20:46:23'),
(303, 1, 0, 'Consumer', '2020-10-27 21:24:59', '2020-10-27 21:24:59'),
(304, 1, 0, 'Consumer', '2020-10-27 21:33:44', '2020-10-27 21:33:44'),
(305, 1, 0, 'Consumer', '2020-10-27 21:33:47', '2020-10-27 21:33:47'),
(306, 1, 0, 'Driver', '2020-10-27 21:34:27', '2020-10-27 21:34:27'),
(307, 1, 0, 'Driver', '2020-10-27 21:35:30', '2020-10-27 21:35:30'),
(308, 13, 0, 'Consumer', '2020-10-27 21:36:55', '2020-10-27 21:36:55'),
(309, 13, 0, 'Consumer', '2020-10-27 23:34:12', '2020-10-27 23:34:12'),
(310, 13, 0, 'Consumer', '2020-10-27 23:51:11', '2020-10-27 23:51:11'),
(311, 13, 0, 'Consumer', '2020-10-28 00:01:10', '2020-10-28 00:01:10'),
(312, 13, 0, 'Consumer', '2020-10-28 13:53:04', '2020-10-28 13:53:04'),
(313, 1, 0, 'Consumer', '2020-10-28 13:58:05', '2020-10-28 13:58:05'),
(314, 1, 0, 'Consumer', '2020-10-28 13:58:18', '2020-10-28 13:58:18'),
(315, 1, 0, 'Consumer', '2020-10-28 13:58:45', '2020-10-28 13:58:45'),
(316, 1, 0, 'Consumer', '2020-10-29 05:12:02', '2020-10-29 05:12:02'),
(317, 1, 0, 'Consumer', '2020-10-30 01:44:21', '2020-10-30 01:44:21'),
(318, 11, 0, 'Consumer', '2020-10-31 10:11:15', '2020-10-31 10:11:15'),
(319, 11, 0, 'Consumer', '2020-10-31 10:21:30', '2020-10-31 10:21:30'),
(320, 11, 0, 'Consumer', '2020-10-31 10:24:00', '2020-10-31 10:24:00'),
(321, 11, 0, 'Consumer', '2020-10-31 10:48:29', '2020-10-31 10:48:29'),
(322, 11, 0, 'Consumer', '2020-10-31 10:55:52', '2020-10-31 10:55:52'),
(323, 1, 0, 'Consumer', '2020-11-01 13:49:25', '2020-11-01 13:49:25'),
(324, 1, 0, 'Consumer', '2020-11-01 13:49:28', '2020-11-01 13:49:28'),
(325, 1, 0, 'Consumer', '2020-11-01 13:58:06', '2020-11-01 13:58:06'),
(326, 1, 0, 'Consumer', '2020-11-01 14:00:28', '2020-11-01 14:00:28'),
(327, 1, 0, 'Consumer', '2020-11-01 14:00:42', '2020-11-01 14:00:42'),
(328, 1, 0, 'Consumer', '2020-11-01 14:07:56', '2020-11-01 14:07:56'),
(329, 1, 0, 'Driver', '2020-11-01 14:08:53', '2020-11-01 14:08:53'),
(330, 1, 0, 'Consumer', '2020-11-01 14:09:00', '2020-11-01 14:09:00'),
(331, 1, 0, 'Consumer', '2020-11-01 14:09:08', '2020-11-01 14:09:08'),
(332, 1, 0, 'Driver', '2020-11-01 14:11:55', '2020-11-01 14:11:55'),
(333, 1, 0, 'Consumer', '2020-11-01 14:16:11', '2020-11-01 14:16:11'),
(334, 1, 0, 'Consumer', '2020-11-01 14:16:54', '2020-11-01 14:16:54'),
(335, 1, 0, 'Consumer', '2020-11-01 14:17:05', '2020-11-01 14:17:05'),
(336, 1, 0, 'Consumer', '2020-11-01 14:20:06', '2020-11-01 14:20:06'),
(337, 1, 0, 'Consumer', '2020-11-01 14:21:04', '2020-11-01 14:21:04'),
(338, 1, 0, 'Consumer', '2020-11-01 14:25:19', '2020-11-01 14:25:19'),
(339, 1, 0, 'Consumer', '2020-11-01 14:26:24', '2020-11-01 14:26:24'),
(340, 1, 0, 'Consumer', '2020-11-01 14:26:27', '2020-11-01 14:26:27'),
(341, 1, 0, 'Consumer', '2020-11-01 14:28:01', '2020-11-01 14:28:01'),
(342, 1, 0, 'Consumer', '2020-11-01 14:28:38', '2020-11-01 14:28:38'),
(343, 1, 0, 'Consumer', '2020-11-01 14:51:11', '2020-11-01 14:51:11'),
(344, 11, 0, 'Consumer', '2020-11-01 15:36:55', '2020-11-01 15:36:55'),
(345, 11, 0, 'Consumer', '2020-11-01 15:41:37', '2020-11-01 15:41:37'),
(346, 1, 0, 'Consumer', '2020-11-01 15:41:42', '2020-11-01 15:41:42'),
(347, 18, 0, 'Consumer', '2020-11-01 15:46:41', '2020-11-01 15:46:41'),
(348, 18, 0, 'Driver', '2020-11-01 15:47:24', '2020-11-01 15:47:24'),
(349, 19, 0, 'Consumer', '2020-11-01 16:18:14', '2020-11-01 16:18:14'),
(350, 19, 0, 'Consumer', '2020-11-01 16:18:29', '2020-11-01 16:18:29'),
(351, 19, 0, 'Consumer', '2020-11-01 16:18:33', '2020-11-01 16:18:33'),
(352, 20, 0, 'Consumer', '2020-11-01 16:19:33', '2020-11-01 16:19:33'),
(353, 9, 0, 'Driver', '2020-11-04 06:49:48', '2020-11-04 06:49:48'),
(354, 9, 0, 'Driver', '2020-11-04 06:50:15', '2020-11-04 06:50:15'),
(355, 9, 0, 'Consumer', '2020-11-04 06:50:20', '2020-11-04 06:50:20'),
(356, 9, 0, 'Consumer', '2020-11-04 06:53:04', '2020-11-04 06:53:04'),
(357, 9, 0, 'Driver', '2020-11-04 06:53:10', '2020-11-04 06:53:10'),
(358, 9, 0, 'Driver', '2020-11-04 06:56:22', '2020-11-04 06:56:22'),
(359, 9, 0, 'Driver', '2020-11-04 06:58:45', '2020-11-04 06:58:45'),
(360, 9, 0, 'Driver', '2020-11-04 07:02:04', '2020-11-04 07:02:04'),
(361, 9, 0, 'Consumer', '2020-11-04 07:02:18', '2020-11-04 07:02:18'),
(362, 9, 0, 'Driver', '2020-11-04 07:03:13', '2020-11-04 07:03:13'),
(363, 9, 0, 'Consumer', '2020-11-04 07:04:19', '2020-11-04 07:04:19'),
(364, 9, 0, 'Driver', '2020-11-04 07:14:07', '2020-11-04 07:14:07'),
(365, 21, 0, 'Consumer', '2020-11-05 16:09:30', '2020-11-05 16:09:30'),
(366, 9, 0, 'Consumer', '2020-11-08 11:05:00', '2020-11-08 11:05:00'),
(367, 9, 0, 'Consumer', '2020-11-08 11:09:59', '2020-11-08 11:09:59'),
(368, 9, 0, 'Consumer', '2020-11-08 11:17:36', '2020-11-08 11:17:36'),
(369, 9, 0, 'Driver', '2020-11-08 12:23:17', '2020-11-08 12:23:17'),
(370, 9, 0, 'Consumer', '2020-11-08 12:31:03', '2020-11-08 12:31:03'),
(371, 9, 0, 'Driver', '2020-11-08 12:43:41', '2020-11-08 12:43:41'),
(372, 9, 0, 'Consumer', '2020-11-08 16:30:18', '2020-11-08 16:30:18'),
(373, 9, 0, 'Driver', '2020-11-08 16:32:38', '2020-11-08 16:32:38'),
(374, 9, 0, 'Driver', '2020-11-08 16:35:16', '2020-11-08 16:35:16'),
(375, 9, 0, 'Consumer', '2020-11-08 16:37:54', '2020-11-08 16:37:54'),
(376, 9, 0, 'Driver', '2020-11-08 16:38:18', '2020-11-08 16:38:18'),
(377, 9, 0, 'Driver', '2020-11-08 16:43:26', '2020-11-08 16:43:26'),
(378, 9, 0, 'Driver', '2020-11-08 16:49:49', '2020-11-08 16:49:49'),
(379, 9, 0, 'Driver', '2020-11-08 16:51:48', '2020-11-08 16:51:48'),
(380, 9, 0, 'Driver', '2020-11-08 16:53:08', '2020-11-08 16:53:08'),
(381, 9, 0, 'Driver', '2020-11-08 16:53:29', '2020-11-08 16:53:29'),
(382, 9, 0, 'Driver', '2020-11-08 16:55:29', '2020-11-08 16:55:29'),
(383, 9, 0, 'Driver', '2020-11-08 16:56:38', '2020-11-08 16:56:38'),
(384, 9, 0, 'Driver', '2020-11-08 17:02:27', '2020-11-08 17:02:27'),
(385, 9, 0, 'Driver', '2020-11-09 04:39:24', '2020-11-09 04:39:24'),
(386, 9, 0, 'Driver', '2020-11-09 04:41:51', '2020-11-09 04:41:51'),
(387, 9, 0, 'Driver', '2020-11-09 04:44:22', '2020-11-09 04:44:22'),
(388, 9, 0, 'Driver', '2020-11-09 05:00:14', '2020-11-09 05:00:14'),
(389, 9, 0, 'Driver', '2020-11-09 05:01:14', '2020-11-09 05:01:14'),
(390, 9, 0, 'Driver', '2020-11-09 05:01:54', '2020-11-09 05:01:54'),
(391, 9, 0, 'Driver', '2020-11-09 05:03:53', '2020-11-09 05:03:53'),
(392, 9, 0, 'Driver', '2020-11-09 05:05:56', '2020-11-09 05:05:56'),
(393, 9, 0, 'Driver', '2020-11-09 05:10:16', '2020-11-09 05:10:16'),
(394, 9, 0, 'Driver', '2020-11-09 05:10:36', '2020-11-09 05:10:36'),
(395, 9, 0, 'Driver', '2020-11-09 05:17:42', '2020-11-09 05:17:42'),
(396, 9, 0, 'Driver', '2020-11-09 06:11:55', '2020-11-09 06:11:55'),
(397, 9, 0, 'Driver', '2020-11-09 06:13:23', '2020-11-09 06:13:23'),
(398, 9, 0, 'Driver', '2020-11-09 06:14:53', '2020-11-09 06:14:53'),
(399, 9, 0, 'Driver', '2020-11-13 15:51:26', '2020-11-13 15:51:26'),
(400, 9, 0, 'Driver', '2020-11-13 15:59:43', '2020-11-13 15:59:43'),
(401, 9, 0, 'Consumer', '2020-11-13 16:02:18', '2020-11-13 16:02:18'),
(402, 9, 0, 'Driver', '2020-11-13 16:04:33', '2020-11-13 16:04:33'),
(403, 9, 0, 'Driver', '2020-11-13 16:10:04', '2020-11-13 16:10:04'),
(404, 9, 0, 'Driver', '2020-11-13 16:11:46', '2020-11-13 16:11:46'),
(405, 9, 0, 'Driver', '2020-11-13 16:12:23', '2020-11-13 16:12:23'),
(406, 9, 0, 'Consumer', '2020-11-13 16:13:14', '2020-11-13 16:13:14'),
(407, 9, 0, 'Driver', '2020-11-13 16:13:31', '2020-11-13 16:13:31'),
(408, 9, 0, 'Driver', '2020-11-13 16:15:02', '2020-11-13 16:15:02'),
(409, 9, 0, 'Driver', '2020-11-13 16:16:16', '2020-11-13 16:16:16'),
(410, 9, 0, 'Driver', '2020-11-13 16:23:38', '2020-11-13 16:23:38'),
(411, 9, 0, 'Driver', '2020-11-13 16:25:18', '2020-11-13 16:25:18'),
(412, 9, 0, 'Driver', '2020-11-13 16:25:51', '2020-11-13 16:25:51'),
(413, 9, 0, 'Driver', '2020-11-13 16:26:44', '2020-11-13 16:26:44'),
(414, 9, 0, 'Driver', '2020-11-13 16:27:21', '2020-11-13 16:27:21'),
(415, 9, 0, 'Driver', '2020-11-13 16:32:25', '2020-11-13 16:32:25'),
(416, 9, 0, 'Driver', '2020-11-13 16:32:40', '2020-11-13 16:32:40'),
(417, 9, 0, 'Driver', '2020-11-13 16:33:22', '2020-11-13 16:33:22'),
(418, 9, 0, 'Driver', '2020-11-13 16:34:00', '2020-11-13 16:34:00'),
(419, 9, 0, 'Driver', '2020-11-13 16:34:34', '2020-11-13 16:34:34'),
(420, 9, 0, 'Driver', '2020-11-13 16:35:21', '2020-11-13 16:35:21'),
(421, 9, 0, 'Driver', '2020-11-13 16:36:07', '2020-11-13 16:36:07'),
(422, 9, 0, 'Driver', '2020-11-13 16:40:07', '2020-11-13 16:40:07'),
(423, 9, 0, 'Driver', '2020-11-13 16:41:44', '2020-11-13 16:41:44'),
(424, 9, 0, 'Driver', '2020-11-13 16:43:05', '2020-11-13 16:43:05'),
(425, 9, 0, 'Driver', '2020-11-13 16:43:59', '2020-11-13 16:43:59'),
(426, 9, 0, 'Driver', '2020-11-13 16:46:22', '2020-11-13 16:46:22'),
(427, 9, 0, 'Consumer', '2020-11-13 16:47:38', '2020-11-13 16:47:38'),
(428, 9, 0, 'Consumer', '2020-11-13 16:50:56', '2020-11-13 16:50:56'),
(429, 9, 0, 'Driver', '2020-11-13 16:52:30', '2020-11-13 16:52:30'),
(430, 9, 0, 'Driver', '2020-11-13 16:53:33', '2020-11-13 16:53:33'),
(431, 9, 0, 'Driver', '2020-11-13 16:56:30', '2020-11-13 16:56:30'),
(432, 9, 0, 'Consumer', '2020-11-13 17:05:50', '2020-11-13 17:05:50'),
(433, 9, 0, 'Driver', '2020-11-13 17:06:14', '2020-11-13 17:06:14'),
(434, 9, 0, 'Driver', '2020-11-13 17:11:39', '2020-11-13 17:11:39'),
(435, 9, 0, 'Driver', '2020-11-13 17:12:45', '2020-11-13 17:12:45'),
(436, 9, 0, 'Driver', '2020-11-18 05:31:19', '2020-11-18 05:31:19'),
(437, 9, 0, 'Driver', '2020-11-24 08:06:27', '2020-11-24 08:06:27'),
(438, 9, 0, 'Driver', '2020-11-24 08:12:23', '2020-11-24 08:12:23'),
(439, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(440, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(441, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(442, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(443, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(444, 9, 0, 'Driver', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(445, 9, 0, 'Consumer', '2020-11-24 08:15:42', '2020-11-24 08:15:42'),
(446, 9, 0, 'Driver', '2020-11-24 08:15:43', '2020-11-24 08:15:43'),
(447, 9, 0, 'Driver', '2020-11-24 08:16:36', '2020-11-24 08:16:36'),
(448, 9, 0, 'Driver', '2020-11-24 08:17:52', '2020-11-24 08:17:52'),
(449, 9, 0, 'Driver', '2020-11-24 08:17:57', '2020-11-24 08:17:57'),
(450, 9, 0, 'Driver', '2020-11-24 08:25:04', '2020-11-24 08:25:04'),
(451, 9, 0, 'Driver', '2020-11-24 08:27:29', '2020-11-24 08:27:29'),
(452, 9, 0, 'Driver', '2020-11-24 08:32:52', '2020-11-24 08:32:52'),
(453, 9, 0, 'Driver', '2020-11-24 09:09:13', '2020-11-24 09:09:13'),
(454, 9, 0, 'Driver', '2020-11-24 09:11:15', '2020-11-24 09:11:15'),
(455, 9, 0, 'Driver', '2020-11-24 09:16:47', '2020-11-24 09:16:47'),
(456, 9, 0, 'Driver', '2020-11-24 09:35:30', '2020-11-24 09:35:30'),
(457, 9, 0, 'Driver', '2020-11-24 09:39:34', '2020-11-24 09:39:34'),
(458, 9, 0, 'Driver', '2020-11-24 09:46:31', '2020-11-24 09:46:31'),
(459, 9, 0, 'Driver', '2020-11-24 09:47:27', '2020-11-24 09:47:27'),
(460, 9, 0, 'Driver', '2020-11-24 10:12:40', '2020-11-24 10:12:40'),
(461, 9, 0, 'Driver', '2020-11-25 15:41:51', '2020-11-25 15:41:51'),
(462, 9, 0, 'Driver', '2020-11-25 15:57:33', '2020-11-25 15:57:33'),
(463, 9, 0, 'Driver', '2020-11-25 16:02:31', '2020-11-25 16:02:31'),
(464, 9, 0, 'Driver', '2020-11-25 16:02:41', '2020-11-25 16:02:41'),
(465, 9, 0, 'Consumer', '2020-11-25 16:13:41', '2020-11-25 16:13:41'),
(466, 9, 0, 'Consumer', '2020-11-25 16:13:41', '2020-11-25 16:13:41'),
(467, 9, 0, 'Consumer', '2020-11-25 16:26:31', '2020-11-25 16:26:31'),
(468, 9, 0, 'Driver', '2020-11-25 16:33:07', '2020-11-25 16:33:07'),
(469, 9, 0, 'Driver', '2020-11-25 16:41:39', '2020-11-25 16:41:39'),
(470, 9, 0, 'Consumer', '2020-11-25 16:41:53', '2020-11-25 16:41:53'),
(471, 9, 0, 'Consumer', '2020-11-25 16:42:35', '2020-11-25 16:42:35'),
(472, 9, 0, 'Driver', '2020-11-25 16:46:42', '2020-11-25 16:46:42'),
(473, 9, 0, 'Driver', '2020-11-25 16:46:43', '2020-11-25 16:46:43'),
(474, 9, 0, 'Driver', '2020-11-25 16:54:11', '2020-11-25 16:54:11'),
(475, 9, 0, 'Consumer', '2020-11-25 16:54:19', '2020-11-25 16:54:19'),
(476, 9, 0, 'Driver', '2020-11-25 16:55:19', '2020-11-25 16:55:19'),
(477, 9, 0, 'Consumer', '2020-11-25 16:57:59', '2020-11-25 16:57:59'),
(478, 9, 0, 'Consumer', '2020-11-25 17:09:48', '2020-11-25 17:09:48'),
(479, 9, 0, 'Consumer', '2020-11-25 17:09:49', '2020-11-25 17:09:49'),
(480, 9, 0, 'Consumer', '2020-11-25 17:13:06', '2020-11-25 17:13:06'),
(481, 9, 0, 'Driver', '2020-11-25 17:16:38', '2020-11-25 17:16:38'),
(482, 9, 0, 'Consumer', '2020-11-25 17:16:38', '2020-11-25 17:16:38'),
(483, 9, 0, 'Driver', '2020-11-25 17:16:38', '2020-11-25 17:16:38'),
(484, 9, 0, 'Driver', '2020-11-25 17:16:39', '2020-11-25 17:16:39'),
(485, 9, 0, 'Driver', '2020-11-25 17:19:37', '2020-11-25 17:19:37'),
(486, 9, 0, 'Consumer', '2020-11-25 17:19:39', '2020-11-25 17:19:39'),
(487, 9, 0, 'Consumer', '2020-11-25 17:19:45', '2020-11-25 17:19:45'),
(488, 9, 0, 'Driver', '2020-11-25 17:20:03', '2020-11-25 17:20:03'),
(489, 9, 0, 'Driver', '2020-11-25 17:20:53', '2020-11-25 17:20:53'),
(490, 9, 0, 'Driver', '2020-11-25 17:27:58', '2020-11-25 17:27:58'),
(491, 9, 0, 'Driver', '2020-11-25 17:31:39', '2020-11-25 17:31:39'),
(492, 9, 0, 'Driver', '2020-11-25 17:31:41', '2020-11-25 17:31:41'),
(493, 9, 0, 'Driver', '2020-11-25 17:31:41', '2020-11-25 17:31:41'),
(494, 9, 0, 'Consumer', '2020-11-25 17:37:01', '2020-11-25 17:37:01'),
(495, 9, 0, 'Driver', '2020-11-25 18:54:45', '2020-11-25 18:54:45'),
(496, 9, 0, 'Consumer', '2020-11-26 16:14:27', '2020-11-26 16:14:27'),
(497, 9, 0, 'Consumer', '2020-11-26 16:52:40', '2020-11-26 16:52:40'),
(498, 9, 0, 'Driver', '2020-11-26 17:07:32', '2020-11-26 17:07:32'),
(499, 9, 0, 'Consumer', '2020-11-26 17:18:07', '2020-11-26 17:18:07'),
(500, 9, 0, 'Consumer', '2020-11-26 17:23:16', '2020-11-26 17:23:16'),
(501, 9, 0, 'Consumer', '2020-11-26 17:24:48', '2020-11-26 17:24:48'),
(502, 9, 0, 'Driver', '2020-11-28 11:39:32', '2020-11-28 11:39:32'),
(503, 9, 0, 'Driver', '2020-11-28 11:41:57', '2020-11-28 11:41:57'),
(504, 9, 0, 'Consumer', '2020-11-28 11:44:14', '2020-11-28 11:44:14'),
(505, 9, 0, 'Consumer', '2020-11-28 13:43:56', '2020-11-28 13:43:56'),
(506, 9, 0, 'Consumer', '2020-11-28 13:45:19', '2020-11-28 13:45:19'),
(507, 9, 0, 'Driver', '2020-11-28 13:46:07', '2020-11-28 13:46:07'),
(508, 9, 0, 'Driver', '2020-11-28 14:14:28', '2020-11-28 14:14:28'),
(509, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(510, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(511, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(512, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(513, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(514, 9, 0, 'Driver', '2020-11-28 15:00:17', '2020-11-28 15:00:17'),
(515, 9, 0, 'Driver', '2020-11-28 15:02:12', '2020-11-28 15:02:12'),
(516, 9, 0, 'Driver', '2020-11-28 15:06:57', '2020-11-28 15:06:57'),
(517, 9, 0, 'Driver', '2020-11-28 15:10:52', '2020-11-28 15:10:52'),
(518, 9, 0, 'Consumer', '2020-11-28 15:44:49', '2020-11-28 15:44:49'),
(519, 9, 0, 'Consumer', '2020-11-29 05:01:49', '2020-11-29 05:01:49'),
(520, 9, 0, 'Driver', '2020-12-03 17:36:23', '2020-12-03 17:36:23'),
(521, 9, 0, 'Consumer', '2020-12-03 17:36:58', '2020-12-03 17:36:58'),
(522, 9, 0, 'Consumer', '2020-12-03 17:37:53', '2020-12-03 17:37:53'),
(523, 9, 0, 'Consumer', '2020-12-03 17:50:55', '2020-12-03 17:50:55'),
(524, 9, 0, 'Consumer', '2020-12-03 17:57:07', '2020-12-03 17:57:07'),
(525, 9, 0, 'Consumer', '2020-12-03 18:17:42', '2020-12-03 18:17:42'),
(526, 9, 0, 'Consumer', '2020-12-03 18:26:44', '2020-12-03 18:26:44'),
(527, 9, 0, 'Consumer', '2020-12-03 18:30:06', '2020-12-03 18:30:06'),
(528, 9, 0, 'Driver', '2020-12-03 18:33:44', '2020-12-03 18:33:44'),
(529, 9, 0, 'Driver', '2020-12-03 18:35:47', '2020-12-03 18:35:47'),
(530, 11, 0, 'Driver', '2020-12-03 18:37:20', '2020-12-03 18:37:20'),
(531, 9, 0, 'Consumer', '2020-12-03 19:03:23', '2020-12-03 19:03:23'),
(532, 9, 0, 'Consumer', '2020-12-03 19:11:23', '2020-12-03 19:11:23'),
(533, 11, 0, 'Driver', '2020-12-03 19:11:43', '2020-12-03 19:11:43'),
(534, 11, 0, 'Driver', '2020-12-03 19:14:47', '2020-12-03 19:14:47'),
(535, 11, 0, 'Driver', '2020-12-03 19:16:47', '2020-12-03 19:16:47'),
(536, 11, 0, 'Driver', '2020-12-03 19:21:22', '2020-12-03 19:21:22'),
(537, 11, 0, 'Driver', '2020-12-03 19:23:36', '2020-12-03 19:23:36'),
(538, 11, 0, 'Driver', '2020-12-03 19:31:54', '2020-12-03 19:31:54'),
(539, 11, 0, 'Driver', '2020-12-03 19:37:23', '2020-12-03 19:37:23'),
(540, 9, 0, 'Driver', '2020-12-03 19:39:01', '2020-12-03 19:39:01'),
(541, 9, 0, 'Driver', '2020-12-03 19:41:36', '2020-12-03 19:41:36'),
(542, 9, 0, 'Consumer', '2020-12-03 19:41:49', '2020-12-03 19:41:49'),
(543, 9, 0, 'Consumer', '2020-12-03 19:46:07', '2020-12-03 19:46:07'),
(544, 9, 0, 'Consumer', '2020-12-03 19:50:05', '2020-12-03 19:50:05'),
(545, 11, 0, 'Driver', '2020-12-03 20:03:23', '2020-12-03 20:03:23'),
(546, 11, 0, 'Driver', '2020-12-03 20:16:17', '2020-12-03 20:16:17'),
(547, 11, 0, 'Consumer', '2020-12-03 20:18:27', '2020-12-03 20:18:27'),
(548, 11, 0, 'Driver', '2020-12-03 20:22:19', '2020-12-03 20:22:19'),
(549, 11, 0, 'Driver', '2020-12-03 20:38:31', '2020-12-03 20:38:31'),
(550, 11, 0, 'Driver', '2020-12-03 20:41:31', '2020-12-03 20:41:31'),
(551, 11, 0, 'Consumer', '2020-12-03 20:49:46', '2020-12-03 20:49:46'),
(552, 11, 0, 'Driver', '2020-12-03 20:50:06', '2020-12-03 20:50:06'),
(553, 11, 0, 'Driver', '2020-12-03 20:52:52', '2020-12-03 20:52:52'),
(554, 11, 0, 'Consumer', '2020-12-03 20:54:59', '2020-12-03 20:54:59'),
(555, 11, 0, 'Driver', '2020-12-03 20:55:44', '2020-12-03 20:55:44'),
(556, 11, 0, 'Driver', '2020-12-03 20:57:44', '2020-12-03 20:57:44'),
(557, 11, 0, 'Driver', '2020-12-03 20:58:56', '2020-12-03 20:58:56'),
(558, 11, 0, 'Driver', '2020-12-03 21:00:49', '2020-12-03 21:00:49'),
(559, 11, 0, 'Consumer', '2020-12-03 21:13:10', '2020-12-03 21:13:10'),
(560, 11, 0, 'Driver', '2020-12-03 21:13:17', '2020-12-03 21:13:17'),
(561, 9, 0, 'Consumer', '2020-12-03 21:13:36', '2020-12-03 21:13:36'),
(562, 9, 0, 'Consumer', '2020-12-03 21:15:32', '2020-12-03 21:15:32'),
(563, 9, 0, 'Consumer', '2020-12-03 21:16:28', '2020-12-03 21:16:28'),
(564, 9, 0, 'Consumer', '2020-12-03 21:28:23', '2020-12-03 21:28:23'),
(565, 9, 0, 'Consumer', '2020-12-03 21:35:21', '2020-12-03 21:35:21'),
(566, 9, 0, 'Consumer', '2020-12-03 21:36:44', '2020-12-03 21:36:44'),
(567, 9, 0, 'Consumer', '2020-12-03 21:37:19', '2020-12-03 21:37:19'),
(568, 9, 0, 'Consumer', '2020-12-03 21:38:08', '2020-12-03 21:38:08'),
(569, 9, 0, 'Driver', '2020-12-03 21:41:49', '2020-12-03 21:41:49'),
(570, 9, 0, 'Consumer', '2020-12-03 21:41:57', '2020-12-03 21:41:57'),
(571, 9, 0, 'Consumer', '2020-12-03 21:42:36', '2020-12-03 21:42:36'),
(572, 9, 0, 'Consumer', '2020-12-03 21:43:32', '2020-12-03 21:43:32'),
(573, 9, 0, 'Consumer', '2020-12-03 21:50:23', '2020-12-03 21:50:23'),
(574, 9, 0, 'Consumer', '2020-12-03 21:51:19', '2020-12-03 21:51:19'),
(575, 9, 0, 'Consumer', '2020-12-03 21:52:24', '2020-12-03 21:52:24'),
(576, 9, 0, 'Consumer', '2020-12-03 21:55:56', '2020-12-03 21:55:56'),
(577, 9, 0, 'Consumer', '2020-12-03 21:57:17', '2020-12-03 21:57:17'),
(578, 9, 0, 'Consumer', '2020-12-03 22:02:28', '2020-12-03 22:02:28'),
(579, 9, 0, 'Consumer', '2020-12-03 22:03:13', '2020-12-03 22:03:13'),
(580, 9, 0, 'Consumer', '2020-12-03 22:04:56', '2020-12-03 22:04:56'),
(581, 9, 0, 'Consumer', '2020-12-03 22:17:15', '2020-12-03 22:17:15'),
(582, 9, 0, 'Consumer', '2020-12-03 22:18:03', '2020-12-03 22:18:03'),
(583, 9, 0, 'Consumer', '2020-12-03 22:23:17', '2020-12-03 22:23:17'),
(584, 9, 0, 'Consumer', '2020-12-03 22:24:08', '2020-12-03 22:24:08'),
(585, 9, 0, 'Consumer', '2020-12-03 22:24:51', '2020-12-03 22:24:51'),
(586, 9, 0, 'Consumer', '2020-12-03 22:25:23', '2020-12-03 22:25:23'),
(587, 9, 0, 'Consumer', '2020-12-03 22:26:53', '2020-12-03 22:26:53'),
(588, 9, 0, 'Consumer', '2020-12-03 22:27:23', '2020-12-03 22:27:23'),
(589, 9, 0, 'Consumer', '2020-12-03 22:28:32', '2020-12-03 22:28:32'),
(590, 9, 0, 'Consumer', '2020-12-03 22:31:28', '2020-12-03 22:31:28'),
(591, 9, 0, 'Consumer', '2020-12-03 22:34:48', '2020-12-03 22:34:48'),
(592, 9, 0, 'Consumer', '2020-12-03 22:37:34', '2020-12-03 22:37:34'),
(593, 9, 0, 'Consumer', '2020-12-03 22:37:47', '2020-12-03 22:37:47'),
(594, 9, 0, 'Consumer', '2020-12-17 10:17:14', '2020-12-17 10:17:14'),
(595, 9, 0, 'Consumer', '2020-12-17 10:21:09', '2020-12-17 10:21:09'),
(596, 9, 0, 'Consumer', '2020-12-17 10:21:55', '2020-12-17 10:21:55'),
(597, 9, 0, 'Consumer', '2020-12-17 10:23:52', '2020-12-17 10:23:52'),
(598, 9, 0, 'Consumer', '2020-12-17 10:25:55', '2020-12-17 10:25:55'),
(599, 9, 0, 'Consumer', '2020-12-17 10:27:06', '2020-12-17 10:27:06'),
(600, 9, 0, 'Consumer', '2020-12-17 10:27:56', '2020-12-17 10:27:56'),
(601, 9, 0, 'Consumer', '2020-12-17 10:32:12', '2020-12-17 10:32:12'),
(602, 9, 0, 'Consumer', '2020-12-17 10:35:15', '2020-12-17 10:35:15'),
(603, 9, 0, 'Consumer', '2020-12-17 10:37:49', '2020-12-17 10:37:49'),
(604, 9, 0, 'Consumer', '2020-12-17 10:39:50', '2020-12-17 10:39:50'),
(605, 9, 0, 'Consumer', '2020-12-17 10:44:48', '2020-12-17 10:44:48'),
(606, 9, 0, 'Consumer', '2020-12-17 10:47:14', '2020-12-17 10:47:14'),
(607, 9, 0, 'Consumer', '2020-12-17 10:48:09', '2020-12-17 10:48:09'),
(608, 9, 0, 'Consumer', '2020-12-17 10:50:28', '2020-12-17 10:50:28'),
(609, 9, 0, 'Consumer', '2020-12-17 10:51:34', '2020-12-17 10:51:34'),
(610, 9, 0, 'Consumer', '2020-12-17 11:04:01', '2020-12-17 11:04:01'),
(611, 9, 0, 'Consumer', '2020-12-17 11:09:35', '2020-12-17 11:09:35'),
(612, 9, 0, 'Consumer', '2020-12-17 11:11:29', '2020-12-17 11:11:29'),
(613, 9, 0, 'Consumer', '2020-12-17 11:12:57', '2020-12-17 11:12:57'),
(614, 9, 0, 'Consumer', '2020-12-17 11:17:56', '2020-12-17 11:17:56'),
(615, 9, 0, 'Consumer', '2020-12-17 11:20:43', '2020-12-17 11:20:43'),
(616, 9, 0, 'Consumer', '2020-12-17 11:22:18', '2020-12-17 11:22:18'),
(617, 9, 0, 'Driver', '2020-12-17 11:22:55', '2020-12-17 11:22:55'),
(618, 9, 0, 'Consumer', '2020-12-17 11:28:25', '2020-12-17 11:28:25'),
(619, 9, 0, 'Consumer', '2020-12-17 11:41:57', '2020-12-17 11:41:57'),
(620, 9, 0, 'Consumer', '2020-12-17 11:44:26', '2020-12-17 11:44:26'),
(621, 9, 0, 'Consumer', '2020-12-17 11:50:39', '2020-12-17 11:50:39'),
(622, 9, 0, 'Consumer', '2020-12-17 11:54:58', '2020-12-17 11:54:58'),
(623, 9, 0, 'Consumer', '2020-12-17 11:58:50', '2020-12-17 11:58:50'),
(624, 9, 0, 'Consumer', '2020-12-17 11:58:50', '2020-12-17 11:58:50'),
(625, 9, 0, 'Consumer', '2020-12-17 12:04:56', '2020-12-17 12:04:56'),
(626, 9, 0, 'Consumer', '2020-12-17 12:14:14', '2020-12-17 12:14:14'),
(627, 9, 0, 'Consumer', '2020-12-17 12:15:25', '2020-12-17 12:15:25'),
(628, 9, 0, 'Consumer', '2020-12-17 12:16:15', '2020-12-17 12:16:15'),
(629, 9, 0, 'Consumer', '2020-12-17 12:17:22', '2020-12-17 12:17:22'),
(630, 9, 0, 'Consumer', '2020-12-17 12:19:17', '2020-12-17 12:19:17'),
(631, 9, 0, 'Consumer', '2020-12-17 12:21:18', '2020-12-17 12:21:18'),
(632, 9, 0, 'Consumer', '2020-12-17 12:24:27', '2020-12-17 12:24:27'),
(633, 9, 0, 'Consumer', '2020-12-17 12:27:21', '2020-12-17 12:27:21'),
(634, 9, 0, 'Driver', '2020-12-17 12:28:45', '2020-12-17 12:28:45'),
(635, 9, 0, 'Consumer', '2020-12-17 12:29:02', '2020-12-17 12:29:02'),
(636, 9, 0, 'Consumer', '2020-12-17 12:33:31', '2020-12-17 12:33:31'),
(637, 9, 0, 'Consumer', '2020-12-17 12:36:36', '2020-12-17 12:36:36'),
(638, 9, 0, 'Consumer', '2020-12-17 12:37:42', '2020-12-17 12:37:42'),
(639, 9, 3, 'Driver', '2022-05-13 11:31:29', '2022-05-13 11:31:29'),
(640, 9, 4, 'Consumer', '2022-05-13 11:32:41', '2022-05-13 11:32:41'),
(641, 9, 4, 'Consumer', '2022-05-13 11:32:47', '2022-05-13 11:32:47'),
(642, 9, 4, 'Consumer', '2022-05-13 11:32:48', '2022-05-13 11:32:48'),
(643, 9, 4, 'Consumer', '2022-05-13 11:32:49', '2022-05-13 11:32:49'),
(644, 9, 4, 'Consumer', '2022-05-13 11:33:00', '2022-05-13 11:33:00'),
(645, 9, 3, 'Driver', '2022-05-13 11:33:03', '2022-05-13 11:33:03'),
(646, 9, 3, 'Driver', '2022-05-13 11:42:02', '2022-05-13 11:42:02'),
(647, 5, 3, 'Driver', '2022-05-15 12:17:48', '2022-05-15 12:17:48'),
(648, 5, 4, 'Consumer', '2022-05-15 12:17:52', '2022-05-15 12:17:52'),
(649, 5, 3, 'Driver', '2022-05-15 12:23:29', '2022-05-15 12:23:29'),
(650, 5, 4, 'Consumer', '2022-05-15 12:24:33', '2022-05-15 12:24:33'),
(651, 5, 3, 'Driver', '2022-05-15 12:26:47', '2022-05-15 12:26:47'),
(652, 5, 3, 'Driver', '2022-05-15 12:27:50', '2022-05-15 12:27:50'),
(653, 9, 3, 'Driver', '2022-05-16 05:24:22', '2022-05-16 05:24:22'),
(654, 9, 3, 'Driver', '2022-05-16 05:35:46', '2022-05-16 05:35:46'),
(655, 9, 3, 'Driver', '2022-05-16 05:37:48', '2022-05-16 05:37:48'),
(656, 9, 3, 'Driver', '2022-05-16 05:39:00', '2022-05-16 05:39:00'),
(657, 9, 3, 'Driver', '2022-05-16 05:41:15', '2022-05-16 05:41:15'),
(658, 9, 3, 'Driver', '2022-05-16 05:41:27', '2022-05-16 05:41:27'),
(659, 9, 3, 'Driver', '2022-05-16 05:43:08', '2022-05-16 05:43:08'),
(660, 9, 3, 'Driver', '2022-05-16 05:45:03', '2022-05-16 05:45:03'),
(661, 9, 3, 'Driver', '2022-05-16 05:45:50', '2022-05-16 05:45:50'),
(662, 9, 3, 'Driver', '2022-05-16 05:46:25', '2022-05-16 05:46:25'),
(663, 9, 3, 'Driver', '2022-05-16 05:47:35', '2022-05-16 05:47:35'),
(664, 9, 3, 'Driver', '2022-05-16 05:48:02', '2022-05-16 05:48:02'),
(665, 9, 3, 'Driver', '2022-05-16 05:49:19', '2022-05-16 05:49:19'),
(666, 9, 3, 'Driver', '2022-05-16 05:51:12', '2022-05-16 05:51:12'),
(667, 9, 3, 'Driver', '2022-05-16 05:51:40', '2022-05-16 05:51:40'),
(668, 9, 3, 'Driver', '2022-05-16 05:52:15', '2022-05-16 05:52:15'),
(669, 9, 3, 'Driver', '2022-05-16 05:52:25', '2022-05-16 05:52:25'),
(670, 9, 3, 'Driver', '2022-05-16 06:03:37', '2022-05-16 06:03:37'),
(671, 9, 3, 'Driver', '2022-05-16 06:04:09', '2022-05-16 06:04:09'),
(672, 9, 3, 'Driver', '2022-05-16 06:04:53', '2022-05-16 06:04:53'),
(673, 9, 3, 'Driver', '2022-05-16 06:05:13', '2022-05-16 06:05:13'),
(674, 9, 3, 'Driver', '2022-05-16 06:05:30', '2022-05-16 06:05:30'),
(675, 9, 3, 'Driver', '2022-05-16 06:06:19', '2022-05-16 06:06:19'),
(676, 9, 3, 'Driver', '2022-05-16 06:07:14', '2022-05-16 06:07:14'),
(677, 9, 3, 'Driver', '2022-05-16 06:09:16', '2022-05-16 06:09:16'),
(678, 10, 3, 'Driver', '2022-05-16 06:14:07', '2022-05-16 06:14:07'),
(679, 10, 3, 'Driver', '2022-05-16 06:14:41', '2022-05-16 06:14:41'),
(680, 10, 3, 'Driver', '2022-05-16 06:15:46', '2022-05-16 06:15:46'),
(681, 10, 3, 'Driver', '2022-05-16 06:16:00', '2022-05-16 06:16:00'),
(682, 10, 3, 'Driver', '2022-05-16 06:16:50', '2022-05-16 06:16:50'),
(683, 10, 3, 'Driver', '2022-05-16 06:17:46', '2022-05-16 06:17:46'),
(684, 10, 4, 'Consumer', '2022-05-16 06:18:34', '2022-05-16 06:18:34'),
(685, 10, 3, 'Driver', '2022-05-16 06:18:51', '2022-05-16 06:18:51'),
(686, 10, 4, 'Consumer', '2022-05-16 06:20:15', '2022-05-16 06:20:15'),
(687, 9, 3, 'Driver', '2022-05-16 06:20:43', '2022-05-16 06:20:43'),
(688, 9, 4, 'Consumer', '2022-05-16 06:21:33', '2022-05-16 06:21:33'),
(689, 9, 3, 'Driver', '2022-05-16 06:21:45', '2022-05-16 06:21:45'),
(690, 9, 3, 'Driver', '2022-05-16 06:22:13', '2022-05-16 06:22:13'),
(691, 9, 3, 'Driver', '2022-05-16 06:22:41', '2022-05-16 06:22:41'),
(692, 9, 3, 'Driver', '2022-05-16 06:22:47', '2022-05-16 06:22:47'),
(693, 9, 3, 'Driver', '2022-05-16 06:22:56', '2022-05-16 06:22:56'),
(694, 9, 3, 'Driver', '2022-05-16 06:23:42', '2022-05-16 06:23:42'),
(695, 9, 3, 'Driver', '2022-05-16 06:27:02', '2022-05-16 06:27:02'),
(696, 9, 3, 'Driver', '2022-05-16 06:27:05', '2022-05-16 06:27:05'),
(697, 9, 4, 'Consumer', '2022-05-16 06:27:08', '2022-05-16 06:27:08'),
(698, 9, 3, 'Driver', '2022-05-16 06:28:26', '2022-05-16 06:28:26'),
(699, 10, 3, 'Driver', '2022-05-16 06:29:37', '2022-05-16 06:29:37'),
(700, 10, 3, 'Driver', '2022-05-16 06:30:05', '2022-05-16 06:30:05'),
(701, 10, 3, 'Driver', '2022-05-16 06:31:12', '2022-05-16 06:31:12'),
(702, 10, 3, 'Driver', '2022-05-16 06:32:39', '2022-05-16 06:32:39'),
(703, 10, 3, 'Driver', '2022-05-16 06:35:03', '2022-05-16 06:35:03'),
(704, 10, 3, 'Driver', '2022-05-16 06:35:07', '2022-05-16 06:35:07'),
(705, 10, 3, 'Driver', '2022-05-16 06:35:53', '2022-05-16 06:35:53'),
(706, 10, 3, 'Driver', '2022-05-16 06:36:03', '2022-05-16 06:36:03'),
(707, 10, 3, 'Driver', '2022-05-16 06:37:24', '2022-05-16 06:37:24'),
(708, 10, 3, 'Driver', '2022-05-16 06:39:35', '2022-05-16 06:39:35'),
(709, 9, 3, 'Driver', '2022-05-16 06:41:18', '2022-05-16 06:41:18'),
(710, 9, 3, 'Driver', '2022-05-16 06:43:43', '2022-05-16 06:43:43'),
(711, 9, 3, 'Driver', '2022-05-16 06:44:01', '2022-05-16 06:44:01'),
(712, 9, 3, 'Driver', '2022-05-16 06:45:08', '2022-05-16 06:45:08'),
(713, 9, 3, 'Driver', '2022-05-16 06:46:35', '2022-05-16 06:46:35'),
(714, 9, 3, 'Driver', '2022-05-16 06:51:38', '2022-05-16 06:51:38'),
(715, 9, 3, 'Driver', '2022-05-16 06:52:36', '2022-05-16 06:52:36'),
(716, 9, 3, 'Driver', '2022-05-16 06:53:35', '2022-05-16 06:53:35'),
(717, 9, 3, 'Driver', '2022-05-16 06:54:03', '2022-05-16 06:54:03'),
(718, 9, 3, 'Driver', '2022-05-16 06:55:18', '2022-05-16 06:55:18'),
(719, 9, 3, 'Driver', '2022-05-16 06:56:11', '2022-05-16 06:56:11'),
(720, 9, 3, 'Driver', '2022-05-16 07:16:47', '2022-05-16 07:16:47'),
(721, 9, 3, 'Driver', '2022-05-16 07:19:08', '2022-05-16 07:19:08'),
(722, 9, 3, 'Driver', '2022-05-16 07:20:28', '2022-05-16 07:20:28'),
(723, 9, 3, 'Driver', '2022-05-16 07:24:01', '2022-05-16 07:24:01'),
(724, 9, 3, 'Driver', '2022-05-16 07:25:59', '2022-05-16 07:25:59'),
(725, 9, 3, 'Driver', '2022-05-16 08:35:44', '2022-05-16 08:35:44'),
(726, 9, 3, 'Driver', '2022-05-16 09:13:41', '2022-05-16 09:13:41'),
(727, 9, 3, 'Driver', '2022-05-16 09:35:17', '2022-05-16 09:35:17'),
(728, 9, 3, 'Driver', '2022-05-16 10:17:58', '2022-05-16 10:17:58'),
(729, 9, 3, 'Driver', '2022-05-16 10:22:33', '2022-05-16 10:22:33'),
(730, 9, 3, 'Driver', '2022-05-16 10:27:58', '2022-05-16 10:27:58'),
(731, 9, 3, 'Driver', '2022-05-16 10:53:32', '2022-05-16 10:53:32'),
(732, 9, 3, 'Driver', '2022-05-16 11:28:18', '2022-05-16 11:28:18');
INSERT INTO `runtime_roles` (`id`, `user_id`, `role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(733, 9, 3, 'Driver', '2022-05-16 11:28:34', '2022-05-16 11:28:34'),
(734, 9, 3, 'Driver', '2022-05-16 11:30:13', '2022-05-16 11:30:13'),
(735, 9, 3, 'Driver', '2022-05-16 11:33:01', '2022-05-16 11:33:01'),
(736, 9, 3, 'Driver', '2022-05-16 11:33:34', '2022-05-16 11:33:34'),
(737, 9, 3, 'Driver', '2022-05-16 11:34:38', '2022-05-16 11:34:38'),
(738, 9, 3, 'Driver', '2022-05-16 11:37:44', '2022-05-16 11:37:44'),
(739, 9, 3, 'Driver', '2022-05-16 11:38:36', '2022-05-16 11:38:36'),
(740, 9, 3, 'Driver', '2022-05-16 11:40:09', '2022-05-16 11:40:09'),
(741, 9, 3, 'Driver', '2022-05-16 11:43:02', '2022-05-16 11:43:02'),
(742, 9, 3, 'Driver', '2022-05-16 11:46:40', '2022-05-16 11:46:40'),
(743, 9, 3, 'Driver', '2022-05-16 11:48:02', '2022-05-16 11:48:02'),
(744, 9, 3, 'Driver', '2022-05-16 11:55:32', '2022-05-16 11:55:32'),
(745, 9, 3, 'Driver', '2022-05-16 12:08:12', '2022-05-16 12:08:12'),
(746, 9, 3, 'Driver', '2022-05-16 12:11:02', '2022-05-16 12:11:02'),
(747, 9, 3, 'Driver', '2022-05-16 12:50:25', '2022-05-16 12:50:25'),
(748, 9, 3, 'Driver', '2022-05-16 12:51:05', '2022-05-16 12:51:05'),
(749, 9, 3, 'Driver', '2022-05-16 12:53:32', '2022-05-16 12:53:32'),
(750, 9, 3, 'Driver', '2022-05-16 12:54:50', '2022-05-16 12:54:50'),
(751, 9, 4, 'Consumer', '2022-05-16 12:55:24', '2022-05-16 12:55:24'),
(752, 9, 3, 'Driver', '2022-05-16 12:55:28', '2022-05-16 12:55:28'),
(753, 9, 3, 'Driver', '2022-05-16 12:55:58', '2022-05-16 12:55:58'),
(754, 9, 3, 'Driver', '2022-05-16 12:57:21', '2022-05-16 12:57:21'),
(755, 9, 3, 'Driver', '2022-05-16 12:57:51', '2022-05-16 12:57:51'),
(756, 9, 4, 'Consumer', '2022-05-16 12:57:55', '2022-05-16 12:57:55'),
(757, 9, 3, 'Driver', '2022-05-16 12:58:05', '2022-05-16 12:58:05'),
(758, 9, 3, 'Driver', '2022-05-16 12:59:36', '2022-05-16 12:59:36'),
(759, 9, 3, 'Driver', '2022-05-16 13:01:04', '2022-05-16 13:01:04'),
(760, 9, 3, 'Driver', '2022-05-16 13:01:25', '2022-05-16 13:01:25'),
(761, 9, 3, 'Driver', '2022-05-16 13:02:50', '2022-05-16 13:02:50'),
(762, 9, 3, 'Driver', '2022-05-16 13:03:17', '2022-05-16 13:03:17'),
(763, 9, 3, 'Driver', '2022-05-16 13:03:45', '2022-05-16 13:03:45'),
(764, 9, 3, 'Driver', '2022-05-16 13:11:20', '2022-05-16 13:11:20'),
(765, 9, 3, 'Driver', '2022-05-16 13:14:42', '2022-05-16 13:14:42'),
(766, 9, 3, 'Driver', '2022-05-16 13:15:37', '2022-05-16 13:15:37'),
(767, 9, 3, 'Driver', '2022-05-16 13:17:27', '2022-05-16 13:17:27'),
(768, 9, 3, 'Driver', '2022-05-16 13:19:19', '2022-05-16 13:19:19'),
(769, 9, 3, 'Driver', '2022-05-16 13:21:04', '2022-05-16 13:21:04'),
(770, 9, 3, 'Driver', '2022-05-16 13:21:07', '2022-05-16 13:21:07'),
(771, 10, 3, 'Driver', '2022-05-16 13:21:24', '2022-05-16 13:21:24'),
(772, 10, 3, 'Driver', '2022-05-16 13:21:57', '2022-05-16 13:21:57'),
(773, 9, 3, 'Driver', '2022-05-16 13:24:33', '2022-05-16 13:24:33'),
(774, 9, 3, 'Driver', '2022-05-16 13:28:13', '2022-05-16 13:28:13'),
(775, 9, 3, 'Driver', '2022-05-16 13:30:50', '2022-05-16 13:30:50'),
(776, 9, 3, 'Driver', '2022-05-16 13:31:04', '2022-05-16 13:31:04'),
(777, 11, 3, 'Driver', '2022-05-17 09:49:30', '2022-05-17 09:49:30'),
(778, 11, 3, 'Driver', '2022-05-17 09:55:37', '2022-05-17 09:55:37'),
(779, 11, 3, 'Driver', '2022-05-17 09:59:42', '2022-05-17 09:59:42'),
(780, 11, 3, 'Driver', '2022-05-17 10:01:27', '2022-05-17 10:01:27'),
(781, 11, 3, 'Driver', '2022-05-17 10:07:59', '2022-05-17 10:07:59'),
(782, 11, 3, 'Driver', '2022-05-17 10:11:22', '2022-05-17 10:11:22'),
(783, 11, 3, 'Driver', '2022-05-17 10:15:11', '2022-05-17 10:15:11'),
(784, 11, 4, 'Consumer', '2022-05-17 10:16:02', '2022-05-17 10:16:02'),
(785, 11, 3, 'Driver', '2022-05-17 10:28:23', '2022-05-17 10:28:23'),
(786, 11, 3, 'Driver', '2022-05-17 10:28:35', '2022-05-17 10:28:35'),
(787, 11, 3, 'Driver', '2022-05-17 10:30:46', '2022-05-17 10:30:46'),
(788, 11, 4, 'Consumer', '2022-05-17 10:55:12', '2022-05-17 10:55:12'),
(789, 11, 3, 'Driver', '2022-05-17 11:28:55', '2022-05-17 11:28:55'),
(790, 11, 3, 'Driver', '2022-05-17 11:30:18', '2022-05-17 11:30:18'),
(791, 11, 3, 'Driver', '2022-05-17 11:31:49', '2022-05-17 11:31:49'),
(792, 11, 3, 'Driver', '2022-05-17 11:35:10', '2022-05-17 11:35:10'),
(793, 11, 3, 'Driver', '2022-05-17 11:36:43', '2022-05-17 11:36:43'),
(794, 11, 3, 'Driver', '2022-05-17 11:38:33', '2022-05-17 11:38:33'),
(795, 11, 3, 'Driver', '2022-05-17 11:40:59', '2022-05-17 11:40:59'),
(796, 11, 3, 'Driver', '2022-05-17 11:41:01', '2022-05-17 11:41:01'),
(797, 9, 3, 'Driver', '2022-05-17 11:41:39', '2022-05-17 11:41:39'),
(798, 9, 4, 'Consumer', '2022-05-17 11:41:45', '2022-05-17 11:41:45'),
(799, 9, 3, 'Driver', '2022-05-17 11:41:51', '2022-05-17 11:41:51'),
(800, 4, 3, 'Driver', '2022-05-17 11:43:28', '2022-05-17 11:43:28'),
(801, 4, 4, 'Consumer', '2022-05-17 11:43:31', '2022-05-17 11:43:31'),
(802, 4, 3, 'Driver', '2022-05-17 11:43:36', '2022-05-17 11:43:36'),
(803, 4, 3, 'Driver', '2022-05-17 11:43:39', '2022-05-17 11:43:39'),
(804, 9, 3, 'Driver', '2022-05-17 11:44:10', '2022-05-17 11:44:10'),
(805, 9, 3, 'Driver', '2022-05-17 11:45:42', '2022-05-17 11:45:42'),
(806, 9, 4, 'Consumer', '2022-05-17 11:46:51', '2022-05-17 11:46:51'),
(807, 9, 3, 'Driver', '2022-05-17 11:49:21', '2022-05-17 11:49:21'),
(808, 9, 3, 'Driver', '2022-05-17 11:51:01', '2022-05-17 11:51:01'),
(809, 9, 3, 'Driver', '2022-05-17 11:52:53', '2022-05-17 11:52:53'),
(810, 9, 3, 'Driver', '2022-05-17 11:53:08', '2022-05-17 11:53:08'),
(811, 9, 3, 'Driver', '2022-05-17 11:54:30', '2022-05-17 11:54:30'),
(812, 9, 3, 'Driver', '2022-05-17 11:55:54', '2022-05-17 11:55:54'),
(813, 9, 3, 'Driver', '2022-05-17 11:56:13', '2022-05-17 11:56:13'),
(814, 9, 3, 'Driver', '2022-05-17 11:59:18', '2022-05-17 11:59:18'),
(815, 9, 3, 'Driver', '2022-05-17 12:18:05', '2022-05-17 12:18:05'),
(816, 9, 3, 'Driver', '2022-05-17 12:19:41', '2022-05-17 12:19:41'),
(817, 9, 3, 'Driver', '2022-05-17 12:22:54', '2022-05-17 12:22:54'),
(818, 9, 3, 'Driver', '2022-05-17 12:24:46', '2022-05-17 12:24:46'),
(819, 9, 3, 'Driver', '2022-05-17 13:09:01', '2022-05-17 13:09:01'),
(820, 9, 3, 'Driver', '2022-05-17 13:18:44', '2022-05-17 13:18:44'),
(821, 9, 3, 'Driver', '2022-05-17 13:26:09', '2022-05-17 13:26:09'),
(822, 9, 3, 'Driver', '2022-05-18 02:45:10', '2022-05-18 02:45:10'),
(823, 9, 3, 'Driver', '2022-05-18 03:26:47', '2022-05-18 03:26:47'),
(824, 9, 3, 'Driver', '2022-05-18 05:48:30', '2022-05-18 05:48:30'),
(825, 9, 3, 'Driver', '2022-05-18 05:49:24', '2022-05-18 05:49:24'),
(826, 9, 3, 'Driver', '2022-05-18 05:55:54', '2022-05-18 05:55:54'),
(827, 9, 3, 'Driver', '2022-05-18 05:58:34', '2022-05-18 05:58:34'),
(828, 9, 3, 'Driver', '2022-05-18 08:15:32', '2022-05-18 08:15:32'),
(829, 9, 3, 'Driver', '2022-05-18 08:22:52', '2022-05-18 08:22:52'),
(830, 9, 3, 'Driver', '2022-05-18 08:26:16', '2022-05-18 08:26:16'),
(831, 9, 3, 'Driver', '2022-05-18 08:26:23', '2022-05-18 08:26:23'),
(832, 9, 3, 'Driver', '2022-05-18 08:26:37', '2022-05-18 08:26:37'),
(833, 12, 3, 'Driver', '2022-05-18 08:28:26', '2022-05-18 08:28:26'),
(834, 12, 3, 'Driver', '2022-05-18 10:21:10', '2022-05-18 10:21:10'),
(835, 12, 3, 'Driver', '2022-05-18 10:37:30', '2022-05-18 10:37:30'),
(836, 12, 3, 'Driver', '2022-05-18 10:40:08', '2022-05-18 10:40:08'),
(837, 12, 3, 'Driver', '2022-05-18 10:41:24', '2022-05-18 10:41:24'),
(838, 12, 3, 'Driver', '2022-05-18 10:42:01', '2022-05-18 10:42:01'),
(839, 12, 3, 'Driver', '2022-05-18 10:44:48', '2022-05-18 10:44:48'),
(840, 12, 3, 'Driver', '2022-05-18 10:45:30', '2022-05-18 10:45:30'),
(841, 12, 3, 'Driver', '2022-05-18 10:49:21', '2022-05-18 10:49:21'),
(842, 12, 3, 'Driver', '2022-05-18 10:51:30', '2022-05-18 10:51:30'),
(843, 12, 3, 'Driver', '2022-05-18 10:55:31', '2022-05-18 10:55:31'),
(844, 12, 4, 'Consumer', '2022-05-18 10:57:23', '2022-05-18 10:57:23'),
(845, 12, 3, 'Driver', '2022-05-18 10:57:31', '2022-05-18 10:57:31'),
(846, 12, 3, 'Driver', '2022-05-18 11:01:44', '2022-05-18 11:01:44'),
(847, 12, 3, 'Driver', '2022-05-18 11:20:22', '2022-05-18 11:20:22'),
(848, 12, 3, 'Driver', '2022-05-18 11:21:27', '2022-05-18 11:21:27'),
(849, 9, 3, 'Driver', '2022-05-18 11:22:08', '2022-05-18 11:22:08'),
(850, 9, 3, 'Driver', '2022-05-18 11:38:04', '2022-05-18 11:38:04'),
(851, 9, 3, 'Driver', '2022-05-18 11:39:35', '2022-05-18 11:39:35'),
(852, 9, 4, 'Consumer', '2022-05-18 11:44:21', '2022-05-18 11:44:21'),
(853, 10, 3, 'Driver', '2022-05-18 12:45:49', '2022-05-18 12:45:49'),
(854, 10, 3, 'Driver', '2022-05-18 12:51:29', '2022-05-18 12:51:29'),
(855, 10, 3, 'Driver', '2022-05-18 12:51:38', '2022-05-18 12:51:38'),
(856, 10, 3, 'Driver', '2022-05-18 12:53:05', '2022-05-18 12:53:05'),
(857, 10, 3, 'Driver', '2022-05-18 12:54:42', '2022-05-18 12:54:42'),
(858, 10, 3, 'Driver', '2022-05-19 03:56:08', '2022-05-19 03:56:08'),
(859, 13, 3, 'Driver', '2022-05-19 03:57:39', '2022-05-19 03:57:39'),
(860, 13, 3, 'Driver', '2022-05-19 03:57:47', '2022-05-19 03:57:47'),
(861, 13, 3, 'Driver', '2022-05-19 03:57:58', '2022-05-19 03:57:58'),
(862, 13, 3, 'Driver', '2022-05-19 03:58:35', '2022-05-19 03:58:35'),
(863, 13, 3, 'Driver', '2022-05-19 03:58:41', '2022-05-19 03:58:41'),
(864, 13, 3, 'Driver', '2022-05-19 04:08:19', '2022-05-19 04:08:19'),
(865, 13, 3, 'Driver', '2022-05-19 04:10:37', '2022-05-19 04:10:37'),
(866, 13, 3, 'Driver', '2022-05-19 04:10:42', '2022-05-19 04:10:42'),
(867, 13, 3, 'Driver', '2022-05-19 04:31:30', '2022-05-19 04:31:30'),
(868, 13, 3, 'Driver', '2022-05-19 05:53:00', '2022-05-19 05:53:00'),
(869, 13, 3, 'Driver', '2022-05-19 05:53:21', '2022-05-19 05:53:21'),
(870, 13, 3, 'Driver', '2022-05-19 06:13:18', '2022-05-19 06:13:18'),
(871, 13, 3, 'Driver', '2022-05-19 06:14:14', '2022-05-19 06:14:14'),
(872, 13, 3, 'Driver', '2022-05-19 06:15:21', '2022-05-19 06:15:21'),
(873, 13, 4, 'Consumer', '2022-05-19 06:21:33', '2022-05-19 06:21:33'),
(874, 13, 3, 'Driver', '2022-05-19 06:33:45', '2022-05-19 06:33:45'),
(875, 13, 4, 'Consumer', '2022-05-19 06:34:34', '2022-05-19 06:34:34'),
(876, 13, 4, 'Consumer', '2022-05-19 06:38:44', '2022-05-19 06:38:44'),
(877, 13, 4, 'Consumer', '2022-05-19 06:59:39', '2022-05-19 06:59:39'),
(878, 13, 4, 'Consumer', '2022-05-19 07:09:49', '2022-05-19 07:09:49'),
(879, 13, 4, 'Consumer', '2022-05-19 07:13:36', '2022-05-19 07:13:36'),
(880, 13, 3, 'Driver', '2022-05-19 07:25:35', '2022-05-19 07:25:35'),
(881, 13, 4, 'Consumer', '2022-05-19 07:43:40', '2022-05-19 07:43:40'),
(882, 13, 3, 'Driver', '2022-05-19 08:07:57', '2022-05-19 08:07:57'),
(883, 13, 3, 'Driver', '2022-05-19 08:11:36', '2022-05-19 08:11:36'),
(884, 13, 3, 'Driver', '2022-05-19 08:41:10', '2022-05-19 08:41:10'),
(885, 13, 3, 'Driver', '2022-05-19 08:41:15', '2022-05-19 08:41:15'),
(886, 13, 3, 'Driver', '2022-05-19 08:41:17', '2022-05-19 08:41:17'),
(887, 13, 4, 'Consumer', '2022-05-19 08:41:18', '2022-05-19 08:41:18'),
(888, 13, 3, 'Driver', '2022-05-19 08:41:33', '2022-05-19 08:41:33'),
(889, 13, 3, 'Driver', '2022-05-19 08:41:56', '2022-05-19 08:41:56'),
(890, 13, 3, 'Driver', '2022-05-19 08:44:54', '2022-05-19 08:44:54'),
(891, 13, 3, 'Driver', '2022-05-19 08:49:53', '2022-05-19 08:49:53'),
(892, 13, 3, 'Driver', '2022-05-19 08:50:21', '2022-05-19 08:50:21'),
(893, 13, 3, 'Driver', '2022-05-19 08:50:22', '2022-05-19 08:50:22'),
(894, 13, 3, 'Driver', '2022-05-19 08:50:23', '2022-05-19 08:50:23'),
(895, 13, 3, 'Driver', '2022-05-19 08:50:23', '2022-05-19 08:50:23'),
(896, 13, 3, 'Driver', '2022-05-19 08:50:23', '2022-05-19 08:50:23'),
(897, 13, 3, 'Driver', '2022-05-19 08:52:53', '2022-05-19 08:52:53'),
(898, 13, 3, 'Driver', '2022-05-19 09:55:31', '2022-05-19 09:55:31'),
(899, 13, 3, 'Driver', '2022-05-19 10:00:39', '2022-05-19 10:00:39'),
(900, 13, 4, 'Consumer', '2022-05-19 10:00:39', '2022-05-19 10:00:39'),
(901, 13, 3, 'Driver', '2022-05-19 10:01:56', '2022-05-19 10:01:56'),
(902, 13, 3, 'Driver', '2022-05-19 10:01:57', '2022-05-19 10:01:57'),
(903, 13, 3, 'Driver', '2022-05-19 10:01:59', '2022-05-19 10:01:59'),
(904, 13, 3, 'Driver', '2022-05-19 10:02:01', '2022-05-19 10:02:01'),
(905, 13, 3, 'Driver', '2022-05-19 10:05:52', '2022-05-19 10:05:52'),
(906, 13, 3, 'Driver', '2022-05-19 10:06:33', '2022-05-19 10:06:33'),
(907, 13, 4, 'Consumer', '2022-05-19 10:08:41', '2022-05-19 10:08:41'),
(908, 13, 3, 'Driver', '2022-05-19 10:12:45', '2022-05-19 10:12:45'),
(909, 13, 4, 'Consumer', '2022-05-19 10:12:49', '2022-05-19 10:12:49'),
(910, 13, 4, 'Consumer', '2022-05-19 10:18:15', '2022-05-19 10:18:15'),
(911, 13, 4, 'Consumer', '2022-05-19 10:18:32', '2022-05-19 10:18:32'),
(912, 13, 3, 'Driver', '2022-05-19 10:44:25', '2022-05-19 10:44:25'),
(913, 13, 3, 'Driver', '2022-05-19 10:45:17', '2022-05-19 10:45:17'),
(914, 13, 3, 'Driver', '2022-05-19 10:49:26', '2022-05-19 10:49:26'),
(915, 13, 3, 'Driver', '2022-05-19 10:52:18', '2022-05-19 10:52:18'),
(916, 13, 3, 'Driver', '2022-05-19 10:57:39', '2022-05-19 10:57:39'),
(917, 13, 3, 'Driver', '2022-05-19 11:27:09', '2022-05-19 11:27:09'),
(918, 13, 3, 'Driver', '2022-05-19 11:30:54', '2022-05-19 11:30:54'),
(919, 13, 3, 'Driver', '2022-05-20 02:53:03', '2022-05-20 02:53:03'),
(920, 13, 3, 'Driver', '2022-05-20 03:15:25', '2022-05-20 03:15:25'),
(921, 13, 3, 'Driver', '2022-05-20 03:18:31', '2022-05-20 03:18:31'),
(922, 13, 3, 'Driver', '2022-05-20 03:19:17', '2022-05-20 03:19:17'),
(923, 13, 3, 'Driver', '2022-05-20 03:20:14', '2022-05-20 03:20:14'),
(924, 13, 3, 'Driver', '2022-05-20 03:21:11', '2022-05-20 03:21:11'),
(925, 13, 3, 'Driver', '2022-05-20 03:23:33', '2022-05-20 03:23:33'),
(926, 13, 3, 'Driver', '2022-05-20 03:25:29', '2022-05-20 03:25:29'),
(927, 13, 3, 'Driver', '2022-05-20 03:26:39', '2022-05-20 03:26:39'),
(928, 13, 3, 'Driver', '2022-05-20 05:14:17', '2022-05-20 05:14:17'),
(929, 13, 3, 'Driver', '2022-05-20 06:09:19', '2022-05-20 06:09:19'),
(930, 13, 3, 'Driver', '2022-05-20 06:09:41', '2022-05-20 06:09:41'),
(931, 13, 3, 'Driver', '2022-05-20 06:10:39', '2022-05-20 06:10:39'),
(932, 13, 3, 'Driver', '2022-05-20 06:11:07', '2022-05-20 06:11:07'),
(933, 13, 3, 'Driver', '2022-05-20 06:11:54', '2022-05-20 06:11:54'),
(934, 13, 3, 'Driver', '2022-05-20 06:29:47', '2022-05-20 06:29:47'),
(935, 13, 3, 'Driver', '2022-05-20 06:37:18', '2022-05-20 06:37:18'),
(936, 13, 3, 'Driver', '2022-05-20 07:04:05', '2022-05-20 07:04:05'),
(937, 13, 3, 'Driver', '2022-05-20 07:04:50', '2022-05-20 07:04:50'),
(938, 13, 3, 'Driver', '2022-05-20 07:06:32', '2022-05-20 07:06:32'),
(939, 13, 3, 'Driver', '2022-05-20 07:07:33', '2022-05-20 07:07:33'),
(940, 13, 3, 'Driver', '2022-05-20 07:10:57', '2022-05-20 07:10:57'),
(941, 13, 3, 'Driver', '2022-05-20 07:16:15', '2022-05-20 07:16:15'),
(942, 13, 3, 'Driver', '2022-05-20 07:16:54', '2022-05-20 07:16:54'),
(943, 13, 3, 'Driver', '2022-05-20 07:17:15', '2022-05-20 07:17:15'),
(944, 13, 3, 'Driver', '2022-05-20 07:21:09', '2022-05-20 07:21:09'),
(945, 13, 3, 'Driver', '2022-05-20 07:21:49', '2022-05-20 07:21:49'),
(946, 13, 3, 'Driver', '2022-05-20 07:22:58', '2022-05-20 07:22:58'),
(947, 13, 3, 'Driver', '2022-05-20 07:25:29', '2022-05-20 07:25:29'),
(948, 13, 3, 'Driver', '2022-05-20 07:26:30', '2022-05-20 07:26:30'),
(949, 13, 3, 'Driver', '2022-05-20 07:27:14', '2022-05-20 07:27:14'),
(950, 13, 3, 'Driver', '2022-05-20 07:30:28', '2022-05-20 07:30:28'),
(951, 13, 3, 'Driver', '2022-05-20 07:31:23', '2022-05-20 07:31:23'),
(952, 13, 3, 'Driver', '2022-05-20 07:33:01', '2022-05-20 07:33:01'),
(953, 13, 3, 'Driver', '2022-05-20 08:39:22', '2022-05-20 08:39:22'),
(954, 13, 3, 'Driver', '2022-05-20 08:39:47', '2022-05-20 08:39:47'),
(955, 13, 3, 'Driver', '2022-05-20 08:40:02', '2022-05-20 08:40:02'),
(956, 13, 3, 'Driver', '2022-05-20 08:40:15', '2022-05-20 08:40:15'),
(957, 13, 3, 'Driver', '2022-05-20 08:40:38', '2022-05-20 08:40:38'),
(958, 13, 3, 'Driver', '2022-05-20 08:42:46', '2022-05-20 08:42:46'),
(959, 13, 3, 'Driver', '2022-05-20 08:43:27', '2022-05-20 08:43:27'),
(960, 13, 3, 'Driver', '2022-05-20 08:44:03', '2022-05-20 08:44:03'),
(961, 13, 3, 'Driver', '2022-05-20 08:44:07', '2022-05-20 08:44:07'),
(962, 13, 3, 'Driver', '2022-05-20 08:49:11', '2022-05-20 08:49:11'),
(963, 13, 3, 'Driver', '2022-05-20 08:50:27', '2022-05-20 08:50:27'),
(964, 13, 3, 'Driver', '2022-05-20 08:53:20', '2022-05-20 08:53:20'),
(965, 13, 3, 'Driver', '2022-05-20 08:55:12', '2022-05-20 08:55:12'),
(966, 13, 3, 'Driver', '2022-05-20 08:55:59', '2022-05-20 08:55:59'),
(967, 13, 3, 'Driver', '2022-05-20 08:55:59', '2022-05-20 08:55:59'),
(968, 13, 3, 'Driver', '2022-05-20 08:55:59', '2022-05-20 08:55:59'),
(969, 13, 3, 'Driver', '2022-05-20 08:58:11', '2022-05-20 08:58:11'),
(970, 13, 3, 'Driver', '2022-05-20 09:00:46', '2022-05-20 09:00:46'),
(971, 13, 3, 'Driver', '2022-05-20 09:01:55', '2022-05-20 09:01:55'),
(972, 13, 3, 'Driver', '2022-05-20 09:04:06', '2022-05-20 09:04:06'),
(973, 13, 3, 'Driver', '2022-05-20 09:04:42', '2022-05-20 09:04:42'),
(974, 13, 3, 'Driver', '2022-05-20 09:12:25', '2022-05-20 09:12:25'),
(975, 13, 3, 'Driver', '2022-05-20 09:12:46', '2022-05-20 09:12:46'),
(976, 13, 3, 'Driver', '2022-05-20 09:13:25', '2022-05-20 09:13:25'),
(977, 13, 3, 'Driver', '2022-05-20 09:14:55', '2022-05-20 09:14:55'),
(978, 13, 3, 'Driver', '2022-05-20 09:55:54', '2022-05-20 09:55:54'),
(979, 13, 3, 'Driver', '2022-05-20 09:56:18', '2022-05-20 09:56:18'),
(980, 13, 3, 'Driver', '2022-05-20 09:59:25', '2022-05-20 09:59:25'),
(981, 13, 3, 'Driver', '2022-05-20 10:04:53', '2022-05-20 10:04:53'),
(982, 13, 3, 'Driver', '2022-05-20 10:09:10', '2022-05-20 10:09:10'),
(983, 13, 3, 'Driver', '2022-05-20 10:11:28', '2022-05-20 10:11:28'),
(984, 13, 3, 'Driver', '2022-05-20 10:11:57', '2022-05-20 10:11:57'),
(985, 13, 3, 'Driver', '2022-05-20 10:13:27', '2022-05-20 10:13:27'),
(986, 13, 3, 'Driver', '2022-05-20 10:14:18', '2022-05-20 10:14:18'),
(987, 13, 3, 'Driver', '2022-05-20 10:18:15', '2022-05-20 10:18:15'),
(988, 13, 3, 'Driver', '2022-05-20 10:22:06', '2022-05-20 10:22:06'),
(989, 13, 3, 'Driver', '2022-05-20 10:23:58', '2022-05-20 10:23:58'),
(990, 13, 3, 'Driver', '2022-05-20 10:24:37', '2022-05-20 10:24:37'),
(991, 13, 3, 'Driver', '2022-05-20 10:28:48', '2022-05-20 10:28:48'),
(992, 13, 3, 'Driver', '2022-05-20 10:31:05', '2022-05-20 10:31:05'),
(993, 13, 3, 'Driver', '2022-05-20 10:32:31', '2022-05-20 10:32:31'),
(994, 13, 3, 'Driver', '2022-05-20 10:33:05', '2022-05-20 10:33:05'),
(995, 13, 3, 'Driver', '2022-05-20 10:39:40', '2022-05-20 10:39:40'),
(996, 13, 3, 'Driver', '2022-05-20 10:41:58', '2022-05-20 10:41:58'),
(997, 13, 3, 'Driver', '2022-05-20 10:42:56', '2022-05-20 10:42:56'),
(998, 13, 3, 'Driver', '2022-05-20 10:43:22', '2022-05-20 10:43:22'),
(999, 13, 3, 'Driver', '2022-05-20 10:44:23', '2022-05-20 10:44:23'),
(1000, 13, 3, 'Driver', '2022-05-20 10:50:29', '2022-05-20 10:50:29'),
(1001, 13, 3, 'Driver', '2022-05-20 11:44:51', '2022-05-20 11:44:51'),
(1002, 13, 3, 'Driver', '2022-05-20 11:49:04', '2022-05-20 11:49:04'),
(1003, 13, 3, 'Driver', '2022-05-20 13:12:40', '2022-05-20 13:12:40'),
(1004, 13, 3, 'Driver', '2022-05-20 13:27:26', '2022-05-20 13:27:26'),
(1005, 13, 3, 'Driver', '2022-05-20 13:28:20', '2022-05-20 13:28:20'),
(1006, 13, 3, 'Driver', '2022-05-21 06:21:16', '2022-05-21 06:21:16'),
(1007, 18, 3, 'Driver', '2022-07-02 11:04:11', '2022-07-02 11:04:11'),
(1008, 18, 3, 'Driver', '2022-07-02 11:04:41', '2022-07-02 11:04:41'),
(1009, 18, 4, 'Consumer', '2022-07-02 11:04:47', '2022-07-02 11:04:47'),
(1010, 18, 4, 'Consumer', '2022-07-02 11:04:59', '2022-07-02 11:04:59'),
(1011, 18, 3, 'Driver', '2022-07-02 11:05:01', '2022-07-02 11:05:01'),
(1012, 18, 3, 'Driver', '2022-07-02 11:05:04', '2022-07-02 11:05:04'),
(1013, 18, 3, 'Driver', '2022-07-02 11:20:24', '2022-07-02 11:20:24'),
(1014, 18, 3, 'Driver', '2022-07-02 11:44:37', '2022-07-02 11:44:37'),
(1015, 18, 3, 'Driver', '2022-07-02 11:46:20', '2022-07-02 11:46:20'),
(1016, 18, 3, 'Driver', '2022-07-02 11:46:22', '2022-07-02 11:46:22'),
(1017, 18, 3, 'Driver', '2022-07-02 11:48:05', '2022-07-02 11:48:05'),
(1018, 18, 3, 'Driver', '2022-07-02 11:52:21', '2022-07-02 11:52:21'),
(1019, 18, 3, 'Driver', '2022-07-02 11:53:31', '2022-07-02 11:53:31'),
(1020, 13, 3, 'Driver', '2022-07-02 12:37:12', '2022-07-02 12:37:12'),
(1021, 13, 3, 'Driver', '2022-07-02 12:48:14', '2022-07-02 12:48:14'),
(1022, 13, 3, 'Driver', '2022-07-02 12:53:46', '2022-07-02 12:53:46'),
(1023, 20, 3, 'Driver', '2022-07-02 13:16:57', '2022-07-02 13:16:57'),
(1024, 20, 3, 'Driver', '2022-07-02 13:17:00', '2022-07-02 13:17:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/December2020/5e03USjW3YUzscSPakCd.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_code` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_verified` int(11) NOT NULL DEFAULT '0',
  `email_verified` int(11) NOT NULL DEFAULT '0',
  `email_verified_at_copy1` timestamp NULL DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token_copy1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `contact`, `city`, `state`, `street`, `unit`, `zip`, `username`, `api_token`, `fcm_token`, `web_token`, `device_token`, `otp_code`, `contact_verified`, `email_verified`, `email_verified_at_copy1`, `document_id`, `remember_token_copy1`, `profile_pic`, `first_name`, `last_name`, `settings`) VALUES
(1, 1, 'admin', 'superadmin@email.com', NULL, '$2y$12$cURTMOtwqbwyjL4qLY3cduewrNjQXF4Pvuy3Iu5qg6W6oh9JtROte', 'QwWvJTbhb457d7kBbfg8Ks1Q2X6qNsneSQKtvxV8HW7A7KS8ubISGNZWi8kY', '2020-12-18 05:59:03', '2020-12-26 01:29:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '{\"locale\":\"en\"}'),
(5, 3, 'sms', 'shoaibuddin12fx@mailinator.com', NULL, '$2y$12$cURTMOtwqbwyjL4qLY3cduewrNjQXF4Pvuy3Iu5qg6W6oh9JtROte', 'meHxEFrVrj', '2022-05-09 11:47:19', '2022-05-15 12:26:47', NULL, '+923432322010', 'karachi', 'Sindh', 'raza square', 'N6', '360005', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shoaib', 'uddin', NULL),
(6, 2, 'joe', 'sms@mailinator.com', NULL, '$2y$10$7/J4w.UsZP98TfUoMpKkqeHesDInL.wk95/ngfUYkGoboc7c1DosK', 'M0W3FnMup2', '2022-05-09 13:15:27', '2022-05-09 13:15:27', NULL, '+923432322010', 'karachi', 'Sindh', 'raza square', 'N6', '360005', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shoaib', 'uddin', NULL),
(7, 2, 'joe', 'bs@mailinator.com', NULL, '$2y$10$Gzpmp375eVjN6oabvsPlSewMI7SKTA42OGXAXCOjcTqiodHf3QJSm', 'seUeorVOrY', '2022-05-10 03:12:17', '2022-05-10 03:12:17', NULL, '+923352757725', 'karachi', 'Sindh', 'raza square', 'N6', '360005', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'shoaib', 'uddin', NULL),
(8, 2, 'joe', 'ab@test.com', NULL, '$2y$10$J5fSns.5lEc/XFljMBYg.uWZ5hRTYmkKkucp9aLodtw6hChjPPq.q', 'xLJXC0HGz1', '2022-05-13 10:33:34', '2022-05-13 10:33:34', NULL, '3456789102', 'sdfj', 'lsdkfj', 'sdf', '56', '74000', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Abdullah', 'P', NULL),
(10, 3, 'joe', 'shoaibuddin12fx@gmail.com', NULL, '$2y$10$epdWl7cMNUNu7TH6yD4sXO16qkZ1qvIpZfjXg5RNDi87grw.8GQNK', 'LzvbXa6Jxb', '2022-05-16 06:14:03', '2022-05-16 06:29:37', NULL, '3105444561', 'Karachi', 'shah faisal', '204', '454', '45641', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Shoaib', 'Uddin', NULL),
(11, 3, 'joe', 'mohsin55@gmail.com', NULL, '$2y$10$Qe35aWXCfq5SrcsVxzXhDuXfbx/jwSwjCmwnmp8ItxJfF45sT//yS', '8Zj5kpVYXu', '2022-05-17 09:49:25', '2022-05-17 11:28:55', NULL, '3102675961', 'karachi', 'sindh', '56', '564', '78600', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Mohsin', 'khan', NULL),
(12, 3, 'joe', 'nomanahmed@test.com', NULL, '$2y$10$9x2JrjHrF.wsOPrpDABgq.mbU04bK22R3Ymh/m/6lKcQcGXQZyINe', 'ftRihxR8mk', '2022-05-18 08:28:22', '2022-05-18 10:57:31', NULL, '3102471803', 'KHI', 'Sindh', '121', '15486', '786000', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Noman', 'Ahmed', NULL),
(20, 3, 'joe', 'parker@yopmail.com', NULL, '$2y$10$ytHm/wx4RCIf0B/fxwR6/e2jcyk2fZtMdkEmUCQkod7Xf0WNRQRX2', 'qavLwNznkF', '2022-07-02 13:16:40', '2022-07-02 13:16:57', NULL, '+923131094717', 'to be', 'shared', 'need', 'no', '78500', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 'Peter', 'Parker', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `registration_number` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_type` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_licence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `user_id`, `registration_number`, `vehicle_type`, `driver_licence`, `is_verified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 'GJ03PK5532Q', 'suv', 'Y4886041', 1, '2020-11-09 06:18:42', '2020-11-09 06:18:42', NULL),
(2, 4, '12345678qweer', 'truck', 'Y4886041', 1, '2020-12-03 18:38:25', '2020-12-03 18:38:25', NULL),
(3, 9, '5245885', '0', 'C:\\fakepath\\Screenshot 2022-05-06 at 11.42.57 AM.png', 1, '2022-05-13 11:35:18', '2022-05-13 11:35:18', NULL),
(4, 5, 'MYU876', '0', 'C:\\fakepath\\WhatsApp Image 2022-04-09 at 3.01.54 PM.jpeg', 1, '2022-05-15 12:24:07', '2022-05-15 12:24:07', NULL),
(5, 10, '4654567', 'suv', 'C:\\fakepath\\Screenshot 2022-05-16 at 3.02.15 PM.png', 1, '2022-05-16 06:14:32', '2022-05-16 06:14:32', NULL),
(6, 10, '456456456', 'truck', 'C:\\fakepath\\Screenshot 2022-05-06 at 11.43.09 AM.png', 1, '2022-05-16 06:33:16', '2022-05-16 06:33:16', NULL),
(7, 11, '66666541', 'sedan', 'C:\\fakepath\\WhatsApp Image 2022-04-09 at 3.01.54 PM.jpeg', 1, '2022-05-17 09:49:56', '2022-05-17 09:49:56', NULL),
(8, 11, '87987451', 'truck', 'C:\\fakepath\\9912fb60-a825-403b-bbad-cb1e8ee7b125.jpeg', 1, '2022-05-17 10:08:39', '2022-05-17 10:08:39', NULL),
(9, 13, '9888561', 'sedan', 'C:\\fakepath\\Screenshot 2022-05-16 at 3.02.15 PM.png', 1, '2022-05-19 04:11:04', '2022-05-19 04:11:04', NULL),
(10, 13, '888555888555', '5', '58585', 2, '2022-05-19 13:14:15', '2022-05-19 13:14:15', NULL),
(11, 13, '888555888555', '5', '58585', 2, '2022-05-19 13:14:43', '2022-05-19 13:14:43', NULL),
(12, 13, '888555888555', '5', '58585', 2, '2022-05-19 13:14:46', '2022-05-19 13:14:46', NULL),
(13, 13, '888555888555', '5', '58585', 2, '2022-05-19 13:14:46', '2022-05-19 13:14:46', NULL),
(14, 13, '888555888555', '5', '58585', 2, '2022-05-19 13:14:47', '2022-05-19 13:14:47', NULL),
(15, 13, '888555888555', '5', '58585', 2, '2022-05-19 13:14:47', '2022-05-19 13:14:47', NULL),
(16, 13, '8885558885551', 'suv', '58585', 1, '2022-05-19 13:23:50', '2022-05-19 13:23:50', NULL),
(17, 13, '8885558885553', 'suv', '58585', 1, '2022-05-20 05:37:31', '2022-05-20 05:37:31', NULL),
(18, 18, '56465464646', 'sedan', 'C:\\fakepath\\Screenshot 2022-06-25 at 6.03.03 PM.png', 1, '2022-07-02 11:05:42', '2022-07-02 11:05:42', NULL),
(19, 20, '585858', 'sedan', 'C:\\fakepath\\Screenshot 2022-06-25 at 6.03.03 PM.png', 1, '2022-07-02 13:17:13', '2022-07-02 13:17:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sizes`
--

CREATE TABLE `vehicle_sizes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_sizes`
--

INSERT INTO `vehicle_sizes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'SUV', '2020-09-24 23:09:04', '2020-09-24 23:09:04'),
(2, 'Truck', '2020-09-24 23:09:04', '2020-09-24 23:09:04'),
(3, 'Sudan', '2020-09-24 23:09:04', '2020-09-24 23:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `is_paid` int(1) DEFAULT '0',
  `paid_date` timestamp NULL DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_amount` double DEFAULT '0',
  `driver_amount` double DEFAULT '0',
  `admin_amount` double DEFAULT '0',
  `time_taken` int(11) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `user_id`, `job_id`, `amount`, `is_paid`, `paid_date`, `order_id`, `customer_amount`, `driver_amount`, `admin_amount`, `time_taken`, `distance`, `created_at`, `updated_at`) VALUES
(1, 13, 3, 12036, 0, NULL, '13', 0, 0, 0, 0, 0, '2022-05-20 10:50:53', '2022-05-20 10:50:53'),
(2, 13, 1, 6372, 0, NULL, '13', 0, 0, 0, 0, 0, '2022-05-20 11:52:39', '2022-05-20 11:52:39'),
(3, 13, 2, 12036, 0, NULL, '13', 0, 0, 0, 0, 0, '2022-05-20 13:29:27', '2022-05-20 13:29:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_settings`
--
ALTER TABLE `admin_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashes`
--
ALTER TABLE `cashes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_types`
--
ALTER TABLE `category_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobstatus`
--
ALTER TABLE `jobstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_prices`
--
ALTER TABLE `job_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_reviews`
--
ALTER TABLE `job_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_urls`
--
ALTER TABLE `job_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `otp_numbers`
--
ALTER TABLE `otp_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `photourls`
--
ALTER TABLE `photourls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runtime_roles`
--
ALTER TABLE `runtime_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_sizes`
--
ALTER TABLE `vehicle_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_settings`
--
ALTER TABLE `admin_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cashes`
--
ALTER TABLE `cashes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `category_types`
--
ALTER TABLE `category_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=575;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jobstatus`
--
ALTER TABLE `jobstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `job_prices`
--
ALTER TABLE `job_prices`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_reviews`
--
ALTER TABLE `job_reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `job_urls`
--
ALTER TABLE `job_urls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `otp_numbers`
--
ALTER TABLE `otp_numbers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `photourls`
--
ALTER TABLE `photourls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `runtime_roles`
--
ALTER TABLE `runtime_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1025;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `vehicle_sizes`
--
ALTER TABLE `vehicle_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
