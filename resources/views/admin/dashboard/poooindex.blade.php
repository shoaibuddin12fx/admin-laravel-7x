@extends('admin.masters.admin_template')

{{--@section('title', 'AdminLTE')--}}

@section('content_header')
    <h1>Settings</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Dashboardj</a></li>
        <li class="active">Settings</li>
    </ol>
@stop

@section('css')
<style>
input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
    height: 22px;
    width: 23px;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Dashbaord Widget</h3>
            </div>
            <!-- /.box-header -->
            <form method="post" action="" class="save_settings" id="save_settings">
                {{ csrf_field() }}
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>Widget</th>
                                <th>Status</th>
                                <th>Color</th>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@stop

@section('js')

@stop
