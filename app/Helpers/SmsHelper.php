<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SmsHelper{

    public function sendsms($contact, $message){
        // $payload = json_encode( $format );
        $client = new Client();
        $url = env('CLICKSEND_SMS_URL');
        $username = env('CLICKSEND_USERNAME');
        $password = env('CLICKSEND_PASSWORD');
        $authkey = "Authorization: Basic ".base64_encode($username.":".$password);
        Log::info('Inside SMS helper', ['str' => $authkey]);


        $format = [
            "messages" => [
                [
                    "source"  => "php",
                    "body" => $message,
                    "to" => $contact
                ]
            ]
        ];

        try{
            $promise = $client->postAsync( $url, [
                'json' =>
                    $format
                ,
                'headers' => [
                    'Authorization' => $authkey,
                    'Content-Type'  => 'application/json',
                    'http_errors' => false
                ],
            ] )->then(function ($response) {

                Log::info('Inside SMS helper response ', ['m' => $response]);
                return json_decode( $response->getBody(), true );
            });
            $promise->wait();
            return true;
        }catch (\Exception $e){

            Log::info('Inside SMS helper Exception', ['error' => $e]);
            return false;
        }

        // $config = \ClickSend\Configuration::getDefaultConfiguration()
        //   ->setUsername("partners@lorvenservices.com")
        //   ->setPassword("38D2E6D1-A944-2584-DE74-07C792300189");
          
        //   $apiInstance = new \ClickSend\Api\SMSApi(new \GuzzleHttp\Client(), $config);
        //   $msg = new \ClickSend\Model\SmsMessage();
        //   $msg->setBody($message); 
        //   $msg->setTo($contact);
        //   $msg->setSource("sdk");
          
        //   // \ClickSend\Model\SmsMessageCollection | SmsMessageCollection model
        //   $sms_messages = new \ClickSend\Model\SmsMessageCollection(); 
        //   $sms_messages->setMessages([$msg]);
        //   try {
        //       $apiInstance->smsSendPost($sms_messages);
        //       return true;
        //   } catch (Exception $e) {
        //       return false;
        //   }
    }
}
