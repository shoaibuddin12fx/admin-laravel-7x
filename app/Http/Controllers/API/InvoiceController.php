<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Job;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InvoiceController extends Controller
{

    public function apiAddInvoices(Request $request){

        $data = $request->all();
        $invoices = $data['invoices'];

        foreach ($invoices as $invoice){

            try{

                $job_id = isset($invoice['job_id']) ? $invoice['job_id'] : 0;
                $job = Job::where('document_id', $job_id)->first();
                if($job){
                    $job_id = $job->id;
                }

                $invoice['job_id'] = $job_id;

                // create type if not created
                Invoice::firstOrCreate([
                    'document_id' => $invoice['id']
                ], $invoice);

            } catch (\Exception $e){
                Log::error( $e->getLine() . " " .$e->getMessage());
                continue;
            }


        }

        return self::success('Invoices Updated');


    }
}
