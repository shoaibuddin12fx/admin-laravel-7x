<?php

namespace App\Http\Controllers\API;

use App\Helpers\Geohash;
use App\Helpers\Helper;
use App\Helpers\PaypalHelper;
use App\Http\Controllers\Controller;
use App\Http\Resources\EarningCollection;
use App\Http\Resources\JobCollection;
use App\Http\Resources\JobResource;
use App\Http\Resources\JobreviewResource;
use App\Http\Resources\WalletCollection;
use App\Http\Resources\WalletResource;
use App\Models\Cash;
use App\Models\Invoice;
use App\Models\Job;
use App\Models\Jobreview;
use App\Models\Jobstatus;
use App\Models\Joburls;
use App\Models\Location;
use App\Models\JobProgressLocation;
use App\Models\OtpNumbers;
use App\Models\Settings;
use App\Models\Wallet;
use App\Models\AdminSetting;
use App\User;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\SmsHelper;

class JobController extends Controller
{
    // helper function
    private function updateJobStatus(Job $job, $jobId, $latitude = null, $longitude = null){


            // $progress = Jobstatus::find($job->id);
            // $progress->update()->all();

            $progress = Jobstatus::updateOrCreate([
                "job_id" => $job["id"],
            ], [
                'job_id' => $job->id,
                'receiver_id' => $job->receiver_id,
                'status' => $job->status,
                'latitude' => $latitude,
                'longitude' => $longitude,
            ]);

            // $progress->update(
            // [ 'job_id' => $job->id,
            // 'receiver_id' => $job->receiver_id,
            // 'status' => $job->status,
            // 'latitude' => $latitude,
            // 'longitude' => $longitude,]
            // )->all(); 
        
            // Jobstatus::create([
            //     'job_id' => $job->id,
            //     'receiver_id' => $job->receiver_id,
            //     'status' => $job->status,
            //     'latitude' => $latitude,
            //     'longitude' => $longitude,
            // ]);
            
            $j = Job::where('id', $job->id)->first();
            if($j){
                $j->status = $job->status;
                $j->save();
            }
    
        // calc amount on status completed


    }

    public function apiAddJobs(Request $request){

        $data = $request->all();
        $jobs = $data['jobs'];

        foreach ($jobs as $job){

            try{
                // create type if not created
                Job::firstOrCreate([
                    'document_id' => $job['id']
                ], $job);

            } catch (\Exception $e){
                Log::error( $e->getLine() . " " .$e->getMessage());
                continue;
            }


        }

        return self::success('Jobs Updated');


    }

    public function nearbyJobs(Request $request){
        $validator = \Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $user_id = \auth()->user()->id;
        $user = User::where('id', $user_id)->first();

        $data = $request->all();
//        $geohash = new Geohash();

        $location = $user->location;
        $latitude = isset($data['latitude']) ? $data['latitude'] : $location->latitude;
        $longitude = isset($data['longitude']) ? $data['longitude'] : $location->longitude;
        $distance = isset($location->distance) ? $location->distance : 25;
//        $bounds = $geohash->bounds($latitude, $longitude, $distance);
//        $bn = $geohash->getBoundsOfDistance($bounds['upper'], $bounds['lower']);

        // see the location filter in future
        $query = Job::query();
        $query->select('*');
//        $query->where('geohash', '<=', $bn['lower']);
//        $query->where('geohash', '>=', $bn['upper']);

        if(isset($data['vehicle_type'])){
            $sizes = $data['vehicle_type']; // collect($data['vehicle_type'])->values()->toArray();
            $query->whereIn('package_size', $sizes);
        }

        if(isset($data['item_category'])){
            $cats = $data['item_category']; // collect($data['vehicle_type'])->values()->toArray();
            $query->whereIn('item_category', $cats);
        }

        if(isset($data['job_price_between'])){
            $start = isset($data['job_price_between'][0]) ? $data['job_price_between'][0] : null;
            $end = isset($data['job_price_between'][1]) ? $data['job_price_between'][1] : null;

            if(isset($start)){
                $query->where('job_price', '>=', $start);
            }

            if(isset($end)){
                $query->where('job_price', '<=', $end);
            }
        }
        //        dd($request->all());

//        $query->addSelect( DB::raw('((ACOS(SIN( ' . $latitude .' * PI() / 180) * SIN(job_latitude * PI() / 180) + COS(' . $latitude .' * PI() / 180) * COS(job_latitude * PI() / 180) * COS((' . $longitude .' - job_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) as distance') );
        $query->addSelect(DB::raw("( FLOOR(6371 * ACOS( COS( RADIANS( '$latitude' ) ) * COS( RADIANS( job_latitude ) ) * COS( RADIANS( job_longitude ) - RADIANS( '$longitude' ) ) + SIN( RADIANS( '$latitude' ) ) * SIN( RADIANS( job_latitude ) ) )) ) distance"));
        $query->having('distance', '<=', $distance);

        $jobs = $query
            ->where('sender_id', '!=', $user_id )
            ->where('status', 'Pending')
            ->orderBy('distance', 'asc')
            ->get();

        if($jobs->isEmpty()) {
            return self::success("Sorry! No jobs found. Try again later!", ['jobs' => $jobs]);
        }

        $jobs = new JobCollection($jobs);
        return self::success("near by job", ['jobs' => $jobs]);
    }

    public function deliveryJobs(Request $request){

        $validator = \Validator::make($request->all(), [
            'deliverer_id' => 'required',
            //'status' => ['required' => 'array'],
        ]);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $user_id = \auth()->user()->id;
        $data = $request->all();
        $jobs = Job::where('receiver_id', $data['deliverer_id'])->where(function ($q) use ($data){
            if(isset($data['status'])){
                $q->whereIn('status', $data['status']);
            }else{
                $q->where('status', '!=', 'Pending')
                    ->where('status', '!=', 'Delivered');
            }
        })->get();
        if($jobs->isEmpty()) {
            return self::success("Sorry! No delivery jobs found. Try again later!", ['jobs' => $jobs]);
        }
        $jobs = new JobCollection($jobs);
        return self::success("Delivery jobs", ['jobs' => $jobs]);
    }

    public function addJob(Request $request, Helper $helper){
        $validator = \Validator::make($request->all(), [
            'job_latitude' => 'required',
            'job_longitude' => 'required',
            'job_address' => "required",
            'delivery_address' => "required",
            'job_price' => "required|integer",
            'priority' => "required|in:Immediate,Flexible",
            'item_category' => "required|in:Electronic,Home,Furniture,Documents,Miscellaneous,Other",
            'package_size' => "required|in:sedan,truck,suv",
            'source_address_appartment' => "required",
            'delivery_address_appartment' => "required",
//            'receiver_contact'
        ], [
            "job_latitude" => "Job Source Coordinates missing",
            "job_longitude" => "Job Source Coordinates missing",
            "delivery_latitude" => "Job Destination Coordinates missing",
            "delivery_longitude" => "Job Destination Coordinates missing",
            "priority" => "Job Delivery Type required",
            'package_size' => "Delivery Vehicle required",
        ]);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $data['status'] = config('enums.job.status.PENDING');
        $job = new Job($data);
        $job->security_code = Helper::incrementalHash(6);
        $job->save();

        // update job status in table
        $this->updateJobStatus($job, $job->id);

        // save photos of job
        $photos_urls = isset($data['photos_urls']) ? $data['photos_urls'] : [];
        foreach ($photos_urls as $url){
            Joburls::create([
               'owner_id' => $user_id,
               'job_id' => $job->id,
               'firebase_url' => $url
            ]);
        }

        $receiver_id = null;
//        if(!isset($data['receiver_contact']) && empty($data['receiver_contact'])) {
//            return self::failure('Sorry, Receiver contact is required for OPT');
//        }
//
//        $otpnumber = OtpNumbers::where( 'phone_number', $data['receiver_contact'] )->first();
//        if($otpnumber){
//            $otp_number_user_id = $otpnumber->user_id;
//            if(isset($otp_number_user_id)){
//                $receiver_id = $otp_number_user_id;
//            }
//        }else{
//            $hash = $helper::generateOtp();
//            OtpNumbers::create(['phone_number' => $data['receiver_contact'], 'is_verified' => 0, 'otp_code' => $hash, 'expires' => Carbon::now()->addMinutes(5) ]);
//        }
        $job->update([
            'sender_id' => $user_id,
            'receiver_id' => $receiver_id,
        ]);

        // add a push notification ping here to all drivers
        $pc = new PushNotificationController();
        $request->request->add(['job_id' => $job->id]);

        $response = $pc->jobPostedAlert($request)->getData();
        
        return self::success("New Job Created", [ 'jobId' => $job->id, 'notification' => $response ] );
    }

    public function postedJobs(Request $request){
        $validator = \Validator::make($request->all(), [
            'jobtype' => ['required' => 'in:active,complete'],
        ]);
       
        
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };
        

        $data = $request->all();
        $user_id = \auth()->user()->id;
        $offset = $request->offset ? $request->offset : 0;
        $query = Job::query();

        if($request->jobtype == 'complete'){
            $query->where('status', config('enums.job.status.DELIVERED'));
        }else{
            $query->where('status', '!=', config('enums.job.status.DELIVERED'));
        }
        $query->where('sender_id', $user_id );

        $jobs = $query->orderBy('created_at', 'DESC' , SORT_NATURAL|SORT_FLAG_CASE)
            ->offset($offset)
            ->limit(10)
            ->get();

        $jobs = new JobCollection($jobs);
        return self::success("Posted Jobs", ['jobs' => $jobs]);
    }

    public function updateJob(Request $request){
        $validator = \Validator::make($request->all(), [
            'job_id' => 'required|integer',
            'status' => ['required' => 'in:Pending,Started,Accepted,Delivered,Received,Completed'],
            'latitude' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ]);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $job = Job::where('id', $request->job_id)->first();
        if(!$job){
            return self::failure("Invalid job");
        }

        $job->update([
            'status' => $request->status,
        ]);

        // update job status in table
        $this->updateJobStatus($job, $data["latitude"], $data["longitude"]);
        $job = Job::where('id', $request->job_id)->first();

        $job = new JobResource($job);
        return self::success("Job is successfully updated.", [ 'job' => $job ] );
    }

    public function verifyCompletedJob(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

    }

    public function deleteJob(Request $request)
    {
        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $job = Job::where('id', $request->job_id)->first();
        if(!$job){
            return self::failure("Invalid Job");
        }

        // update job status in table
        $job->status = config('enums.job.status.DELETED');
        $this->updateJobStatus($job);
        $job->delete();

        return self::success("Job Deleted");
    }

    public function trackJob(Request $request, $id = null){
        //$user_id = \auth()->user()->id;
        if(!isset($id)) {
            return self::failure('Sorry, Job id is required.');
        }

        $query = Job::query();
        $job = $query->where('id', $id )->first();
        if(!isset($job) && empty($job)) {
            return self::success('Sorry, No job found.');
        }

        $job = new JobResource($job);
        return self::success("Posted Job", ['job' => $job]);
    }

    public function trackJobLocations(Request $request, $id = null){


        $validator = \Validator::make($request->all(), [
            'id' => 'required|exists:jobs,id',
            'driver_lat' => 'required',
            'driver_lng' => 'required',
            'status' => 'required'

        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $data = $request->all();
        $query = Job::query();
        $job = $query->where('id', $id )->first();

        $jobLocations = JobProgressLocation::where(['job_id' => $id])->first();
        if($jobLocations != null && $jobLocations['status'] != $data['status']){
            $user = User::where(['id' => $job->sender_id])->first();
            $fcm_token = $user['fcm_token'];
            
            if($fcm_token != null){
                $fcm_data = [];
                switch($data['status']){
                    case ('Accepted'):
                        $fcm_data = ['user_id' => $job->sender_id, 'body' => 'Request has been accepted by driver', 'title' => 'Request Accepted', 'token' => $fcm_token, 'notificationType' => 'job_location', '_id' => $job->id];
                    break;

                    case ('arrived_at_pickup'):
                        $fcm_data = ['user_id' => $job->sender_id, 'body' => 'The driver has been reached at pickup point', 'title' => 'Driver arrived at pickup', 'token' => $fcm_token, 'notificationType' => 'job_location', '_id' => $job->id];
                    break;

                    case ('arrived_at_delivery'):
                        $fcm_data = ['user_id' => $job->sender_id, 'body' => 'Driver has been arrived at your location kindly collect your parcel', 'title' => 'Driver Arrived', 'token' => $fcm_token, 'notificationType' => 'job_location', '_id' => $job->id];
                    break;

                    case ('delivery_complete'):
                        $fcm_data = ['user_id' => $job->sender_id, 'body' => 'Delivery has been completed, Thank you for choosing Deliveroo', 'title' => 'Delivery completed', 'token' => $fcm_token, 'notificationType' => 'job_location', '_id' => $job->id];
                    break;
                }
                $pc = new PushNotificationController();
                //dd($fcm_data);
                if(isset($fcm_data['body']))
                $response = $pc->sendPushNotification(new Request($fcm_data));
            }
        }

        $jobLocations = JobProgressLocation::updateOrCreate(['job_id' => $id], [
            'driver_lat' => $data['driver_lat'],
            'driver_lng' => $data['driver_lng'],
            'status' => $data['status'],
            'target_lat' => $data['status'] == 'arrived_at_pickup' ? $job->delivery_latitude : $job->job_latitude,
            'target_lng' => $data['status'] == 'arrived_at_pickup' ? $job->delivery_longitude : $job->job_longitude
        ]);

        // $status = $data['status'];        
        // if($status == 'arrived_at_pickup'){

        //     $jobLocations->update([
        //         'target_lat' => $job->delivery_latitude,
        //         'target_lng' => $job->delivery_longitude
        //     ]);

        // }else{
        //     $jobLocations->update([
        //         'target_lat' => $job->job_latitude,
        //         'target_lng' => $job->job_longitude
        //     ]);
        // }

        $jobLocations = JobProgressLocation::where('job_id', $id)->first();
        // update locations 
        return self::success("Posted Job locations", ['job_locations' => $jobLocations]);

    }

    public function gettrackJobLocations(Request $request, $id = null){

        $data = $request->all();
        if(!isset($id)) {
            return self::failure('Sorry, Job id is required.');
        }

        $query = Job::query();
        $job = $query->where('id', $id )->first();
        if(!isset($job) && empty($job)) {
            return self::success('Sorry, No job found.');
        }

        
        $query = Job::query();
        $job = $query->where('id', $id )->first();

        $jobLocations = JobProgressLocation::where('job_id', $id)->first();

        // if(!$jobLocations){
        $jobLocations = JobProgressLocation::updateOrCreate(['job_id' => $id], [
            'driver_lat' => $data['driver_lat'],
            'driver_lng' => $data['driver_lng'],
            'target_lat' => $job->job_latitude,
            'target_lng' => $job->job_longitude,
            'status' => $data['status'],
        ]);

        $jobLocations = JobProgressLocation::where('job_id', $id)->first();
        // }
        

        // update locations 
        return self::success("Posted Job locations", ['job_locations' => $jobLocations]);



    }

    public function currentTrackJobProgress(Request $request){
        $data = $request->all();
        if(!isset($data['id'])) {
            return self::failure('Sorry, Job id is required.');
        }
        $id = $data['id'];
        $jobs = Job::where('id', $id)->with(['receiver', 'jobstatus', 'jobstatus.receiver','jobprogress'])->get();
        $jobLocation = JobProgressLocation::where(['job_id'=> $id])->get();
        return self::success("Current Job", ['jobs' => $jobs, 'jobLocation' => $jobLocation]);

    }


    // public function getCurrentJobData(Request $request){
      

    //     return self::success("Job Detail");
    // }


    public function getPendingJobs(Request $request)
    {
        $user = auth()->user(); 

        $jobs = Job::with(['receiver', 'jobstatus', 'jobstatus.receiver','jobprogress'])->where('sender_id', $user->id)->whereHas('jobstatus', function($query){
            return $query->where('status', 'Pending');
        })->get();

        return self::success("Pending Jobs" , ['jobs' => $jobs]);

    }


    public function getPayableAmount(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'distance' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $settings = Settings::first();
        $distance = $data['distance'];

        if($settings){
            $milePerMeter = $settings->dollar_per_mile;
            $mpg = config('enums.payment.mpg');;
            $gallon = config('enums.payment.gallon');
            $minimumCharge = $settings->min_fare;
            $tax = $settings->min_fare_distance;
            $serviceCharge = $settings->admin_commission;
        }else{
            $milePerMeter = config('enums.payment.milePerMeter');
            $mpg = config('enums.payment.mpg');
            $gallon = config('enums.payment.gallon');
            $minimumCharge = config('enums.payment.minimumCharge');
            $tax = config('enums.payment.tax');
            $serviceCharge = config('enums.payment.serviceCharge');
        }




        $distanceMiles = $distance * $milePerMeter;
        $amount = ($distanceMiles / $mpg) * $gallon;

        if($amount < $minimumCharge) {
            $amount = $minimumCharge;
        }
        $tax_amount = $amount * $tax;
        $service_amount = $amount * $serviceCharge;

        $result = [
            'amount' => (int)$amount,
            'tax' => (int)$tax,
            'service_charge' => (int)$service_amount,
            'total' => (int)($amount + $tax + $service_amount)
        ];

        return self::success("Payable Amount", $result);


    }

    public function getPaymentMethods(Request $request){
    }

    public function acceptJob(Request $request){
        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required'
        ]);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        $job->update([
            'receiver_id' => $user_id,
            'status' => config('enums.job.status.ACCEPTED')
        ]);
        $this->updateJobStatus($job , $jobId);

        $job = new JobResource($job);
        $user = User::where(['id' => $job->sender_id])->first();
        $fcm_token = $user['fcm_token'];
        $fcm_data = ['user_id' => $job->sender_id, 'body' => 'Request has been accepted by driver', 'title' => 'Request Accepted', 'token' => $fcm_token, 'notificationType' => 'job_location', '_id' => $job->id];
        $pc = new PushNotificationController();
        $response = $pc->sendPushNotification(new Request($fcm_data));
        return self::success("Job successfully accepted", ['job' => $job]);
    }

    public function ratingsFromDriver(Request $request){
        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
            'rating' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $jobId = $data['job_id'];
        $job = Job::find($jobId);
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        $review = new Jobreview([
            'job_id' => $job->id,
            'reviewer_id' => $user_id,
            'sender_id' => $job->sender_id,
            'rating' => $data['rating'],
            'review' => isset($data['review']) ? $data['review'] : null,
            'client_reviewed' => isset($data['client_reviewed']) ? $data['client_reviewed'] : false,
            'driver_reviewed' => isset($data['driver_reviewed']) ? $data['driver_reviewed'] : false,
        ]);
        $review->save();

        $review = new JobreviewResource($review);
        return self::success("Review is successfully added" , ['review' => $review]);
    }

    public function getWalletCashout(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();
        $source = 'paypal';

        $wallets = Wallet::where(['user_id' => $user_id, 'is_paid' => 0 ]);
        $total = $wallets->sum('amount');

        if( $total == 0 ){
            return self::failure("No Amount to Cashout" , [ 'total' => $total]);
        }

        $wallets->update([
            'is_paid' => 1,
            'paid_date' => Carbon::now()
        ]);

        $cashout = new Cash([
           'user_id' => $user_id,
           'cashout' => $total,
           'source' => $source
        ]);

        $cashout->save();



        return self::success("Added Amount" , [ 'total' => $total, 'cashout' => $cashout ]);

    }

    public function getWalletRecords(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'wallet_type' => ['required' => 'in:all,active,paid'],
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $query = Wallet::query();
        $query->where('user_id', $user_id);

        if($data['wallet_type'] == "active" ){
            $query->where('is_paid', 0);
        }

        if($data['wallet_type'] == "paid" ){
            $query->where('is_paid', 1);
        }

        $wallets = $query->get();
        $total = $wallets->sum('amount');

        $wallets = new WalletCollection($wallets);
        return self::success("Added Amount" , [ 'total' => $total, 'wallets' => $wallets]);

    }

    public function setAmountInWallet(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
            'amount' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };
        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        // comment for testing
        $walletExist = Wallet::where(['user_id' => $user_id, 'job_id' => $jobId])->count();        

        if($walletExist){
            return self::failure("Job Amount Exist in Wallet ");
        }

        //TODO uncomment
        //$paypal = new PaypalHelper();
        $order_id = $user_id; // $paypal::callJobPaypalOrderCreate($jobId, $data['amount']);

        if($order_id == -1){
            return self::failure("Failed to create order" , ['order_id' => $order_id]);
        }


        // get total time spend for job
        // Pending,Started,Accepted,Delivered,Received,Completed
        // config('enums.job.status.ACCEPTED')
        // config('enums.job.status.COMPLETED')
        $jobstatusAccepted = Jobstatus::where(['job_id' => $jobId, 'receiver_id' => $user_id, 'status' => config('enums.job.status.ACCEPTED')])->first();
        $jobstatusComplete = Jobstatus::where(['job_id' => $jobId, 'receiver_id' => $user_id, 'status' => config('enums.job.status.COMPLETED')])->first();

        $time_taken = 0;
        $distance = 0;
        if($jobstatusAccepted && $jobstatusComplete){
            $time_taken = (new Carbon($jobstatusAccepted->created_at))->diffInSeconds($jobstatusComplete->created_at);
            // calculate distance in km of two lat long
            $distance = Helper::getDistanceBetweenPoints($jobstatusAccepted->latitude, $jobstatusAccepted->longitude, $jobstatusComplete->latitude, $jobstatusComplete->longitude);
        }

        // Get Admin setting
        $adminSetting =  AdminSetting::where(['id' => 1])->first();

        $wallet = new Wallet([
            'user_id' => $user_id,
            'job_id' => $job->id,
            'amount' => $data['amount'] - $adminSetting['admin_commission'],
            'order_id' => $order_id,
            'time_taken' => $time_taken,
            'distance' => $distance,
        ]);

        $wallet->save();

        $admin = User::where(['role_id' => 1])->first();

        $adminWallet = new Wallet([
            'user_id' => $admin->id,
            'job_id' => $job->id,
            'amount' => $adminSetting['admin_commission'],
            'order_id' => $order_id,
            'time_taken' => $time_taken,
            'distance' => $distance,
        ]);
        
        $adminWallet->save();

        $wallet = new WalletResource($wallet);
        $wallet = new WalletResource($adminWallet);

        return self::success("Added Amount" , ['wallet' => $wallet]);

    }

    public function createPaypalCapture(Request $request){

        $user = \auth()->user();
        $user_id = $user->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'order_id' => 'required',
            'job_id' => 'required',
            'amount' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $paypal = new PaypalHelper();
        $capture_id = $paypal::callJobPaypalOrderCapture($data['order_id']);

        $invoice = new Invoice([
           'user_id' => $user_id,
           'job_id' => $data['job_id'],
           'amount' => $data['amount'],
           'order_id' => $data['order_id'],
           'capture_id' => $capture_id
        ]);
        $invoice->save();

        return self::success("Payment Captured" , ['invoice' => $invoice, 'orderCaptured' => true, 'capture_id' => $capture_id]);





    }

    public function findNearByDrivers(Request $request){

        // get the job id and find nearby users with driver flag
        $user = \auth()->user();
        $user_id = $user->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };
        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        // find lat long of job
        $latitude = $job->job_latitude;
        $longitude = $job->job_longitude;

        $location = $user->location;
        $distance = isset($location->distance) ? $location->distance : 25;

        $query = Location::query();
        $query->select('*');

        $query->addSelect( DB::raw('((ACOS(SIN( ' . $latitude .' * PI() / 180) * SIN(latitude * PI() / 180) + COS(' . $latitude .' * PI() / 180) * COS(latitude * PI() / 180) * COS((' . $longitude .' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) as distance') );
        $query->having('distance', '<=', $distance);
        $locations = $query->where('user_id', '!=', $user_id)
            ->orderBy('distance', 'asc')
            ->get();

        return self::success("Found Locations" , ['locations' => $locations]);






    }

    //driver earnings
    public function getWalletEarnings(Request $request)
    {
        //userid
        $user = \auth()->user();
        $user_id = $user->id;
        $data = $request->all();
        $category = isset($data['category']) ? $data['category'] : 'today';

        //daily earning
        $query = Wallet::where('user_id', '=', $user_id);

        // $today=date('Y-m-d h:i:s');
        if ($category == 'today') {
            $startDay = Carbon::now()->startOfDay();
            $endDay   = $startDay->copy()->endOfDay();
            $query = $query->whereBetween('paid_date', [$startDay, $endDay]);
        }
        if ($category == 'weakly') {
            $query = $query->whereBetween('paid_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        }
        if ($category == 'monthly') {
            $query = $query->where('paid_date', '>=', Carbon::now()->firstOfMonth()->toDateTimeString() );
        }
        if ($category == 'yearly') {
            $query = $query->where('paid_date', '>=', Carbon::now()->startOfYear()->toDateTimeString());
        }

        $earnings = new EarningCollection($query->get());

        return self::success('Earnings', ['earnings' => $earnings]);

        //weakly earning
    }


    public function sendJobCompleteOtp(SmsHelper $smsHelper) {
        try {
            $data = request()->all();
            $contact = $data[0]['receiver_contact'];
            $hash = $data[0]['security_code'];
            $response = $smsHelper->sendsms($contact, "Use the following $hash to verify the job completion.");
            return self::success("OTP has been successfully sent.");
        } catch (\Exception $e) {
            return self::fail($e->getMessage());
        }
    }

}
