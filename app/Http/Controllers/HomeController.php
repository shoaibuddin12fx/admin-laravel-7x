<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Job;
use App\Models\Vehicle;
use App\Models\RuntimeRole;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
// {
//     /**
//      * Create a new controller instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         $this->middleware('auth');
//     }

//     /**
//      * Show the application dashboard.
//      *
//      * @return \Illuminate\Contracts\Support\Renderable
//      */
//     public function index()
//     {
//         return view('home');
//     }
// }
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_count = User::count();
        $job_count = Job::count();
        $product_count = Product::count();
        $invoice_count = Invoice::count();
        $drivers = User::whereHas('vehicle')->count();
        $driversonduty = User::whereHas('vehicle')->where('role_id', 5)->count();
        $customers = User::where('role_id', 6)->count();

        $deliveredorders=Job::where('status','=',"Delivered")->count();
        $pendingorders=Job::where('status','=',"Pending")->count();
        $canceledgorders=Job::where('status','=',"Canceled")->count();
        $assignedorders=Job::where('status','=',"Assigned")->count();
        $dispatchedorders=Job::where('status','=',"Dispatched")->count();

        return view('admin.dashboard.index', [
            'user_count' => $user_count,
            'product_count' => $product_count,
            'invoice_count' => $invoice_count,
            'job_count' => $job_count,
            'drivers'=>$drivers,
            'driversonduty'=>$driversonduty,
            'customers'=>$customers,
            'deliveredorders'=>$deliveredorders,
            'pendingorders'=>$pendingorders,
            'canceledgorders'=>$canceledgorders,
            'assignedorders'=>$assignedorders,
            'dispatchedorders'=>$dispatchedorders
        ]);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    function updaterecord(Request $request){
       return $request->all();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
