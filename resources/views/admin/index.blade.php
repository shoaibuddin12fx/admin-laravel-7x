@extends('voyager::master')

<!-- <div class="customLoader">
    <img src="{{asset('/images/spinner.gif')}}" alt="Loading...">
</div> -->

@section('css')
    <link rel="stylesheet" href="{{ asset( 'plugins/fontawesome-free/css/all.min.css') }} ">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset( 'plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href=" {{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
{{--    <link rel="stylesheet" href=" {{ asset('plugins/jqvmap/jqvmap.min.css') }}">--}}
{{--    <link rel="stylesheet" href=" {{ asset('dist/css/adminlte.min.css') }}">--}}
{{--    <link rel="stylesheet" href=" {{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">--}}
{{--    <link rel="stylesheet" href=" {{ asset('plugins/daterangepicker/daterangepicker.css') }}">--}}
{{--    <link rel="stylesheet" href=" {{ asset('plugins/summernote/summernote-bs4.css') }}">--}}
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset( 'css/font-awesome.min.css') }} " rel="stylesheet">

    <!-- -->
    <link rel="stylesheet" href=" {{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
{{--    <link rel="stylesheet" href="/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">--}}
{{--    <link rel="stylesheet" href="/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">--}}
    <!-- Theme style -->
{{--    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">--}}
    <link rel="stylesheet" href=" {{ asset('css/admin_custom.css') }}">

    <link rel="stylesheet" href="{{asset('css/cardstyle.css')}}">
    <link rel="stylesheet" href="{{asset('css/tablestyle.css')}}">
    <link rel="stylesheet" href="{{asset('dist/noty/noty.css')}}">
    <!-- datatable -->
{{--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">--}}
@stop


@section('js')
{{--    <script src=" {{ asset('plugins/jquery/jquery.min.js') }}"></script>--}}
{{--    <script src=" {{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>--}}
{{--    <script>$.widget.bridge('uibutton', $.ui.button)</script>--}}
{{--    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>--}}
    <script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
// $('#edit').click(function addseries(e){
//    var i = $(e.currentTarget).attr('data-id');
//    $.ajax({
//       type: "GET",
//      url: '/Getvehicle/2',
//      data: i,
//      success: function(data) {
//      alert('done')
//    }

// });
   </script>


    @if (Request::path() == '/' || Request::path() == 'home' )
        <script src=" {{ asset('plugins/chart.js/Chart.min.js') }} "></script>
        <script src=" {{ asset('plugins/sparklines/sparkline.js') }} "></script>
        <script src=" {{ asset('plugins/jqvmap/jquery.vmap.min.js') }} "></script>
        <script src=" {{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }} "></script>
    <!-- jQuery Knob Chart -->
        <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
        // code

        <!-- daterangepicker -->
        <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
        <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
        <!-- Summernote -->
        <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
        <!-- overlayScrollbars -->
        <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

    @endif

    <!-- AdminLTE App -->
{{--    <script src="{{asset('/dist/js/adminlte.js') }} "></script>--}}
    @if (Request::path() == '/' || Request::path() == 'home' )
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{asset('/dist/js/pages/dashboard.js') }} "></script>
    @endif
    <!-- AdminLTE for demo purposes -->
{{--    <script src="{{asset('/dist/js/demo.js') }} "></script>--}}
    <!-- -->


    <!-- AdminLTE App -->
{{--    <script src="{{asset('/dist/js/adminlte.min.js') }} "></script>--}}
    <!-- AdminLTE for demo purposes -->
{{--    <script src="{{asset('/dist/js/demo.js') }} "></script>--}}
    <!-- page script -->
{{--    <script src="{{asset('dist/noty/noty.min.js')}}"></script>--}}

@stop
