<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\job;
use App\Models\jobprice;
class InvoiceRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
      
      $driver = isset($obj->job->receiver) ? $obj->job->receiver->fullName : "";
      $sender= isset($obj->job->sender) ? $obj->job->sender->fullName :"";
        $jobpriceobj=new jobprice();
        $driveramount=$jobpriceobj->driver_amount($obj->id);
            if($obj->price>0){
                $driverpercentage=$driveramount*100/$obj->price;
            }else{
                $driverpercentage="";
            }
       
        return [
            "id"=>$obj->id,
            "booking_at"=>$obj->created_at->format('m-d-Y h:i a'),
            "driver"  =>$driver,
             "customer"=>$sender,
            // "servicetype " //job type from 
             "miles"=> $obj->distance,
             "total"=>$obj->price,
             "delivery"=>$driveramount,
             "driverpercentage"=>$driverpercentage,
            


        ];
    }
}
