<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vRole extends Model
{
    protected $table = "roles";
    //
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
