<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class JobreviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
        $fullname = "";
        $reviewer = User::where(['id' => $obj->reviewer_id])->first();
        if($reviewer){
            $fullname = $reviewer->fullName;
        }
        return [
            "id" => $obj->id,
            "job_id" => $obj->job_id,
            "rating" => $obj->rating,
            "review" => $obj->review,
            "reviewer" => $fullname
        ];
    }
}
