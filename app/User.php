<?php

namespace App;

use App\Models\Message;
use App\Models\RuntimeRole;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends \TCG\Voyager\Models\User
{
    
    use HasApiTokens, Notifiable , SoftDeletes;

    protected $guarded = [];

    public function vehicle(){
        return $this->hasOne('App\Models\Vehicle', 'user_id');
    }

    public function scopeMyScope($query)
    {
        return $query->where('role_id', 1);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function favorites() {
        return $this->belongsToMany('App\Models\Favorite');
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }

    public function getFullNameAttribute(){

        $firstname = "";
        $lastname = "";


        if( isset($this->attributes['first_name']) && !is_null($this->attributes['first_name']) ){
            $firstname = $this->attributes['first_name'];
        }

        if( isset($this->attributes['last_name']) && !is_null($this->attributes['last_name']) ){
            $lastname = $this->attributes['last_name'];
        }

        return $firstname . " " . $lastname;

    }

    public function isSuperAdmin(){
        return $this->hasRole('SuperAdmin');
    }

    public function getCurrentRoleAttribute(){

        $role = RuntimeRole::where('user_id', $this->attributes['id'])->orderBy('id')->first();
//        $role = $this->hasMany('App\Models\RuntimeRole', 'user_id', 'id')->latest()->first();
        if($role){
            return $role->role_name;
        }else{
            return "user";
        }
    }

    public function location(){
        return $this->hasOne('App\Models\Location', 'user_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }



    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    public function runtimeRoles(){
        return $this->hasMany('App\Models\RuntimeRole', 'user_id');
    }

    public function getRuntimeRoleAttribute(){

        $runtime_role = RuntimeRole::where('user_id', $this->attributes['id'])->orderBy('id', 'desc')->first();
        if($runtime_role){
            return $runtime_role->role_name;
        }
        return "";
    }

}
