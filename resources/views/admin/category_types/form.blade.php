<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Name</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="name" value="{{ $category_type->name ?? old('name') }}" name="name" placeholder="Name">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Image</label>
    <div class="col-md-10 col-sm-9">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="image" name="image">
            <label class="custom-file-label" for="image">Choose file</label>
            @isset($category_type)
                <a href="{{ $category_type->image }}" target="_blank">Click here to see Image</a>
            @endisset
        </div>
    </div>
</div>