<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Traits\PushnotificationTrait;
use App\Models\Location;
use App\User;
use Illuminate\Http\Request;

class PushNotificationController extends Controller
{
    use PushnotificationTrait;
    //

    public function sendPushNotification(Request $request)
    {
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'body' => 'required',
            'title' => "required",
            'token' => "required",
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };


        $ret = null;
        if($data['token'] != null){
            // token, user_id, title, description, key_id, link, info
            $ret = $this->sendPushToUser($data);
        }

        return $ret;
    }

    public function jobPostedAlert(Request $request){

        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        // search nearby drivers for that job
        $jobController = new JobController();
        $locations = $jobController->findNearByDrivers($request);



        return self::success("Drivers", ['locations' => $locations]);

    }




}
