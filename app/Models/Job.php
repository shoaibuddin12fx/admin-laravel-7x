<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int|mixed security_code
 * @property mixed id
 */
class Job extends Model
{
    use SoftDeletes;
    //
    protected $table = "jobs";
    protected $fillable = [
        'delivery_address', 'delivery_latitude', 'delivery_longitude', 'description', 'expected_delivery_time',
        'geohash', 'item_category', 'job_address', 'job_latitude', 'job_longitude', 'job_price', 'package_size',
        'posted_at', 'priority', 'receiver_instructions', 'security_code', 'status', 'document_id', 'receiver_name', 'receiver_contact',
        'sender_id', 'receiver_id', 'source_address_appartment', 'delivery_address_appartment', 'distance',

    ];


    public function sender(){
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function receiver(){
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function jobstatus(){
        return $this->hasMany('App\Models\Jobstatus', 'job_id', 'id');
    }

    public function jobprogress(){
        return $this->hasMany('App\Models\JobProgressLocation', 'job_id', 'id');
    }

    public function getPickupDateAttribute(){

        if($this->jobstatus()){
            $accepted = config('enums.job.status.ACCEPTED');
            $jobstatus = $this->jobstatus()->where('status', $accepted)->orderBy('id', 'desc')->first();
            if($jobstatus){
                return $jobstatus->created_at->format('m-d-Y h:i a');
            }else{
                return null;
            }
        }

        return null;
    }

    public function wallet(){
        return $this->hasOne('App\Models\Wallet', 'job_id', 'id');
    }

    public function jobprices(){
        return $this->hasOne('App\Models\jobprice', 'job_id', 'id');
    }

    public function getTotalRatingAttribute(){
        return Jobreview::where('job_id', $this->attributes['id'])->sum('rating');
    }

    public function getDriverReviewedAttribute(){
        return Jobreview::where('job_id', $this->attributes['id'])->sum('driver_reviewed');
    }

    public function getClientReviewedAttribute(){
        $jobreview = Jobreview::where('job_id', $this->attributes['id'])->orderBy('id', 'desc')->first();
        if($jobreview){
            return $jobreview->client_reviewed;
        }
        return 0;
    }
}
