<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jobstatus extends Model
{
    //
    protected $table = "jobstatus";
    protected $fillable = [
        'receiver_id', 'job_id', 'status', 'latitude', 'longitude'
    ];

    public function receiver(){
        return $this->belongsTo('App\User', 'receiver_id');
    }

}
