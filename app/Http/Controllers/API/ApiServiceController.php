<?php

namespace App\Http\Controllers\API;

use App\Helpers\FirebaseHelper;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ApiServiceController extends Controller
{
    //
    public function verifyDriverLicense(Request $request){

        Log::debug(print_r($request));
        return self::success("Verify Result"  );
    }

    public function createSuperadminPassword(){

        $user = User::where('id', 1)->first();
        if($user){
            $user->password = bcrypt('admin123$');
            $user->assignRole('SuperAdmin');
            $user->save();



        }

        return self::success("Password updated" );


    }



}
