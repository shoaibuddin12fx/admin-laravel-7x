<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photourl extends Model
{
    //
    protected $table = "photourls";
    protected $fillable = [
        'name', 'type', 'owner_id', 'product_id', 'firebase_url',
    ];
}
