<?php

namespace App\Http\Resources;

use App\Models\Job;
use Illuminate\Http\Resources\Json\ResourceCollection;

class JobCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Job $job){
            return new JobResource($job);
        });
        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {
        $obj->transform(function (Job $job){
            return new JobResource($job);
        });
        return $obj;
    }
}
