<?php

namespace App\Http\Resources;

use App\Models\Job;
use App\Models\Joburls;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class JobViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

        $sendername = isset($obj->sender) ? $obj->sender->fullName : "";
        $receivername = isset($obj->receiver) ? $obj->receiver->fullName : "";
        $data = [
            "id" => $obj->id,
            "customer" => $sendername,
            "driver" =>$receivername,
            "price" => $obj->job_price,
            "miles" => $obj->distance ?? 0 . 'mi',
            "status" => $obj->status,
            "created_on" => $obj->created_at->format('Y-m-d'),
            "pickup_date" => $obj->pickupDate,
            "pickup_location" => $obj->source_address_appartment,
            "target_location" => $obj->delivery_address_appartment,
        ];



        return $data;


    }
}
