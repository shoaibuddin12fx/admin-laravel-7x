<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    //
    protected $table = "cashes";
    protected $fillable = [
        'user_id', 'cashout', 'source'
    ];
}
