<?php

namespace App\Http\Resources;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

        $data = [
            "id" => $obj->id,
            "sent_to"=>$obj->user->fullName,
            "heading"=>$obj->heading,
            "discription"=>$obj->discription,
            "created_at" => $obj->created_at->format('m-d-Y'),

           // "action"=>"<a href='deletevehicle/$obj->id' onclick='return confirm('Do you wants to delete')'><i class='fa fa-trash text-danger' aria-hidden='true'></i></a>"
        ];

        return $data;


    }
}
