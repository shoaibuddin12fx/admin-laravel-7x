<?php

return [
    'job' => [
        'status' => [
            'PENDING' => 'Pending',
            'ACCEPTED' => 'Accepted',
            'STARTED' => 'Started',
            'DELIVERED' => 'Delivered',
            'RECEIVED' => 'Received',
            'COMPLETED' => 'Completed',
            'DELETED' => 'Deleted',
            // new status for jobs
            'ASSIGNING' => 'Assigning',
            'ASSIGNED' => 'Assigned',
            'PICKEDUP' => 'Pickedup',
            'ARRIVED' => 'Arrived',

        ],
        'location' => [
        'radius' => 25000, // 25 km
            'precision' => 11, // highest geohashing precision
        ]
    ],
    'product' => [
        'categories' => [
            'Household' => 'Household',
                'Electronics' => 'Electronics',
                'Automobile' => 'Automobile',
                'Toy' => 'Toy',
                'Fashion' => 'Fashion',
                'Service' => 'Service',
                'Food' => 'Food',
                'RealState' => 'RealState'
        ],
        'location' => [
            'radius' => 25000, // 25 km
                'precision' => 11, // highest geohashing precision
            ]
    ],
    'image_types' => [
        // all supported image types
        'IMG_PNG' => 'image/png',
        'IMG_JPEG' => 'image/jpeg',
        'IMG_JPG' => 'image/jpg'
    ],
    'rtc' => [
        'messagesToFetch' => 10
    ],
    'user' => [
        'role' => [
            'Consumer' => 'Consumer',
                'Driver' => 'Driver'
            ]
    ],
    'tracking' => [
        'location' => [
            'radius' => 50000, // 50 km
                'precision' => 11, // highest geohashing precision
            ]
    ],
    'notification' => [
        'location' => [
            'radius' => 50000, // 50 km
                'precision' => 11, // highest geohashing precision
            ],
        'notifyDrivers' => 30
    ],
    'payment' => [
        'minimumCharge' => 10.0,
        'mpg' => 30, // miles per gallon
        'gallon' => 2.2, // price per gallon
        'tax' => 0.0775, // california sales and use tax
        'milePerMeter' => 0.000621371,
        'serviceCharge' => 0.010,
    ]
];
