<?php

namespace App\Widgets;

use App\Models\Job;
use App\Models\Jobs;
use App\Models\Product;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class OrderCardDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Job::count();
        $string = "Orders";

        $deliveredorders = Job::where('status','=',"Delivered")->count();
        $pendingorders = Job::where('status','=',"Pending")->count();
        $canceledgorders = Job::where('status','=',"Canceled")->count();
        $assignedorders = Job::where('status','=',"Assigned")->count();
        $dispatchedorders = Job::where('status','=',"Dispatched")->count();

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => "$deliveredorders Delivered - $pendingorders Pending - $canceledgorders Canceled - $assignedorders Assigned - $dispatchedorders Dispatched",
            'button' => [
                'text' => 'Click to view them',
                'link' => route('voyager.jobs.index'),
            ],
            'image' => asset('images/dashboard/bg-d1.jpeg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
