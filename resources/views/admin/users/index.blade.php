@extends('admin.index')
@section('content')
    @php $actions = isset($actions) ? $actions : [] @endphp
    @include('admin.tables.index', [ 'columns' => $columns, 'data' => $data, 'actions' => $actions   ])
@endsection
