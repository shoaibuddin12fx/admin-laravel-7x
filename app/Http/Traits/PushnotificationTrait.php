<?php

namespace App\Http\Traits;
use App\Http\Controllers\Backend\EmailTemplatesController;
use App\Mail\ZUULSystem;
use App\Models\EmailTemplates;
use App\Models\LogsSms;
use App\Models\Pass;
use App\Models\PCNotificaitons;
use App\Models\PushNotification;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Models\ParentalControl;
use DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

trait PushnotificationTrait
{

    public function sendPushToUser($data)
    {
//        if($userToReceivePush){
//            if($userToReceivePush->push_notification == 0) {
//                if($isReturn){
//                    return response()->json(['bool' => false, 'message' => 'Push notification is disabled from user settings', 'id'=> $userToReceivePush->id], 200);
//                }
//            };
//        }

        $obj = [
            "user_id" => $data["user_id"],
            'body' => $data['body'],
            'title' => $data['title'],
            'key_id' => isset( $data['key_id'] ) ? $data['key_id'] : null,
            "click_action" => isset( $data['link'] ) ? $data['link'] : null,
            "info" => isset( $data['info'] ) ? $data['info'] : null,
            "notificationType" => isset( $data['notificationType'] ) ? $data['notificationType'] : null,
            "_id" => isset( $data['_id'] ) ? $data['_id'] : null,
            "test_key" => 'Hello'

        ];

        $url = 'https://fcm.googleapis.com/fcm/send';
        $format = array (
            'to' => "".$data['token']."",
            'notification' => $obj,
            'data' => $obj
        );

        // $ch = curl_init( $url );
        # Setup request to send json via POST.
        //$payload = json_encode( $format );

        $client = new Client();

        $promise = $client->postAsync( $url, [
            'json' =>
                $format
            ,
            'headers' => [
                'Authorization' => 'key=AAAAdeFsNRE:APA91bF1Z3NyFd7AKzayC1R9Je6JV6SW099zgX-r2wUoxev4xQf2llHdf7gYu1DwdtRvzqcMNBPL85v7qLLY2KG0mFXnHDL5MMFb5PDbhgndLz0435esnux5l3ITeIHEU_NWHFCPkPp5',
                'Content-Type'  => 'application/json',
            ],
        ] )->then(function ($response) {
            return json_decode( $response->getBody(), true );
        });
        $promise->wait();

    }

    public function sendSMSToUser($d, $userToReceivePush)
    {
        if($_SERVER['SERVER_NAME'] == "localhost"){
            return;
        };
        $phone_number = "";
        $smsContent = "";
        $dial_code = "";

        if( $userToReceivePush->dial_code != null && $userToReceivePush->phone_number != null){
            $dial_code = $userToReceivePush->dial_code;
            $phone_number = $userToReceivePush->dial_code.$userToReceivePush->phone_number;
        }


        $isUserRegisteredOnApp = false;

        //before sending sms notificatoin check user is registered with password exist logic
        if( $userToReceivePush->password != null ){
            $isUserRegisteredOnApp = true;
        }

        if($isUserRegisteredOnApp == false && $phone_number != ""){
            if( $d['title'] == 'New Pass Received' ){
                $enterCode = ' ';

                $pn = ($d['nid_type'] == 'parental') ? PCNotificaitons::where('id', $d['nid'])->first() : PushNotification::where('id', $d['nid'])->first();

                if(!$pn){
                    return true;
                }

                $pass = Pass::where('id', $pn->pr_id)->first();


                // $smsContent .= 'ZUUL Systems - '.$enterCode;
                $smsContent .= $pass->itsOwner->fullName.' has invited you on '. $pass->getEvent->event_name.$enterCode;
                // $smsContent .=', follow the link for more details'.$enterCode;
                $smsContent .= env('APP_URL', url('')).'/nd/'.$pn->id.$enterCode;

                $smsContent .= 'Download ZUUL APP'.$enterCode;
                // $smsContent .= ' http://tiny.cc/zuul-ios';  //Zuul ios play store
                // $smsContent .= ' http://tiny.cc/zuul-android'; //Zuul google play store link
                //$smsContent .= 'https://apps.apple.com/us/app/zuul-systems/id1438385504'.$enterCode;
                $smsContent .= env('APP_URL', url('')).'download';
                //$smsContent .= 'https://play.google.com/store/apps/details?id=com.reavertechnologies.zuulmaster';

            }
            else if( false && $d['title'] == 'Someone added you as a family member, please login again to reflect changes' ){
                $smsContent .= 'ZUUL Systems - ';
                $smsContent .= "Someone added you as a family member, please login to check ";
                $smsContent .= env('APP_URL', url(''));
            }
        }

        if($smsContent != "" && $phone_number != ""){
            $data = [];
            $data["content"] = $smsContent;
            $data["to"] = [];
            $data["to"][] = $phone_number;

            $data["dial_code"] = [];
            $data["dial_code"][] = $dial_code;


            $data["binary"] = false;
            $data["validityPeriod"] = 1;
            $data["charset"] = "UTF-8";
            $response = $this->sendSMSCurl($data);

            $final_res = json_decode($response, true);

            $created_at = date('Y-m-d h:i:s');
            foreach ($final_res['messages'] as $i => $message) {
                $log = array(
                    'phone_number' => $message['to'],
                    'receiver_id' => $userToReceivePush->id,
                    'nid' => $d['nid'],
                    'sms_content' => $smsContent,
                    'api_response' => json_encode($message),
                    'created_at' => $created_at,
                );
                $logsms = LogsSms::create($log);
            }
        }
    }

    function sendSMSCurl($data)
    {
        //FOR US users we require two way settings
        $smsAPIkey = env('CLICK_A_TELL_2_WAY_API_KEY');
        $data['from'] = env('CLICK_A_TELL_LONG_NUMBER');

        $url = "https://platform.clickatell.com/messages";
        try{
            if( $data['dial_code'][0] != '+1' ){
                $smsAPIkey = env('CLICK_A_TELL_1_WAY_API_KEY');
                unset($data['from']);
            }
            unset($data['dial_code']);
        }
        catch(\Exception $e) {
            // echo 'Click a tell Message: ' .$e->getMessage();
        }

        $ch = curl_init( $url );
        # Setup request to send json via POST.
        $payload = json_encode( $data );

        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Authorization:'.$smsAPIkey,'Content-Type:application/json'));

        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        # Send request.
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    function sendNotificaion($user_id, $self)
    {
        $req_user = User::find($user_id);
        $push = array(
            'description' => "Offensive Words are being used in request a pass feature from one of your family member. Check your email for more details.",
            'title' => 'Warning! Offensive Words',
            'nid' => null
        );
        $self->sendPushNotification($push, false, $req_user);
    }

    function sendEmailNotificationForOffensiveWords($eData)
    {
        $email = new \stdClass();
        $email->template = "offensive_words";
        $email->subject = 'ZUUL Systems - Offensive Words';
        $email->sender = 'ZUUL Team';

        $email->receiver = $eData['receiver'];
        $email->sender_name = $eData['sender_name'];
        $email->requested_to_name = $eData['requested_to_name'];
        $email->wordings = $eData['wordings'];

        $dyn_template = EmailTemplates::where('assign_to', $email->template)->first();
        $email->dynamic_template = EmailTemplatesController::replaceTemplateStringsAndSendmail($dyn_template, $email);

        try {
            Mail::to($eData['email'])->send(new ZUULSystem($email));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    function sendEmailToHead($user, $activity, $pass_id = null, $title = "Parental Control", $notification = null) {

        $user = User::where('id', $user->id)->with(['member_parental_control'])->first();
        if(!$user){
            return;
        }

        if( $user->head_of_family != 1 ) {

            if( $user->member_parental_control && count($user->member_parental_control) ) {
                if( (int) $user->member_parental_control->{$activity} ) {
                    $head = User::where('house', $user->house)->where('head_of_family', 1)->first();

                    $allowed_parents = $head ? ParentalControl::where('allow_parental_controls_to_member', 1)
                        ->where('parent_id', $head->id)->get() : [];

                    $push_notifications = $head ? [
                        [
                            'device_id' => null,
                            'user_id' => $head->id,
                            'pr_id' => $pass_id,
                            'status' => 0,
                            'type' => 5,
                            'heading' => $title,
                            'text' => $notification,
                        ]
                    ] : [];
                    $push_users =  $head ? [$head] : [];
                    foreach($allowed_parents as $allowed_parent) {
                        $push_notifications[] = [
                            'device_id' => null,
                            'user_id' => $allowed_parent->member_id,
                            'pr_id' => $pass_id,
                            'status' => 0,
                            'type' => 5,
                            'heading' => $title,
                            'text' => $notification,
                        ];
                        $push_users[] = $allowed_parent;
                    }

                    //dd($push_notifications);
                    foreach($push_notifications as $key => $push_notification) {
                        $noti = PushNotification::create($push_notification);

                        $push = [
                            'title' => $title,
                            'description' => $notification,
                            'nid' => $noti->id
                        ];

                        $this->sendPushNotification($push, false, $push_users[$key]);
                    }
                    //Mail::to($head->email)->send(new SendMailable("Test email to head of the family."));
                }
            }


        }

    }

    private function sendPushNotificationWithGuzzle( $data, $isReturn = true, $userToReceivePush = null, $sendSMS = false )
    {
        $ret = null;

        $data['token'] = "";

        if ($userToReceivePush) {
            if ($userToReceivePush->push_notification == 0) {
                if ($isReturn) {
                    return response()->json(['bool' => false, 'message' => 'Push notification is disabled from user settings', 'id' => $userToReceivePush->id], 200);
                }
            };

            if ($userToReceivePush->web_token != null) {
                $data['token'] = $userToReceivePush->web_token;
                $returnOfWeb = $this->sendPushToUser($data, $isReturn, $userToReceivePush);
                if ($ret == null) {
                    $ret = $returnOfWeb;
                }
            }

            if ($userToReceivePush->phone_number != null && $sendSMS) {
                $this->sendSMSToUser($data, $userToReceivePush);
            }

            $data['token'] = $userToReceivePush->fcm_token;
            if(!isset($data['token'])){
                return response()->json(['bool' => false, 'message' => 'Data Token Not Found', 'id'=> $userToReceivePush->id], 200);
            }
        }



        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $format = array (
            'to' => "".$data['token']."",
            'notification' =>
                array (
                    'body' => $data['description'],
                    'title' => $data['title'],
                    'key_id' => $data['nid'],
                    "sound" => "marimba.mp3",
                    "click_action" => isset( $data['link'] ) ? $data['link'] : null
                ),
            'data' =>
                array (
                    'body' => $data['description'],
                    'title' => $data['title'],
                    'key_id' => $data['nid'],
                    "sound" => "marimba.mp3",
                    "info" => isset( $data['info'] ) ? $data['info'] : null
                )
        );

        if( isset($data['showalert']) ) {
            $format["notification"]["showalert"] = $data['showalert'];
            $format["data"]["showalert"] = $data['showalert'];
        }

        //$payload = json_encode( $format );

        $client = new Client();

        $promise = $client->postAsync( $url, [
            'json' =>
                $format
            ,
            'headers' => [
                'Authorization' => 'key=AIzaSyB2QSVMpn3YE1yPx6YRBHDA3KEgvaVgas8',
                'Content-Type'  => 'application/json',
            ],
        ] )->then(function ($response) {
            return json_decode( $response->getBody(), true );
        });
        $promise->wait();
    }

    public function testPushNotificationWithGuzzle($data){

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $format = array (
            'to' => "".$data['token']."",
            'notification' =>
                array (
                    'body' => "",
                    'title' => "Welcome Back",
                    "sound" => "marimba.mp3",
                ),
            'data' =>
                array (
                    'body' => "",
                    'title' => "Welcome Back",
                    "sound" => "marimba.mp3",
                )
        );

        //$payload = json_encode( $format );

        $client = new Client();

        $promise = $client->postAsync( $url, [
            'json' =>
                $format
            ,
            'headers' => [
                'Authorization' => 'key=AIzaSyB2QSVMpn3YE1yPx6YRBHDA3KEgvaVgas8',
                'Content-Type'  => 'application/json',
            ],
        ] )->then(function ($response) {
            return json_decode( $response->getBody(), true );
        });
        $resp = $promise->wait();

        return $resp;

    }

}
