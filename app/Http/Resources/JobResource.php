<?php

namespace App\Http\Resources;

use App\Models\Jobreview;
use App\Models\Joburls;
use App\Models\JobProgressLocation;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function getClientReviewed($id){
        $jobreview = Jobreview::where('job_id', $id)->orderBy('id', 'desc')->first();
        if($jobreview){
            return $jobreview->client_reviewed;
        }

        return 0;

    }

    public static function getDriverReviewed($id){
        $jobreview = Jobreview::where('job_id', $id)->orderBy('id', 'desc')->first();
        if($jobreview){
            return $jobreview->driver_reviewed;
        }

        return 0;

    }

    public static function getJobProgressStatus($id){
        $jobProgressLocations = JobProgressLocation::where('job_id', $id)->orderBy('id', 'desc')->first();
        if($jobProgressLocations){
            return $jobProgressLocations->status;
        }

        return 'pending';
    }

    public static function getTotalRating($id){

        $jobreview = Jobreview::where('reviewer_id', $id)->get();
        $jobreview_count = $jobreview->count();
        return $jobreview_count > 0 ? $jobreview->sum('rating') / $jobreview->count() : 0;
    }


    public static function toObject($obj, $lang = 'en')
    {
        //"poster_id" => $obj->sender ?? $obj->sender->id = -1
        //dd($obj);
//        dd($obj->sender);
//        dd($obj->sender->id);
        $data = [
            "id" => $obj->id,
            "poster_id" => $obj->sender->id,
            "poster_name" => $obj->sender->fullName,
            "poster_contact" => $obj->sender->contact,
            "poster_profile_pic" => $obj->sender->profile_pic ?? "https://secure.gravatar.com/avatar/122312f1de71a17d09e1341a4264bbda?s=96&d=mm&r=g",
            "delivery_address" => $obj->delivery_address,
            "delivery_latitude" => $obj->delivery_latitude,
            "delivery_longitude" => $obj->delivery_longitude,
            "distance" => $obj->distance,
            "description" => $obj->description,
            "expected_delivery_time" => $obj->expected_delivery_time,
            "item_category" => $obj->item_category,
            "job_address" => $obj->job_address,
            "job_latitude" => $obj->job_latitude,
            "job_longitude" => $obj->job_longitude,
            "job_price" => $obj->job_price,
            "package_size" => $obj->package_size,
            "posted_at" => $obj->posted_at,
            "priority" => $obj->priority,
            "receiver_id" => $obj->receiver_id,
            "receiver_name" => $obj->fullName,
            "receiver_contact" => isset($obj->receiver) ? $obj->receiver->contact : null,
            "receiver_instructions" => $obj->receiver_instructions,
            "security_code" => $obj->security_code,
            "status" => $obj->status,
            "source_address_appartment" => $obj->source_address_appartment,
            "delivery_address_appartment" => $obj->delivery_address_appartment,
            "photos_urls" => Joburls::where('job_id', $obj->id)->pluck('firebase_url'),
            "rating" => isset($obj->receiver_id) ? JobResource::getTotalRating($obj->receiver_id) : 0,
            "clientReviewed" => JobResource::getClientReviewed($obj->id),
            "driverReviewed" => JobResource::getDriverReviewed($obj->id),
            "job_progresss_status" => JobResource::getJobProgressStatus($obj->id)
        ];
        //dd($data);

        if($obj->distance){
            $data['distance'] = round($obj->distance, 2);
        }

        if($obj->receiver_id){
            $driver = User::where('id', $obj->receiver_id)->first();
            if($driver){
                $data["receiver_id"] = $obj->receiver_id;
                $data["receiver_name"] = $driver->fullName;
                $data["receiver_contact"] = $driver->contact;
                $data["receiver_instructions"] = $obj->receiver_instructions;
                $data["security_code"] = $obj->security_code;
            }
        }
        return $data;
    }
}
