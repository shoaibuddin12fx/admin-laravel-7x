<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EarningResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
        return [
            "id" => $obj->id,
            'user_id' => $obj->user_id,
            'job_id' => $obj->job_id,
            'amount' => $obj->amount,
            'is_paid' => $obj->is_paid,
            'time_taken' => $obj->time_taken,
            'distance' => $obj->distance,
        ];
    }
}
