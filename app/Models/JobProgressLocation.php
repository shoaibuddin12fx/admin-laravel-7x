<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobProgressLocation extends Model
{
    //

    protected $fillable = [
        'job_id', 
        'driver_lat', 
        'driver_lng', 
        'target_lat', 
        'target_lng', 
        'status'
    ];

}
    